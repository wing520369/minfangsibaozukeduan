//
//  AppDelegate.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "AppDelegate.h"
#import "Tools.h"
#import "WCRTabBarController.h"
#import "WCRBMKTool.h"
#import "WCRGuidePageVC.h"
#import "MiPushSDK.h"
#import "WCRWXTools.h"
#import "WCRAlipayTools.h"
#import "WCRLoginVC.h"

@interface AppDelegate ()<MiPushSDKDelegate, UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
    
    if ([Tools isFirstOpenApp]) {
        [self ShowGuidePage];
    }
    else {
        WCRTabBarController *tabBarController = [[WCRTabBarController alloc] init];
        self.window.rootViewController = tabBarController;
    }
    [self.window makeKeyAndVisible];

    // 网络监控
    [Tools openNetworkMonitoring];
    // 地图
    [[WCRBMKTool sharedInstance] initBMKMapManager];
    // 推送
    [MiPushSDK registerMiPush:self type:0 connect:YES];
    // 处理点击通知打开app的逻辑
    NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    [self dealWithRemoteNotification:userInfo];
    // 微信支付
    [WCRWXTools registerWXApi];
    
    return YES;
}
- (void)ShowGuidePage {
    
    NSArray *imags = @[@"1",@"2",@"3"];
    if (kMainHeight < 568) {
        imags = @[@"4-1",@"4-2",@"4-3"];
    }
    
    __weak typeof(self) weakSelf = self;
    WCRGuidePageVC *pageVC = [[WCRGuidePageVC alloc]initWithImages:imags ButtonTitle:@"" EnterAppBlock:^(WCRGuidePageVC *__weak weakGuideVC) {
        WCRTabBarController *tabBarController = [[WCRTabBarController alloc] init];
        weakSelf.window.rootViewController = tabBarController;
    }];
    self.window.rootViewController = pageVC;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark 注册push服务.
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    // 注册APNS成功, 注册deviceToken
    [MiPushSDK bindDeviceToken:deviceToken];
}
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err{
    // 注册APNS失败.自行处理.
    
}
#pragma mark - 接收远程推送
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // 使用此方法后，所有消息会进行去重，然后通过miPushReceiveNotification:回调返回给App
    [MiPushSDK handleReceiveRemoteNotification :userInfo];
}
// iOS10新加入的回调方法
// 应用在前台收到通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [MiPushSDK handleReceiveRemoteNotification:userInfo];
    }
    //    completionHandler(UNNotificationPresentationOptionAlert);
}
// 点击通知进入应用
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [MiPushSDK handleReceiveRemoteNotification:userInfo];
    }
    completionHandler();
}

#pragma mark MiPushSDKDelegate
- (void)miPushRequestSuccWithSelector:(NSString *)selector data:(NSDictionary *)data {
    if ([selector isEqualToString:@"bindDeviceToken:"]) {
        NSString *regid =[data objectForKey:@"regid"];
        [Tools saveToken:regid];
    }
}

- (void)miPushReceiveNotification:(NSDictionary *)data {
    // 长连接收到的消息。消息格式跟APNs格式一样
    [self dealWithRemoteNotification:data];
}
// 处理远程消息的通知
- (void)dealWithRemoteNotification:(NSDictionary *)userInfo {
    
//    NSLog(@"远程消息的通知 : %@",userInfo);
    if (!userInfo) {
        return;
    }
    
    // 在后台接收远程通知
    NSString *alertString = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    if ([alertString isEqualToString:@"下线通知"]) {
        alertString = @"您已在其他设备登录";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"民房四宝提醒您：" message:alertString delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertView show];
    
    if ([alertString isEqualToString:@"您已在其他设备登录"]) {
        [WCRUserModelTool logOut];
        WCRLoginVC *loginVC = [[WCRLoginVC alloc] init];
        loginVC.backToHomePage = YES;
        self.window.rootViewController = loginVC;
    }
}


#pragma mark - 支付回调处理
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    [WCRWXTools handleWXApiOPenURL:url];
    return YES;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    // 支付，处理支付结果
    [WCRAlipayTools AlipayWithOpenURL:url];
    [WCRWXTools handleWXApiOPenURL:url];
    return YES;
}
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    // 支付，处理支付结果
    [WCRAlipayTools AlipayWithOpenURL:url];
    [WCRWXTools handleWXApiOPenURL:url];
    return YES;
}

@end
