//
//  WCRUserModelTool.m
//  CarAccessories
//
//  Created by eliot on 16/9/26.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRUserModelTool.h"
#import "WCRNSUserDefaultsTool.h"
#import "SAMKeychain.h"
#import "Tools.h"

@implementation WCRUserModelTool

+ (WCRUserModelTool *)sharedSingleInstance {
    static WCRUserModelTool *tool = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tool = [[WCRUserModelTool alloc] init];
    });
    return tool;
}

#pragma mark - 登陆方式
+ (void)saveLoginType:(LoginType)type {
    [WCRNSUserDefaultsTool setInt:type key:@"loginType"];
}
+ (LoginType)getLoginType {
    LoginType type = [WCRNSUserDefaultsTool intForKey:@"loginType"];
    if (!type) {
        [WCRUserModelTool saveLoginType:kNoLogin_Type];
        return kNoLogin_Type;
    }
    return type;
}
#pragma mark - 是否已经登录
+ (BOOL)isLogin {
    if ([WCRUserModelTool getLoginType] == kNoLogin_Type) {
        return NO;
    }
    else{
        return YES;
    }
}
#pragma mark - 用户id
+ (void)saveUserId:(NSString *)userId {
    NSString *userIdKey = @"_userID";
    [WCRNSUserDefaultsTool setStr:userId key:userIdKey];
}
+ (NSString *)getUserId {
    NSString *userIdKey = @"_userID";
    return [WCRNSUserDefaultsTool strForKey:userIdKey];
}

#pragma makr - 当前用户的账号和密码
+ (void)saveAccount:(NSString *)account Password:(NSString *)password {
    NSString *accountKey = @"_account";
    [WCRNSUserDefaultsTool setStr:account key:accountKey];
    NSString *passwordKey = @"_password";
    [WCRNSUserDefaultsTool setStr:password key:passwordKey];
}
+ (void)cleanPassword:(BOOL)cleanPassword Account:(BOOL)cleanAccount {
    NSString *accountKey = @"_account";
    if (cleanAccount) {
        [WCRNSUserDefaultsTool setStr:nil key:accountKey];
    }
    NSString *passwordKey = @"_password";
    [WCRNSUserDefaultsTool setStr:nil key:passwordKey];
}
#pragma mark - 用户当前的账号
+ (NSString *)getCurrentAccount {
    NSString *accountKey = @"_account";
    NSString *account = [WCRNSUserDefaultsTool strForKey:accountKey];
    if (account.length == 0) {
        return @"";
    }
    return account;
}
#pragma mark - 用户当前的密码
+ (NSString *)getCurrentPassword {
    NSString *passwordKey = @"_password";
    NSString *password = [WCRNSUserDefaultsTool strForKey:passwordKey];
    if (password.length == 0) {
        return @"";
    }
    return password;
}
#pragma mark - 保存用户资料
+ (void)saveUserInfo:(id)userInfo {
    NSString *userInfoKey = @"_userInfo";
    [WCRUserModel encodeModel:userInfo key:userInfoKey];
}
+ (WCRUserModel *)getUserInfo {
    NSString *userInfoKey = @"_userInfo";
    return [WCRUserModel coderModelWithKey:userInfoKey];
}

#pragma mark - 保存用户信息
+ (void)saveUserModel:(id)userInfo UserID:(NSString *)userID AccountName:(NSString *)accountName Password:(NSString *)password LoginType:(LoginType)loginType {
    [self saveLoginType:loginType];
    [self saveUserInfo:userInfo];
    [self saveUserId:userID];
    [self saveAccount:accountName Password:password];
}

#pragma mark - 退出登录
+ (void)logOut {
    
    [SAMKeychain setPassword:@"" forService:@"wcr.com" account:@"user"];
    [WCRUserModelTool saveUserId:nil];
    [WCRUserModelTool saveLoginType:kNoLogin_Type];
    [WCRUserModelTool saveUserInfo:nil];
    [WCRUserModelTool cleanPassword:YES Account:NO];
}

+ (NSString *)getToken {
    return [Tools tokenName];
}

@end
