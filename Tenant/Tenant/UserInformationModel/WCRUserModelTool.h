//
//  WCRUserModelTool.h
//  CarAccessories
//
//  Created by eliot on 16/9/26.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WCRUserModel.h"

//登陆方式
typedef NS_ENUM(NSInteger, LoginType){
    /** 未登陆*/
    kNoLogin_Type = 100,
    /** 账号登陆*/
    kAccoutLogin_Type,
    /** 第三方登陆*/
    kThirdLogin_Type,
};

/** 用户信息管理工具 */
@interface WCRUserModelTool : NSObject

+ (WCRUserModelTool *)sharedSingleInstance;
#pragma mark - 保存登陆方式
/**
 *  保存登陆方式 第三方或账号
 *
 *  
 */
+ (void)saveLoginType:(LoginType)type;
#pragma mark - 获取登陆方式
/**
 *  获取登陆方式
 *
 *  @return 登陆方式
 */
+ (LoginType)getLoginType;
#pragma mark - 是否已经登录
/**
 *  是否已经登录
 *
 *  @return YES：登录，NO：未登录
 */
+ (BOOL)isLogin;
#pragma mark - 保存用户id
/**
 *  保存用户id
 *
 *  @param userId 用户id
 */
+ (void)saveUserId:(NSString *)userId;
#pragma mark - 读取用户id
/**
 *  读取用户id
 *
 *  @return 用户id
 */
+ (NSString *)getUserId;

#pragma makr - 保存当前用户的账号和密码
/**
 *  保存当前用户的账号和密码
 *
 *  @param account  账号
 *  @param password 密码
 */
+ (void)saveAccount:(NSString *)account Password:(NSString *)password;
/**
 *  清除密码和账号
 *
 *  @param cleanPassword 是否清除密码
 *  @param cleanAccount  清除账号
 */
+ (void)cleanPassword:(BOOL)cleanPassword Account:(BOOL)cleanAccount;
#pragma mark - 取用户当前的账号
/**
 *  取用户的账号
 *
 *  @return 用户账号
 */
+ (NSString *)getCurrentAccount;

#pragma mark - 取用户当前的密码
/**
 *  用户当前的密码
 *
 *  @return 用户密码
 */
+ (NSString *)getCurrentPassword;

#pragma mark - 保存用户资料
/**
 *  保存用户资料
 *
 *  @param userInfo 用户资料模型
 */
+ (void)saveUserInfo:(WCRUserModel *)userInfo;
#pragma mark - 获取用户资料
/**
 *  获取用户资料
 *
 *  @return 用户资料
 */
+ (WCRUserModel *)getUserInfo;

//保存用户密码和账号
#pragma mark - 保存用户信息
/**
 *  保存用户信息
 *
 *  @param userInfo    用户model
 *  @param userID      用户的id
 *  @param accountName 用户名（手机号）
 *  @param password    密码
 */
+ (void)saveUserModel:(id)userInfo UserID:(NSString *)userID AccountName:(NSString *)accountName Password:(NSString *)password LoginType:(LoginType)loginType;

#pragma mark - 退出登录
/**
 *  退出登录
 */
+ (void)logOut;

+ (NSString *)getToken;

@end
