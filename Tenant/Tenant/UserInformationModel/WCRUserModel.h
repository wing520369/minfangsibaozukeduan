//
//  WCRUserModel.h
//  CarAccessories
//
//  Created by eliot on 16/9/26.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

@interface WCRUserModel : WCRBasicModel

/// 用户ID
@property (nonatomic, copy) NSString *ID;
/// 头像
@property (nonatomic, copy) NSString *avatar;
/// 用户类型 1 业主（房东） 2.租客
@property (nonatomic, copy) NSString *userType;
/// 钱包帐号
@property (nonatomic, copy) NSString *moneyNo;
/// 手机号码
@property (nonatomic, copy) NSString *phone;
/// 账号
@property (nonatomic, copy) NSString *account;
/// 身份证号码
@property (nonatomic, copy) NSString *identifyNo;
/// 身份证正面
@property (nonatomic, copy) NSString *identifyFace;
/// 身份证反面
@property (nonatomic, copy) NSString *identifyBack;
/// 绑定方式 1-支付宝 2-银行卡
@property (nonatomic, copy) NSString *bindType;
/// 充值方式 1-支付宝 2-微信
@property (nonatomic, copy) NSString *rechType;
/// 交易方式
@property (nonatomic, copy) NSString *payType;
/// 真实姓名
@property (nonatomic, copy) NSString *trueName;
/// 银行名称
@property (nonatomic, copy) NSString *bankName;
/// 昵称
@property (nonatomic, copy) NSString *nickname;
/// 性别 0-保密 1-男 2-女
@property (nonatomic, copy) NSString *sex;
/// 登陆状态：1正常，0冻结
@property (nonatomic, copy) NSString *loginStatus;
/// 微信号
@property (nonatomic, copy) NSString *wxNo;
/// 绑定手机：1是，0否
@property (nonatomic, copy) NSString *bindPhone;
/// 微信二维码
@property (nonatomic, copy) NSString *wxImg;
/// 微信的unionId
@property (nonatomic, copy) NSString *unionId;
/// QQ号
@property (nonatomic, copy) NSString *qqNo;
/// 交易密码
@property (nonatomic, copy) NSString *passwordPay;
/// 未加密的密码
@property (nonatomic, copy) NSString *noSecPwd;
/// 账户总金额
@property (nonatomic, copy) NSString *amount;
/// 用户积分
@property (nonatomic, copy) NSString *score;
/// 等级评价星星
@property (nonatomic, copy) NSString *level;

- (NSString *)showName;

@end


