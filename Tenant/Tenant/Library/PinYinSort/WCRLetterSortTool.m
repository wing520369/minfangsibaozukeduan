//
//  WCRContactsSortTool.m
//  CarAccessories
//
//  Created by eliot on 17/1/18.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRLetterSortTool.h"
#import "WCRLetterSortModel.h"

@implementation WCRLetterSortTool

+ (void)sortWithNameArray:(NSArray *)names Block:(void (^) (NSArray *resultArray, NSArray *letters))block {
    
    __block NSMutableDictionary *mutabeDict = [NSMutableDictionary dictionaryWithCapacity:names.count];
    
    [names enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSString class]]) {
            NSString *name = obj;
            // 获取到姓名的大写首字母
            NSString *firstLetterString = [self getFirstLetterFromString:name];
            // 首字母firstLetterString为key value为首字母对应的名字
            if ([mutabeDict objectForKey:firstLetterString]) {
                [[mutabeDict objectForKey:firstLetterString] addObject:name];
            }
            else {
                NSMutableArray *groupNames = [NSMutableArray arrayWithObject:name];
                [mutabeDict setObject:groupNames forKey:firstLetterString];
            }
        }
    }];
    
    // 所有Key值进行排序: A~Z
    NSArray *nameKeys = [[mutabeDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    // 将 "#" 排列在 A~Z 的后面
    NSMutableArray *mutableNamekeys = [NSMutableArray arrayWithArray:nameKeys];
    if ([nameKeys.firstObject isEqualToString:@"#"]) {
        [mutableNamekeys insertObject:nameKeys.firstObject atIndex:nameKeys.count];
        [mutableNamekeys removeObjectAtIndex:0];
    }
    
    __block NSMutableArray *resutlArray = [NSMutableArray arrayWithCapacity:mutableNamekeys.count];
    [mutableNamekeys enumerateObjectsUsingBlock:^(NSString * obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        NSDictionary *dict = @{obj:[mutabeDict objectForKey:obj]};
//        [resutlArray addObject:dict];
        [resutlArray addObject:[mutabeDict objectForKey:obj]];
    }];
    
    if (block) {
        block([resutlArray copy], [mutableNamekeys copy]);
    }
}

+ (void)sortWithModelArray:(NSArray<WCRLetterSortModel *> *)originalModels Block:(void (^)(NSArray<WCRLetterSortModel *> *, NSArray *))block {
    if (originalModels.count == 0) {
        if (block) {
            block(nil, nil);
        }
    }
    
    __block NSMutableDictionary *mutabeDict = [NSMutableDictionary dictionary];
    [originalModels enumerateObjectsUsingBlock:^(WCRLetterSortModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 获取到name的大写首字母
        NSString *firstLetterString = [self getFirstLetterFromString:obj.name];
        // 首字母firstLetterString为key value为首字母对应的名字
        if ([mutabeDict objectForKey:firstLetterString]) {
            [[mutabeDict objectForKey:firstLetterString] addObject:obj];
        }
        else {
            NSMutableArray *groupNames = [NSMutableArray arrayWithObject:obj];
            [mutabeDict setObject:groupNames forKey:firstLetterString];
        }
    }];
    
    // 所有Key值进行排序: A~Z
    NSArray *nameKeys = [[mutabeDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    // 将 "#" 排列在 A~Z 的后面
    NSMutableArray *mutableNamekeys = [NSMutableArray arrayWithArray:nameKeys];
    if ([nameKeys.firstObject isEqualToString:@"#"]) {
        [mutableNamekeys insertObject:nameKeys.firstObject atIndex:nameKeys.count];
        [mutableNamekeys removeObjectAtIndex:0];
    }
    
    __block NSMutableArray *resutlArray = [NSMutableArray arrayWithCapacity:originalModels.count];
    [mutableNamekeys enumerateObjectsUsingBlock:^(NSString * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [resutlArray addObject:[mutabeDict objectForKey:obj]];
    }];
    
    if (block) {
        block([resutlArray copy], [mutableNamekeys copy]);
    }
}

+ (void)sortWithUserModelModels:(NSArray<WCRUserModel *> *)originalModels Block:(void (^)(NSArray<WCRUserModel *> *, NSArray *))block {
    
    if (originalModels.count == 0) {
        if (block) {
            block(nil, nil);
        }
    }
    
    __block NSMutableDictionary *mutabeDict = [NSMutableDictionary dictionary];
    [originalModels enumerateObjectsUsingBlock:^(WCRUserModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 获取到name的大写首字母
        NSString *firstLetterString = [self getFirstLetterFromString:obj.nickname];
        // 首字母firstLetterString为key value为首字母对应的名字
        if ([mutabeDict objectForKey:firstLetterString]) {
            [[mutabeDict objectForKey:firstLetterString] addObject:obj];
        }
        else {
            NSMutableArray *groupNames = [NSMutableArray arrayWithObject:obj];
            [mutabeDict setObject:groupNames forKey:firstLetterString];
        }
    }];
    
    // 所有Key值进行排序: A~Z
    NSArray *nameKeys = [[mutabeDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    // 将 "#" 排列在 A~Z 的后面
    NSMutableArray *mutableNamekeys = [NSMutableArray arrayWithArray:nameKeys];
    if ([nameKeys.firstObject isEqualToString:@"#"]) {
        [mutableNamekeys insertObject:nameKeys.firstObject atIndex:nameKeys.count];
        [mutableNamekeys removeObjectAtIndex:0];
    }
    
    __block NSMutableArray *resutlArray = [NSMutableArray arrayWithCapacity:originalModels.count];
    [mutableNamekeys enumerateObjectsUsingBlock:^(NSString * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [resutlArray addObject:[mutabeDict objectForKey:obj]];
    }];
    
    if (block) {
        block([resutlArray copy], [mutableNamekeys copy]);
    }
}

+ (NSString *)getFirstLetterFromString:(NSString *)aString {
    /**
     * **************************************** START ***************************************
     * 之前PPGetAddressBook对联系人排序时在中文转拼音这一部分非常耗时
     * 参考博主-庞海礁先生的一文:iOS开发中如何更快的实现汉字转拼音 http://www.olinone.com/?p=131
     * 使PPGetAddressBook对联系人排序的性能提升 3~6倍, 非常感谢!
     */
    
    NSMutableString *mutableString = [NSMutableString stringWithString:aString];
    CFStringTransform((CFMutableStringRef)mutableString, NULL, kCFStringTransformToLatin, false);
    NSString *pinyinString = [mutableString stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale currentLocale]];
    /**
     *  *************************************** END ******************************************
     */
    
    // 将拼音首字母装换成大写
    NSString *strPinYin = [[self polyphoneStringHandle:aString pinyinString:pinyinString] uppercaseString];
    // 截取大写首字母
    NSString *firstString = [strPinYin substringToIndex:1];
    // 判断姓名首位是否为大写字母
    NSString * regexA = @"^[A-Z]$";
    NSPredicate *predA = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regexA];
    // 获取并返回首字母
    return [predA evaluateWithObject:firstString] ? firstString : @"#";
    
}

/**
 多音字处理
 */
+ (NSString *)polyphoneStringHandle:(NSString *)aString pinyinString:(NSString *)pinyinString {
    if ([aString hasPrefix:@"长"]) { return @"chang";}
    if ([aString hasPrefix:@"沈"]) { return @"shen"; }
    if ([aString hasPrefix:@"厦"]) { return @"xia";  }
    if ([aString hasPrefix:@"地"]) { return @"di";   }
    if ([aString hasPrefix:@"重"]) { return @"chong";}
    return pinyinString;
}


@end
