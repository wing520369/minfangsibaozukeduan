//
//  WCRContactsSortTool.h
//  CarAccessories
//
//  Created by eliot on 17/1/18.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WCRLetterSortModel;

/// 用于通讯录 城市列表等 获取首字母分组
@interface WCRLetterSortTool : NSObject

/**
 *  字符串数组，首字母排序
 *
 *  @param names    字符串数组
 *  @param block    letters：索引数组
 *                  resultArray：所有的内容数组，[subArray1,]
 *
 */
+ (void)sortWithNameArray:(NSArray *)names Block:(void (^) (NSArray *resultArray, NSArray *letters))block;


/**
 *  用户数组，首字母排序
 *
 *  @param originalModels       用户数组
 *  @param block                letters：索引数组
 *                              resultArray：所有的内容数组，[subArray1,]
 *
 */
+ (void)sortWithUserModelModels:(NSArray<WCRUserModel *> *)originalModels Block:(void (^)(NSArray<WCRUserModel *> *, NSArray *))block;

/**
 *  model（城市）数组，首字母排序
 *
 *  @param originalModels       model（城市）数组
 *  @param block                letters：索引数组
 *                              resultArray：所有的内容数组，[subArray1,]
 *
 */
+ (void)sortWithModelArray:(NSArray<WCRLetterSortModel *> *)originalModels Block:(void (^) (NSArray<WCRLetterSortModel *> *resultArray, NSArray *letters))block;

@end
