//
//  WCRLetterSortModel.h
//  Tenant
//
//  Created by eliot on 2017/5/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

/// 首字母分组 对应的model
@interface WCRLetterSortModel : WCRBasicModel
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *name;
@end
