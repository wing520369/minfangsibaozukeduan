//
//  Tools.h
//  EHealth_HM
//
//  Created by skusdk on 13-10-12.
//  Copyright (c) 2013年 dengwz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Tools : NSObject

/**
 *	@brief	是否iOS7系统判断
 *
 *	@return	return value description
 */
+(BOOL) IS_IOS_7;
+(BOOL) IS_IOS_8;
#pragma mark - Token
/*
 ** 保存推送的注册id 作为token
 */
+ (void)saveToken:(NSString*)regin;
/*
 ** 获取token 如保存有推送的注册id 否测返设备回UUID
 */
+ (NSString *)tokenName;

#pragma makr - 是否第一次打开app
/**
 *  是否第一次打开app
 *
 *  @return YES： NO：
 */
+ (BOOL)isFirstOpenApp;
#pragma mark - 拨打电话
/**
 *  拨打电话
 *
 *  @param phoneNum 电话号码
 */
+ (void)dialPhoneNumber:(NSString *)phoneNum;
#pragma mark - 获取软件版本号
/**
 *  获取软件版本号
 *
 *  @return 版本号
 */
+(NSString*)getSoftVersion;
#pragma mark - 取得wifi名
/**
 *  取得wifi名
 *
 *  @return wifi名
 */
+ (NSString *)getWifiName;
#pragma mark - 打开网络状态监控
/**
 *  打开网络状态监控
 */
+ (void)openNetworkMonitoring;

#pragma mark - 获取网络状态
/**
 *  获取网络是否正常
 *
 *  @return yes 正常 no 没网络
 */
+ (BOOL)getNetworkStatus;
#pragma mark - 获取缓存大小
/**
 *  获取缓存大小
 *
 *  @param completion 回调
 */
+ (void)requestCachesFileSize:(void (^)(NSString *size))completion;
//单个文件的大小
+ (long long)fileSizeAtPath:(NSString*) filePath;
#pragma mark - 清理缓存
/**
 *  清理缓存
 *
 *  @param completion 回调
 */
+(void)clearCache:(void (^)(BOOL flag))completion;

+ (void)clearCancheProgress:(void(^)(float progress))progressBlock
                   complete:(void(^)(NSString *size))complete;

#pragma mark - 将字典，数组写到沙盒
/**
 *  将字典，数组写到沙盒
 *
 *  @param dataSource 字典，数组
 *  @param fileName   文件名：region.plist
 *
 *  @return 文件的路径
 */
+ (NSString *)writeToDocumentsWithDataSource:(id)dataSource FileName:(NSString *)fileName;
#pragma mark - 判断金额的输入
/**
 *  判断金额的输入
 *
 *  @param textField textfield
 *  @param range     范围
 *  @param string    当前输入的内容
 *
 *  @return 输入是否正确
 */
+ (BOOL)checkTrueNumberWithTextField:(UITextField *)textField InRange:(NSRange)range CurrentString:(NSString *)string;

#pragma mark - 获取Bundle version版本号，bundleID，appName
/// 获取appName
+ (NSString *)getAppName;
/// 获取Bundle version版本号
+ (NSString *)getAppVersion;
/// 获取Bundle 版本
+ (NSInteger)getAppBundleValue;
/// bundleID
+ (NSString *)getBundleID;

/// 检测是否已经登录
+ (BOOL)checkIsLogin;

@end
