//
//  WCRBMKTool.h
//  CarAccessories
//
//  Created by eliot on 16/9/28.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BMKAddressComponent;
// 地址更新的回调
typedef  void (^UpdateAddressBlock)(NSString *province, NSString *city, NSString *district, NSString *street, NSString *streetNumber);

@interface WCRBMKTool : NSObject

@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *latitude;
/** 省 */
@property (nonatomic, strong) NSString *province;
/** 城市 */
@property (nonatomic, strong) NSString *city;
/** 区县 */
@property (nonatomic, strong) NSString *district;
/** 街道 */
@property (nonatomic, strong) NSString *street;
/** 街道号 */
@property (nonatomic, strong) NSString *streetNumber;
@property (nonatomic, strong) UpdateAddressBlock addressBlock;

/** 能不用单例尽量不用，单例不释放 */
+ (WCRBMKTool *)sharedInstance;

/**
 *  在(BOOL)application:didFinishLaunchingWithOptions:调用
 *  使用百度地图，请先启动BaiduMapManager
 */
- (void)initBMKMapManager;

/** 开始定位服务 */
- (void)startLocation;

/** 关闭定位服务，将代理置nil */
- (void)stopLocation;

@end
