//
//  WCRBMKLocationModel.h
//  Landlord
//
//  Created by eliot on 2017/5/2.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WCRBMKLocationModel : NSObject

@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *provinceId;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *district;
@property (nonatomic, copy) NSString *districtId;
/// 详细地址（一般手动输入）
@property (nonatomic, copy) NSString *detailAddress;

@property (nonatomic, copy) NSString *streetName;
@property (nonatomic, copy) NSString *streetNumber;

@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *latitude;

@end
