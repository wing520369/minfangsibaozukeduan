//
//  WCRBMKTool.m
//  CarAccessories
//
//  Created by eliot on 16/9/28.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRBMKTool.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>

static NSString *appKey = @"o8vrL6zpTY8TOnIgEZGV6zSSGcE54zBM";

@interface WCRBMKTool ()<BMKGeneralDelegate,BMKLocationServiceDelegate,BMKGeoCodeSearchDelegate>
@property (nonatomic, strong) BMKLocationService *locationService;
@property (nonatomic,assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) BMKGeoCodeSearch *geoCode;
@end

@implementation WCRBMKTool

+ (WCRBMKTool *)sharedInstance {
    static WCRBMKTool *tool = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tool = [[self alloc] init];
    });
    return tool;
}
- (instancetype)init {
    if (self = [super init]) {
        self.province = @"";
        self.city = @"";
        self.district = @"";
        self.street = @"";
        self.streetNumber = @"";
        self.latitude = @"";
        self.longitude = @"";
    }
    return self;
}
/**
 *  在(BOOL)application:didFinishLaunchingWithOptions:调用
 *  使用百度地图，请先启动BaiduMapManager
 */
- (void)initBMKMapManager {
    BMKMapManager *manager = [[BMKMapManager alloc]init];
    [manager start:appKey generalDelegate:self];
}
- (void)onGetPermissionState:(int)iError {
    if (0 == iError) {
        // 百度地图 授权成功
        // 开启定位服务
//        [[WCRBMKTool sharedInstance] startLocation];
//        [WCRBMKTool sharedInstance].addressBlock = ^(NSString *province, NSString *city, NSString *district, NSString *street, NSString *streetNumber) {
//            // 停止定位
//            [[WCRBMKTool sharedInstance] stopLocation];
//        };
    }
    else {
        NSLog(@"onGetPermissionState %d",iError);
    }
}
#pragma mark - 开始定位服务，初始化参数，代理
- (void)startLocation {
    _locationService = [[BMKLocationService alloc] init];
    _locationService.delegate = self;
    // 启动LocationService
    [_locationService startUserLocationService];
    [self updateCoordinate:_locationService.userLocation];
    
    _geoCode = [[BMKGeoCodeSearch alloc]init];
    _geoCode.delegate = self;
}

#pragma mark - 关闭定位服务，将代理置nil
- (void)stopLocation {
    [self setPropertyToNull];
    [_locationService stopUserLocationService];
    _locationService.delegate = nil;
    _geoCode.delegate = nil;
}

- (void)setPropertyToNull {
    // 将所有的属性值致nil
//    self.address = nil;
//    self.province = nil;
//    self.city = nil;
//    self.street = nil;
//    self.streetNumber = nil;
//    self.locationBlock = nil;
//    self.addressBlock = nil;
}

#pragma mark - 定位delegate
// 在地图View将要启动定位时，会调用此函数
- (void)willStartLocatingUser {
//    NSLog(@"开始定位");
}
- (void)didStopLocatingUser {
//    NSLog(@"结束定位");
}
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation {
    [self updateCoordinate:userLocation];
}
// 用户位置更新后，会调用此函数
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation {
    [self updateCoordinate:userLocation];
}
- (void)updateCoordinate:(BMKUserLocation *)userLocation {
    self.coordinate = userLocation.location.coordinate;
    // 反地理编码
    [self WCRBMKToolReverseGeoWithCoordinate:userLocation.location.coordinate];
}
// 定位失败后，会调用此函数
- (void)didFailToLocateUserWithError:(NSError *)error{
//   NSLog(@"定位失败:%@",error);
    if (self.addressBlock) {
        self.addressBlock(self.province, self.city, self.district, self.street, self.streetNumber);
    }
}

#pragma mark - 反地理编码
- (void)WCRBMKToolReverseGeoWithCoordinate:(CLLocationCoordinate2D)coordinate {
    
    BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
    reverseGeocodeSearchOption.reverseGeoPoint = coordinate;
    [_geoCode reverseGeoCode:reverseGeocodeSearchOption];
}

-(void) onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error{
    
//    NSLog(@"result:province--->%@  city--->%@  district--->%@  streetName--->%@  streetNumber--->%@",result.addressDetail.province,result.addressDetail.city,result.addressDetail.district,result.addressDetail.streetName,result.addressDetail.streetNumber);
//    NSLog(@"address:%@",result.address);
    
    self.province = result.addressDetail.province;
    self.city = result.addressDetail.city;
    self.district = result.addressDetail.district;
    self.street = result.addressDetail.streetName;
    self.streetNumber = result.addressDetail.streetNumber;

    if (self.addressBlock) {
        self.addressBlock(self.province, self.city, self.district, self.street, self.streetNumber);
    }
}

@end
