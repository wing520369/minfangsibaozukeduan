//
//  SelectPhotosTools.h
//  Model
//
//  Created by ZhouZhenFu on 15/5/8.
//  Copyright (c) 2015年 优一. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SelectPhotosBlock)(UIImage *image);

@interface SelectPhotosTools : NSObject<UINavigationControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate>

@property (nonatomic,strong) UIImage *image;

@property (nonatomic,weak) UIViewController *controller;

@property (nonatomic,copy) SelectPhotosBlock imageBlock;

@property (nonatomic,copy) void(^disMissBlock)();


/**
 *  从相机或相册获取图片
 *
 *  @param controller  控制器
 *  @param imageBlock 选择图片后的回调
 */
+(void)showAtController:(__weak UIViewController *)controller
              backImage:(SelectPhotosBlock)imageBlock;


@end
