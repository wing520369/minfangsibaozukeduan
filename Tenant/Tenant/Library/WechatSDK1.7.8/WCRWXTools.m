//
//  WCRWXTools.m
//  CarAccessories
//
//  Created by eliot on 16/11/25.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRWXTools.h"
#import "WCRNSUserDefaultsTool.h"

//#define AppID @"wx6e1386fb3a4fa486"

#define AppID @"wx367c5e80b1b4232f"

@interface WCRWXTools ()
@property (nonatomic, strong) WXPayResponseBlock payBlock;  // 微信支付结果的回调
@end

@implementation WCRWXTools

+ (WCRWXTools *)sharedSingleton {
    static WCRWXTools *tools = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tools = [[WCRWXTools alloc]init];
    });
    return tools;
}
#pragma mark - 注册
+ (void)registerWXApi {
    
    BOOL isSuccess = [WXApi registerApp:AppID];
    if (isSuccess) {
        WCRLog(@"向微信终端程序注册第三方应用成功");
    }
    else {
        WCRLog(@"向微信终端程序注册第三方应用失败");
    }
}

+ (BOOL)handleWXApiOPenURL:(NSURL *)url {
    /*! @brief 处理微信通过URL启动App时传递的数据
     *
     * 需要在 application:openURL:sourceApplication:annotation:或者application:handleOpenURL中调用。
     * @param url 微信启动第三方应用时传递过来的URL
     * @param delegate  WXApiDelegate对象，用来接收微信触发的消息。
     * @return 成功返回YES，失败返回NO。
     */
    return  [WXApi handleOpenURL:url delegate:[WCRWXTools sharedSingleton]];
}

#pragma mark - 回调
- (void)onResp:(BaseResp *)resp {
    WCRLog(@"error:%@ code:%d",resp.errStr,resp.errCode);
    if ([resp isKindOfClass:[SendMessageToWXResp class]]) {
        
    } else if ([resp isKindOfClass:[SendAuthResp class]]) { // 登录回调
        
        
    } else if ([resp isKindOfClass:[AddCardToWXCardPackageResp class]]) {
        
    }else if([resp isKindOfClass:[PayResp class]]){
        //支付返回结果，实际支付结果需要去微信服务器端查询
        [WCRNSUserDefaultsTool setStr:@"1" key:@"payValue"];
        self.payBlock(resp.errCode);
    }
}

#pragma mark - 调起微信支付
+ (void)WXPayAction:(NSDictionary *)response PayResultBlock:(WXPayResponseBlock)block{
    
    NSDictionary *dict = response;
    //调起微信支付
    PayReq* req             = [[PayReq alloc] init];
    NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
    req.timeStamp           = stamp.intValue;
    req.partnerId           = [dict objectForKey:@"partnerid"];
    req.prepayId            = [dict objectForKey:@"prepayid"];
    req.nonceStr            = [dict objectForKey:@"noncestr"];
    req.package             = [dict objectForKey:@"package"];
    req.sign                = [dict objectForKey:@"sign"];
    if ([WXApi sendReq:req]) {
        [WCRNSUserDefaultsTool setStr:@"0" key:@"payValue"];
        // 已经跳到支付的页面：0去支付，1支付完成
        WCRLog(@"****************************************\n微信支付: \npartnerid:%@\n prepayId:%@\n nonceStr:%@\n sign:%@\n timeStamp:%d\n****************************************",req.partnerId,req.prepayId,req.nonceStr,req.sign,(unsigned int)req.timeStamp);
        
    }
    else {
        WCRLog(@"调用微信支付失败");
    }
    [WCRWXTools sharedSingleton].payBlock = block;
}

@end
