//
//  WCRWXTools.h
//  CarAccessories
//
//  Created by eliot on 16/11/25.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"

typedef void (^ResponseBlock)(NSDictionary *response, NSString *access_token);
typedef void (^WXPayResponseBlock) (int errCode);

@interface WCRWXTools : NSObject<WXApiDelegate>

+ (WCRWXTools *)sharedSingleton;
/**
 *  向微信终端程序注册第三方应用
 */
+ (void)registerWXApi;
/**
 *  处理微信通过URL启动App时传递的数据
 *
 *  @param url 微信启动第三方应用时传递过来的URL
 *
 *  @return 成功返回YES，失败返回NO。
 */
+ (BOOL)handleWXApiOPenURL:(NSURL *)url;

/** 调起微信支付 */
+ (void)WXPayAction:(NSDictionary *)response PayResultBlock:(WXPayResponseBlock)block;

@end
