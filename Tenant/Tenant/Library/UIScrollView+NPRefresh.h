//
//  UIScrollView+NPRefresh.h
//  KDExpressClient
//
//  Created by HzB on 16/9/12.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJRefresh/MJRefresh.h>

@interface UIScrollView (NPRefresh)

- (void)MJFooterWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock;

- (void)MJHeaderWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock;


@end
