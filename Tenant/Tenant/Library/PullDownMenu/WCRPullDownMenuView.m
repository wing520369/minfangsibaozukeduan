//
//  WCRPullDownMenuView.m
//  Tenant
//
//  Created by eliot on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRPullDownMenuView.h"
#import "WCRDataSourceModel.h"

@interface WCRMenuCell : UITableViewCell
@property (nonatomic, strong) WCRMenuModel *model;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *lineView;
@end
@implementation WCRMenuCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat cellHeight = kDefaultHeight;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, cellHeight)];
        _titleLabel = label;
        [label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
        [self.contentView addSubview:label];
        
        _lineView = [UIView lineView];
        _lineView.frame = CGRectMake(0, cellHeight - 1, kMainWidth, 1);
        [self.contentView addSubview:_lineView];
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)setModel:(WCRMenuModel *)model {
    _titleLabel.text = model.name;
    if (model.selected) {
        _titleLabel.textColor = [UIColor colorWithHexString:kMainColor];
    }
    else {
        _titleLabel.textColor = [UIColor blackColor];
    }
}
@end

static NSString *identifier = @"cell";

@interface WCRPullDownMenuView ()<UITableViewDelegate>
@property (nonatomic, copy) void (^clickModelBlock) (WCRMenuModel *model);
@property (nonatomic, strong) WCRDataSourceModel *dataSourceModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRPullDownMenuView

+ (WCRPullDownMenuView *)popMenuViewWithClickViewBottomY:(CGFloat)bottomY MenuModels:(NSArray<WCRMenuModel *> *)models ClickActionBlock:(void (^) (WCRMenuModel *model))clickBlock {
    
    // 背景view
    WCRPullDownMenuView *pullDownMenuView = [[self alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [[UIApplication sharedApplication].keyWindow addSubview:pullDownMenuView];
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    
    [pullDownMenuView popMenuViewWithClickViewBottomY:bottomY MenuModels:models ClickActionBlock:^(WCRMenuModel *model) {
        clickBlock(model);
    }];
    
    return pullDownMenuView;
}

- (void)popMenuViewWithClickViewBottomY:(CGFloat)bottomY MenuModels:(NSArray<WCRMenuModel *> *)models ClickActionBlock:(void (^) (WCRMenuModel *model))clickBlock {
    
    self.backgroundColor = [UIColor clearColor];
    self.clickModelBlock = clickBlock;
    
    // 分三部分视图 tableView 以上是透明，以下就
    // 透明视图
    UIView *clearView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, bottomY + 64)];
    clearView.backgroundColor = [UIColor clearColor];
    [self addSubview:clearView];
    // 添加手势移除视图
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeBgViewFromWindow:)];
    [clearView addGestureRecognizer:tap];
    
    // tableView下面暗层
    CGFloat height = 80;
    CGFloat y = kMainHeight - height;
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, height)];
    blackView.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.15];
    [self addSubview:blackView];
    // 添加手势移除视图
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeBgViewFromWindow:)];
    [blackView addGestureRecognizer:tap1];
    
    // 内容视图 tableView
    _dataSourceModel = [[WCRDataSourceModel alloc] init];
    [_dataSourceModel.dataSource addObjectsFromArray:models];
    
    // 需要加上64 ： 导航栏的高度
    y = clearView.bottom_wcr;
    height = blackView.y_wcr - y;
    @weakify(self);
    UITableView *tableView = [self.dataSourceModel createTableViewWithFrame:CGRectMake(0, y, kMainWidth, height) CellIdentifier:identifier CellClassName:@"WCRMenuCell" ConfigureCellBlock:^(WCRMenuCell *cell, NSIndexPath *indexPath, WCRMenuModel *model) {
        @strongify(self);
        cell.model = model;
        cell.lineView.hidden = indexPath.row == self.dataSourceModel.dataSource.count - 1 ? YES : NO;
    }];
    tableView.backgroundColor = blackView.backgroundColor;
    _tableView = tableView;
    tableView.delegate = self;
//    tableView.bounces = NO;
    [self addSubview:tableView];
    
    tableView.rowHeight = kDefaultHeight;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.clickModelBlock) {
        WCRMenuModel *model = [_dataSourceModel modelAtIndexPath:indexPath];
        self.clickModelBlock(model);
    }
    [self removeFromSuperview];
}

#pragma mark - 手势移除视图
- (void)removeBgViewFromWindow:(UITapGestureRecognizer *)tapGR {
    
    [self removeFromSuperview];
}

- (void)setSelectWithMenuModelId:(NSString *)modelId {
    
    [self.dataSourceModel.dataSource enumerateObjectsUsingBlock:^(WCRMenuModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.ID isEqualToString:modelId]) {
            obj.selected = YES;
        }
        else {
            obj.selected = NO;
        }
    }];
    [self.tableView reloadData];
}

@end

@implementation WCRMenuModel
- (instancetype)initWithID:(NSString *)ID Name:(NSString *)name {
    if (self = [super init]) {
        self.ID = ID;
        self.name = name;
        self.selected = NO;
    }
    return self;
}
@end
