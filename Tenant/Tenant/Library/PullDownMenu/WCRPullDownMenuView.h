//
//  WCRPullDownMenuView.h
//  Tenant
//
//  Created by eliot on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRMenuModel;
/// 弹出下拉框视图
@interface WCRPullDownMenuView : UIView

/**
 *  弹出下拉框
 *
 *  @param bottomY  点击当前View的底部y值
 *  @param models   下拉菜单内容数据
 *
 */
+ (WCRPullDownMenuView *)popMenuViewWithClickViewBottomY:(CGFloat)bottomY MenuModels:(NSArray<WCRMenuModel *> *)models ClickActionBlock:(void (^) (WCRMenuModel *model))clickBlock;

- (void)setSelectWithMenuModelId:(NSString *)modelId;

@end

@interface WCRMenuModel : WCRBasicModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL selected;
- (instancetype)initWithID:(NSString *)ID Name:(NSString *)name;
@end
