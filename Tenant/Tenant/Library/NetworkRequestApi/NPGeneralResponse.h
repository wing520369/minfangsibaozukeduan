//
//  NPGeneralResponse.h
//  Demo0822
//
//  Created by HzB on 16/7/26.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger,NPGeneralResponseCode)
{
    /** 没和服务器交换的失败*/
    NPGeneralResponseCode_Failure,
    /** 返回错误*/
    NPGeneralResponseCode_Error,
    /** 返回成功*/
    NPGeneralResponseCode_Success,
 
};


/**
 *  网络请求返回数据
 */
@interface NPGeneralResponse : NSObject

/** 服务器返回的code，一般只用来设置 backCode*/
@property(nonatomic,copy) NSString *resultCode;

/** 服务器返回的提示语*/
@property(nonatomic,copy) NSString *msg;

/** 服务器返回的主数据*/
@property(nonatomic,strong) id response;

/** 登陆是否过期 1 过期*/
@property (nonatomic, assign,getter=isLoginExpired) BOOL LoginExpired;

// 请求结果状态，根据resultCode来赋值的
@property(nonatomic,assign) NPGeneralResponseCode code;

// 是否成功 根据code判断 服务器返回正确的code才为yes
@property (nonatomic,assign,getter=isOK,readonly) BOOL OK;

- (instancetype)initWithResponseObj:(id)responseObj;

+ (instancetype)responseModeWithError:(NSError *)error;

@end
