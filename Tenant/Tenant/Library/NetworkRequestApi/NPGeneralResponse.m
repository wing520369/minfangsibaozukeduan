//
//  NPGeneralResponse.m
//  Demo0822
//
//  Created by HzB on 16/7/26.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import "NPGeneralResponse.h"
#import "WCRLoginVC.h"

@interface NPGeneralResponse()

@property (nonatomic, weak) id  rawData ;

@end

@implementation NPGeneralResponse

- (instancetype)initWithResponseObj:(id)responseObj{
    if(self = [self init] ){
        self.rawData = responseObj;
    }
    return self;
}

- (void)setRawData:(id)rawData{
    
    if (!rawData || ![rawData isKindOfClass:[NSDictionary class]]) {
        _msg = @"网络异常，连接超时！";
        _msg = @"100000";
        _code = NPGeneralResponseCode_Failure;
        return;
    }
    NSDictionary *dic = rawData;
    _msg = [dic objectForKey:@"resultMessage"];
    _resultCode = [dic objectForKey:@"resultCode"];
    _response = [dic objectForKey:@"resultData"];
    if ((_msg == nil || [_msg isKindOfClass:[NSNull class]])) {
        _msg = @"";
    }
    if ([_resultCode intValue] == 0 || [_resultCode integerValue] == 1015) {
        _code = NPGeneralResponseCode_Success;
    }
    else{
        _code = NPGeneralResponseCode_Error;
        /***/
        if ([_resultCode intValue] == 500) {
            // 服务器内部错误
            _msg = @"网络请求失败";
        }
         
        if ([self.resultCode integerValue] == 1005 || [self.msg isEqualToString:@"未登录"]) {
            // 如果是退出登录接口请求，不提示
            self.msg = @"登录已过期或在别的设备登陆！请重新登录！";
            self.LoginExpired = YES;
            [WCRUserModelTool logOut];
            // 调到登录页面
            [[UIApplication sharedApplication] keyWindow].rootViewController = [[WCRLoginVC alloc] init];
        }
    }
}


- (BOOL)isOK{
    if (_code == NPGeneralResponseCode_Success) {
        return YES;
    }
    return NO;
}

+ (instancetype)responseModeWithError:(NSError *)error
{
    NPGeneralResponse *model = [[NPGeneralResponse alloc]init];
    model.response = nil;
    model.code = NPGeneralResponseCode_Failure;
    model.msg = error.localizedDescription;
    return model;
}


@end
