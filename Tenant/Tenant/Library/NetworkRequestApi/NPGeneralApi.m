//
//  NPGeneralApi.m
//  Demo0822
//
//  Created by HzB on 16/7/26.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import "NPGeneralApi.h"
#import "DZNEmptyDataProvider.h"
#import "UIWindow+WCRCustom.h"
#import "NPTip.h"
#import "NPHUD.h"
#import "NSString+WCRCustom.h"

@interface NPGeneralApi()
@property (nonatomic, weak) UIViewController *topVC;
@property (nonatomic, copy) NPGeneralResponseBlock responseBlock;
@end

@implementation NPGeneralApi

- (void)dealloc{

}

- (instancetype)init{
    if (self = [super init]) {
        _showHud = YES;
        _showErrorMsg = YES;
        _showFailureMsg = YES;
        _fail = NO;
    }
    return self;
}


- (instancetype)initWithParameters:(NSDictionary *)parameters tailUrl:(NSString *)tailUrl {
    if (self = [super init]) {
        
        self.requestParameters = parameters;
        self.tailUrl = tailUrl;
        self.showHud = YES;
        self.showErrorMsg = YES;
        self.showFailureMsg = YES;
    }
    return self;
}

+ (instancetype)apiWithParameters:(NSDictionary *)parameters
                          tailUrl:(NSString *)tailUrl {
    return [[self alloc] initWithParameters:parameters tailUrl:tailUrl];
}

- (void)setTailUrl:(NSString *)tailUrl{
    //一个完整的地址由服务器地址 和 后半部分组成
    _tailUrl = tailUrl;
    self.baseUrl = [NSString stringWithFormat:@"%@%@",SERVERCE_HOST,tailUrl];
}

- (DRDRequestSerializerType)apiRequestSerializerType {
    return DRDRequestSerializerTypeHTTP;
}

- (nullable NSDictionary *)apiRequestHTTPHeaderField {
    return nil;
}

- (void)addUpdateImagesWithNameObj:(id)nameObj
                            images:(NSArray<UIImage *> *)images{
    __block NSArray *blockImages = images;
    
    [self setApiRequestConstructingBodyBlock:^(id<DRDMultipartFormData> _Nonnull formData) {
        //要提交的图片信息
        for (int i = 0; i < blockImages.count; i++) {
            
            NSString *type = @"image/jpeg";
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss:SS";
            NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
            NSString *fileName = [NSString stringWithFormat:@"%@_%d.jpg",dateString,i];
            
            UIImage *image = nil;
            if ([[blockImages objectAtIndex:i] isKindOfClass:[NSString class]]) {
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[blockImages objectAtIndex:i]]]];
            }
            else if ([[blockImages objectAtIndex:i] isKindOfClass:[UIImage class]]) {
                image = [blockImages objectAtIndex:i];
            }
            
//            UIImage *image = [blockImages objectAtIndex:i];
            if (!image) {
                continue;
            }
            CGFloat scale = 1.0;
            if (image.size.width > 414.0) {
                scale = 414.0 / image.size.width;
            }
            else if (image.size.height > 480.0){
                scale =  480.0 / image.size.height;
            }
            NSData *imageData = UIImageJPEGRepresentation(image, scale);
            
            NSString *name = nil;
            if ([nameObj isKindOfClass:[NSString class]]) {
                name = nameObj;
            }
            else if ([nameObj isKindOfClass:[NSArray class]]){
                NSArray *nameArray = nameObj;
                if (i < nameArray.count) {
                    name = [nameArray objectAtIndex:i];
                }
            }
            else{
                name = [NSString stringWithFormat:@"img%d",i + 1];
            }
            
            [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:type];
        }
    }];
}

- (void)start{
    _topVC = [UIWindow topViewController];
    if (_showHud) {
        [_topVC showHud];
    }
    [super start];
}


- (void)startWithResponseCompletion:(NPGeneralResponseBlock)responseBlock{
    self.responseBlock = responseBlock;
    [self start];
}

#pragma mark -
- (void)showTipWithResponse:(NPGeneralResponse *)responseMd {
    switch (responseMd.code) {
        case NPGeneralResponseCode_Success: {
            if (_showSuccessMsg) {
                [NPTip showTip:responseMd.msg];
            }
            break;
        }
        case NPGeneralResponseCode_Error: {
            if (_showErrorMsg) {
                [NPTip showTip:responseMd.msg];
            }
            break;
        }
        case NPGeneralResponseCode_Failure: {
            if (!self.showFailureTipsBgView) {
                break;
            }
            //如果在当前的页面还没释放
            if (!self.topVC) {
                break;
            }
            __block NPGeneralApi *blockApi = self;
            DZNEmptyDataProvider *dataProvider = [DZNEmptyDataProvider providerWithEmptyType:DZNEmptyDataType_LoadFail];
            [dataProvider didTapButtonBlock:^(UIScrollView *scrollView, UIButton *button, DZNEmptyDataProvider *provider) {
                [blockApi start];
                [blockApi.topVC dismissEmptyView];
            }];
            [self.topVC showEmptyViewWithProvider:dataProvider];
            break;
        }
    }
}
// 显示加载失败的View
- (void)showTipsBgView {
    // 如果在当前的页面还没释放
    if (!self.topVC) {
        return;
    }
    __block NPGeneralApi *blockApi = self;
    DZNEmptyDataProvider *dataProvider = [DZNEmptyDataProvider providerWithEmptyType:DZNEmptyDataType_LoadFail];
    [dataProvider didTapButtonBlock:^(UIScrollView *scrollView, UIButton *button, DZNEmptyDataProvider *provider) {
        [blockApi start];
        [blockApi.topVC dismissEmptyView];
    }];
    [self.topVC showEmptyViewWithProvider:dataProvider];
}
#pragma mark - 对数据进行处理 统一先转为 ResponseModel 子线程
- (nullable id)apiResponseObjReformer:(id)responseObject andError:(NSError * _Nullable)error {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if (_showHud) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.topVC hideHud];
        });
    }
    
    if (self.objReformerDelegate) {
        return [self.objReformerDelegate apiResponseObjReformerWithGeneralAPI:self
                                                            andResponseObject:responseObject
                                                                     andError:error];
    }
    
    if (self.apiResponseObjReformerBlock) {
        return self.apiResponseObjReformerBlock(responseObject, error);
    }
    
    // 默认处理为 NPGeneralResponse
    NPGeneralResponse * responseMd = nil;
    if (!error) {
        responseMd = [[NPGeneralResponse alloc]initWithResponseObj:responseObject];
    }
    else{
        responseMd = [NPGeneralResponse responseModeWithError:error];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.topVC hideHud];
        [self showTipWithResponse:responseMd];
        if (self.responseBlock) {
            self.responseBlock(responseMd);
        }
    });
#ifdef DEBUG
    //打印链接、参数、返回数据
    NSString *string = nil;
    if (responseMd.code == NPGeneralResponseCode_Failure) {
        string = [NSString stringWithFormat:@"请求失败 -> %@",error.localizedDescription];
        self.fail = YES;
        if (![string isEqualToString:@"请求失败 -> Request send too fast, please try again later"]) {
            [NPTip showTip:string];
        }
    }
    else{
        self.fail = NO;
        string = [NSString stringWithFormat:@"返回数据 -> %@",[NSString stringWithJsonbject:responseObject]];
    }
    NSLog(@"%@\n%@",[self urlWithParameters],string);
#else
#endif
    return responseMd;
}

/**
 *  链接和参数
 */
- (NSString *)urlWithParameters {
    NSMutableString *urlStr = [NSMutableString stringWithFormat:@"%@", self.baseUrl];
    if (self.requestParameters) {
        [urlStr appendString:@"?"];
        NSMutableArray *pairs = [NSMutableArray array];
        [self.requestParameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSString *pair = [NSString stringWithFormat:@"%@=%@", key, [[obj description] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [pairs addObject:pair];
        }];
        [urlStr appendString:[pairs componentsJoinedByString:@"&"]];
    }
    return [NSString stringWithFormat:@"请求url -> %@",urlStr];
}

#pragma mark - 自定义的网络请求
#pragma mark -
+ (NPGeneralApi *)WCRApiWithParameters:(NSDictionary *)parameters tailUrl:(NSString *)tailUrl ResponseBlock:(NPGeneralResponseBlock)responseBlock {
    
    NPGeneralApi *api = [self apiWithParameters:parameters tailUrl:tailUrl];
    __block NPGeneralResponseBlock block = responseBlock;
    [api startWithResponseCompletion:^(NPGeneralResponse *responseMd) {
        block(responseMd);
    }];
    return api;
}
// 请求完成，如果失败显示加载失败的View
+ (NPGeneralApi *)WCRApiShowFailureTipsBgViewWithParameters:(NSDictionary *)parameters tailUrl:(NSString *)tailUrl ResponseBlock:(NPGeneralResponseBlock)responseBlock {
    
    NPGeneralApi *api = [self apiWithParameters:parameters tailUrl:tailUrl];
    __block NPGeneralResponseBlock block = responseBlock;
    [api startWithResponseCompletion:^(NPGeneralResponse *responseMd) {
        block(responseMd);
        if (!responseMd.isOK) {
            // 显示加载失败的View
            [api showTipsBgView];
        }
    }];
    return api;
}
// 不提示任何信息
+ (NPGeneralApi *)WCRApiNoShowMessageViewWithParameters:(NSDictionary *)parameters tailUrl:(NSString *)tailUrl ResponseBlock:(NPGeneralResponseBlock)responseBlock {
    
    NPGeneralApi *api = [self apiWithParameters:parameters tailUrl:tailUrl];
    api.showHud = NO;
    api.showErrorMsg = NO;
    api.showFailureMsg = NO;
    api.showSuccessMsg = NO;
    __block NPGeneralResponseBlock block = responseBlock;
    [api startWithResponseCompletion:^(NPGeneralResponse *responseMd) {
        block(responseMd);
    }];
    return api;
}
// 上传图片
+ (NPGeneralApi *)WCRApiWithParameters:(NSDictionary *)parameters tailUrl:(NSString *)tailUrl ImagesWithNameObj:(id)nameObj images:(NSArray<UIImage *> *)images ResponseBlock:(NPGeneralResponseBlock)responseBlock {
    NPGeneralApi *api = [self apiWithParameters:parameters tailUrl:tailUrl];
    [api addUpdateImagesWithNameObj:nameObj images:images];
    __block NPGeneralResponseBlock block = responseBlock;
    [api startWithResponseCompletion:^(NPGeneralResponse *responseMd) {
        block(responseMd);
    }];
    return api;
}

+ (NPGeneralApi *)WCRmultiApiWithParameters:(NSDictionary *)parameters tailUrl:(NSString *)tailUrl ResponseBlock:(NPGeneralResponseBlock)responseBlock {
    
    NPGeneralApi *api = [self apiWithParameters:parameters tailUrl:tailUrl];
    api.showHud = NO;
    api.showErrorMsg = NO;
    api.showFailureMsg = NO;
    api.showSuccessMsg = NO;
    api.responseBlock = responseBlock;

    return api;
}

@end
