//
//  NPGeneralApi.h
//  Demo0822
//
//  Created by HzB on 16/7/26.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import <DRDNetworking/DRDNetworking.h>
#import "NPGeneralResponse.h"

typedef void(^NPGeneralResponseBlock)(NPGeneralResponse *responseMd);

/**
 *  网络请求一般用这个 基于DRDNetworking, 默认将返回的数据处理成 NPGeneralResponse
 */
@interface NPGeneralApi : DRDGeneralAPI


/**
 *  是否显示hud 默认显示
 */
@property (nonatomic, assign) BOOL showHud;

/**
 *  请求成功后 是否显示提示语 默认不显示
 */
@property (nonatomic, assign) BOOL showSuccessMsg;

/**
 *  请求错误（后台返回错误码）是否显示提示语 默认显示
 */
@property (nonatomic, assign) BOOL showErrorMsg;

/**
 *  请求失败时是否显示提示语 默认显示
 */
@property (nonatomic, assign) BOOL showFailureMsg;


/**
 *  请求失败时，是否显示重新加载的提示view，默认不显示
 */
@property (nonatomic, assign) BOOL showFailureTipsBgView;

/** 接口后半段*/
@property (nonatomic, copy) NSString *tailUrl;


/**
 *  初始化一个api
 *
 *  @param parameters    请求参数
 *  @param tailUrl 接口后半段
 *
 *  @return api
 */
+ (instancetype)apiWithParameters:(NSDictionary *)parameters
                          tailUrl:(NSString *)tailUrl;

- (instancetype)initWithParameters:(NSDictionary *)parameters
                           tailUrl:(NSString *)tailUrl;

/**
 *  添加多张图片
 *
 *  @param nameObj   对应的字段 如传数组就要和图片
 *   一一对应 如 @[@"img1",@"img2"],如填字符串就所有图片都是这个字段
 *  如传nil 就默认 @[@"img1",@"img2"]
 *  @param images 图片
 */
- (void)addUpdateImagesWithNameObj:(id)nameObj
                            images:(NSArray<UIImage *> *)images;

/**
 *  开始请求
 *
 *  @param responseBlock 请求结束回调
 */
- (void)startWithResponseCompletion:(NPGeneralResponseBlock)responseBlock;

#pragma mark - 自定义
/**
 *  默认的网络请求
 *
 *  @param parameters    参数列表
 *  @param tailUrl       拼接的请求字符串
 *  @param responseBlock 回调
 *
 *  @return 返回网络请求对象，可以修改提示等信息
 */
+ (NPGeneralApi *)WCRApiWithParameters:(NSDictionary *)parameters
                     tailUrl:(NSString *)tailUrl
               ResponseBlock:(NPGeneralResponseBlock)responseBlock;

/**
 *  网络请求，不提示任何信息
 *
 *  @param parameters    参数列表
 *  @param tailUrl       拼接的请求字符串
 *  @param responseBlock 回调
 *
 *  @return 返回网络请求对象，不提示信息
 */
+ (NPGeneralApi *)WCRApiNoShowMessageViewWithParameters:(NSDictionary *)parameters
                                                tailUrl:(NSString *)tailUrl
                                          ResponseBlock:(NPGeneralResponseBlock)responseBlock;

/**
 *  请求完成，如果失败显示加载失败的View
 *
 *  @param parameters    参数列表
 *  @param tailUrl       拼接的请求字符串
 *  @param responseBlock 回调
 *
 *  @return 返回网络请求对象，可以修改提示等信息
 */
+ (NPGeneralApi *)WCRApiShowFailureTipsBgViewWithParameters:(NSDictionary *)parameters
                                                    tailUrl:(NSString *)tailUrl
                                              ResponseBlock:(NPGeneralResponseBlock)responseBlock;

/**
 *  上传图片（单或多张）
 *
 *  @param parameters    参数列表
 *  @param tailUrl       拼接的请求字符串
 *  @param nameObj       对应的字段 如传数组就要和图片，一一对应 如 @[@"img1",@"img2"],如填字符串就所有图片都是这个字段，如传nil 就默认 @[@"img1",@"img2"]
 *  @param images        图片数组
 *  @param responseBlock 回调
 *
 *  @return 返回网络请求对象，可以修改提示等信息
 */
+ (NPGeneralApi *)WCRApiWithParameters:(NSDictionary *)parameters
                               tailUrl:(NSString *)tailUrl
                     ImagesWithNameObj:(id)nameObj
                                images:(NSArray<UIImage *> *)images
                         ResponseBlock:(NPGeneralResponseBlock)responseBlock;


/*********************************
 *
 *  同时发起多个请求
 *
 ********************************/
@property (nonatomic, assign) BOOL fail;    // 判断此次网络是否请求成功
+ (NPGeneralApi *)WCRmultiApiWithParameters:(NSDictionary *)parameters
                                                tailUrl:(NSString *)tailUrl
                                          ResponseBlock:(NPGeneralResponseBlock)responseBlock;

@end
