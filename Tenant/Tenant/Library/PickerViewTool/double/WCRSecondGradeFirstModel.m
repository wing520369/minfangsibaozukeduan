//
//  WCRSecondGradeFirstModel.m
//  SchoolFriendMeet
//
//  Created by eliot on 16/12/29.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRSecondGradeFirstModel.h"

@implementation WCRSecondGradeFirstModel

+ (NSDictionary *)objectClassInArray{
    return @{@"secondModels" : [WCRSecondGradeSecondModel class]};
}

@end
@implementation WCRSecondGradeSecondModel

@end
