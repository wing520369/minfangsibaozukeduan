//
//  WCRDoubleModelPickerView.m
//  SchoolFriendMeet
//
//  Created by eliot on 16/12/29.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRDoubleModelPickerView.h"
#import "WCRSecondGradeFirstModel.h"
#import "UIFont+WCRCustom.h"
#import "UIButton+WCRCustom.h"
#import "UIColor+WCRCustom.h"

@interface WCRDoubleModelPickerView ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, assign) CGFloat pickerViewHeight;
@property (nonatomic, assign) CGFloat buttonHeight;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) NSString *cancelTitle;
@property (nonatomic, strong) UIColor *cancelButtonColor;
@property (nonatomic, strong) NSString *confirmTitle;
@property (nonatomic, strong) UIColor *confirmButtonColor;
@property (nonatomic, strong) UIColor *barColor;
@property (nonatomic, strong) void (^block) (WCRSecondGradeFirstModel *firstModel, WCRSecondGradeSecondModel *secondModel);
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UILabel *resultLabel;
@property (nonatomic, strong) WCRSecondGradeFirstModel *currentFirstModel;
@property (nonatomic, strong) WCRSecondGradeSecondModel *currentSecondModel;
@property (nonatomic, strong) NSMutableArray *firstModels;
@property (nonatomic, strong) NSMutableArray *secondModels;

@end

@implementation WCRDoubleModelPickerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.font = [UIFont CustomFontWithSize:15.0];
        self.buttonHeight = 50.0;
        self.pickerViewHeight = 170.0;
        self.cancelTitle = @"取消";
        self.cancelButtonColor = [UIColor grayColor];
        self.confirmTitle = @"完成";
        self.confirmButtonColor = [UIColor orangeColor];
        self.barColor = [UIColor colorWithRGB:223 G:223 B:223];
    }
    return self;
}

+ (void)popDoublePickerViewWithDataSource:(NSArray<WCRSecondGradeFirstModel *> *)dataSource SelectedActionBlock:(void (^)(WCRSecondGradeFirstModel *firstModel, WCRSecondGradeSecondModel *secondModel))block {
    
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height = [[UIScreen mainScreen] bounds].size.height;
    WCRDoubleModelPickerView *doublePickerView = [[self alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [[UIApplication sharedApplication].keyWindow addSubview:doublePickerView];
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    
    [doublePickerView popDoublePickerViewWithDataSource:dataSource SelectedActionBlock:^(WCRSecondGradeFirstModel *firstModel, WCRSecondGradeSecondModel *secondModel) {
        block(firstModel, secondModel);
    }];
}

- (void)popDoublePickerViewWithDataSource:(NSArray<WCRSecondGradeFirstModel *> *)dataSource SelectedActionBlock:(void(^)(WCRSecondGradeFirstModel *firstModel, WCRSecondGradeSecondModel *secondModel))block {
    
    self.block = block;
    self.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.3];
    // 添加手势移除视图
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeBgViewFromWindow:)];
    [self addGestureRecognizer:tap];
    
    _firstModels = [NSMutableArray array];
    for (WCRSecondGradeFirstModel *firstModel in dataSource) {
        [_firstModels addObject:firstModel];
    }
    
    _secondModels = [NSMutableArray array];
    if (_firstModels.count > 0) {
        WCRSecondGradeFirstModel *firstModel = [_firstModels objectAtIndex:0];
        [_secondModels addObjectsFromArray:firstModel.secondModels];
    }
    
    // 内容View：button+pickerView
    CGFloat height = self.buttonHeight + self.pickerViewHeight;
    CGFloat width = self.frame.size.width;
    CGFloat y = self.frame.size.height;//  - height
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, y, width, height)];
    contentView.backgroundColor = [UIColor colorWithRed:243 / 255.0 green:243 / 255.0 blue:243 / 255.0 alpha:1];
    [self addSubview:contentView];
    
    // 左边取消按钮
    CGFloat offsetX = 15;
    CGFloat x = offsetX;
    y = 0;
    width = 40;
    height = self.buttonHeight;
    UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [cancelButton setTitle:self.cancelTitle forState:0];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [cancelButton setTitleColor:self.cancelButtonColor forState:0];
    cancelButton.titleLabel.font = self.font;
    cancelButton.tag = 1117;
    [cancelButton addTarget:self action:@selector(viewButtonAciton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setEnlargeEdgeWithTop:0 Right:20 Bottom:10 Left:offsetX];
    [contentView addSubview:cancelButton];
    cancelButton.hidden = YES;
    cancelButton.userInteractionEnabled = NO;
    
    // 右边完成按钮
    x = self.frame.size.width - width - offsetX;
    UIButton *completeButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [completeButton setTitle:self.confirmTitle forState:0];
    completeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [completeButton setTitleColor:self.confirmButtonColor forState:0];
    completeButton.tag = 1118;
    completeButton.titleLabel.font = self.font;
    [completeButton addTarget:self action:@selector(viewButtonAciton:) forControlEvents:UIControlEventTouchUpInside];
    [completeButton setEnlargeEdgeWithTop:0 Right:offsetX Bottom:10 Left:20];
    [contentView addSubview:completeButton];
    
    // 显示当前选中的结果
    x = CGRectGetMaxX(cancelButton.frame);
    width = CGRectGetWidth(contentView.frame) - x * 2;
    UILabel *resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    _resultLabel = resultLabel;
    resultLabel.font = self.font;
    resultLabel.textAlignment = NSTextAlignmentCenter;
    resultLabel.backgroundColor = [UIColor clearColor];
    [contentView addSubview:resultLabel];
    
    // pickerView
    y = CGRectGetMaxY(cancelButton.frame);
    x = 0;
    width = CGRectGetWidth(contentView.frame);
    height = self.pickerViewHeight;
    UIPickerView *pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(x, y, width, height)];
    _pickerView = pickerView;
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    [contentView addSubview:pickerView];
    
    
    if (_firstModels.count > 0) {
        NSInteger index = 0;
        _currentFirstModel = [_firstModels objectAtIndex:index];
        [_secondModels removeAllObjects];
        [_secondModels addObjectsFromArray:_currentFirstModel.secondModels];
        if (_secondModels.count > 0) {
            _currentSecondModel = [_secondModels objectAtIndex:0];
            [pickerView reloadComponent:1];
            [pickerView selectRow:0 inComponent:1 animated:YES];
        }
        [pickerView selectRow:index inComponent:0 animated:YES];
        _resultLabel.text = [NSString stringWithFormat:@"%@ %@",_currentFirstModel.name,_currentSecondModel.name];
    }
    
    __block UIView *weakView = contentView;
    CGRect frame = contentView.frame;
    frame.origin.y = self.frame.size.height - frame.size.height;
    [UIView animateWithDuration:0.10 animations:^{
        weakView.frame = frame;
    }];
}

#pragma mark - 手势移除视图
- (void)removeBgViewFromWindow:(UITapGestureRecognizer *)tapGR {
    [self removeFromSuperview];
}
#pragma mark - 取消、完成
- (void)viewButtonAciton:(UIButton *)button {
    if (button.tag == 1117) {
        // 取消
        
    }
    else if (button.tag == 1118) {
        // 完成
        if (self.block) {
            self.block(_currentFirstModel, _currentSecondModel);
        }
    }
    [self removeFromSuperview];
}
#pragma mark - PickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return _firstModels.count;
    }
    else {
        return _secondModels.count;
    }
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        WCRSecondGradeFirstModel *model = [_firstModels objectAtIndex:row];
        return model.name;
    }else {
        WCRSecondGradeSecondModel *model = [_secondModels objectAtIndex:row];
        return model.name;
    }
}
#pragma mark - PickView Delegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (component == 0) {
        
        WCRSecondGradeFirstModel *model = [_firstModels objectAtIndex:row];
        _currentFirstModel = model;
        
        [_secondModels removeAllObjects];
        [_secondModels addObjectsFromArray:model.secondModels];
        _currentSecondModel = [_secondModels objectAtIndex:0];
        
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        
    }
    if (component == 1) {
        // 选中的市
        _currentSecondModel = [_secondModels objectAtIndex:row];
    }
    
    _resultLabel.text = [NSString stringWithFormat:@"%@ %@",_currentFirstModel.name,_currentSecondModel.name];
}
#pragma mark - 改变pickerView的显示字体和颜色
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label = [[UILabel alloc]init];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = self.font;
    label.text = [self pickerView:_pickerView titleForRow:row forComponent:component];
    return label;
}

@end
