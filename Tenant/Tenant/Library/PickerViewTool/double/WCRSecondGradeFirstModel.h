//
//  WCRSecondGradeFirstModel.h
//  SchoolFriendMeet
//
//  Created by eliot on 16/12/29.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

/**
 *  用于两列的pickerView：两层模型
 */
@class WCRSecondGradeSecondModel;
@interface WCRSecondGradeFirstModel : WCRBasicModel

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) NSArray<WCRSecondGradeSecondModel *> *secondModels;

@end

@interface WCRSecondGradeSecondModel : WCRBasicModel

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *name;

@end