//
//  WCRDoubleModelPickerView.h
//  SchoolFriendMeet
//
//  Created by eliot on 16/12/29.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 两列的pickerView */
@class WCRSecondGradeFirstModel,WCRSecondGradeSecondModel;
@interface WCRDoubleModelPickerView : UIView

+ (void)popDoublePickerViewWithDataSource:(NSArray<WCRSecondGradeFirstModel *> *)dataSource SelectedActionBlock:(void(^)(WCRSecondGradeFirstModel *firstModel, WCRSecondGradeSecondModel *secondModel))block;

@end
