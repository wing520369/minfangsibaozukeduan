//
//  WCRSingleModel.h
//  SchoolFriendMeet
//
//  Created by eliot on 16/12/17.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

@interface WCRSingleModel : WCRBasicModel

@property (nonatomic, assign) NSInteger year;

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *intro;

@property (nonatomic, copy) NSString *delFlag;

@property (nonatomic, copy) NSString *setupDate;

@property (nonatomic, copy) NSString *schoolId;

@property (nonatomic, copy) NSString *createDate;

@property (nonatomic, copy) NSString *oldName;

@property (nonatomic, copy) NSString *avatar;

@property (nonatomic, copy) NSString *level;

@property (nonatomic, copy) NSString *instituteNo;

@property (nonatomic, copy) NSString *updateDate;

@property (nonatomic, copy) NSString *remarks;

@property (nonatomic, copy) NSString *name;

@end
