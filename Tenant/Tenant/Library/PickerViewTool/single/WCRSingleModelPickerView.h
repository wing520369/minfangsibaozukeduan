//
//  WCRSingleModelPickerView.h
//  SchoolFriendMeet
//
//  Created by eliot on 16/12/17.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WCRSingleModel.h"

typedef void (^SingleModelActionBlock) (WCRSingleModel *model);

/**
 *  单列，使用model
 */
@interface WCRSingleModelPickerView : UIView

/**
 *  弹出单列pickerView
 *
 *  @param models   内容数组
 *  @param block    选中的标题的回调
 */
+ (void)popSinglePickViewWithModels:(NSArray<WCRSingleModel *> *)models SelectedActionBlock:(SingleModelActionBlock)block;

@end
