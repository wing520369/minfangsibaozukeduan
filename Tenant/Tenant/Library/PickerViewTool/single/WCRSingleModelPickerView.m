//
//  WCRSingleModelPickerView.m
//  SchoolFriendMeet
//
//  Created by eliot on 16/12/17.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRSingleModelPickerView.h"
#import "UIFont+WCRCustom.h"
#import "UIButton+WCRCustom.h"
#import "UIColor+WCRCustom.h"

#define CONTIRMCOLOR [UIColor orangeColor]

@interface WCRSingleModelPickerView ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, assign) CGFloat pickerViewHeight;
@property (nonatomic, assign) CGFloat buttonHeight;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) NSString *confirmTitle;
@property (nonatomic, strong) UIColor *confirmButtonColor;
@property (nonatomic, strong) UIColor *barColor;
@property (nonatomic, strong) SingleModelActionBlock block;
@property (nonatomic, strong) NSArray *models;
@property (nonatomic, strong) WCRSingleModel *currentModel;
@property (nonatomic, strong) UILabel *resultLabel;
@property (nonatomic, strong) UIPickerView *pickerView;

@end

@implementation WCRSingleModelPickerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.font = [UIFont CustomFontWithSize:15.0];
        self.buttonHeight = 50.0;
        self.pickerViewHeight = 170.0;
        self.confirmTitle = @"完成";
        self.confirmButtonColor = CONTIRMCOLOR;
        self.barColor = [UIColor colorWithRGB:223 G:223 B:223];
        self.models = [NSArray array];
    }
    return self;
}
+ (void)popSinglePickViewWithModels:(NSArray<WCRSingleModel *> *)models SelectedActionBlock:(SingleModelActionBlock)block {
    
    if (models.count == 0) {
        return;
    }
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height = [[UIScreen mainScreen] bounds].size.height;
    WCRSingleModelPickerView *singlePickerView = [[WCRSingleModelPickerView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    [[UIApplication sharedApplication].keyWindow addSubview:singlePickerView];
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    
    __block SingleModelActionBlock tempBlock = block;
    [singlePickerView popSinglePickViewWithModels:models SelectedActionBlock:^(WCRSingleModel *model) {
        tempBlock(model);
    }];
}
- (void)popSinglePickViewWithModels:(NSArray<WCRSingleModel *> *)models SelectedActionBlock:(SingleModelActionBlock)block {
    
    self.block = block;
    self.models = models;
    self.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.3];
    
    // 添加手势移除视图
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeBgViewFromWindow:)];
    [self addGestureRecognizer:tap];
    
    // 内容View：button+pickerView
    CGFloat height = self.buttonHeight + self.pickerViewHeight;
    CGFloat y = self.frame.size.height;
    CGFloat width = self.frame.size.width;
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, y, width, height)];
    contentView.backgroundColor = self.barColor;
    [self addSubview:contentView];
    
    CGFloat offsetX = 15;
    y = 0;
    width = 40;
    height = self.buttonHeight;
    // 右边完成按钮
    CGFloat x = self.frame.size.width - width - offsetX;
    UIButton *completeButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [completeButton setTitle:self.confirmTitle forState:0];
    completeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [completeButton setTitleColor:self.confirmButtonColor forState:0];
    completeButton.tag = 2141;
    completeButton.titleLabel.font = self.font;
    [completeButton addTarget:self action:@selector(viewButtonAciton:) forControlEvents:UIControlEventTouchUpInside];
    [completeButton setEnlargeEdgeWithTop:0 Right:offsetX Bottom:15 Left:20];
    [contentView addSubview:completeButton];
    
    // 显示当前选中的结果
    x = completeButton.width_wcr + offsetX;
    width = CGRectGetWidth(contentView.frame) - x * 2;
    UILabel *resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    _resultLabel = resultLabel;
    resultLabel.font = self.font;
    resultLabel.textAlignment = NSTextAlignmentCenter;
    resultLabel.backgroundColor = [UIColor clearColor];
    [contentView addSubview:resultLabel];
    
    // pickerView
    y = completeButton.bottom_wcr;
    x = 0;
    width = CGRectGetWidth(contentView.frame);
    height = self.pickerViewHeight;
    UIPickerView *pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(x, y, width, height)];
    _pickerView = pickerView;
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    [contentView addSubview:pickerView];
    
    __block UIView *weakView = contentView;
    CGRect frame = contentView.frame;
    frame.origin.y = self.frame.size.height - frame.size.height;
    [UIView animateWithDuration:0.15 animations:^{
        weakView.frame = frame;
    }];
    
    [self pickerView:pickerView didSelectRow:0 inComponent:0];
}

#pragma mark - 手势移除视图
- (void)removeBgViewFromWindow:(UITapGestureRecognizer *)tapGR {
    [self removeFromSuperview];
}
#pragma mark - 取消、完成
- (void)viewButtonAciton:(UIButton *)button {
    if (self.block) {
        self.block(_currentModel);
    }
    [self removeFromSuperview];
}
#pragma mark - PickerView DataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return _models.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [[_models objectAtIndex:row] name];
}
#pragma mark - PickView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    _currentModel = [_models objectAtIndex:row];
    _resultLabel.text = _currentModel.name;
}
#pragma mark - 改变pickerView的显示字体和颜色
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = self.font;
    label.text = [self pickerView:_pickerView titleForRow:row forComponent:0];
    return label;
}

@end
