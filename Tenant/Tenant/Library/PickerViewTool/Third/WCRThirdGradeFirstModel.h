//
//  WCRThirdGradeModel.h
//  SchoolFriendMeet
//
//  Created by eliot on 16/12/19.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRThirdGradeSecondModel;
@class WCRThirdGradeThirdModel;
/**
 *  用于三列的pickerView：三层模型
 */
@interface WCRThirdGradeFirstModel : WCRBasicModel

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *createDate;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *updateDate;

@property (nonatomic, strong) NSArray<WCRThirdGradeSecondModel *> *secondModels;

@end

@interface WCRThirdGradeSecondModel : WCRBasicModel

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *pname;

@property (nonatomic, copy) NSString *createDate;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *pid;

@property (nonatomic, copy) NSString *updateDate;

@property (nonatomic, strong) NSArray<WCRThirdGradeThirdModel *> *thirdModels;

@end

@interface WCRThirdGradeThirdModel : WCRBasicModel

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *pname;

@property (nonatomic, copy) NSString *createDate;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *pid;

@property (nonatomic, copy) NSString *updateDate;

@end