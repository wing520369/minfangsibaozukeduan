//
//  WCRThirdGradeModel.m
//  SchoolFriendMeet
//
//  Created by eliot on 16/12/19.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRThirdGradeFirstModel.h"

@implementation WCRThirdGradeFirstModel
+ (NSDictionary *)objectClassInArray{
    return @{@"secondModels" : [WCRThirdGradeSecondModel class]};
}
@end

@implementation WCRThirdGradeSecondModel
+ (NSDictionary *)objectClassInArray{
    return @{@"thirdModels" : [WCRThirdGradeThirdModel class]};
}
@end

@implementation WCRThirdGradeThirdModel

@end
