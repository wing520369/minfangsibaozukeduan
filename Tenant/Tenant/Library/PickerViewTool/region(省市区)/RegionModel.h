//
//  RegionModel.h
//  LianJian
//
//  Created by 吴前途 on 16/4/30.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "WCRBasicModel.h"

/** 地区模型：省市区 */
@class CityModel,DistrictModel;
@interface RegionModel : WCRBasicModel

@property (nonatomic, strong) NSString *provinceId;

@property (nonatomic, strong) NSString *province;

@property (nonatomic, strong) NSArray<CityModel*> *cities;

@end

@interface CityModel : WCRBasicModel

@property (nonatomic, strong) NSString *cityId;

@property (nonatomic, strong) NSString *city;

@property (nonatomic, strong) NSString *provinceId;

@property (nonatomic, strong) NSString *firstSpell;

@property (nonatomic, strong) NSString *fullSpell;

@property (nonatomic, strong) NSArray<DistrictModel*> *districts;

@end

@interface DistrictModel : WCRBasicModel

@property (nonatomic, strong) NSString *district;

@property (nonatomic, strong) NSString *districtId;

@property (nonatomic, strong) NSString *cityId;

@property (nonatomic, strong) NSString *provinceId;

@property (nonatomic, strong) NSString *postCode;

@end
