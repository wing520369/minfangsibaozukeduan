//
//  RegionModel.m
//  LianJian
//
//  Created by 吴前途 on 16/4/30.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "RegionModel.h"

@implementation RegionModel

- (instancetype)init {
    if (self = [super init]) {
        self.province = @"";
        self.provinceId = @"";
    }
    return self;
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"provinceId":@"ID"};
}

+ (NSDictionary *)objectClassInArray{
    return @{@"cities" : [CityModel class]};
}
@end

@implementation CityModel

- (instancetype)init {
    if (self = [super init]) {
        self.city = @"";
        self.cityId = @"";
    }
    return self;
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"cityId":@"ID"};
}

+ (NSDictionary *)objectClassInArray{
    return @{@"districts" : [DistrictModel class]};
}
@end

@implementation DistrictModel

- (instancetype)init {
    if (self = [super init]) {
        self.district = @"";
        self.districtId = @"";
    }
    return self;
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"districtId":@"ID"};
}
@end