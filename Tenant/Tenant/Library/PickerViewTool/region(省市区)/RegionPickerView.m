//
//  RegionPickerView.m
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/4.
//  Copyright © 2016年 吴传荣. All rights reserved.
//


#import "RegionPickerView.h"
#import "UIFont+WCRCustom.h"
#import "UIButton+WCRCustom.h"

@interface RegionPickerView ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, assign) CGFloat pickerViewHeight;
@property (nonatomic, assign) CGFloat buttonHeight;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) NSString *cancelTitle;
@property (nonatomic, strong) UIColor *cancelButtonColor;
@property (nonatomic, strong) NSString *confirmTitle;
@property (nonatomic, strong) UIColor *confirmButtonColor;

@property (nonatomic, strong) RegionPickerViewBlock block;
@property (nonatomic, assign) kRegionPickerViewType regionType;
@property (nonatomic, strong) UILabel *resultLabel;
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) RegionModel *proviceModel;
@property (nonatomic, strong) CityModel *cityModel;
@property (nonatomic, strong) DistrictModel *districtModel;
@property (nonatomic, strong) NSMutableArray *provinces; // 所有省
@property (nonatomic, strong) NSMutableArray *cities;    // 所有市
@property (nonatomic, strong) NSMutableArray *districts; // 所有区

@end

@implementation RegionPickerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.font = [UIFont CustomFontWithSize:15.0];
        self.buttonHeight = 50.0;
        self.pickerViewHeight = 170.0;
        self.cancelTitle = @"取消";
        self.cancelButtonColor = [UIColor grayColor];
        self.confirmTitle = @"完成";
        self.confirmButtonColor = [UIColor blueColor];
    }
    return self;
}
// 初始化数据源
- (void)initDataSource {
    
    _proviceModel = [[RegionModel alloc]init];
    _cityModel = [[CityModel alloc]init];
    _districtModel = [[DistrictModel alloc]init];
    
    _provinces = [NSMutableArray array];
    _cities = [NSMutableArray array];
    _districts = [NSMutableArray array];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"region" ofType:@"plist"];
    NSArray *regions = [NSArray arrayWithContentsOfFile:path];
    // 省
    [self.provinces addObjectsFromArray:[RegionModel jsonWithArray:regions]];
    if (self.provinces.count > 0) {
        // 市
        RegionModel *provincemodel = [self.provinces objectAtIndex:0];
        [_cities addObjectsFromArray:provincemodel.cities];
        // 区
        if (_cities.count > 0) {
            CityModel *cityModel = [_cities objectAtIndex:0];
            [_districts addObjectsFromArray:cityModel.districts];
        }
    }
}
+ (void)popProvinceCityPickerViewWithRegionType:(kRegionPickerViewType)regionType CompleteBlock:(RegionPickerViewBlock)block {
    
    // 背景
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height = [[UIScreen mainScreen] bounds].size.height;
    RegionPickerView *regionPickerView = [[self alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    regionPickerView.backgroundColor = [UIColor clearColor];
    
    [[UIApplication sharedApplication].keyWindow addSubview:regionPickerView];
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    
    [regionPickerView popProvinceCityPickerViewWithRegionType:regionType CompleteBlock:^(RegionModel *regionModel, CityModel *cityModel, DistrictModel *districtModel) {
        block(regionModel, cityModel, districtModel);
    }];
}
- (void)popProvinceCityPickerViewWithRegionType:(kRegionPickerViewType)regionType CompleteBlock:(RegionPickerViewBlock)block {
    
    // 添加手势移除视图
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeBgViewFromWindow:)];
    [self addGestureRecognizer:tap];
    
    self.block = block;
    self.regionType = regionType;
    [self initDataSource];
    
    // 内容View：button+pickerView
    CGFloat height = self.buttonHeight + self.pickerViewHeight;
    CGFloat width = self.frame.size.width;
    CGFloat y = self.frame.size.height;
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, y, width, height)];
    contentView.backgroundColor = [UIColor colorWithRed:243 / 255.0 green:243 / 255.0 blue:243 / 255.0 alpha:1];
    [self addSubview:contentView];
    
    // 左边取消按钮
    CGFloat offsetX = 15;
    CGFloat x = offsetX;
    y = 0;
    width = 40;
    height = self.buttonHeight;
    UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [cancelButton setTitle:self.cancelTitle forState:0];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [cancelButton setTitleColor:self.cancelButtonColor forState:0];
    cancelButton.titleLabel.font = self.font;
    cancelButton.tag = 1117;
    [cancelButton addTarget:self action:@selector(viewButtonAciton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setEnlargeEdgeWithTop:0 Right:20 Bottom:10 Left:offsetX];
    [contentView addSubview:cancelButton];
    
    // 右边完成按钮
    x = self.frame.size.width - width - offsetX;
    UIButton *completeButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [completeButton setTitle:self.confirmTitle forState:0];
    completeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [completeButton setTitleColor:self.confirmButtonColor forState:0];
    completeButton.tag = 1118;
    completeButton.titleLabel.font = self.font;
    [completeButton addTarget:self action:@selector(viewButtonAciton:) forControlEvents:UIControlEventTouchUpInside];
    [completeButton setEnlargeEdgeWithTop:0 Right:offsetX Bottom:10 Left:20];
    [contentView addSubview:completeButton];
    
    // 显示当前选中的结果
    x = CGRectGetMaxX(cancelButton.frame);
    width = CGRectGetWidth(contentView.frame) - x * 2;
    UILabel *resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    _resultLabel = resultLabel;
    resultLabel.font = self.font;
    resultLabel.textAlignment = NSTextAlignmentCenter;
    resultLabel.backgroundColor = [UIColor clearColor];
    [contentView addSubview:resultLabel];
    
    // pickerView
    y = CGRectGetMaxY(cancelButton.frame);
    x = 0;
    width = CGRectGetWidth(contentView.frame);
    height = self.pickerViewHeight;
    UIPickerView *pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(x, y, width, height)];
    _pickerView = pickerView;
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    [contentView addSubview:pickerView];
    
    if (_provinces.count > 0) {
        NSString *province =  @"广东省";
        NSString *city = @"广州市";
        NSString *district = @"天河区";
        NSInteger proviceIndex = 0;
        for (RegionModel *tempProvince in self.provinces) {
            if ([province rangeOfString:tempProvince.province].location != NSNotFound || [tempProvince.province rangeOfString:province].location != NSNotFound ) {
                self.proviceModel = tempProvince;
                proviceIndex = [self.provinces indexOfObject:tempProvince];
                break;
            }
        }
        
        [self.cities removeAllObjects];
        [self.cities addObjectsFromArray:self.proviceModel.cities];
        
        NSInteger cityRow = 0;
        for (CityModel *tempCityModel in self.cities) {
            if ([city rangeOfString:tempCityModel.city].location != NSNotFound  || [tempCityModel.city rangeOfString:city].location != NSNotFound ) {
                self.cityModel = tempCityModel;
                cityRow = [self.cities indexOfObject:tempCityModel];
                break;
            }
        }
        [pickerView reloadComponent:1];
        [pickerView selectRow:cityRow inComponent:1 animated:YES];
        
        if (regionType == kAll_RegionType && self.cityModel.districts.count > 0) {
            NSInteger districtIndex = 0;
            [self.districts removeAllObjects];
            [self.districts addObjectsFromArray:self.cityModel.districts];
            
            for (DistrictModel *tempDistrictModel in self.districts) {
                if ([district rangeOfString:tempDistrictModel.district].location != NSNotFound  || [tempDistrictModel.district rangeOfString:district].location != NSNotFound ) {
                    self.districtModel = tempDistrictModel;
                    districtIndex = [self.districts indexOfObject:self.districtModel];
                    break;
                }
            }
            
            [pickerView reloadComponent:2];
            [pickerView selectRow:districtIndex inComponent:2 animated:YES];
        }
        
        [pickerView selectRow:proviceIndex inComponent:0 animated:YES];
        _resultLabel.text = [NSString stringWithFormat:@"%@ %@ %@",_proviceModel.province,_cityModel.city,_districtModel.district];
        
    }
    __block UIView *weakView = contentView;
    CGRect frame = contentView.frame;
    frame.origin.y = self.frame.size.height - frame.size.height;
    [UIView animateWithDuration:0.10 animations:^{
        weakView.frame = frame;
    }];
}

#pragma mark - 手势移除视图
- (void)removeBgViewFromWindow:(UITapGestureRecognizer *)tapGR {
    [self removeFromSuperview];
}
#pragma mark - 取消、完成
- (void)viewButtonAciton:(UIButton *)button {
    if (button.tag == 1117) {
        // 取消
    }
    else if (button.tag == 1118) {
        // 完成
        if (self.block) {
            self.block(_proviceModel,_cityModel,_districtModel);
        }
    }
    [self removeFromSuperview];
}
#pragma mark - PickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if (_regionType == kProCity_RegionType) {
        return 2;
    }
    return 3;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (_regionType == kProCity_RegionType) {
        if (component == 0) {
            return _provinces.count;
        }else {
            return _cities.count;
        }
    }
    else {
        if (component == 0) {
            return _provinces.count;
        }else if (component == 1) {
            return _cities.count;
        }else {
            return _districts.count;
        }
    }
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (_regionType == kProCity_RegionType) {
        if (component == 0) {
            RegionModel *provinceModel = [_provinces objectAtIndex:row];
            return provinceModel.province;
        }else {
            CityModel *cityModel = [_cities objectAtIndex:row];
            return cityModel.city;
        }
    }
    else {
        if (component == 0) {
            RegionModel *provinceModel = [_provinces objectAtIndex:row];
            return provinceModel.province;
        }else if (component == 1) {
            CityModel *cityModel = [_cities objectAtIndex:row];
            return cityModel.city;
        }else {
            if (_districts.count > row) {
                DistrictModel *districtModel = [_districts objectAtIndex:row];
                return districtModel.district;
            }
            return @"";
        }
    }
}
#pragma mark - PickView Delegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (component == 0) {
        
        RegionModel *provinceModel = [_provinces objectAtIndex:row];
        _proviceModel = provinceModel;
        [_cities removeAllObjects];
        [_cities addObjectsFromArray:provinceModel.cities];
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        
        CityModel *cityModel = [_cities objectAtIndex:0];
        _cityModel = cityModel;
        if (_regionType == kAll_RegionType) {
            [_districts removeAllObjects];
            [_districts addObjectsFromArray:cityModel.districts];
            [pickerView reloadComponent:2];
            [pickerView selectRow:0 inComponent:2 animated:YES];
            if (_districts.count > 0) {
                _districtModel = [_districts objectAtIndex:0];
            }
        }
    }
    if (component == 1) {
        // 选中的市
        CityModel *cityModel = [_cities objectAtIndex:row];
        _cityModel = cityModel;
        if (_regionType == kAll_RegionType) {
            [_districts removeAllObjects];
            [_districts addObjectsFromArray:cityModel.districts];
            [pickerView reloadComponent:2];
            [pickerView selectRow:0 inComponent:2 animated:YES];
            if (_districts.count > 0) {
                _districtModel = [_districts objectAtIndex:0];
            }
        }
    }
    
    if (component == 2) {
        DistrictModel *districtModel = nil;
        if (_regionType == kAll_RegionType) {
            if (_districts.count > 0) {
                districtModel = [_districts objectAtIndex:row];
            }
        }
        _districtModel = districtModel;
    }
    
    if (_regionType == kProCity_RegionType) {
        _resultLabel.text = [NSString stringWithFormat:@"%@ %@",_proviceModel.province,_cityModel.city];
    }
    else {
        _resultLabel.text = [NSString stringWithFormat:@"%@ %@ %@",_proviceModel.province,_cityModel.city,_districtModel.district];
    }
    
}
#pragma mark - 改变pickerView的显示字体和颜色
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label = [[UILabel alloc]init];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = self.font;
    label.text = [self pickerView:_pickerView titleForRow:row forComponent:component];
    return label;
}

#pragma mark - 获取省ID
+ (NSString *)getPronviceID:(NSString *)pronviceName {
    RegionPickerView *regionPickerView = [[self alloc] init];
    [regionPickerView initDataSource];
    NSString *provinceID = nil;
    for (RegionModel *model in regionPickerView.provinces) {
        if ([pronviceName rangeOfString:model.province].location != NSNotFound ) {
            provinceID = model.provinceId;
            break;
        }
    }
    return provinceID;
}

+ (NSString *)getCityID:(NSString *)cityName {
    RegionPickerView *regionPickerView = [[self alloc] init];
    [regionPickerView initDataSource];
    NSString *cityID = nil;
    for (RegionModel *model in regionPickerView.provinces) {
        for (CityModel *cityModel in model.cities) {
            if ([cityName rangeOfString:cityModel.city].location != NSNotFound ) {
                cityID = cityModel.cityId;
                break;
            }
        }
    }
    
    return cityID;
}

+ (NSString *)getDistrictID:(NSString *)districtName {
    RegionPickerView *regionPickerView = [[self alloc] init];
    [regionPickerView initDataSource];
    NSString *districtID = nil;
    for (RegionModel *model in regionPickerView.provinces) {
        for (CityModel *cityModel in model.cities) {
            for (DistrictModel *districtModel in cityModel.districts) {
                if ([districtName rangeOfString:districtModel.district].location != NSNotFound ) {
                    districtID = districtModel.districtId;
                    break;
                }
            }
        }
    }
    return districtID;
}

+ (NSArray *)getAllDistrictWithCityId:(NSString *)cityId {
    NSMutableArray *allDistricts = [NSMutableArray array];
    RegionPickerView *regionPickerView = [[self alloc] init];
    [regionPickerView initDataSource];
    [regionPickerView.provinces enumerateObjectsUsingBlock:^(RegionModel *provinceModel, NSUInteger idx, BOOL * _Nonnull stop) {
        [provinceModel.cities enumerateObjectsUsingBlock:^(CityModel *cityModel, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([cityId isEqualToString:cityModel.cityId]) {
                [allDistricts addObjectsFromArray:cityModel.districts];
            }
        }];
    }];

    return [allDistricts copy];
}

@end
