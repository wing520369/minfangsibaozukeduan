//
//  RegionPickerView.h
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/4.
//  Copyright © 2016年 吴传荣. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegionModel.h"

typedef NS_ENUM(NSInteger,kRegionPickerViewType) {
    kProCity_RegionType = 902, // 省市
    kAll_RegionType, // 省市区
};

typedef void (^RegionPickerViewBlock) (RegionModel *regionModel, CityModel *cityModel, DistrictModel *districtModel);

/** 省市区pickerView */
@interface RegionPickerView : UIView

+ (void)popProvinceCityPickerViewWithRegionType:(kRegionPickerViewType)regionType CompleteBlock:(RegionPickerViewBlock)block;

/**
 *  获取省ID
 *
 *  @return 省ID
 */
+ (NSString *)getPronviceID:(NSString *)pronviceName;
+ (NSString *)getCityID:(NSString *)cityName;
+ (NSString *)getDistrictID:(NSString *)districtName;

/// 根据城市ID 获取所有的区数据
+ (NSArray *)getAllDistrictWithCityId:(NSString *)cityId;

@end
