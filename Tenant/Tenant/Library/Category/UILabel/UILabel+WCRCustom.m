//
//  UILabel+WCRCustom.m
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "UILabel+WCRCustom.h"

@implementation UILabel (WCRCustom)

#pragma mark - 设置label的一些属性
- (instancetype)initWithFrame:(CGRect)frame TextColor:(UIColor *)textColor Font:(UIFont *)font BackgroundColor:(UIColor *)backgroundColor Text:(NSString *)text {
    if (self = [super initWithFrame:frame]) {
        [self labelWithTextColor:textColor Font:font BackgroundColor:backgroundColor Text:text];
    }
    return self;
}
- (void)labelWithTextColor:(UIColor *)textColor Font:(UIFont *)font BackgroundColor:(UIColor *)backgroundColor Text:(NSString *)text {
    if (textColor) {
        self.textColor = textColor;
    }
    self.font = font;
    if (backgroundColor) {
        self.backgroundColor = backgroundColor;
    }
    else {
        self.backgroundColor = [UIColor clearColor];
    }
    self.lineBreakMode = NSLineBreakByTruncatingTail;
    self.text = text;
}
#pragma mark - 添加左视图
- (void)labelSetLeftViewWithWidth:(CGFloat)width {
    CGFloat x = self.frame.origin.x;
    CGFloat y = self.frame.origin.y;
    UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, self.frame.size.height)];
    leftView.backgroundColor = self.backgroundColor;
    [self.superview addSubview:leftView];
    self.frame = CGRectMake(x + width, y, self.frame.size.width - width, self.frame.size.height);
}
#pragma mark - 首行缩进
- (void)labelSetTextIndent {
    
    NSString *text = self.text;
    NSMutableAttributedString *attributedString1 = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setFirstLineHeadIndent:self.font.pointSize * 2];
    
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [text length])];
    [self setAttributedText:attributedString1];
}

- (CGFloat)labelWithTextColor:(UIColor *)textColor Font:(UIFont *)font BackgroundColor:(UIColor *)bgColor lineSpacing:(CGFloat)lineSpacing isIndent:(BOOL)isIndent Text:(NSString *)text {
    if (text.length == 0 || [text isKindOfClass:[NSNull class]]) {
        return 0;
    }
    self.text = text;
    if (textColor) {
        self.textColor = textColor;
    }
    
    self.font = font;
    
    if (bgColor) {
        self.backgroundColor = bgColor;
    }else {
        self.backgroundColor = [UIColor clearColor];
    }
    
    self.numberOfLines = 0;
    
    NSMutableAttributedString *attributedString1 = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    if (isIndent) {
        //首行缩进
        [paragraphStyle1 setFirstLineHeadIndent:font.pointSize * 2];
    }
    
    [paragraphStyle1 setLineSpacing:lineSpacing];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [text length])];
    [self setAttributedText:attributedString1];
    [self sizeToFit];
    
    CGRect rect = self.frame;
    rect.size.height += lineSpacing * 2;
    self.frame = rect;
    
    return self.frame.size.height;
}

@end
