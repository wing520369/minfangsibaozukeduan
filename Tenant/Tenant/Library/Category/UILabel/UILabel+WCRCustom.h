//
//  UILabel+WCRCustom.h
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (WCRCustom)

#pragma mark - 设置label属性
- (instancetype)initWithFrame:(CGRect)frame TextColor:(UIColor *)textColor Font:(UIFont *)font BackgroundColor:(UIColor *)backgroundColor Text:(NSString *)text ;
/**
 *  设置label属性
 *
 *  @param textColor        内容字体的颜色（nil为默认颜色）
 *  @param font             字体
 *  @param backgroundColor  背景颜色（nil为透明）
 *  @param text             内容
 *
 */
- (void)labelWithTextColor:(UIColor *)textColor
                      Font:(UIFont *)font
           BackgroundColor:(UIColor *)backgroundColor
                      Text:(NSString *)text;

/**
 *  label文字首行缩进
 */
-(void)labelSetTextIndent;

/**
 *  设置label左视图
 *
 *  @param width  左视图的宽度
 *
 */
- (void)labelSetLeftViewWithWidth:(CGFloat)width;

/**
 *  有设置行距，并返回label的实际高度+两倍的行距，设置label的一些常用属性
 *
 *  @param textColor    字体颜色
 *  @param font         字体大小
 *  @param bgColor      背景颜色
 *  @param lineSpacing  行距
 *  @param isIndent     首行是否缩进
 *  @param text         内容
 */
-(CGFloat)labelWithTextColor:(UIColor *)textColor
                        Font:(UIFont *)font
             BackgroundColor:(UIColor *)bgColor
                 lineSpacing:(CGFloat)lineSpacing
                    isIndent:(BOOL)isIndent
                        Text:(NSString *)text
;

@end
