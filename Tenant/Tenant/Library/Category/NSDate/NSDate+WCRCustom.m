//
//  NSDate+WCRCustom.m
//  Landlord
//
//  Created by 吴传荣 on 2017/5/4.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "NSDate+WCRCustom.h"

@implementation NSDate (WCRCustom)

+ (NSDate *)dateFromString:(NSString *)string FormatterType:(NSString *)formatterType {
    if (!formatterType.length) {
        formatterType = @"YYYY-MM-dd";
    }
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:formatterType];
    NSDate *date = [formater dateFromString:string];
    return date;
}

- (NSString *)dateChangeToStringWithFormatterType:(NSString *)formatterType {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if (formatterType.length == 0) {
        formatterType = @"YYYY-MM-dd";
    }
    formatter.dateFormat = formatterType;
    
    NSString *time = [formatter stringFromDate:self];
    return time;
}

- (NSDate *)dateDelayWithUnit:(NSString *)unit Count:(NSInteger)count {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = self;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    // 设置时间为：推(前)后多久
    if ([unit isEqualToString:@"Y"]) {
        [comps setYear:count];
    }
    else if ([unit isEqualToString:@"M"]) {
        [comps setMonth:count];
    }
    else if ([unit isEqualToString:@"D"]) {
        [comps setDay:count];
    }
    else if ([unit isEqualToString:@"H"]) {
        [comps setHour:count];
    }
    else if ([unit isEqualToString:@"m"]) {
        [comps setMinute:count];
    }
    else if ([unit isEqualToString:@"s"]) {
        [comps setSecond:count];
    }
    else {
        return currentDate;
    }
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    return maxDate;
}

- (NSString *)datePartWithUnit:(NSString *)unit {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:self];
    
    NSInteger time = 0.0;
    if ([unit isEqualToString:@"Y"]) {
        time = [dateComponent year];
    }
    else if ([unit isEqualToString:@"M"]) {
        time =  [dateComponent month];
    }
    else if ([unit isEqualToString:@"D"]) {
        time = [dateComponent day];
    }
    else if ([unit isEqualToString:@"H"]) {
        time =  [dateComponent hour];
    }
    else if ([unit isEqualToString:@"m"]) {
        time =  [dateComponent minute];
    }
    else if ([unit isEqualToString:@"s"]) {
        time = [dateComponent second];
    }
    else {
        time = [dateComponent year];
    }
    
    return [NSString stringWithFormat:@"%ld",time];
}

+ (void)dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime CompleteBlock:(void (^) (int day, int hour, int minute, int second))block {
    
    NSDateFormatter *date = [[NSDateFormatter alloc]init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *startD =[date dateFromString:startTime];
    NSDate *endD = [date dateFromString:endTime];
    
    NSTimeInterval start = [startD timeIntervalSince1970] * 1;
    NSTimeInterval end = [endD timeIntervalSince1970] * 1;
    NSTimeInterval value = end - start; // 秒
    int second = value;
    int minute = (int)value / 60;
    int hour = (int)value / 3600;
    int day = (int)value / (24 * 3600);
    
    block(day, hour, minute, second);
}

#pragma mark - 获取指定月份的第一天和最后一天，返回格式：03.01 - 03.31
- (NSString *)dateGetMonthBeginAndEnd {
    
    NSDate *newDate = self;
    double interval = 0;
    NSDate *beginDate = nil;
    NSDate *endDate = nil;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar setFirstWeekday:2]; //设定周一为周首日
    BOOL ok = [calendar rangeOfUnit:NSCalendarUnitMonth startDate:&beginDate interval:&interval forDate:newDate];
    // 分别修改为 NSDayCalendarUnit NSWeekCalendarUnit NSYearCalendarUnit
    if (ok) {
        endDate = [beginDate dateByAddingTimeInterval:interval - 1];
    }else {
        return @"";
    }
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"MM.dd"];
    NSString *beginString = [myDateFormatter stringFromDate:beginDate];
    NSString *endString = [myDateFormatter stringFromDate:endDate];
    NSString *s = [NSString stringWithFormat:@"%@ - %@",beginString,endString];
    
//    NSDate *date = [NSDate date];
//    NSString *thisYear = [date datePartWithUnit:@"Y"];
//    NSString *otherYear = [self datePartWithUnit:@"Y"];
//    if (![thisYear isEqualToString:otherYear]) {
//        s = [NSString stringWithFormat:@"%@.%@ - %@",otherYear,beginString,endString];
//    }
    
    return s;
}

#pragma mark - 将时间字符串转时间戳 : 2017-05-23 12:00:00 YYYY-MM-dd HH:mm:ss
+ (NSTimeInterval)dateGetTimeSpWithDateString:(NSString *)dateString Format:(NSString *)format {
    if (!format) {
        format = @"yyyy-MM-dd HH:mm:ss";
    }
    NSTimeInterval time = 0;
    NSDateFormatter *g_dayDateFormatter = [[NSDateFormatter alloc] init];
    [g_dayDateFormatter setDateFormat:format];
    NSDate *fromdate=[g_dayDateFormatter dateFromString:dateString];
    time = [fromdate timeIntervalSince1970];
    
    return time;
}

#pragma mark - 时间 转换 : 返回 多少天， 星期几 
+ (NSString *)dateChangeTimeFormatWithTimeStamp:(NSTimeInterval)timeStamp showDetail:(BOOL)showDetail {
    // 今天的时间
    NSDate * nowDate = [NSDate date];
    NSDate * msgDate = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    NSString *result = nil;
    NSCalendarUnit components = (NSCalendarUnit)(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday|NSCalendarUnitHour | NSCalendarUnitMinute);
    NSDateComponents *nowDateComponents = [[NSCalendar currentCalendar] components:components fromDate:nowDate];
    NSDateComponents *msgDateComponents = [[NSCalendar currentCalendar] components:components fromDate:msgDate];
    
    NSInteger hour = msgDateComponents.hour;
    NSTimeInterval gapTime = -msgDate.timeIntervalSinceNow;
    double onedayTimeIntervalValue = 24*60*60;  //一天的秒数
    result = [NSDate getPeriodOfTime:hour withMinute:msgDateComponents.minute];
    if (hour > 12) {
        hour = hour - 12;
    }
    if (gapTime < onedayTimeIntervalValue * 3 && gapTime > 0) {
        int gapDay = gapTime/(60*60*24) ;
        if(gapDay == 0) {
            //在24小时内,存在跨天的现象. 判断两个时间是否在同一天内
            BOOL isSameDay = msgDateComponents.day == nowDateComponents.day;
            result = isSameDay ? [[NSString alloc] initWithFormat:@"%@ %zd:%02d",result,hour,(int)msgDateComponents.minute] : (showDetail?  [[NSString alloc] initWithFormat:@"昨天%@ %zd:%02d",result,hour,(int)msgDateComponents.minute] : @"昨天");
        }
        else if(gapDay == 1) {
            result = showDetail?  [[NSString alloc] initWithFormat:@"昨天%@ %zd:%02d",result,hour,(int)msgDateComponents.minute] : @"昨天";
        }
        else if(gapDay == 2) {
            result = showDetail? [[NSString alloc] initWithFormat:@"前天%@ %zd:%02d",result,hour,(int)msgDateComponents.minute] : @"前天";
        }
    }
    //    else if([nowDate timeIntervalSinceDate:msgDate] < 7 * onedayTimeIntervalValue && gapTime > 0) {
    //        // 一周内
    //        NSString *weekDay = [Tools weekdayStr:msgDateComponents.weekday];
    //        result = showDetail? [weekDay stringByAppendingFormat:@"%@ %zd:%02d",result,hour,(int)msgDateComponents.minute] : weekDay;
    //    }
    else { //显示日期
        NSString *day = [NSString stringWithFormat:@"%zd-%zd-%zd", msgDateComponents.year, msgDateComponents.month, msgDateComponents.day];
        result = showDetail? [day stringByAppendingFormat:@" %@ %zd:%02d",result,hour,(int)msgDateComponents.minute]:day;
    }
    return result;
}

+ (NSString *)getPeriodOfTime:(NSInteger)time withMinute:(NSInteger)minute {
    NSInteger totalMin = time *60 + minute;
    NSString *showPeriodOfTime = @"";
    if (totalMin > 0 && totalMin <= 5 * 60)
    {
        showPeriodOfTime = @"凌晨";
    }
    else if (totalMin > 5 * 60 && totalMin < 12 * 60)
    {
        showPeriodOfTime = @"上午";
    }
    else if (totalMin >= 12 * 60 && totalMin <= 18 * 60)
    {
        showPeriodOfTime = @"下午";
    }
    else if ((totalMin > 18 * 60 && totalMin <= (23 * 60 + 59)) || totalMin == 0)
    {
        showPeriodOfTime = @"晚上";
    }
    return showPeriodOfTime;
}

@end
