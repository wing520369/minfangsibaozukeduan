//
//  NSDate+WCRCustom.h
//  Landlord
//
//  Created by 吴传荣 on 2017/5/4.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (WCRCustom)

/// 时间字符串转date  formatterType: YYYY-MM-dd(默认)
+ (NSDate *)dateFromString:(NSString *)string FormatterType:(NSString *)formatterType;

/// 将date 转 string ；  formatterType 返回格式，默认 @"YYYY-MM-dd"
- (NSString *)dateChangeToStringWithFormatterType:(NSString *)formatterType;

/// date往前（后）延时多少 unit:单位（Y M D H m s） count：多久
- (NSDate *)dateDelayWithUnit:(NSString *)unit Count:(NSInteger)count;

/// date拆分成年月日时分秒 unit:单位（Y M D H m s）
- (NSString *)datePartWithUnit:(NSString *)unit;

/**
 *  比较两个时间差
 *
 *  @param startTime    开始时间
 *  @param endTime      结束时间
 *  @param block        返回天，时，分，秒
 *
 */
+ (void)dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime CompleteBlock:(void (^) (int day, int hour, int minute, int second))block;

/// 获取月份的第一天和最后一天，返回格式：03.01 - 03.31，如果月份不是今年，前面加上年份 2016.03.01 - 03.31
- (NSString *)dateGetMonthBeginAndEnd;

/// 将时间字符串转时间戳 : 2017-05-23 12:00:00 YYYY-MM-dd HH:mm:ss(默认)
+ (NSTimeInterval)dateGetTimeSpWithDateString:(NSString *)dateString Format:(NSString *)format;

/// 时间 转换 : 返回 多少天， 星期几
+ (NSString *)dateChangeTimeFormatWithTimeStamp:(NSTimeInterval)timeStamp showDetail:(BOOL)showDetail;

@end
