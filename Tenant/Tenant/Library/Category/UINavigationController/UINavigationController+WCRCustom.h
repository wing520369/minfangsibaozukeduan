//
//  UINavigationController+WCRCustom.h
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (WCRCustom)

/// 设置默认的导航栏样式
- (void)setDefaultStyle;

/// 设置其他的样式（根据需求修改）:
- (void)setOtherStyle;

/// font:           标题字体
/// fontColor:      标题字体颜色
/// barTintColor:   导航栏颜色
/// tintColor:      导航栏按钮文字颜色
- (void)setNavigationFont:(UIFont *)font FontColor:(UIColor *)fontColor BarTintColor:(UIColor *)barTintColor TintColor:(UIColor *)tintColor;

@end
