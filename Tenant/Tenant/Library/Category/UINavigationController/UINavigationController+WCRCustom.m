//
//  UINavigationController+WCRCustom.m
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "UINavigationController+WCRCustom.h"

@implementation UINavigationController (WCRCustom)

- (void)setDefaultStyle {
    UIFont *font = [UIFont CustomBoldFontWithSize:18.0];
    UIColor *fontColor = [UIColor blackColor];
    UIColor *barTintColor = [UIColor whiteColor];
    UIColor *tintColor = [UIColor blackColor];
    [self setNavigationFont:font FontColor:fontColor BarTintColor:barTintColor TintColor:tintColor];
}
- (void)setOtherStyle {
    UIFont *font = [UIFont CustomBoldFontWithSize:18.0];
    UIColor *fontColor = [UIColor whiteColor];
    UIColor *barTintColor = [UIColor colorWithHexString:kMainColor];
    UIColor *tintColor = [UIColor whiteColor];
    [self setNavigationFont:font FontColor:fontColor BarTintColor:barTintColor TintColor:tintColor];
}

/// font:           标题字体
/// fontColor:      标题字体颜色
/// barTintColor:   导航栏颜色
/// tintColor:      导航栏按钮文字颜色
- (void)setNavigationFont:(UIFont *)font FontColor:(UIColor *)fontColor BarTintColor:(UIColor *)barTintColor TintColor:(UIColor *)tintColor {
    // 设置标题字体，颜色
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:fontColor,NSForegroundColorAttributeName,font,NSFontAttributeName, nil];
    [self.navigationBar setTitleTextAttributes:attributes];
    // 设置导航栏颜色
    [self.navigationBar setBarTintColor:barTintColor];
    // 设置导航栏按钮文字颜色
    [self.navigationBar setTintColor:tintColor];
}

@end
