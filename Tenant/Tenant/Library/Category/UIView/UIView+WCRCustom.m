//
//  UIView+WCRCustom.m
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/2.
//  Copyright © 2016年 吴传荣. All rights reserved.
//

#import "UIView+WCRCustom.h"
#import "UIColor+WCRCustom.h"

@implementation UIView (WCRCustom)

#pragma mark - frame size x y
-(void)setX_wcr:(CGFloat)x_wcr{
    CGRect frame = self.frame;
    frame.origin.x = x_wcr;
    self.frame = frame;
}
- (CGFloat)x_wcr{
    return self.frame.origin.x;
}
-(void)setMaxX_wcr:(CGFloat)maxX_wcr{
    self.x_wcr = maxX_wcr - self.frame.size.width;
}
- (CGFloat)maxX_wcr{
    return CGRectGetMaxX(self.frame);
}
-(void)setMaxY_wcr:(CGFloat)maxY_wcr{
    self.y_wcr = maxY_wcr - self.frame.size.height;
}
- (CGFloat)maxY_wcr{
    return CGRectGetMaxY(self.frame);
}
-(void)setY_wcr:(CGFloat)y_wcr{
    CGRect frame = self.frame;
    frame.origin.y = y_wcr;
    self.frame = frame;
}
- (CGFloat)y_wcr{
    return self.frame.origin.y;
}
-(void)setCenterX_wcr:(CGFloat)centerX_wcr{
    CGPoint center = self.center;
    center.x = centerX_wcr;
    self.center = center;
}
- (CGFloat)centerX_wcr{
    return self.center.x;
}
-(void)setCenterY_wcr:(CGFloat)centerY_wcr{
    CGPoint center = self.center;
    center.y = centerY_wcr;
    self.center = center;
}
- (CGFloat)centerY_wcr{
    return self.center.y;
}
-(void)setWidth_wcr:(CGFloat)width_wcr{
    CGRect frame = self.frame;
    frame.size.width = width_wcr;
    self.frame = frame;
}

- (CGFloat)width_wcr{
    return self.frame.size.width;
}
-(void)setHeight_wcr:(CGFloat)height_wcr{
    CGRect frame = self.frame;
    frame.size.height = height_wcr;
    self.frame = frame;
}
- (CGFloat)height_wcr{
    return self.frame.size.height;
}
-(void)setSize_wcr:(CGSize)size_wcr{
    CGRect frame = self.frame;
    frame.size = size_wcr;
    self.frame = frame;
}
- (CGSize)size_wcr{
    return self.frame.size;
}

- (CGFloat)bottom_wcr
{
    return CGRectGetMaxY(self.frame);
}

- (void)setBottom_wcr:(CGFloat)bottom_wcr
{
    CGRect frame = self.frame;
    frame.origin.y = bottom_wcr - frame.size.height;
    self.frame = frame;
}

- (CGFloat)right_wcr
{
    return CGRectGetMaxX(self.frame);
}

- (void)setRight_wcr:(CGFloat)right_wcr
{
    CGRect frame = self.frame;
    frame.origin.x = right_wcr - frame.size.width;
    self.frame = frame;
}


#pragma mark - 在window上创建一个背景View
+ (UIView *)popBackgroudViewInWindowWithColor:(UIColor *)color {
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height = [[UIScreen mainScreen] bounds].size.height;
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    UIColor *bgColor = color;
    if (!bgColor) {
        bgColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    }
    bgView.backgroundColor = bgColor;
    
    [[UIApplication sharedApplication].keyWindow addSubview:bgView];
    
    return bgView;
}
#pragma mark - 设置边框颜色，圆角
- (void)setLayerWithBorderWidth:(CGFloat)borderWidth Corner:(CGFloat)corner BorderColor:(UIColor *)borderColor {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(corner, corner)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
    
    CAShapeLayer *borderLayer=[CAShapeLayer layer];
    borderLayer.path    =   maskPath.CGPath;
    borderLayer.strokeColor    = borderColor.CGColor;
    borderLayer.lineWidth      = borderWidth;
    borderLayer.frame = self.bounds;
    borderLayer.fillColor = [UIColor clearColor].CGColor;
    [self.layer addSublayer:borderLayer];
}
#pragma mark - 设置View的某一个方向的圆角
- (void)setViewCornerByRoundingCorners:(UIRectCorner)corners CornerRadii:(CGSize)cornerRadii {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:cornerRadii];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    maskLayer.masksToBounds = YES;
    self.layer.mask = maskLayer;
}
- (void)setViewCorner:(CGFloat)cornerSize {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(cornerSize, cornerSize)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    maskLayer.masksToBounds = YES;
    self.layer.mask = maskLayer;
}
#pragma mark - 设置圆形
- (void)setRound {
    CGFloat corner = 0.0;
    if (self.frame.size.width == self.frame.size.height) {
        corner = self.frame.size.width / 2.0;
    }
    else {
        corner = self.frame.size.width > self.frame.size.height ? self.frame.size.height / 2.0 : self.frame.size.width / 2.0;
    }
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(corner, corner)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

#pragma mark - 获取当前view所在的控制器
- (UIViewController *)getCurrentViewController {
    for (UIView *next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

#pragma mark - 给view添加一条分割线
+ (instancetype)lineView {
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor lineColor];
    return lineView;
}
- (UIView *)addLineViewPositionType:(PositionType)positionType {
    UIView *lineView = [UIView lineView];
    [self addSubview:lineView];
    
    CGFloat y = 0.0;
    CGFloat height = 1.0;
    CGFloat width = self.frame.size.width;
    CGFloat x = 0;
    if (positionType == kTop_Type) {
        y = 0.0;
    }
    else {
        y = self.frame.size.height - height;
    }
    lineView.frame = CGRectMake(x, y, width, height);
    
    return lineView;
}

@end
