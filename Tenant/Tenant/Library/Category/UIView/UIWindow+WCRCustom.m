//
//  UIWindow+WCRCustom.m
//  CarAccessories
//
//  Created by eliot on 16/9/10.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "UIWindow+WCRCustom.h"

@implementation UIWindow (WCRCustom)

+ (UIViewController *) getVisibleViewControllerFrom:(UIViewController *) vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [UIWindow getVisibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [UIWindow getVisibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    } else {
        if (vc.presentedViewController) {
            return [UIWindow getVisibleViewControllerFrom:vc.presentedViewController];
        } else {
            return vc;
        }
    }
}

- (UIViewController *)topViewController{
    UIViewController *rootViewController = self.rootViewController;
    return [UIWindow getVisibleViewControllerFrom:rootViewController];
}

#pragma mark class method
+ (UIViewController *)topViewController{
    return [[self currentWindow] topViewController];
}

+ (UIWindow *)currentWindow{
    return [[[UIApplication sharedApplication] delegate] window];
}

@end
