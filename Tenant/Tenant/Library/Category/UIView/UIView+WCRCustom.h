//
//  UIView+WCRCustom.h
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/2.
//  Copyright © 2016年 吴传荣. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,PositionType){
    kTop_Type = 200,
    kBottom_Type,
};

@interface UIView (WCRCustom)

@property (nonatomic, assign) CGFloat x_wcr;
@property (nonatomic, assign) CGFloat y_wcr;

@property (nonatomic, assign) CGFloat maxX_wcr;
@property (nonatomic, assign) CGFloat maxY_wcr;

@property (nonatomic, assign) CGFloat centerX_wcr;
@property (nonatomic, assign) CGFloat centerY_wcr;
@property (nonatomic, assign) CGFloat width_wcr;
@property (nonatomic, assign) CGFloat height_wcr;
@property (nonatomic, assign) CGSize  size_wcr;
@property (nonatomic, assign) CGFloat right_wcr;
@property (nonatomic, assign) CGFloat bottom_wcr;

#pragma mark - 在window上创建一个背景View
/**
 *  在window上创建一个背景View
 *
 *  @param color 背景的颜色，如果传nil，就默认为[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2]
 *
 *  @return 背景view
 */
+ (UIView *)popBackgroudViewInWindowWithColor:(UIColor *)color;


#pragma mark - 设置边框颜色，圆角
/**
 *  设置边框颜色，圆角
 *
 *  @param borderWidth 边框的宽度
 *  @param corner      圆角
 *  @param borderColor 边框的颜色
 */
- (void)setLayerWithBorderWidth:(CGFloat)borderWidth Corner:(CGFloat)corner BorderColor:(UIColor *)borderColor;

#pragma mark - 设置View的某一个方向的圆角
/**
 *  设置View的某一个方向的圆角
 *
 *  @param corners     UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param cornerRadii 圆角的大小
 */
- (void)setViewCornerByRoundingCorners:(UIRectCorner)corners CornerRadii:(CGSize)cornerRadii;
/**
 *  设置四个方向的圆角
 *
 *  @param cornerSize 圆角的大小
 */
- (void)setViewCorner:(CGFloat)cornerSize;
#pragma mark - 设置圆形
/**
 *  设置圆形
 */
- (void)setRound;

#pragma mark - 获取当前view所在的控制器
/**
 *  获取当前view所在的控制器
 *
 *  @return 当前控制器
 */
- (UIViewController*)getCurrentViewController;

#pragma mark - 给view添加一条分割线
/**
 *  给view添加一条分割线
 *
 *  @param positionType 线的位置
 *
 *  @return 分割线
 */
- (UIView *)addLineViewPositionType:(PositionType)positionType;


+ (instancetype)lineView;

@end
