//
//  UIWindow+WCRCustom.h
//  CarAccessories
//
//  Created by eliot on 16/9/10.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (WCRCustom)

/**
 * 获取最上层的UIViewController
 * @return topViewController 最上层的UIViewController
 */
+ (UIViewController *)topViewController;

/**
 * 获取当前的UIWindow
 * @return currentWindow 当前使用的UIWindow
 */
+ (UIWindow *)currentWindow;

@end
