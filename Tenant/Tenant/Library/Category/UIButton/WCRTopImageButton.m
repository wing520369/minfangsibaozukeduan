//
//  WCRTopImageButton.m
//  CarAccessories
//
//  Created by eliot on 16/9/20.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRTopImageButton.h"
#import "UIView+WCRCustom.h"
#import "NSString+WCRCustom.h"
#import "UILabel+WCRCustom.h"
#import "UIFont+WCRCustom.h"
#import "UIColor+WCRCustom.h"

@interface WCRTopImageButton ()
@property (nonatomic, assign) CGFloat imageWidth;
@property (nonatomic, assign) CGFloat imageHeight;
@property (nonatomic, assign) CGFloat textHeight; // 文字的实际高度
@property (nonatomic, strong) UILabel *pointLabel;
@property (nonatomic, assign) CGFloat border;
@property (nonatomic, assign) CGFloat pointLabelHeight;
@end

@implementation WCRTopImageButton

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]){
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame ImageSize:(CGSize)size TitleColor:(UIColor *)titleColor Font:(UIFont *)font Title:(NSString *)title {
    if (self = [super initWithFrame:frame]){
        
        self.border = 3.0;
        self.pointLabelHeight = 12.0;
        self.imageWidth = size.width;
        self.imageHeight = size.height;
        self.textHeight = [NSString calculateWithContent:title MaxWidth:frame.size.width Font:font];
        
        self.titleLabel.font = font;
        [self setTitleColor:titleColor forState:0];
        [self setTitle:title forState:0];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        self.adjustsImageWhenHighlighted = NO;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        UIFont *font = [UIFont CustomFontWithSize:9.0];
        UIColor *color = [UIColor orangeColor];
        
        // 右上角数量提示label
        CGFloat x = frame.size.width / 2.0 + self.imageWidth / 2.0 - _pointLabelHeight / 2.0 - 5;
        CGFloat y = (self.frame.size.height - self.imageHeight - self.textHeight - self.border) / 2.0 - 3 ;
        self.pointLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, _pointLabelHeight, _pointLabelHeight)];
        [self.pointLabel labelWithTextColor:[UIColor whiteColor] Font:font BackgroundColor:color Text:nil];
        [self.pointLabel.layer setCornerRadius:_pointLabelHeight / 2.0];
        self.pointLabel.clipsToBounds = YES;
        self.pointLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.pointLabel];
        self.pointLabel.hidden = YES;
        
    }
    return self;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect{
    
    CGFloat imageW = self.imageWidth > 0 ? self.imageWidth : 30.0;
    CGFloat imageH = self.imageHeight > 0 ? self.imageHeight : 30.0;
    CGFloat imageX = (contentRect.size.width - imageW) / 2.0;
    CGFloat imageY = (self.frame.size.height - imageH - self.textHeight - self.border) / 2.0;
    
    return CGRectMake(imageX, imageY, imageW, imageH);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect{

    CGFloat titleX = 0;
    CGFloat titleY = self.imageView.maxY_wcr + self.border;
    CGFloat titleW = self.frame.size.width;
    CGFloat titleH = self.textHeight;
    
    return CGRectMake(titleX, titleY, titleW, titleH);
}

#pragma mark - 右上角数量提示
- (void)WCRTopImageButtonSetRightTopPointWithNumber:(NSInteger)number {
    if (number <= 0) {
        self.pointLabel.hidden = YES;
        return;
    }
    
    self.pointLabel.hidden = NO;
    
    self.pointLabel.text = [NSString stringWithFormat:@"%lu",(long)number];
    if (number > 99) {
        self.pointLabel.text = @"99+";
    }
    
    CGFloat width = [NSString calculateWithFont:self.pointLabel.font Content:self.pointLabel.text];
    self.pointLabel.width_wcr = width > _pointLabelHeight ? width + 5 : _pointLabelHeight;
    
}

@end
