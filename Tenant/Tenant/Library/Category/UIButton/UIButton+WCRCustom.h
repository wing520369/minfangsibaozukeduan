//
//  UIButton+WCRCustom.h
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, JKImagePosition) {
    LXMImagePositionLeft = 0,              //图片在左，文字在右，默认
    LXMImagePositionRight = 1,             //图片在右，文字在左
    LXMImagePositionTop = 2,               //图片在上，文字在下
    LXMImagePositionBottom = 3,            //图片在下，文字在上
};

@interface UIButton (WCRCustom)

/**
 *  利用UIButton的titleEdgeInsets和imageEdgeInsets来实现文字和图片的自由排列
 *  注意：这个方法需要在设置图片和文字之后才可以调用，且button的大小要大于 图片大小+文字大小+spacing
 *
 *  @param spacing 图片和文字的间隔
 */
- (void)setImagePosition:(JKImagePosition)postion spacing:(CGFloat)spacing;

/**
 *  增大按钮的点击范围
 *
 *  @param top    上
 *  @param right  右
 *  @param bottom 下
 *  @param left   左
 */
- (void)setEnlargeEdgeWithTop:(CGFloat)top Right:(CGFloat)right Bottom:(CGFloat) bottom Left:(CGFloat)left;

/**
 *  设置button的属性
 *
 *  @param titleColor 标题颜色
 *  @param bgObject   NSString:背景图片名 UIColor:背景色
 *  @param font       字体
 *  @param title      标题
 */
- (void)buttonWithTitleColor:(UIColor *)titleColor
                  Background:(id)bgObject
                        Font:(UIFont *)font
                       Title:(NSString *)title;

- (void)buttonWithImageName:(NSString *)imageName SelectedImageName:(NSString *)selectedImageName;

- (void)addTarget:(id)target action:(SEL)action;

/// 获取一个按钮（登录，注册等），根据项目需求定制
+ (UIButton *)getButtonWithYpoint:(CGFloat)yPoint Title:(NSString *)title;

@end
