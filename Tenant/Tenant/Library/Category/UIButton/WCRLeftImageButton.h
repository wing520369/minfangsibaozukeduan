//
//  WCRLeftImageButton.h
//  LianJian
//
//  Created by U1KJ on 16/4/13.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 左边图片，右边文字，左对齐 */
@interface WCRLeftImageButton : UIButton


/**
 *  图片在文字左边，左对齐
 *
 *  @param frame      坐标
 *  @param border     图片和文字的间距
 *  @param leftBorder 距离左边的边距
 *  @param imageSize  图片的大小
 *
 *  @return button
 */
- (instancetype)initWithFrame:(CGRect)frame ImageTitleBorder:(CGFloat)border LeftBorder:(CGFloat)leftBorder ImageSize:(CGSize)imageSize;

@end
