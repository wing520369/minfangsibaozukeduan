//
//  WCRLeftImageButton.m
//  LianJian
//
//  Created by U1KJ on 16/4/13.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "WCRLeftImageButton.h"

@interface WCRLeftImageButton ()
@property (nonatomic, assign) CGFloat imageWidth;
@property (nonatomic, assign) CGFloat imageHeight;
@property (nonatomic, assign) CGSize size;      // 图片的大小
@property (nonatomic, assign) CGFloat border;   // 图片和文字的距离
@property (nonatomic, assign) CGFloat leftBorder; // 左边距
@end

@implementation WCRLeftImageButton

- (instancetype)initWithFrame:(CGRect)frame ImageTitleBorder:(CGFloat)border LeftBorder:(CGFloat)leftBorder ImageSize:(CGSize)imageSize {
    if (self = [super initWithFrame:frame]) {
        self.border = border == 0.0 ? 10 : border;
        self.size = imageSize.width == 0.0 ? CGSizeMake(20, 20) : imageSize;
        self.leftBorder = leftBorder;
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        self.adjustsImageWhenHighlighted = NO;
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        self.border = 10;
        self.size = CGSizeMake(20, 20);
        self.leftBorder = 0;
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        self.adjustsImageWhenHighlighted = NO;
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    return self;
}


- (CGRect)imageRectForContentRect:(CGRect)contentRect{
    CGFloat imageX = self.leftBorder;
    CGFloat imageW = self.size.width ;
    CGFloat imageH = self.size.height;
    CGFloat imageY = (self.frame.size.height - imageH) / 2.0;
    return CGRectMake(imageX, imageY, imageW, imageH);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect{
    CGFloat imageW = self.size.width;
    CGFloat titleX = self.leftBorder + imageW + self.border;
    CGFloat titleY = 0;
    CGFloat titleW = self.frame.size.width - titleX;
    CGFloat titleH = self.frame.size.height;
    return CGRectMake(titleX, titleY, titleW, titleH);
}

@end
