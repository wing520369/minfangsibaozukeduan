//
//  WCRTopImageButton.h
//  CarAccessories
//
//  Created by eliot on 16/9/20.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 上图片，下文字，整体居中对齐 */
@interface WCRTopImageButton : UIButton

/**
 *  上图片，下文字，整体居中对齐
 *
 *  @param frame      坐标
 *  @param size       图片的大小
 *  @param titleColor 标题的颜色
 *  @param font       字体的大小
 *  @param title      标题
 *
 *  @return button
 */
- (instancetype)initWithFrame:(CGRect)frame
                    ImageSize:(CGSize)size
                   TitleColor:(UIColor *)titleColor
                         Font:(UIFont *)font
                        Title:(NSString *)title;

/**
 *  右上角数量提示
 *
 *  @param number 数量
 */
- (void)WCRTopImageButtonSetRightTopPointWithNumber:(NSInteger)number;

@end
