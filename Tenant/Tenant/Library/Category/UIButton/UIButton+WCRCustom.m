//
//  UIButton+WCRCustom.m
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "UIButton+WCRCustom.h"
#import "UIImage+WCRCustom.h"
#import "objc/runtime.h"

@implementation UIButton (WCRCustom)

static char topNameKey;
static char rightNameKey;
static char bottomNameKey;
static char leftNameKey;

#pragma mark - 设置button点击范围
- (void)setEnlargeEdgeWithTop:(CGFloat)top Right:(CGFloat)right Bottom:(CGFloat)bottom Left:(CGFloat)left {
    objc_setAssociatedObject(self, &topNameKey, [NSNumber numberWithFloat:top], OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &rightNameKey, [NSNumber numberWithFloat:right], OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &bottomNameKey, [NSNumber numberWithFloat:bottom], OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &leftNameKey, [NSNumber numberWithFloat:left], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CGRect) enlargedRect{
    NSNumber* topEdge = objc_getAssociatedObject(self, &topNameKey);
    NSNumber* rightEdge = objc_getAssociatedObject(self, &rightNameKey);
    NSNumber* bottomEdge = objc_getAssociatedObject(self, &bottomNameKey);
    NSNumber* leftEdge = objc_getAssociatedObject(self, &leftNameKey);
    if (topEdge && rightEdge && bottomEdge && leftEdge){
        return CGRectMake(self.bounds.origin.x - leftEdge.floatValue,
                          self.bounds.origin.y - topEdge.floatValue,
                          self.bounds.size.width + leftEdge.floatValue + rightEdge.floatValue,
                          self.bounds.size.height + topEdge.floatValue + bottomEdge.floatValue);
    }
    else {
        return self.bounds;
    }
}

- (UIView*) hitTest:(CGPoint) point withEvent:(UIEvent*) event{
    CGRect rect = [self enlargedRect];
    if (CGRectEqualToRect(rect, self.bounds)){
        return [super hitTest:point withEvent:event];
    }
    return CGRectContainsPoint(rect, point) ? self : nil;
}

- (void)setImagePosition:(JKImagePosition)postion spacing:(CGFloat)spacing {
    CGFloat imageWith = self.imageView.image.size.width;
    CGFloat imageHeight = self.imageView.image.size.height;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    CGFloat labelWidth = [self.titleLabel.text sizeWithFont:self.titleLabel.font].width;
    CGFloat labelHeight = [self.titleLabel.text sizeWithFont:self.titleLabel.font].height;
#pragma clang diagnostic pop
    
    CGFloat imageOffsetX = (imageWith + labelWidth) / 2 - imageWith / 2;//image中心移动的x距离
    CGFloat imageOffsetY = imageHeight / 2 + spacing / 2;//image中心移动的y距离
    CGFloat labelOffsetX = (imageWith + labelWidth / 2) - (imageWith + labelWidth) / 2;//label中心移动的x距离
    CGFloat labelOffsetY = labelHeight / 2 + spacing / 2;//label中心移动的y距离
    
    switch (postion) {
        case LXMImagePositionLeft:
            self.imageEdgeInsets = UIEdgeInsetsMake(0, -spacing/2, 0, spacing/2);
            self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing/2, 0, -spacing/2);
            break;
            
        case LXMImagePositionRight:{
            if (labelWidth > self.width_wcr - imageWith) {
                labelWidth = self.width_wcr - imageWith;
            }
            self.imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth + spacing/2, 0, -(labelWidth + spacing/2));
            self.titleEdgeInsets = UIEdgeInsetsMake(0, -(imageHeight + spacing/2), 0, imageHeight + spacing/2);
        }
            break;
            
        case LXMImagePositionTop:
            self.imageEdgeInsets = UIEdgeInsetsMake(-imageOffsetY, imageOffsetX, imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(labelOffsetY, -labelOffsetX, -labelOffsetY, labelOffsetX);
            break;
            
        case LXMImagePositionBottom:
            self.imageEdgeInsets = UIEdgeInsetsMake(imageOffsetY, imageOffsetX, -imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(-labelOffsetY, -labelOffsetX, labelOffsetY, labelOffsetX);
            break;
            
        default:
            break;
    }
    
}

#pragma mark - button属性
- (void)buttonWithTitleColor:(UIColor *)titleColor Background:(id)bgObject Font:(UIFont *)font Title:(NSString *)title {
    [self setTitle:title forState:0];
    if (titleColor) {
        [self setTitleColor:titleColor forState:0];
    }
    else {
        [self setTitleColor:[UIColor blackColor] forState:0];
    }
    if ([bgObject isKindOfClass:[NSString class]]) {
        // 图片名
        NSString *bgImageName = bgObject;
        if (bgImageName.length > 0) {
            [self setBackgroundImage:[UIImage imageFileNamed:bgImageName] forState:0];
        }
        else {
            self.backgroundColor = [UIColor clearColor];
        }
    }
    else if ([bgObject isKindOfClass:[UIColor class]]) {
        UIColor *bgColor = bgObject;
        self.backgroundColor = bgColor;
    }
    else {
        self.backgroundColor = [UIColor clearColor];
    }
    self.titleLabel.font = font;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.adjustsImageWhenHighlighted = NO;
}

- (void)buttonWithImageName:(NSString *)imageName SelectedImageName:(NSString *)selectedImageName {
    [self setImage:[UIImage imageFileNamed:imageName] forState:0];
    [self setImage:[UIImage imageFileNamed:selectedImageName] forState:UIControlStateSelected];
}
#pragma mark - button点击事件
- (void)addTarget:(id)target action:(SEL)action{
    [self addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

+ (UIButton *)getButtonWithYpoint:(CGFloat)yPoint Title:(NSString *)title {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(kSpaceX, yPoint, kMainWidth - kSpaceX * 2, 40);
    [button buttonWithTitleColor:[UIColor whiteColor] Background:[UIColor colorWithHexString:kMainColor] Font:[UIFont CustomFontWithSize:15.0] Title:title];
    [button setViewCorner:button.height_wcr / 2.0];

    return button;
}

@end
