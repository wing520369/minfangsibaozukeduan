//
//  UIImageView+WCRCustom.m
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "UIImageView+WCRCustom.h"
#import "UIImage+WCRCustom.h"
#import "UIView+WCRCustom.h"
#import "UIImageView+WebCache.h"

@implementation UIImageView (WCRCustom)

- (void)imageViewWithImageName:(NSString *)imageName {
    self.backgroundColor = [UIColor clearColor];
    self.opaque = YES;
    self.image = [UIImage imageFileNamed:imageName];
    self.contentMode = UIViewContentModeScaleAspectFill;
    self.clipsToBounds = YES;
}


- (void)setContentModeToScaleAspectFill {
    self.backgroundColor = [UIColor clearColor];
    self.opaque = YES;
    self.contentMode = UIViewContentModeScaleAspectFill;
    self.clipsToBounds = YES;
}

- (void)loadImageWithUrl:(NSURL *)url placeholderImage:(UIImage *)placeholder{
    [self setContentModeToScaleAspectFill];
    [self sd_setImageWithURL:url placeholderImage:placeholder];
}

@end
