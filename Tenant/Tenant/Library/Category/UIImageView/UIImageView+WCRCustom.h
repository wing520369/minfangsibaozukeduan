//
//  UIImageView+WCRCustom.h
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (WCRCustom)

/**
 *  设置imageView的image：等比填充
 *
 *  @param imageName 图片名
 */
- (void)imageViewWithImageName:(NSString*)imageName;

- (void)setContentModeToScaleAspectFill;

/** 加载图片 */
- (void)loadImageWithUrl:(NSURL *)url placeholderImage:(UIImage *)placeholder;

@end
