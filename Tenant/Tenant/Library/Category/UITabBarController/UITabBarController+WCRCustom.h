//
//  UITabBarController+WCRCustom.h
//  LianJian
//
//  Created by 吴前途 on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (WCRCustom)

/** 设置tabBarController可以左右滑动 */
- (void)setOpenGestury;

@end
