//
//  UITextView+WCRCustom.m
//  LianJian
//
//  Created by 吴前途 on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "UITextView+WCRCustom.h"
#import "NPTip.h"
#import <objc/runtime.h>

@implementation UITextView (WCRCustom)

#pragma mark - 在键盘上方添加完成栏
- (CGFloat)addDoneButtonCloseKeyBoard {
    
    CGFloat barHeight = 40;
    UIToolbar *toolBar = [[UIToolbar alloc] init];
    toolBar.bounds = CGRectMake(0, 0, kMainWidth, barHeight);
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    // 按添加顺序显示
    toolBar.items = @[flexible, doneBtn];
    // 添加工具条
    self.inputAccessoryView = toolBar;
    
    return barHeight;
}
- (void)doneClick {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (void)textViewlimitInputWithNumber:(NSInteger)number AlertMessage:(NSString *)alertMessage {
    NSInteger length = number;
    NSString *toBeString = self.text;
    NSString *lang = [self.textInputMode primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"] || [lang isEqualToString:@"zh-Hant"]) {
        // 简体中文输入，包括简体拼音，简体五笔，简体手写(zh-Hans)
        // 繁体中文输入，包括繁体拼音，繁体五笔，繁体手写(zh-Hant)
        UITextRange *selectedRange = [self markedTextRange];
        // 获取高亮部分（联想部分）
        UITextPosition *position = [self positionFromPosition:selectedRange.start offset:0];
        // 没有联想，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > length) {
                self.text = [toBeString substringToIndex:length];
                [self resignFirstResponder];
                [NPTip showTip:alertMessage];
            }
        }
        // 有联想，则暂不对联想的文字进行统计
        else {
            
        }
    }
    // 中文输入法以外的直接对其统计限制即可，暂时不考虑其他语种情况
    else {
        if (toBeString.length > length) {
            self.text = [toBeString substringToIndex:length];
            [self resignFirstResponder];
            [NPTip showTip:alertMessage];
        }
    }
}

#pragma mark - 设置placeholder
/*
 OBJC_ASSOCIATION_ASSIGN;            //assign策略
 OBJC_ASSOCIATION_COPY_NONATOMIC;    //copy策略
 OBJC_ASSOCIATION_RETAIN_NONATOMIC;  // retain策略
 
 OBJC_ASSOCIATION_RETAIN;
 OBJC_ASSOCIATION_COPY;
 */
/*
 * id object 给哪个对象的属性赋值
 const void *key 属性对应的key
 id value  设置属性值为value
 objc_AssociationPolicy policy  使用的策略，是一个枚举值，和copy，retain，assign是一样的，手机开发一般都选择NONATOMIC
 objc_setAssociatedObject(id object, const void *key, id value, objc_AssociationPolicy policy);
 */
/**
static char *placeholderKey = "placeholderKey";

- (void)setPlaceholder:(NSString *)placeholder {
    objc_setAssociatedObject(self, placeholderKey, placeholder, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (NSString *)placeholder {
    return objc_getAssociatedObject(self, placeholderKey);
}
- (void)textViewSetPlaceholder:(NSString *)placeholder {
    
    if ([placeholder isEqualToString:self.placeholder]) {
        return;
    }
    self.placeholder = placeholder;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveTextDidChangeNotification:)
                                                 name:UITextViewTextDidChangeNotification
                                               object:self];
}
- (void)didReceiveTextDidChangeNotification:(NSNotification *)noti {
    [self setNeedsDisplay];
}
- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];
    if (self.text.length == 0 && self.placeholder) {
        CGRect placeHolderRect = CGRectMake(10.0f,
                                            7.0f,
                                            rect.size.width,
                                            rect.size.height);
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        paragraphStyle.alignment = self.textAlignment;
        
        [self.placeholder drawInRect:placeHolderRect
                 withAttributes:@{ NSFontAttributeName : self.font,
                                   NSForegroundColorAttributeName : [UIColor grayColor],
                                   NSParagraphStyleAttributeName : paragraphStyle }];
    }
}
- (void)dealloc {
    self.placeholder = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:self];
}
*/

@end
