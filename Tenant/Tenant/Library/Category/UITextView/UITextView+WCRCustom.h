//
//  UITextView+WCRCustom.h
//  LianJian
//
//  Created by 吴前途 on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (WCRCustom)

#pragma mark - 在键盘上方添加完成栏，返回栏的高度
- (CGFloat)addDoneButtonCloseKeyBoard;

#pragma mark - 限制最大的输入字数
/**
 *  限制最大的输入字数 , textView的代理方法textViewDidChange:里实现此方法
 *
 *  @param number       限制的长度
 *  @param alertMessage 提示的信息
 */
- (void)textViewlimitInputWithNumber:(NSInteger)number AlertMessage:(NSString *)alertMessage;

@end
