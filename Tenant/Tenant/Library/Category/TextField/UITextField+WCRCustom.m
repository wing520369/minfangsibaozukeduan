//
//  UITextField+WCRCustom.m
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "UITextField+WCRCustom.h"
//#import "WCRProjectToolModel.h"
#import "UIImage+WCRCustom.h"
#import "NPTip.h"

@implementation UITextField (WCRCustom)

#pragma mark - textfield的属性
- (void)textFieldWithPlaceholder:(NSString *)placeholder Font:(UIFont *)font {
    self.placeholder = placeholder;
    self.font = font;
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
}
#pragma mark - textfield键盘类型，密码框
- (void)textFieldWithSecureTextEntry:(BOOL)isSecure ReturnKey:(UIReturnKeyType)returnKey keyboardType:(UIKeyboardType)keyboardType {
    // 是否安全模式（密码）
    self.secureTextEntry = isSecure;
    if (returnKey) {
        self.returnKeyType = returnKey;
    }
    if (keyboardType) {
        self.keyboardType = keyboardType;
    }
}
#pragma mark - textfield的属性：有左视图为图片的输入框
- (void)textFieldWithFont:(UIFont *)font
              Placeholder:(NSString *)placeholder
            LeftImageName:(NSString *)imageName
               ImageWidth:(CGFloat)imageWidth
                   Border:(CGFloat)border {
    self.placeholder = placeholder;
    self.font = font;
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
    // 左视图
    CGFloat width = 15;
    if (imageName) {
        UIImage *leftImage = [UIImage imageNamed:imageName];
        width = imageWidth == 0 ? leftImage.size.width : imageWidth;
        CGFloat height = width * (leftImage.size.height / leftImage.size.width);
        UIImageView *leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(border, (self.height_wcr - height) / 2.0, width, height)];
        leftImageView.contentMode = UIViewContentModeScaleAspectFill;
        leftImageView.image = leftImage;
        
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width + border * 2, self.height_wcr)];
        leftView.backgroundColor = [UIColor clearColor];
        [leftView addSubview:leftImageView];
        
        self.leftView = leftView;
        self.leftViewMode = UITextFieldViewModeAlways;
    }
    else {
        UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width + border * 2, self.frame.size.height)];
        self.leftView = leftView;
        self.leftViewMode = UITextFieldViewModeAlways;
    }
}
#pragma mark - textfield的属性：有左视图为标签的输入框
- (void)textFieldWithFont:(UIFont *)font PlaceHolder:(NSString *)placeholder LeftLabelText:(NSString *)leftText Border:(CGFloat)border {
    self.placeholder = placeholder;
    self.font = font;
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    CGFloat leftViewWidth = leftText.length * font.pointSize;
    if (leftViewWidth == 0) {
        leftViewWidth = kSpaceX;
    }
    // 左视图
    UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, leftViewWidth + border * 2, self.frame.size.height)];
    //标签
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(border, 0, leftViewWidth, self.frame.size.height)];
    label.font = font;
    [leftView addSubview:label];
    label.text = leftText;
    
    self.leftView = leftView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void)textFieldAddRightImage:(NSString *)imageName Width:(CGFloat)imageWidth RightBorder:(CGFloat)border {
    
    if (!imageName.length) {
        // 没有图片
        CGFloat width = imageWidth;
        CGFloat height = self.frame.size.height;
        
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        rightView.backgroundColor = [UIColor clearColor];
        
        self.rightView = rightView;
    }
    else {
        UIImage *rightImage = [UIImage imageFileNamed:imageName];
        CGFloat width = imageWidth;
        if (width == 0) {
            width = rightImage.size.width;
        }
        CGFloat height = width * (rightImage.size.height / rightImage.size.width);
        UIImageView *rightImageView = [[UIImageView alloc]initWithImage:rightImage];
        rightImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        if (border > 0) {
            // 右边间距
            UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width + border, self.height_wcr)];
            rightImageView.frame = CGRectMake(0, (self.height_wcr - height) / 2.0, width, height);
            [rightView addSubview:rightImageView];
            self.rightView = rightView;
        }
        else {
            rightImageView.frame = CGRectMake(0, (self.height_wcr - height) / 2.0, width, height);
            self.rightView = rightImageView;
        }
    }
    
    self.rightViewMode = UITextFieldViewModeAlways;
}

#pragma mark - 设置水印的颜色
- (void)setPlaceholderColor:(UIColor *)color{
    if (color) {
        [self setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    }
}
#pragma mark - 设置水印的字体
- (void)setPlaceholderFont:(UIFont *)font{
    if (font) {
        [self setValue:font forKeyPath:@"_placeholderLabel.font"];
    }
}

#pragma mark - 在键盘上方添加完成栏
- (CGFloat)addDoneButtonCloseKeyBoard {
    
    CGFloat barHeight = 40;
    UIToolbar *toolBar = [[UIToolbar alloc] init];
    toolBar.bounds = CGRectMake(0, 0, kMainWidth, barHeight);
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    // 按添加顺序显示
    toolBar.items = @[flexible, doneBtn];
    // 添加工具条
    self.inputAccessoryView = toolBar;
    
    return barHeight;
}
- (void)doneClick {
    UIWindow *window = [[[UIApplication sharedApplication] delegate]window];
    [window endEditing:YES];
}
- (void)textFieldlimitInputWithNumber:(NSInteger)number AlertMessage:(NSString *)alertMessage {
    NSInteger length = number;
    NSString *toBeString = self.text;
    NSString *lang = [self.textInputMode primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"] || [lang isEqualToString:@"zh-Hant"]) {
        // 简体中文输入，包括简体拼音，简体五笔，简体手写(zh-Hans)
        // 繁体中文输入，包括繁体拼音，繁体五笔，繁体手写(zh-Hant)
        UITextRange *selectedRange = [self markedTextRange];
        // 获取高亮部分（联想部分）
        UITextPosition *position = [self positionFromPosition:selectedRange.start offset:0];
        // 没有联想，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > length) {
                self.text = [toBeString substringToIndex:length];
                [self resignFirstResponder];
                [NPTip showTip:alertMessage];
            }
        }
        // 有联想，则暂不对联想的文字进行统计
        else {
            
        }
    }
    // 中文输入法以外的直接对其统计限制即可，暂时不考虑其他语种情况
    else {
        if (toBeString.length > length) {
            self.text = [toBeString substringToIndex:length];
            [self resignFirstResponder];
            [NPTip showTip:alertMessage];
        }
    }
}

@end
