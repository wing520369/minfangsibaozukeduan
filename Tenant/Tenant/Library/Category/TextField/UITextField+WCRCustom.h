//
//  UITextField+WCRCustom.h
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (WCRCustom)

/**
 *  设置textfield的属性
 *
 *  @param placeholder 水印
 *  @param font        字体
 */
- (void)textFieldWithPlaceholder:(NSString *)placeholder
                            Font:(UIFont *)font;

/**
 *  textfield的属性:密码框
 *
 *  @param isSecure     是否安全模式（密码）
 *  @param returnKey    返回键类型
 *  @param keyboardType 键盘类型
 */
- (void)textFieldWithSecureTextEntry:(BOOL)isSecure
                           ReturnKey:(UIReturnKeyType)returnKey
                        keyboardType:(UIKeyboardType)keyboardType;

/**
 *  textfield的属性，设置左视图图片
 *
 *  @param placeholder  水印
 *  @param font         字体
 *  @param imageName    左视图图片名
 *  @param imageWidth   图片宽度
 *  @param border       图片左右的边距
 */
- (void)textFieldWithFont:(UIFont *)font
              Placeholder:(NSString *)placeholder
            LeftImageName:(NSString *)imageName
               ImageWidth:(CGFloat)imageWidth
                   Border:(CGFloat)border;

/**
 *  添加右视图
 *
 *  @param imageName    图片名
 *  @param imageWidth   图片的宽度，0即为图片的实际宽度
 *  @param border       距离最右边的间距
 */
- (void)textFieldAddRightImage:(NSString *)imageName Width:(CGFloat)imageWidth RightBorder:(CGFloat)border;

/**
 *  textfield的属性，设置左视图文字
 *
 *  @param placeholder  水印
 *  @param font         字体
 *  @param leftText     左视图文字
 *  @param border       文字左右的边距
 */
- (void)textFieldWithFont:(UIFont *)font
              PlaceHolder:(NSString *)placeholder
            LeftLabelText:(NSString *)leftText
                   Border:(CGFloat)border;

/**
 *  水印字体颜色
 *
 *  @param color 颜色
 */
- (void)setPlaceholderColor:(UIColor *)color;

/**
 *  水印字体
 *
 *  @param font 字体
 */
- (void)setPlaceholderFont:(UIFont *)font;

#pragma mark - 在键盘上方添加完成栏，返回栏的高度
- (CGFloat)addDoneButtonCloseKeyBoard;

#pragma mark - 限制输入的字数
/**
 *  限制输入的字数，textFieldDidChange:里实现此方法
 *
 *  @param number       最大的长度
 *  @param alertMessage 提示信息
 */
- (void)textFieldlimitInputWithNumber:(NSInteger)number AlertMessage:(NSString *)alertMessage;

@end
