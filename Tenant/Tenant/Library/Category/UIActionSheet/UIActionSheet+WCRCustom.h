//
//  UIActionSheet+WCRCustom.h
//  CarAccessories
//
//  Created by 吴传荣 on 16/10/4.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ActionSheetBlock)(NSInteger);

@interface UIActionSheet (WCRCustom)<UIActionSheetDelegate>
- (void)showInView: (UIView *)view completionHandler: (ActionSheetBlock)block;
- (void)clearActionBlock;
@end
