//
//  UIViewController+WCRCustom.h
//  LianJian
//
//  Created by 吴前途 on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (WCRCustom)

/**
 *  根据图片名 添加左边导航item
 *
 *  @param imageName 图片名
 *  @param width     宽 默认填0
 *
 *  @return 按钮
 */
- (UIButton *)addLeftBarButtonItemImageName:(NSString *)imageName width:(CGFloat)width;

/**
 *  根据标题 添加左边导航item
 *
 *  @param title 标题
 *
 *  @return 按钮
 */
- (UIButton *)addLeftBarButtonItemTitle:(NSString *)title;

/**
 *  左边导航itme 响应
 *
 *  @param button button
 */
- (void)leftBarButtonItemAction:(UIButton *)button;

/**
 *  根据图片名 添加右边导航item
 *
 *  @param imageName 图片名
 *  @param width     宽 默认填0
 *
 *  @return 按钮
 */
- (UIButton *)addRightBarButtonItemImageName:(NSString *)imageName width:(CGFloat)width;;

/**
 *  根据标题 添加右边导航item
 *
 *  @param title 标题
 *
 *  @return 按钮
 */
- (UIButton *)addRightBarButtonItemTitle:(NSString *)title;

/**
 *  右边导航itme 响应
 *
 *  @param button 按钮
 */
- (void)rightBarButtonItemAction:(UIButton *)button;

#pragma mark 导航设置 获取导航堆栈内对应的UIViewController对象
/**
 *	@brief	获取导航堆栈内对应的UIViewController对象
 *
 *	@param 	navControllers 	导航堆栈
 *	@param 	outViewController 	需要获取的UIViewController
 *
 */
+ (UIViewController *)FindSpecificViewController:(NSArray *)navControllers outViewController:(Class)outViewController;

@end
