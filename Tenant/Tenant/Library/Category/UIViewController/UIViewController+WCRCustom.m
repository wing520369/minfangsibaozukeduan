//
//  UIViewController+WCRCustom.m
//  LianJian
//
//  Created by 吴前途 on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "UIViewController+WCRCustom.h"
#import "UIImage+WCRCustom.h"
#import "UIButton+WCRCustom.h"
#import "UIFont+WCRCustom.h"

@implementation UIViewController (WCRCustom)

#pragma mark - 左边buttonItem
- (UIButton *)addLeftBarButtonItemImageName:(NSString *)imageName width:(CGFloat)width {
    UIImage *image = [UIImage imageNamed:imageName];
    if (!width) {
        width = image.size.width;
    }
    float height = [image InProportionAtWidth:width];
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    [button setBackgroundImage:image forState:0];
    [button.imageView setContentMode:UIViewContentModeScaleAspectFill];
    [button addTarget:self action:@selector(leftBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    button.adjustsImageWhenHighlighted = NO;
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    return button;
}
- (UIButton *)addLeftBarButtonItemTitle:(NSString *)title {
    UIButton *leftBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIFont *font = [UIFont CustomFontWithSize:15.0];
    [leftBarButton buttonWithTitleColor:self.navigationController.navigationBar.tintColor Background:nil Font:font Title:title];
    leftBarButton.frame = CGRectMake(0, 0, [title sizeWithAttributes:@{NSFontAttributeName:font}].width, 44);
    leftBarButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [leftBarButton addTarget:self action:@selector(leftBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftBarButton];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    return leftBarButton;
}
- (void)leftBarButtonItemAction:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 右边buttonItem
- (UIButton *)addRightBarButtonItemImageName:(NSString *)imageName width:(CGFloat)width{
    UIImage *image = [UIImage imageNamed:imageName];
    if (!width) {
        width = image.size.width;
    }
    float height = [image InProportionAtWidth:width];
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    [button setBackgroundImage:image forState:0];
    [button addTarget:self action:@selector(rightBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    [button.imageView setContentMode:UIViewContentModeScaleAspectFill];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    return button;
}

- (UIButton *)addRightBarButtonItemTitle:(NSString *)title{
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIFont *font = [UIFont CustomFontWithSize:15.0];
    [rightButton buttonWithTitleColor:self.navigationController.navigationBar.tintColor Background:nil Font:font Title:title];
    rightButton.frame = CGRectMake(0, 0, [title sizeWithAttributes:@{NSFontAttributeName:font}].width, 44);
    rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [rightButton addTarget:self action:@selector(rightBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = item;
    
    return rightButton;
}
- (void)rightBarButtonItemAction:(UIButton *)button{
    
}
#pragma mark - 获取导航堆栈内对应的UIViewController对象
+(UIViewController *)FindSpecificViewController:(NSArray *)navControllers outViewController:(Class)outViewController{
    if(navControllers == nil){
        return nil;
    }
    __block UIViewController *outController = nil;
    [navControllers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIViewController *controoler = obj;
        if([controoler isKindOfClass:outViewController]){
            outController = controoler;
            *stop = YES;
        }
    }];
    return outController;
}
@end
