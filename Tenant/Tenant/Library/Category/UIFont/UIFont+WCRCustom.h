//
//  UIFont+WCRCustom.h
//  LianJian
//
//  Created by 吴前途 on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 ** 自定义字体 : 需要在plist文件设置自定义字体
 */
@interface UIFont (WCRCustom)

/**
 *  创建字体
 *
 *  @param size 字体大小
 *
 *  @return 字体
 */
+ (instancetype)CustomFontWithSize:(CGFloat)size;

/**
 *  创建粗体字体字体
 *
 *  @param size 字体大小
 *
 *  @return 字体
 */
+ (instancetype)CustomBoldFontWithSize:(CGFloat)size;

@end
