//
//  UIFont+WCRCustom.m
//  LianJian
//
//  Created by 吴前途 on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

/*
 ** 需要在plist文件设置平方字体
 */
#import "UIFont+WCRCustom.h"

@implementation UIFont (WCRCustom)

+ (instancetype)CustomFontWithSize:(CGFloat)size{
    return [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:size];
}

+ (instancetype)CustomBoldFontWithSize:(CGFloat)size{
    return [UIFont fontWithName:@"SanFranciscoText-Semibold" size:size];
}

@end
