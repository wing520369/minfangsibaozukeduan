//
//  UIColor+WCRCustom.h
//  LianJian
//
//  Created by 吴前途 on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (WCRCustom)


+ (UIColor *)colorWithRGB:(CGFloat)R G:(CGFloat) G B:(CGFloat) B;

/**
 *  将颜色编码 字符串转为颜色
 *
 *  @param color 颜色字符串
 *  @param alpha 透明度
 *
 *  @return 颜色
 */
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;
+ (UIColor *)colorWithHexString:(NSString *)color;
/**
 *  默认的线条颜色
 *
 *  @return color：[UIColor colorWithRed:213 / 255.0 green:213 / 255.0 blue:213 / 255.0 alpha:1.0]
 */
+ (UIColor *)lineColor;

@end
