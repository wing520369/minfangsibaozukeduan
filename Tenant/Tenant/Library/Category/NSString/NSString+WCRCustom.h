//
//  NSString+WCRCustom.h
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (WCRCustom)

#pragma mark - 判断字符串是否为null @“”
/**
 *	@brief	字符空值判断
 *
 *	@return	是否为NUll
 */
- (BOOL)checkBlankString;
#pragma mark - 判断是否全部都是数字
/**
 *  字符串是否全部都是数字
 *
 *  @return 是否为数字
 */
- (BOOL)isPureNumandCharacters;
#pragma mark - 文件夹 目录相关
/*
 *  document根文件夹
 */
+ (NSString *)documentFolder;

/*
 *  caches根文件夹
 */
+ (NSString *)cachesFolder;

/**
 *  生成子文件夹
 *
 *  如果子文件夹不存在，则直接创建；如果已经存在，则直接返回
 *
 *  @param subFolder 子文件夹名
 *
 *  @return 文件夹路径
 */
- (NSString *)createSubFolder:(NSString *)subFolder;

#pragma mark - 格式化html5字符串
/**
 *  将html 转为适合屏幕
 *
 *  @param htmlString 字符串
 *
 *  @return 新的字符串
 */
+ (NSString *)formatHTMLString:(NSString *)htmlString MaxWidth:(CGFloat)width;

#pragma mark - MD5加密
/**
 *  把字符串MD5加密
 *
 *  @param inPutText 要加密的字符
 *
 *  @return 加密后的字符串
 */
+(NSString *)md5:(NSString *)inPutText;

/**
 *  把字符串MD5加密
 *
 *  @param inPutText 要加密的字符
 *
 *  @return 加密后(大写)的字符串
 */
+(NSString *)md5UP:(NSString *)inPutText;

#pragma mark - SHA1加密
+ (NSString *)sha1:(NSString *)inPutText;


#pragma mark - 根据给定的宽度计算字符串的实际高度
/**
 *  根据给定的宽度计算字符串的实际高度
 *
 *  @param content 文字内容
 *  @param width   最大的宽度
 *  @param font    字体
 *
 *  @return 高度
 */
+ (CGFloat)calculateWithContent:(NSString *)content MaxWidth:(CGFloat)width Font:(UIFont *)font;

#pragma mark - 计算文字的宽度，单行文字
/**
 *  计算文字的宽度，单行文字
 *
 *  @param content 文字内容
 *  @param font    字体
 *
 *  @return 文字的宽度
 */
+ (CGFloat)calculateWithFont:(UIFont *)font Content:(NSString *)content;

#pragma mark - 字典序列化成字符串
/// 将字典数组转成json字符串
+ (NSString *)stringWithJsonbject:(id)obj;
/// 将json字符串转成字典或数组
- (id)jsonObject;
+ (id)jsonObjectWithFileName:(NSString *)fileName;

+ (NSString *)moneyFormString:(NSString *)money;
+ (NSString *)stringFromMoneyFloatValue:(CGFloat)value;

/// 格式化时间字符串(2017-05-06 12:06:12)，返回2017-05-06
- (NSString *)formatAllDateString;

@end
