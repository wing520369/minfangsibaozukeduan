//
//  NSString+WCRCustom.m
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "NSString+WCRCustom.h"
#import "CommonCrypto/CommonDigest.h"

@implementation NSString (WCRCustom)

#pragma mark - 判断字符串是否为null @“”
- (BOOL)checkBlankString {
    if (nil == self) {
        return YES;
    }
    NSString *temp = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (temp == nil) {
        return YES;
    }
    if (temp == NULL) {
        return YES;
    }
    if ([temp isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if(temp.length < 1){
        return YES;
    }
    return NO;
}
/// 方法一、判断是否为纯数字
- (BOOL)checkPureInt {
    
    NSScanner* scan = [NSScanner scannerWithString:self];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}
/// 方法二、判断是否为纯数字
- (BOOL)isPureNumandCharacters {
    NSString *string = self;
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(string.length > 0) {
        return NO;
    }
    return YES;
}

#pragma mark - document根文件夹
+ (NSString *)documentFolder {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - caches根文件夹
+ (NSString *)cachesFolder {
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - 生成子文件夹
- (NSString *)createSubFolder:(NSString *)subFolder {
    NSString *subFolderPath=[NSString stringWithFormat:@"%@/%@",self,subFolder];
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:subFolderPath isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) ){
        [fileManager createDirectoryAtPath:subFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return subFolderPath;
}

#pragma mark - 格式化html字符串
+ (NSString *)formatHTMLString:(NSString *)htmlString MaxWidth:(CGFloat)width {
    
    if (htmlString.length == 0) {
        return nil;
    }
    
    NSString *firstString = @"<html><head><style>*{margin-left:0;margin-right:0;padding-left:0;padding-right:0;}img{width:100%!important;height:auto !important;margin:0 !important;padding:0 !important;}p{padding:0 !important;margin-left:0 !important;margin-right:0 !important;}p *{padding:0 !important;margin:0 !important;position:relative !important;}</style></head><body><div id=\"small\" class=\"main\"style=\"color:#000000\" style=\"width:device-width\" type = \"application/x-shockwave-flash\">";
    htmlString = [NSString stringWithFormat:@"%@%@</div></body><html>",firstString,htmlString];
     
    return htmlString;
}
#pragma mark - md5
+ (NSString *)md5:(NSString *)inPutText {
    const char *cStr = [inPutText UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (unsigned int)strlen(cStr), result);
    
    return [[NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
             result[0], result[1], result[2], result[3],
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]
             ] lowercaseString];
}

+ (NSString *)md5UP:(NSString *)inPutText {
    const char *cStr = [inPutText UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (unsigned int)strlen(cStr), result);
    
    return [[NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
             result[0], result[1], result[2], result[3],
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]
             ] uppercaseString];
}


#pragma mark - sha1
+ (NSString *)sha1:(NSString *)inPutText {
    const char *cstr = [inPutText cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:inPutText.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (unsigned int)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

#pragma mark - 计算字符串的高度
+ (CGFloat)calculateWithContent:(NSString *)content MaxWidth:(CGFloat)width Font:(UIFont *)font {
    UILabel *label = [[UILabel alloc]init];
    label.font = font;
    label.text = content;
    label.numberOfLines = 0;
    CGSize size = [label sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    return size.height;
}
#pragma mark - 计算当行内容的宽度
+ (CGFloat)calculateWithFont:(UIFont *)font Content:(NSString *)content {
    return [content sizeWithAttributes:@{NSFontAttributeName:font}].width;
}

#pragma mark - json数据字典转换
// 字典序列化成字符串
+ (NSString *)stringWithJsonbject:(id)obj {
    if([NSJSONSerialization isValidJSONObject:obj]){
        NSError *error;
        NSData *dictionaryData = [NSJSONSerialization dataWithJSONObject:obj options:NSJSONWritingPrettyPrinted error:&error];
        return [[NSString alloc]initWithData:dictionaryData encoding:NSUTF8StringEncoding];
    }
    return nil;
}

// json反序列化方法
- (id)jsonObject {
    if(self.length){
        NSError *error;
        NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
        id obj = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        return obj;
    }
    return nil;
}

#pragma mark - 读取本地的txt格式json数据 并解析返回字典
+ (id)jsonObjectWithFileName:(NSString *)fileName{
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"txt"];
    NSString * jsonString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    return [jsonString jsonObject];
}

+ (NSString *)moneyFormString:(NSString *)money{
    
    NSString *moneyStr = nil;
    if ([money isEqualToString:@""] || [money isEqualToString:@"."]) {
        moneyStr = @"0.00";
        return moneyStr;
    }
    
    //
    NSRange decimalRange = [money rangeOfString:@"."];
    if(decimalRange.location != NSNotFound)
    {
        //假如有小数点，小数点后面最多两位
        if (decimalRange.location == 0) {
            moneyStr = [NSString stringWithFormat:@"0%@",money];
        }
        else if (money.length - decimalRange.location == 1){
            moneyStr = [NSString stringWithFormat:@"%@00",money];
        }
        else if (money.length - decimalRange.location == 2){
            moneyStr = [NSString stringWithFormat:@"%@0",money];
        }
        else{
            moneyStr = money;
        }
    }
    else{
        moneyStr = [NSString stringWithFormat:@"%@.00",money];
    }
    return moneyStr;
}

+ (NSString *)stringFromMoneyFloatValue:(CGFloat)value {
    if (value == 0.0) {
        return @"";
    }
    else {
        return [NSString stringWithFormat:@"%.2f",value];
    }
}

/// 格式化时间字符串(2017-05-06 12:06:12)，返回2017-05-06
- (NSString *)formatAllDateString {
    NSString *string = self;
    if (string.length == 19) {
        string = [string substringWithRange:NSMakeRange(0, string.length - 9)];
    }
    return string;
}

@end
