//
//  NSMutableAttributedString+WCRCustom.h
//  LianJian
//
//  Created by U1KJ on 16/4/13.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableAttributedString (WCRCustom)

/*
 ** 设置某个范围的颜色，使用NSMakeRange，不使用text去获取range
 */
- (void)setColor:(UIColor *)color range:(NSRange)range;
/*
 ** 设置某个范围的字体， 使用NSMakeRange，不使用text去获取range
 */
- (void)setFont:(UIFont *)font range:(NSRange)range;

@end
