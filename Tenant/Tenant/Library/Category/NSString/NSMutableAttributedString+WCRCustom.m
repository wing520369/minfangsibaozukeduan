//
//  NSMutableAttributedString+WCRCustom.m
//  LianJian
//
//  Created by U1KJ on 16/4/13.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "NSMutableAttributedString+WCRCustom.h"

@implementation NSMutableAttributedString (WCRCustom)

/// 使用NSMakeRange，不使用text去获取range
- (void)setFont:(UIFont *)font range:(NSRange)range{
    [self addAttributes:@{NSFontAttributeName: font} range:range];
}
- (void)setColor:(UIColor *)color range:(NSRange)range{
    [self addAttributes:@{NSForegroundColorAttributeName : color} range:range];
}
/*
 ** 不使用text去获取range，一个字符串中出现多个相同的内容，获取不准确
 */
/*
- (void)setColor:(UIColor *)color text:(NSString *)text{
    NSRange range = [self.mutableString rangeOfString:text];
    [self setColor:color range:range];
}
- (void)setFont:(UIFont *)font text:(NSString *)text{
    NSRange range = [self.mutableString rangeOfString:text];
    [self setFont:font range:range];
}
*/
@end
