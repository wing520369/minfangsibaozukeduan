//
//  UIImage+WCRCustom.h
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (WCRCustom)

/**
 *  根据文件名加载图片(PNG)，如果是JPG需要写后缀名
 *
 *  @param imageName 文件名
 *
 *  @return image
 */
+ (UIImage *)imageFileNamed:(NSString *)imageName;

/**
 *  返回特定尺寸的UImage
 *
 *  @param image      原图片
 *  @param targetSize 要修改的size
 *
 *  @return 修改后的图片
 */
+ (UIImage *)image:(UIImage*)image byScalingToSize:(CGSize)targetSize;

/**
 *  按图片的比例，根据宽取得 按比例的高
 *
 *  @param width 宽
 *
 *  @return 高
 */
- (CGFloat)InProportionAtWidth:(CGFloat)width;

/**
 *  取图片的比例，根据高取得 按比例的宽
 *
 *  @param height 高
 *
 *  @return 宽
 */
- (CGFloat)InProportionAtHeight:(CGFloat)height;

/**
 *  截屏操作，返回截屏后的图片
 *
 *  @return 截屏后的图片
 */
+ (UIImage *)imageWithScreenView;

/**
 *  颜色转为图片
 *
 *  @param color 颜色
 *
 *  @return 图片
 */
+ (UIImage*)imageWithColor:(UIColor*)color;

/**
 *  根据图片的宽度压缩图片
 *
 *  @param sourceImage 原图
 *  @param defineWidth 压缩的图片的宽度
 *
 *  @return 压缩的图片
 */
+ (UIImage *)imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth;

#pragma mark - 将图片缓存到本地：保存文件的路径如果需要请自行修改
+ (BOOL)saveImageToLocationWithImage:(UIImage *)image;
+ (BOOL)saveImageToLocationWithImageURL:(NSString *)url;

#pragma mark - 获取本地缓存图片：保存文件的路径如果需要请自行修改
+ (UIImage *)getLocationImage;

#pragma mark - 根据字符串生成二维码
/// 根据字符串生成二维码
+ (UIImage *)createQRCodeWithText:(NSString *)text;

@end
