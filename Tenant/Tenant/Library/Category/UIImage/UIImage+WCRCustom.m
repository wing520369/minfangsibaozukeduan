//
//  UIImage+WCRCustom.m
//  LianJian
//
//  Created by U1KJ on 16/4/11.
//  Copyright © 2016年 U1KJ. All rights reserved.
//

#import "UIImage+WCRCustom.h"

@implementation UIImage (WCRCustom)

#pragma mark - 加载本地图片
+ (UIImage *)imageFileNamed:(NSString *)imageName{
    UIImage * tempImage = nil;
    NSString * imagePath = nil;
    
    if ([imageName hasSuffix:@"png"]) {
        imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:nil];
    }else {
        imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:@"png"];
    }
    
    if ([imageName rangeOfString:@"jpg"].location != NSNotFound) {
        // jpg
        imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:nil];
    }
    
    tempImage = [[UIImage alloc] initWithContentsOfFile:imagePath];
    
    return tempImage ;
}

#pragma mark - 修改图片size
+ (UIImage *)image:(UIImage*)image byScalingToSize:(CGSize)targetSize {
    UIImage *sourceImage = image;
    UIImage *newImage = nil;
    
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = CGPointZero;
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage ;
}

- (CGFloat)InProportionAtWidth:(CGFloat)width {
    return  width / (self.size.width / self.size.height);
}

- (CGFloat)InProportionAtHeight:(CGFloat)height {
    return height * (self.size.width / self.size.height);
}

#pragma mark - 截屏
+(UIImage *)imageWithScreenView {
    CGFloat mainWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat mainHeight = [[UIScreen mainScreen] bounds].size.height;
    //根据屏幕大小，获取上下文
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(mainWidth, mainHeight), YES, 0);     //设置截屏大小
    [[[UIApplication sharedApplication].keyWindow layer] renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRef imageRef = viewImage.CGImage;
    CGFloat width = mainWidth > 375 ? mainWidth * 3 : mainWidth * 2;
    CGFloat height = mainHeight > 1242 ? mainHeight * 3 : mainHeight * 2;
    CGRect rect = CGRectMake(0, 0, width, height);//这里可以设置想要截图的区域
    CGImageRef imageRefRect =CGImageCreateWithImageInRect(imageRef, rect);
    UIImage *sendImage = [[UIImage alloc] initWithCGImage:imageRefRect];
    
    /**
     //保存图片到照片库
     UIImageWriteToSavedPhotosAlbum(sendImage, nil, nil, nil);
     //保存照片到沙盒目录
     NSData *imageViewData = UIImagePNGRepresentation(sendImage);
     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     NSString *documentsDirectory = [paths objectAtIndex:0];
     NSString *pictureName= [NSString stringWithFormat:@"screenShow_%@.png",[NSDate date]];
     NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:pictureName];
     [imageViewData writeToFile:savedImagePath atomically:YES];
     CGImageRelease(imageRefRect);
     NSLog(@"截屏路径打印: %@", savedImagePath);
     */
    return sendImage;
}
#pragma mark - 颜色转图片
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect=CGRectMake(0,0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

#pragma mark - 根据图片的宽度压缩图片
+ (UIImage *)imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth {
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = defineWidth;
    CGFloat targetHeight = (targetWidth / width) * height;
    UIGraphicsBeginImageContext(CGSizeMake(targetWidth, targetHeight));
    [sourceImage drawInRect:CGRectMake(0,0,targetWidth,  targetHeight)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - 缓存图片到本地
+ (BOOL)saveImageToLocationWithImage:(UIImage *)image {
    NSData *imageData = nil;
    if (UIImagePNGRepresentation(image) == nil) {
        imageData = UIImageJPEGRepresentation(image, 1);
    }
    else {
        imageData = UIImagePNGRepresentation(image);
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // 设置时间格式
    formatter.dateFormat = @"yyyyMMddHHmmssSS";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *pictureName= [NSString stringWithFormat:@"二维码_%@.png",str];
    
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",pictureName]];   // 保存文件的名称
    BOOL isFlag = [imageData writeToFile:filePath atomically:YES];
    return isFlag;
}
+ (BOOL)saveImageToLocationWithImageURL:(NSString *)url {
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"pic_2016-05-18.png"]];   // 保存文件的名称
    BOOL isFlag = [imageData writeToFile:filePath atomically:YES];
    return isFlag;
}

+ (UIImage *)getLocationImage {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"pic_2016-05-18.png"]];   // 保存文件的名称
    UIImage *img = [UIImage imageWithContentsOfFile:filePath];
    return img;
}

#pragma mark - 生成二维码
// 生成二维码
+ (UIImage *)createQRCodeWithText:(NSString *)text {
    UIImage *image = [self createNonInterpolatedUIImageFromCIImage:[self createQRForString:text] Size:CGSizeMake(150, 150)];
    return image;
}

+ (CIImage *)createQRForString:(NSString *)qrString {
    // Need to convert the string to a UTF-8 encoded NSData object
    NSData *stringData = [qrString dataUsingEncoding: NSISOLatin1StringEncoding];
    
    // Create the filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // Set the message content and error-correction level
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    // Send the image back
    return qrFilter.outputImage;
}

+ (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image Size:(CGSize)size {
    // Render the CIImage into a CGImage
    CGImageRef cgImage = [[CIContext contextWithOptions:nil] createCGImage:image fromRect:image.extent];
    
    // Now we'll rescale using CoreGraphics
    //    UIGraphicsBeginImageContext(CGSizeMake(image.extent.size.width * scale, image.extent.size.width * scale));
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    // We don't want to interpolate (since we've got a pixel-correct image)
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextDrawImage(context, CGContextGetClipBoundingBox(context), cgImage);
    // Get the image out
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // Tidy up
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    return scaledImage;
}

@end
