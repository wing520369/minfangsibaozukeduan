//
//  WCRDatePickerView.h
//  Landlord
//
//  Created by 吴传荣 on 2017/5/4.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,kDateFormatType) {
    kHHmm_Type = 1006, //HH:mm
    kHHmmss_Type,   //HH:mm:ss
    kYYYYMMdd_Type,   //YYYYMMdd
    kYYYYMMddHHmm_Type, //YYYY-MM-dd HH:mm
    kMMddHHmm_Type,   //MMddHHmm
    kDateAll_Type,   //YYYY-MM-dd HH:mm:ss
};

/// 日期控件
@interface WCRDatePickerView : UIView

+ (WCRDatePickerView *)popDatePickerViewWithDatePickerMode:(UIDatePickerMode)datePickerMode ReturnDateFormat:(kDateFormatType)dateormatType ReturnBlock:(void (^) (NSString *dateString, NSDate *date))dateBlock;

@property (nonatomic, strong) UIDatePicker *datePicker;

@end
