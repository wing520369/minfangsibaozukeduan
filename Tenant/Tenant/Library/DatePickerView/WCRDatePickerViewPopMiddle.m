//
//  WCRDatePickerViewPopMiddle.m
//  Landlord
//
//  Created by eliot on 2017/5/10.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRDatePickerViewPopMiddle.h"

#define kTitle @"实际到账日期"

@interface WCRDatePickerViewPopMiddle ()

@property (nonatomic, assign) CGFloat pickerViewHeight;
@property (nonatomic, assign) CGFloat buttonHeight;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) NSString *cancelTitle;
@property (nonatomic, strong) UIColor *cancelButtonColor;
@property (nonatomic, strong) NSString *confirmTitle;
@property (nonatomic, strong) UIColor *confirmButtonColor;
@property (nonatomic, strong) UIColor *barColor;

@property (nonatomic, copy) void (^returnBlock)(NSString *dateString, NSDate *date);
/// 标题
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, assign) kDateFormatType dateFormatType;

@end

@implementation WCRDatePickerViewPopMiddle

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.font = [UIFont CustomFontWithSize:14.0];
        self.buttonHeight = 40.0;
        self.pickerViewHeight = 150.0;
        self.cancelTitle = @"取消";
        self.cancelButtonColor = [UIColor grayColor];
        self.confirmTitle = @"完成";
        self.confirmButtonColor = [UIColor orangeColor];
        self.barColor = [UIColor colorWithRGB:223 G:223 B:223];
    }
    return self;
}

+ (void)popDatePickerViewWithDatePickerMode:(UIDatePickerMode)datePickerMode ReturnDateFormat:(kDateFormatType)dateormatType Title:(NSString *)title ReturnBlock:(void (^)(NSString *dateString, NSDate *date))dateBlock {
    
    // 背景view
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height = [[UIScreen mainScreen] bounds].size.height;
    WCRDatePickerViewPopMiddle *datePickerView = [[self alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [[UIApplication sharedApplication].keyWindow addSubview:datePickerView];
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    
    [datePickerView popDatePickerViewWithDatePickerMode:datePickerMode ReturnDateFormat:dateormatType Title:title ReturnBlock:^(NSString *dateString, NSDate *date) {
        dateBlock(dateString, date);
    }];
}

- (void)popDatePickerViewWithDatePickerMode:(UIDatePickerMode)datePickerMode ReturnDateFormat:(kDateFormatType)dateormatType Title:(NSString *)title ReturnBlock:(void (^)(NSString *dateString, NSDate *date))dateBlock {
    
    self.dateFormatType = dateormatType;
    self.returnBlock = dateBlock;
    self.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.3];
    // 添加手势移除视图
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeBgViewFromWindow:)];
    [self addGestureRecognizer:tap];
    
    // 内容View：button+pickerView
    CGFloat x = AUTO_MATE_WIDTH(20);
    CGFloat height = self.buttonHeight * 2 + self.pickerViewHeight;
    CGFloat width = self.frame.size.width - x * 2;
    CGFloat y = self.frame.size.height;
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, height)];
    contentView.backgroundColor = [UIColor colorWithRed:243 / 255.0 green:243 / 255.0 blue:243 / 255.0 alpha:1];
    [self addSubview:contentView];

    // 标题
    height = self.buttonHeight;
    y = 0;
    x = 0;
    width = CGRectGetWidth(contentView.frame) - x * 2;
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    titleLabel.font = self.font;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = title.length ? title : kTitle;
    [contentView addSubview:titleLabel];
    
    // pickerView
    y = CGRectGetMaxY(titleLabel.frame);
    x = 0;
    width = CGRectGetWidth(contentView.frame);
    height = self.pickerViewHeight;
    _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(x, y, width, height)];
    _datePicker.datePickerMode = datePickerMode;
    //设置中文显示
    _datePicker.locale=[[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"];
    [contentView addSubview:_datePicker];
    
    
    // 左边取消按钮
    NSArray *titles = @[@"取消",@"确认"];
    NSArray *tags = @[@(1117),@(1118)];
    NSArray *titleColors = @[[UIColor colorWithHexString:kCustomGrayColor],[UIColor colorWithHexString:kMainColor]];
    y = _datePicker.bottom_wcr;
    x = 0;
    width = CGRectGetWidth(contentView.frame) / 2.0;
    height = self.buttonHeight;
    for (NSInteger i = 0; i < titles.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(x, y, width, height);
        [button buttonWithTitleColor:[titleColors objectAtIndex:i] Background:nil Font:self.font Title:[titles objectAtIndex:i]];
        button.tag = [[tags objectAtIndex:i] integerValue];
        [button setEnlargeEdgeWithTop:10 Right:20 Bottom:10 Left:10];
        [button addTarget:self action:@selector(viewButtonAciton:)];
        [button addLineViewPositionType:kTop_Type];
        [contentView addSubview:button];
        x = button.right_wcr;
        if (i == titles.count - 1) {
            UIView *lineView = [UIView lineView];
            lineView.frame = CGRectMake(0, 0, 1, button.height_wcr);
            [button addSubview:lineView];
        }
    }
    
    __block UIView *weakView = contentView;
    CGRect frame = contentView.frame;
    frame.origin.y = (self.frame.size.height - frame.size.height) / 2.0 - 30;
    [UIView animateWithDuration:0.10 animations:^{
        weakView.frame = frame;
    }];
}

#pragma mark - 手势移除视图
- (void)removeBgViewFromWindow:(UITapGestureRecognizer *)tapGR {
    [self removeFromSuperview];
}

#pragma mark - 取消、完成
- (void)viewButtonAciton:(UIButton *)button {
    if (button.tag == 1117) {
        // 取消
        
    }
    else if (button.tag == 1118) {
        // 完成
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        switch (self.dateFormatType) {
            case kHHmm_Type:
                formatter.dateFormat = @"HH:mm";
                break;
            case kHHmmss_Type:
                formatter.dateFormat = @"HH:mm:ss";
                break;
            case kYYYYMMdd_Type:
                formatter.dateFormat = @"YYYY-MM-dd";
                break;
            case kMMddHHmm_Type:
                formatter.dateFormat = @"MM-dd HH:mm";
                break;
            case kDateAll_Type:
                formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
                break;
            default:
                formatter.dateFormat = @"YYYY-MM-dd";
                break;
        }
        if (!_datePicker.date) {
            _datePicker.date = [NSDate new];
        }
        NSString *timestamp = [formatter stringFromDate:_datePicker.date];
        self.returnBlock(timestamp, _datePicker.date);
    }
    [self removeFromSuperview];
}

@end
