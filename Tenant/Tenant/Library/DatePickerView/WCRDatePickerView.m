//
//  WCRDatePickerView.m
//  Landlord
//
//  Created by 吴传荣 on 2017/5/4.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRDatePickerView.h"

@interface WCRDatePickerView ()

@property (nonatomic, assign) CGFloat pickerViewHeight;
@property (nonatomic, assign) CGFloat buttonHeight;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) NSString *cancelTitle;
@property (nonatomic, strong) UIColor *cancelButtonColor;
@property (nonatomic, strong) NSString *confirmTitle;
@property (nonatomic, strong) UIColor *confirmButtonColor;
@property (nonatomic, strong) UIColor *barColor;

@property (nonatomic, copy) void (^returnBlock)(NSString *dateString, NSDate *date);
@property (nonatomic, strong) UILabel *resultLabel;

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, assign) kDateFormatType dateFormatType;
@end

@implementation WCRDatePickerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.font = [UIFont CustomFontWithSize:15.0];
        self.buttonHeight = 50.0;
        self.pickerViewHeight = 170.0;
        self.cancelTitle = @"取消";
        self.cancelButtonColor = [UIColor grayColor];
        self.confirmTitle = @"完成";
        self.confirmButtonColor = [UIColor orangeColor];
        self.barColor = [UIColor colorWithRGB:223 G:223 B:223];
    }
    return self;
}

+ (WCRDatePickerView *)popDatePickerViewWithDatePickerMode:(UIDatePickerMode)datePickerMode ReturnDateFormat:(kDateFormatType)dateormatType ReturnBlock:(void (^)(NSString *dateString, NSDate *date))dateBlock {
    
    // 背景view
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height = [[UIScreen mainScreen] bounds].size.height;
    WCRDatePickerView *datePickerView = [[self alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [[UIApplication sharedApplication].keyWindow addSubview:datePickerView];
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    
    [datePickerView popDatePickerViewWithDatePickerMode:datePickerMode ReturnDateFormat:dateormatType ReturnBlock:^(NSString *dateString, NSDate *date) {
        dateBlock(dateString, date);
    }];
    
    return datePickerView;
}

- (void)popDatePickerViewWithDatePickerMode:(UIDatePickerMode)datePickerMode ReturnDateFormat:(kDateFormatType)dateormatType ReturnBlock:(void (^)(NSString *dateString, NSDate *date))dateBlock {
    
    self.dateFormatType = dateormatType;
    self.returnBlock = dateBlock;
    self.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.3];
    // 添加手势移除视图
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeBgViewFromWindow:)];
    [self addGestureRecognizer:tap];
    
    // 内容View：button+pickerView
    CGFloat height = self.buttonHeight + self.pickerViewHeight;
    CGFloat width = self.frame.size.width;
    CGFloat y = self.frame.size.height;
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, y, width, height)];
    contentView.backgroundColor = [UIColor colorWithRed:243 / 255.0 green:243 / 255.0 blue:243 / 255.0 alpha:1];
    [self addSubview:contentView];
    
    // 左边取消按钮
    CGFloat offsetX = 15;
    CGFloat x = offsetX;
    y = 0;
    width = 40;
    height = self.buttonHeight;
    UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [cancelButton setTitle:self.cancelTitle forState:0];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [cancelButton setTitleColor:self.cancelButtonColor forState:0];
    cancelButton.titleLabel.font = self.font;
    cancelButton.tag = 1117;
    [cancelButton addTarget:self action:@selector(viewButtonAciton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setEnlargeEdgeWithTop:0 Right:20 Bottom:10 Left:offsetX];
    [contentView addSubview:cancelButton];
    cancelButton.hidden = YES;
    cancelButton.userInteractionEnabled = NO;
    
    // 右边完成按钮
    x = self.frame.size.width - width - offsetX;
    UIButton *completeButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [completeButton setTitle:self.confirmTitle forState:0];
    completeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [completeButton setTitleColor:self.confirmButtonColor forState:0];
    completeButton.tag = 1118;
    completeButton.titleLabel.font = self.font;
    [completeButton addTarget:self action:@selector(viewButtonAciton:) forControlEvents:UIControlEventTouchUpInside];
    [completeButton setEnlargeEdgeWithTop:0 Right:offsetX Bottom:10 Left:20];
    [contentView addSubview:completeButton];
    
    // 显示当前选中的结果
    x = CGRectGetMaxX(cancelButton.frame);
    width = CGRectGetWidth(contentView.frame) - x * 2;
    UILabel *resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    _resultLabel = resultLabel;
    resultLabel.font = self.font;
    resultLabel.textAlignment = NSTextAlignmentCenter;
    resultLabel.backgroundColor = [UIColor clearColor];
    [contentView addSubview:resultLabel];
    
    // pickerView
    y = CGRectGetMaxY(cancelButton.frame);
    x = 0;
    width = CGRectGetWidth(contentView.frame);
    height = self.pickerViewHeight;
    _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(x, y, width, height)];
    _datePicker.datePickerMode = datePickerMode;
    //设置中文显示
    _datePicker.locale=[[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"];
    [contentView addSubview:_datePicker];

    __block UIView *weakView = contentView;
    CGRect frame = contentView.frame;
    frame.origin.y = self.frame.size.height - frame.size.height;
    [UIView animateWithDuration:0.10 animations:^{
        weakView.frame = frame;
    }];
}

#pragma mark - 手势移除视图
- (void)removeBgViewFromWindow:(UITapGestureRecognizer *)tapGR {
    [self removeFromSuperview];
}

#pragma mark - 取消、完成
- (void)viewButtonAciton:(UIButton *)button {
    if (button.tag == 1117) {
        // 取消
        
    }
    else if (button.tag == 1118) {
        // 完成
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        switch (self.dateFormatType) {
            case kHHmm_Type:
                formatter.dateFormat = @"HH:mm";
                break;
            case kHHmmss_Type:
                formatter.dateFormat = @"HH:mm:ss";
                break;
            case kYYYYMMdd_Type:
                formatter.dateFormat = @"YYYY-MM-dd";
                break;
            case kYYYYMMddHHmm_Type:
                formatter.dateFormat = @"YYYY-MM-dd HH:mm";
                break;
            case kMMddHHmm_Type:
                formatter.dateFormat = @"MM-dd HH:mm";
                break;
            case kDateAll_Type:
                formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
                break;
            default:
                formatter.dateFormat = @"YYYY-MM-dd";
                break;
        }
        if (!_datePicker.date) {
            _datePicker.date = [NSDate new];
        }
        NSString *timestamp = [formatter stringFromDate:_datePicker.date];
        self.returnBlock(timestamp, _datePicker.date);
    }
    [self removeFromSuperview];
}

@end
