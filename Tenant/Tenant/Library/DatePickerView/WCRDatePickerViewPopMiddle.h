//
//  WCRDatePickerViewPopMiddle.h
//  Landlord
//
//  Created by eliot on 2017/5/10.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,kDateFormatType) {
    kHHmm_Type = 1006, //HH:mm
    kHHmmss_Type,   //HH:mm:ss
    kYYYYMMdd_Type,   //YYYYMMdd
    kMMddHHmm_Type,   //MMddHHmm
    kDateAll_Type,   //YYYY-MM-dd HH:mm
};

/// 在屏幕中间弹出一个datePickerView
@interface WCRDatePickerViewPopMiddle : UIView

+ (void)popDatePickerViewWithDatePickerMode:(UIDatePickerMode)datePickerMode ReturnDateFormat:(kDateFormatType)dateormatType Title:(NSString *)title ReturnBlock:(void (^) (NSString *dateString, NSDate *date))dateBlock;

@end
