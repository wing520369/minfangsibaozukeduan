//
//  WCRMutilImageViews.m
//  CarAccessories
//
//  Created by 吴传荣 on 16/9/23.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRMutilImageViews.h"
#import "UIImageView+WebCache.h"
#import "WCRImageCollectionViewFlowLayout.h"
#import "WCRAddImageCell.h"
#import "WCRImageCollectionViewCell.h"
#import "TZImagePickerController.h"

@interface WCRMutilImageViews ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TZImagePickerControllerDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) CGFloat itemHeight;

@end

@implementation WCRMutilImageViews

- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self defaultSetting];
        [self createMutilImageViewsContentUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame MaxCount:(NSInteger)maxCount Column:(NSInteger)column {
    if (self = [super initWithFrame:frame]) {
        self.column = column;
        self.maxCount = maxCount;
        _images = [NSMutableArray arrayWithCapacity:_maxCount];
        [self createMutilImageViewsContentUI];
    }
    return self;
}

// 默认设置
- (void)defaultSetting {
    _column = 4;
    _maxCount = 8;
    _images = [NSMutableArray arrayWithCapacity:_maxCount];
}
// 先确定frame，高度再调整
- (void)createMutilImageViewsContentUI {
    
    WCRImageCollectionViewFlowLayout *flowLayout = [[WCRImageCollectionViewFlowLayout alloc] init];
    
    NSInteger column = _column; // 一行有几张图片
    CGFloat mainWidth = self.width_wcr;
    CGFloat width = (mainWidth - kSpaceX * 2 - kImageBorder * column - 1) / column;
    flowLayout.itemSize = CGSizeMake(width, width);
    
    // collectionView先给一个cell的高度
    _itemHeight = flowLayout.itemSize.height;
    CGFloat height = _itemHeight + kImageBorder;
    CGFloat y = 0;
    CGFloat x = 0;
    width = self.width_wcr;
    CGRect frameOfCV = CGRectMake(x, y, width, height);
    _collectionView = [[UICollectionView alloc] initWithFrame:frameOfCV collectionViewLayout:flowLayout];
    [_collectionView registerClass:[WCRAddImageCell class] forCellWithReuseIdentifier:@"addCell"];
    [_collectionView registerClass:[WCRImageCollectionViewCell class] forCellWithReuseIdentifier:@"imageCell"];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.bounces = NO;
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.opaque = YES;
    [self addSubview:_collectionView];
 
    self.height_wcr = _collectionView.maxY_wcr;
}

#pragma mark - UICollectionView DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_images.count == _maxCount) {
        return _images.count;
    }
    return _hiddenAddButton ? _images.count : _images.count + 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_hiddenAddButton) {
        return [self getImageCellWithCollectionView:collectionView IndexPath:indexPath];
    }
    else {
        if (indexPath.item == _images.count) {
            WCRAddImageCell *addImageCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"addCell" forIndexPath:indexPath];
            return addImageCell;
        }
        else {
            return [self getImageCellWithCollectionView:collectionView IndexPath:indexPath];
        }
    }
}

- (WCRImageCollectionViewCell *)getImageCellWithCollectionView:(UICollectionView *)collectionView IndexPath:(NSIndexPath *)indexPath {
    WCRImageCollectionViewCell *imageCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
    id object = [_images objectAtIndex:indexPath.item];
    if ([object isKindOfClass:[NSString class]]) {
        __block NSMutableArray *tempImages = _images;
        NSInteger index = [_images indexOfObject:object];
        [imageCell.loadImageView sd_setImageWithURL:[NSURL URLWithString:object] placeholderImage:[UIImage imageFileNamed:kPlaceholderImageName] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                [tempImages insertObject:image atIndex:index];
                [tempImages removeObject:object];
            }
        }];
    }
    else if ([object isKindOfClass:[UIImage class]]) {
        imageCell.loadImageView.image = object;
    }
    imageCell.deleteButton.tag = indexPath.item;
    [imageCell.deleteButton addTarget:self action:@selector(WCRImageDeleteAction:)];
    imageCell.deleteButton.hidden = _hiddenAddButton;
    imageCell.deleteButton.userInteractionEnabled = !_hiddenAddButton;
    return imageCell;
}

// 删除照片
- (void)WCRImageDeleteAction:(UIButton *)button {
    NSInteger index = button.tag;
    [_images removeObjectAtIndex:index];
    [self updateView];
}

#pragma mark - UICollectionView Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == _images.count) { // 添加照片
        if (_maxCount == _images.count) {
            return;
        }
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:_maxCount - _images.count delegate:self];
        imagePickerVc.allowPickingOriginalPhoto = NO;
        [[self getCurrentViewController] presentViewController:imagePickerVc animated:YES completion:nil];
    }
    else {  // 点击图片
        if (_delegate && [_delegate respondsToSelector:@selector(mutilImageViewsClickImageWithIndex:)]) {
            [_delegate mutilImageViewsClickImageWithIndex:indexPath.item];
        }
    }
}

#pragma mark - TZImagePickerControllerDelegate 选取照片
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto infos:(NSArray<NSDictionary *> *)infos {
    
    [_images addObjectsFromArray:photos];
    [self updateView];
}

// 刷新视图，计算collectionView视图的高度
- (void)updateView {
    __weak typeof(self) weakSelf = self;
    __block BOOL isNeedRefreshHeight = NO;
    __block CGFloat tempHeight = self.collectionView.height_wcr;
    [UIView animateWithDuration:0.1 delay:0 options:7 animations:^{
        if (weakSelf.images.count < weakSelf.column) {
            weakSelf.collectionView.height_wcr = weakSelf.itemHeight + kImageBorder;
        }
        else {
            weakSelf.collectionView.height_wcr = (weakSelf.itemHeight + kImageBorder) * 2;
        }
        if (weakSelf.collectionView.height_wcr != tempHeight) {
            isNeedRefreshHeight = YES;
        }
    } completion:^(BOOL finished) {
        
        [weakSelf.collectionView reloadData];
        if (isNeedRefreshHeight) {
            weakSelf.height_wcr = weakSelf.collectionView.maxY_wcr;
        }
        [weakSelf updatgeImages:isNeedRefreshHeight];
    }];
}
- (void)updatgeImages:(BOOL)IsNeedRefreshHeight {
    if (_delegate && [_delegate respondsToSelector:@selector(mutilImageViewsUpdateImages:IsNeedRefreshHeight:)]) {
        [_delegate mutilImageViewsUpdateImages:_images IsNeedRefreshHeight:IsNeedRefreshHeight];
    }
}
- (void)WCRMutilImageViewsReloadWithImages:(NSArray *)mulitImages {
    [_images addObjectsFromArray:mulitImages];
//    [self.collectionView reloadData];
    [self updateView];
}

@end
