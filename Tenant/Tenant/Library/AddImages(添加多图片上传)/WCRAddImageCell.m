//
//  WCRAddImageCell.m
//  CarAccessories
//
//  Created by 吴传荣 on 16/9/25.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRAddImageCell.h"
#import "UIImageView+WCRCustom.h"
#import "UIColor+WCRCustom.h"

@interface WCRAddImageCell ()

@end

@implementation WCRAddImageCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIImageView *loadImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        // dfjewopij:默认图
        // icon_tupian：在具体项目中默认图
        [loadImageView imageViewWithImageName:@"1206icon_tupian"];
        [loadImageView setLayerWithBorderWidth:1.0 Corner:0 BorderColor:[UIColor lineColor]];
        [self.contentView addSubview:loadImageView];
    }
    return self;
}

@end
