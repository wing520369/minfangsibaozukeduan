//
//  WCRImageCollectionViewCell.m
//  CarAccessories
//
//  Created by 吴传荣 on 16/9/25.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRImageCollectionViewCell.h"
#import "UIImageView+WCRCustom.h"

@implementation WCRImageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        _loadImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [_loadImageView setContentModeToScaleAspectFill];
        [self.contentView addSubview:_loadImageView];
        
        CGFloat widthAndHeight = 15;
        _deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width - widthAndHeight, 0, widthAndHeight, widthAndHeight)];
        [_deleteButton.imageView setContentModeToScaleAspectFill];
        [_deleteButton setBackgroundImage:[UIImage imageFileNamed:@"shanchutupian_icon"] forState:0];
        [_deleteButton setEnlargeEdgeWithTop:0 Right:0 Bottom:20 Left:20];
        [self.contentView addSubview:_deleteButton];
        
    }
    return self;
}

@end
