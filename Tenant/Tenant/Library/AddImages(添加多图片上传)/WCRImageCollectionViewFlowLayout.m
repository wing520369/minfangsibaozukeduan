//
//  WCRImageCollectionViewFlowLayout.m
//  CarAccessories
//
//  Created by 吴传荣 on 16/9/25.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRImageCollectionViewFlowLayout.h"

@implementation WCRImageCollectionViewFlowLayout

- (instancetype)init {
    if (self = [super init]) {
        
        NSInteger column = 4; // 一行有几张图片
        self.sectionInset = UIEdgeInsetsMake(kImageBorder, 0, 0, kImageBorder);
        self.minimumInteritemSpacing = kImageBorder;
        self.minimumLineSpacing = kImageBorder;
        CGFloat width = (kMainWidth - kSpaceX * 2 - kImageBorder * column - 1) / column;
        self.itemSize = CGSizeMake(width, width);
    }
    return self;
}

@end
