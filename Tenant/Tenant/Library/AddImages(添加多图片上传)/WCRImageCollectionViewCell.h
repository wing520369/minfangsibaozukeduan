//
//  WCRImageCollectionViewCell.h
//  CarAccessories
//
//  Created by 吴传荣 on 16/9/25.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 上传照片的cell */
@interface WCRImageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *loadImageView;

/** 右上角的删除按钮 */
@property (nonatomic, strong) UIButton *deleteButton;

@end
