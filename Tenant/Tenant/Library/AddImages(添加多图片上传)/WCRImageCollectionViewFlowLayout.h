//
//  WCRImageCollectionViewFlowLayout.h
//  CarAccessories
//
//  Created by 吴传荣 on 16/9/25.
//  Copyright © 2016年 aleven. All rights reserved.
//

#define kImageBorder 5 // 图片之间的边距

#import <UIKit/UIKit.h>

@interface WCRImageCollectionViewFlowLayout : UICollectionViewFlowLayout


@end
