//
//  WCRMutilImageViews.h
//  CarAccessories
//
//  Created by 吴传荣 on 16/9/23.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WCRMutilImageViewsDelegate <NSObject>

/**
 *  图片数组发生变化，高度更新
 *
 *  @param images    图片数组
 *  @param isRefresh 是否需要刷新高度
 */
- (void)mutilImageViewsUpdateImages:(NSMutableArray *)images IsNeedRefreshHeight:(BOOL)isRefresh;

@optional
- (void)mutilImageViewsClickImageWithIndex:(NSInteger)index;

@end

/**
 *  多图片的view
 *  评价列表的图片
 *  先设置 frame =（x, y, width, height）,height根据图片的返回
 */
@interface WCRMutilImageViews : UIView

/** 最大的图片数量 */
@property (nonatomic, assign) NSInteger maxCount;
/** 图片数组 */
@property (nonatomic, strong) NSMutableArray *images;
/** 一行显示几张图片 */
@property (nonatomic, assign) NSInteger column;

@property (nonatomic, weak) id<WCRMutilImageViewsDelegate> delegate;
/** 隐藏添加图片的按钮 */
@property (nonatomic, assign) BOOL hiddenAddButton;

- (instancetype)initWithFrame:(CGRect)frame MaxCount:(NSInteger)maxCount Column:(NSInteger)column;

- (void)WCRMutilImageViewsReloadWithImages:(NSArray *)mulitImages;

@end
