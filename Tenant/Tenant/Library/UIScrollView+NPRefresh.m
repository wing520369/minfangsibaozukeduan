//
//  UIScrollView+NPRefresh.m
//  KDExpressClient
//
//  Created by HzB on 16/9/12.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import "UIScrollView+NPRefresh.h"

@implementation UIScrollView (NPRefresh)

- (void)MJFooterWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock{
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:refreshingBlock];
    footer.hidden = YES;
    footer.automaticallyHidden = NO;
    self.mj_footer = footer;
}

- (void)MJHeaderWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:refreshingBlock];
    self.mj_header = header;
}

@end
