//
//  MJExtensionConfig.m
//  MJExtensionExample
//
//  Created by MJ Lee on 15/4/22.
//  Copyright (c) 2015年 小码哥. All rights reserved.
//

#import "MJExtensionConfig.h"
#import <MJExtension/MJExtension.h>

@implementation MJExtensionConfig
/**
 *  这个方法会在MJExtensionConfig加载进内存时调用一次
 */
+ (void)load {
#pragma mark 如果使用NSObject来调用这些方法，代表所有继承自NSObject的类都会生效
#pragma mark NSObject中的ID属性对应着字典中的id
    [NSObject mj_setupReplacedKeyFromPropertyName:^NSDictionary *{
        return @{
                 @"ID" : @"id",
                 @"descriptionStr":@"description",
                 };
    }];
    

#pragma mark Book的日期处理、字符串nil值处理
    [NSObject mj_setupNewValueFromOldValue:^id(id object, id oldValue, MJProperty *property) {
        
        if (property.type.typeClass == [NSString class]) {
            if (oldValue == nil || [oldValue isKindOfClass:[NSNull class]]) return @"";
        }
        return oldValue;
    }];
    
//    [Book mj_setupNewValueFromOldValue:^id(id object, id oldValue, MJProperty *property) {
//        if ([property.name isEqualToString:@"publisher"]) {
//            if (oldValue == nil || [oldValue isKindOfClass:[NSNull class]]) return @"";
//        } else if (property.type.typeClass == [NSDate class]) {
//            NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
//            fmt.dateFormat = @"yyyy-MM-dd";
//            return [fmt dateFromString:oldValue];
//        }
//            
//        return oldValue;
//    }];
    // 相当于在Book.中实现了- (id)mj_newValueFromOldValue:property:方法
}
@end
