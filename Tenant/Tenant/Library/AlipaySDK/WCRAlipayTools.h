//
//  WCRAlipayTools.h
//  CarAccessories
//
//  Created by eliot on 16/11/15.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AlipaySDK/AlipaySDK.h>

typedef void (^payResultBlock)(NSDictionary *resultDictionary);

@interface WCRAlipayTools : NSObject

/**
 *  跳转支付宝钱包进行支付，处理支付结果
 *
 *  @param url 回调url
 */
+ (void)AlipayWithOpenURL:(NSURL *)url;

/**
 *  跳起支付宝
 *
 *  @param orderStr  需要的参数（后台拼接好）
 *  @param payResult 结果回调
 */
+ (void)AlipayWithpayOrder:(NSString *)orderStr CompleteResult:(payResultBlock)payResult;

@end
