//
//  WCRAlipayTools.m
//  CarAccessories
//
//  Created by eliot on 16/11/15.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRAlipayTools.h"

@interface WCRAlipayTools ()

@end

@implementation WCRAlipayTools

+ (void)AlipayWithOpenURL:(NSURL *)url {
    
    // 如果极简开发包不可用，会跳转支付宝钱包进行支付，需要将支付宝钱包的支付结果回传给开发包
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            
            //【由于在跳转支付宝客户端支付的过程中，商户app在后台很可能被系统kill了，所以pay接口的callback就会失效，请商户对standbyCallback返回的回调结果进行处理,就是在这个方法里面处理跟callback一样的逻辑】
            NSLog(@"safepay由于在跳转支付宝客户端支付的过程中result = %@",resultDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AlipayResult" object:nil userInfo:resultDic];
        }];
    }
    
    if ([url.host isEqualToString:@"platformapi"]){
        
        //支付宝钱包快登授权返回authCode
        [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
            //【由于在跳转支付宝客户端支付的过程中，商户app在后台很可能被系统kill了，所以pay接口的callback就会失效，请商户对standbyCallback返回的回调结果进行处理,就是在这个方法里面处理跟callback一样的逻辑】
            NSLog(@"platformapi由于在跳转支付宝客户端支付的过程中result = %@",resultDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AlipayResult" object:nil userInfo:resultDic];
        }];
    }
    
}

+ (void)AlipayWithpayOrder:(NSString *)orderStr CompleteResult:(payResultBlock)payResult {
    [[AlipaySDK defaultService] payOrder:orderStr fromScheme:@"com.haoshilai.Tenant" callback:^(NSDictionary *resultDic) {
        payResult(resultDic);
    }];
}

@end
