//
//  Tools.m
//  EHealth_HM
//
//  Created by skusdk on 13-10-12.
//  Copyright (c) 2013年 dengwz. All rights reserved.
//

#import "Tools.h"
#import "WCRNSUserDefaultsTool.h"
#import "SAMKeychain.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "ReachabilityTool.h"
#import "NPTip.h"
#import "WCRLoginVC.h"
#import "UIWindow+WCRCustom.h"

@implementation Tools

+(BOOL)IS_IOS_7{
    return [[UIDevice currentDevice].systemVersion floatValue] > 6.2;
}
+(BOOL)IS_IOS_8{
    return [[UIDevice currentDevice].systemVersion floatValue] > 7.9;
}

#pragma mark - Token
+ (void)saveToken:(NSString *)regin {
    if ([regin length] == 0) {
        return;
    }
    [WCRNSUserDefaultsTool setStr:regin key:@"miPushToken"];
}
+ (NSString *)tokenName {
    NSString *miPhushToken = [WCRNSUserDefaultsTool strForKey:@"miPushToken"];
    if (nil == miPhushToken) {
        return [Tools getUUID];
    }
    return miPhushToken;
}
+ (NSString *)getUUID{
    NSString *retrieveuuid = [SAMKeychain passwordForService:@"wcr.com" account:@"user"];
    if (nil == retrieveuuid) {
        CFUUIDRef puuid = CFUUIDCreate( nil );
        CFStringRef uuidString = CFUUIDCreateString( nil, puuid );
        NSString * resultUUID = (__bridge NSString *)CFStringCreateCopy( NULL, uuidString);
        [SAMKeychain setPassword: [NSString stringWithFormat:@"%@", resultUUID] forService:@"wcr.com" account:@"user"];
        return resultUUID;
    }
    return retrieveuuid;
}
#pragma makr - 是否第一次打开app
+(BOOL)isFirstOpenApp{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL frist = [[defaults objectForKey:@"frist"] boolValue];
    if (frist) {
        if (![WCRNSUserDefaultsTool isSavedCurrentVersionInfo]) {
            [WCRNSUserDefaultsTool saveCurrentVersionInfo];
            return YES;
        }
        return NO;
    }
    else{
        [WCRNSUserDefaultsTool saveCurrentVersionInfo];
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"frist"];
        [defaults synchronize];
        return YES;
    }
}
#pragma mark - 拨打电话
+ (void)dialPhoneNumber:(NSString *)phoneNum{
    UIWebView *phoneCallWebView = nil;
    NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNum]];
    if (!phoneCallWebView ) {
        phoneCallWebView = [[UIWebView alloc] initWithFrame:CGRectZero];
    }
    [phoneCallWebView loadRequest:[NSURLRequest requestWithURL:phoneURL]];
    [[UIApplication sharedApplication].keyWindow addSubview:phoneCallWebView];
}
#pragma mark - 获取app版本
+(NSString*)getSoftVersion{
    return [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleVersion"];
}
#pragma mark - 取得wifi名
+ (NSString *)getWifiName{
    NSString *wifiName = nil;
    CFArrayRef wifiInterfaces = CNCopySupportedInterfaces();
    if (!wifiInterfaces) {
        return nil;
    }
    NSArray *interfaces = (__bridge NSArray *)wifiInterfaces;
    for (NSString *interfaceName in interfaces) {
        CFDictionaryRef dictRef = CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interfaceName));
        
        if (dictRef) {
            NSDictionary *networkInfo = (__bridge NSDictionary *)dictRef;
            NSLog(@"network info -> %@", networkInfo);
            wifiName = [networkInfo objectForKey:(__bridge NSString *)kCNNetworkInfoKeySSID];
            
            CFRelease(dictRef);
        }
    }
    CFRelease(wifiInterfaces);
    return wifiName;
}
#pragma mark - 打开网络状态监控
+ (void)openNetworkMonitoring{
    [[ReachabilityTool defatueReachabilityTool] starCheckingNetwork];
}
#pragma mark - 获取网络状态
+ (BOOL)getNetworkStatus{
    return [ReachabilityTool defatueReachabilityTool].hasNetWorking;
}
#pragma mark - 获取缓存大小
//单个文件的大小
+ (long long)fileSizeAtPath:(NSString*) filePath {
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}
//遍历文件夹获得文件夹大小，返回多少M
+ (void)requestCachesFileSize:(void (^)(NSString *))completion {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
         NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES) objectAtIndex:0];
        NSFileManager* manager = [NSFileManager defaultManager];
        NSString *sizeStr = @"0M";
        if (![manager fileExistsAtPath:folderPath])
        {
            dispatch_sync(dispatch_get_main_queue(), ^{
                completion(sizeStr);
            });
        }
        else
        {
            NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
            NSString* fileName;
            long long folderSize = 0;
            while ((fileName = [childFilesEnumerator nextObject]) != nil){
                NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
                folderSize += [Tools fileSizeAtPath:fileAbsolutePath];
            }
            float sizeM = folderSize/(1024.0*1024.0);
            
            if (sizeM < 1) {
                
                int size = (int)1024 * sizeM;
                
                sizeStr = [NSString stringWithFormat:@"%d k",size];
            }
            else{
                sizeStr = [NSString stringWithFormat:@"%0.2f M",sizeM];
            }
            dispatch_sync(dispatch_get_main_queue(), ^{
                completion(sizeStr);
            });
        }
    });
}
#pragma mark -清理缓存
+(void)clearCache:(void (^)(BOOL))completion{
    dispatch_async(
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                   , ^{
                       NSString *cachPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES) objectAtIndex:0];
                       NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachPath];
                       for (NSString *p in files) {
                           NSError *error;
                           NSString *path = [cachPath stringByAppendingPathComponent:p];
                           if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                               [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                           }
                       }
                       dispatch_async(dispatch_get_main_queue(), ^{
                           completion(YES);
                       });
                   });
}

+ (void)clearCancheProgress:(void (^)(float))progressBlock
                   complete:(void (^)(NSString *))complete{

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES) objectAtIndex:0];
        NSFileManager* manager = [NSFileManager defaultManager];
        NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
        NSString* fileName;
        long long folderSize = 0;
        while ((fileName = [childFilesEnumerator nextObject]) != nil){
            NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
            long long oneFileSize = [Tools fileSizeAtPath:fileAbsolutePath];
            folderSize += oneFileSize;
        }
        //清楚缓存
        long long clearedSize = 0;
        NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:folderPath];
        for (NSString *fileName in files) {
            NSString *path = [folderPath stringByAppendingPathComponent:fileName];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                NSError *error;
                clearedSize += [Tools fileSizeAtPath:path];
                [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (progressBlock) {
                        progressBlock((double)clearedSize / (double)folderSize);
                    };
                });
            }
        }
//        childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
//        while ((fileName = [childFilesEnumerator nextObject]) != nil){
//            NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
//            if ([[NSFileManager defaultManager] fileExistsAtPath:fileAbsolutePath]) {
//                 NSError *error;
//                  clearedSize += [Tools fileSizeAtPath:fileAbsolutePath];
//                [[NSFileManager defaultManager] removeItemAtPath:fileAbsolutePath error:&error];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    if (progressBlock) {
//                        progressBlock((double)clearedSize / (double)folderSize);
//                    };
//                });
//            }
//        }
        //
        NSString *sizeStr = nil;
        double sizeM = folderSize/(1024.0*1024.0);
        if (sizeM < 1) {
            int size = (int)1024 * sizeM;
            sizeStr = [NSString stringWithFormat:@"%d KB",size];
        }
        else{
            sizeStr = [NSString stringWithFormat:@"%0.2f MB",sizeM];
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            complete(sizeStr);
        });
    });
}

#pragma mark - 将字典，数组写到沙盒
+ (NSString *)writeToDocumentsWithDataSource:(id)dataSource FileName:(NSString *)fileName {
    
    NSString *path = [NSString stringWithFormat:@"%@/Documents/%@",NSHomeDirectory(),fileName];
    //    NSLog(@"path:%@",path);
    // 写入本地
    if ([dataSource isKindOfClass:[NSArray class]]) {
        NSArray *array = (NSArray *)dataSource;
        [array writeToFile:path atomically:YES];
        return path;
    }
    if ([dataSource isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dict = (NSDictionary *)dataSource;
        [dict writeToFile:path atomically:YES];
        
        return path;
    }
    
    return @"";
}
#pragma mark - 判断金额的输入
+ (BOOL)checkTrueNumberWithTextField:(UITextField *)textField InRange:(NSRange)range CurrentString:(NSString *)string {
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    if ([string length] > 0) {
        //当前输入的字符
        unichar single = [string characterAtIndex:0];
        if ((single >= '0' && single <= '9') || single == '.') {
            //数据格式正确
            // 首字母不能为0和小数点
            if([textField.text length] == 0){
                if(single == '.') {
                    [NPTip showTip:@"第一个数字不能为小数点"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            // 小数点前面只能有一个0，不能连续输入多个0
            if ([textField.text length] == 1) {
                unichar first = [textField.text characterAtIndex:0];
                if (single == '0' && first == '0') {
                    [NPTip showTip:@"只能有一个0开头"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    [NPTip showTip:@"您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                if (isHaveDian) {
                    //存在小数点
                    //判断小数点的位数
                    NSRange ran = [textField.text rangeOfString:@"."];
                    if (range.location - ran.location <= 2) {
                        return YES;
                    }else {
                        [NPTip showTip:@"最多输入两位小数"];
                        return NO;
                    }
                }else {
                    return YES;
                }
            }
        }else {
            //输入的数据格式不正确
            [NPTip showTip:@"输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else {
        return YES;
    }
}

#pragma mark - 获取Bundle version版本号，bundleID，appName
/// 获取appName
+ (NSString *)getAppName {
    return  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
}
/// 获取Bundle version版本号
+ (NSString *)getAppVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}
/// 获取Bundle 版本
+ (NSInteger)getAppBundleValue {
    return [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] integerValue];
}
/// bundleID
+ (NSString *)getBundleID {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
}

+ (BOOL)checkIsLogin {
    if (![WCRUserModelTool isLogin]) {
        // 跳转到登录页面
        WCRLoginVC *loginVC = [[WCRLoginVC alloc] init];
        UIViewController *vc = [UIWindow topViewController];
        loginVC.hidesBottomBarWhenPushed = YES;
        [vc.navigationController pushViewController:loginVC animated:YES];
        return NO;
    }
    return YES;
}


@end
