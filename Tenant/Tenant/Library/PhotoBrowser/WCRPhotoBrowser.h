//
//  WCRPhotoBrowser.h
//  CarAccessories
//
//  Created by eliot on 2017/2/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WCRPhotoBrowser : UIView

+ (void)showWithPhotoUrlArray:(NSArray<NSString *> *)photoUrlArray
                 currentIndex:(NSInteger)currentIndex;

+ (void)showWithImageArray:(NSArray<UIImage *> *)imageArray
              currentIndex:(NSInteger)currentIndex;

@end
