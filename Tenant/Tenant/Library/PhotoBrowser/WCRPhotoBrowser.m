//
//  WCRPhotoBrowser.m
//  CarAccessories
//
//  Created by eliot on 2017/2/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRPhotoBrowser.h"
#import "MWPhotoBrowser.h"
#import "UIWindow+WCRCustom.h"

@interface WCRPhotoBrowser ()<MWPhotoBrowserDelegate>
@property (nonatomic,strong) NSMutableArray *MWPhotoArray;
/** 图片数组*/
@property (nonatomic, strong) NSArray *photoArray;
/** 当前图片下标*/
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) NSArray *images;
@end

@implementation WCRPhotoBrowser

+ (void)showWithPhotoUrlArray:(NSArray *)photoUrlArray
                 currentIndex:(NSInteger)currentIndex {
    WCRPhotoBrowser *browser = [[WCRPhotoBrowser alloc] init];
    [browser showWithImages:photoUrlArray currentIndex:currentIndex];
}

+ (void)showWithImageArray:(NSArray<UIImage *> *)imageArray currentIndex:(NSInteger)currentIndex {
    WCRPhotoBrowser *browser = [[WCRPhotoBrowser alloc] init];
    [browser showWithImages:imageArray currentIndex:currentIndex];
}

- (void)showWithImages:(NSArray *)images
          currentIndex:(NSInteger)currentIndex {
    self.photoArray = images;
    self.currentIndex = currentIndex;
    self.images = [images copy];
    
    MWPhotoBrowser *mvc = [[MWPhotoBrowser alloc] init];
    mvc.delegate = self;
    mvc.displayActionButton = NO;
    [mvc setCurrentPhotoIndex:currentIndex];
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:mvc];
    UIViewController *topVC = [UIWindow topViewController];
    [topVC presentViewController:nvc animated:YES completion:nil];
}

- (void)setPhotoArray:(NSArray *)photoArray{
    _MWPhotoArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < photoArray.count; i++) {
        
        MWPhoto *mwPhotoInfo = nil;
        id obj = [photoArray objectAtIndex:i];
        if ([obj isKindOfClass:[NSString class]]) {
            mwPhotoInfo = [[MWPhoto alloc]initWithURL:[NSURL URLWithString:obj]];
        }
        else if ([obj isKindOfClass:[UIImage class]]){
            mwPhotoInfo = [[MWPhoto alloc]initWithImage:obj];
        }
        if (mwPhotoInfo) {
            [_MWPhotoArray addObject:mwPhotoInfo];
        }
    }
}

#pragma mark- MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _MWPhotoArray.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    return [_MWPhotoArray objectAtIndex:index];
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser{
    UIViewController *topVC = [UIWindow topViewController];
    __weak typeof(self) weakSelf = self;
    [topVC dismissViewControllerAnimated:YES completion:^{
        [weakSelf removeFromSuperview];
    }];
}

@end
