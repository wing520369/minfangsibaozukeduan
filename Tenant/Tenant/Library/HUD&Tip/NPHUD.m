//
//  NPHUD.m
//  ShareBey
//
//  Created by HzB on 16/7/26.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import "NPHUD.h"
#import <SVProgressHUD/SVProgressHUD.h>

@implementation NPHUD

+ (instancetype)showHUDAddedTo:(UIView *)view{
    return [self showHUDAddedTo:view msg:nil];
}

+ (instancetype)showHUDAddedTo:(UIView *)view msg:(NSString *)msg
{
    return [self showHUDAddedTo:view msg:msg animated:YES];
}

+ (instancetype)showHUDAddedTo:(UIView *)view msg:(NSString *)msg animated:(BOOL)animated
{
    NPHUD *hud = [NPHUD showHUDAddedTo:view animated:animated];
    if (msg) {
        hud.labelText = msg;
    }
    return hud;
}

//+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view
//{
//    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
//    // 快速显示一个提示信息
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//    hud.labelText = text;
//    // 设置图片
//    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", icon]]];
//    // 再设置模式
//    hud.mode = MBProgressHUDModeCustomView;
//    // 隐藏时候从父控件中移除
//    hud.removeFromSuperViewOnHide = YES;
//    // 1秒之后再消失
//    [hud hide:YES afterDelay:0.7];
//}
//
//
//+ (instancetype)showMessage:(NSString *)message toView:(UIView *)view {
//    if (!view){
//        view = [[UIApplication sharedApplication].windows lastObject];
//    }
//    // 快速显示一个提示信息
//    NPHUD *hud = [NPHUD showHUDAddedTo:view animated:YES];
//    hud.labelText = message;
//    // 隐藏时候从父控件中移除
//    hud.removeFromSuperViewOnHide = YES;
//    // YES代表需要蒙版效果
//    //    hud.dimBackground = YES;
//    return hud;
//}

+ (void)showProgress:(float)progress msg:(NSString *)msg{
    
    if (progress < 1.0) {
        [SVProgressHUD showProgress:progress status:msg];
    }
    else{
        [SVProgressHUD dismiss];
    }
}

@end

#pragma mark - ViewController_HUD

@implementation UIViewController (NPHUD)

- (void)showHud{
    [self showHudWithMsg:nil];
}

- (void)showHudWithMsg:(NSString *)msg
{
//    if ([NSThread isMainThread]) {
//        [NPHUD showHUDAddedTo:self.view msg:msg];
//    }
//    else{
//        dispatch_async(dispatch_get_main_queue(), ^{
//              [NPHUD showHUDAddedTo:self.view msg:msg];
//        });
//    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [NPHUD showHUDAddedTo:self.view msg:msg];
    });
}

- (BOOL)hideHud{
    
//    if ([NSThread isMainThread]) {
//      return  [NPHUD hideHUDForView:self.view animated:YES];
//    }
//    else{
//        dispatch_async(dispatch_get_main_queue(), ^{
//          [NPHUD hideHUDForView:self.view animated:YES];
//        });
//    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [NPHUD hideHUDForView:self.view animated:YES];
    });
    return YES;
}

@end
