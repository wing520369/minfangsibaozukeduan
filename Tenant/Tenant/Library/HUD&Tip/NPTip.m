//
//  NPTip.m
//  Demo0822
//
//  Created by HzB on 16/8/30.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import "NPTip.h"
#import <UIView+Toast.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "UIWindow+WCRCustom.h"

static  NSTimeInterval TipDefalutDelay = 1.5;

@implementation NPTip

+ (void)load{
    //设置SVP 样式
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
    });
}

+ (void)showTip:(NSString *)tip{
  
    [NPTip showTip:tip delay:TipDefalutDelay];
}

+ (void)showTip:(NSString *)tip
          delay:(NSTimeInterval)delay;
{
    [NPTip showTip:tip
             delay:TipDefalutDelay
          position:NPTipPosition_Top];// NPTipPosition_Bottom
}

+ (void)showTip:(NSString *)tip
       position:(NPTipPosition)position
{
    [NPTip showTip:tip
             delay:TipDefalutDelay
          position:position];
}

+ (void)showTip:(NSString *)tip
          delay:(NSTimeInterval)delay
       position:(NPTipPosition)position
{
    id positionObj = nil;
    switch (position) {
        case NPTipPosition_Top: {
            positionObj = CSToastPositionTop;
            break;
        }
        case NPTipPosition_Center: {
            positionObj = CSToastPositionCenter;
            break;
        }
        case NPTipPosition_Bottom: {
            positionObj = CSToastPositionBottom;
            break;
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIWindow currentWindow] makeToast:tip duration:delay position:positionObj];
    });

}

+ (void)showWithStatus:(NPTipStatus)status
                   msg:(NSString *)msg
{
    switch (status) {
        case NPTipStatus_Error:
        case NPTipStatus_Fail:{
            [SVProgressHUD showErrorWithStatus:msg];
            break;
        }
        case NPTipStatus_Success:
        case NPTipStatus_True:{
            [SVProgressHUD showSuccessWithStatus:msg];
            break;
        }
        case NPTipStatus_Prompt:{
            [SVProgressHUD showInfoWithStatus:msg];
            break;
        }
    }
}

@end
