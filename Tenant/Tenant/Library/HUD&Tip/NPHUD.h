//
//  NPHUD.h
//  Demo160726
//
//  Created by HzB on 16/7/28.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface NPHUD : MBProgressHUD

+ (instancetype)showHUDAddedTo:(UIView *)view;

+ (instancetype)showHUDAddedTo:(UIView *)view
                           msg:(NSString *)msg;

+ (instancetype)showHUDAddedTo:(UIView *)view
                           msg:(NSString *)msg
                      animated:(BOOL)animated;


/**
 *  显示进度条
 *
 *  @param progress 百分比
 *  @param msg      提示语
 */
+ (void)showProgress:(float)progress
                 msg:(NSString*)msg;

@end

#pragma mark - ViewController_HUD
@interface UIViewController (NPHUD)

- (void)showHud;

- (void)showHudWithMsg:(NSString *)msg;

- (BOOL)hideHud;

@end


