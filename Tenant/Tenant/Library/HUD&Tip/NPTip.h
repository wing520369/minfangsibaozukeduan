//
//  NPTip.h
//  Demo0822
//
//  Created by HzB on 16/8/30.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,NPTipPosition){
    /** 顶部*/
    NPTipPosition_Top,
    /** 中部*/
    NPTipPosition_Center,
    /** 底部*/
    NPTipPosition_Bottom,
};


typedef NS_ENUM(NSInteger,NPTipStatus){
    
    /** 成功*/
    NPTipStatus_Success = 10,
    /** 失败*/
    NPTipStatus_Fail,
    /** 错误*/
    NPTipStatus_Error,
    /** 正确*/
    NPTipStatus_True,
    /** 提示*/
    NPTipStatus_Prompt,
};

/**
 *  提示语
 */
@interface NPTip : NSObject

//显示提示语，3秒后消失,键盘弹起时再顶部，其余在底部
+ (void)showTip:(NSString *)tip;

/**
 *  显示提示语，几秒后消失
 *
 *  @param tip   提示语
 *  @param delay 秒数
 */
+ (void)showTip:(NSString *)tip
          delay:(NSTimeInterval)delay;

/**
 *  显示提示语,3秒后消失
 *
 *  @param tip      提示语
 *  @param position 位置
 */
+ (void)showTip:(NSString *)tip
       position:(NPTipPosition)position;

/**
 *  显示提示语
 *
 *  @param tip      提示语
 *  @param delay    秒数
 *  @param position 位置
 */
+ (void)showTip:(NSString *)tip
          delay:(NSTimeInterval)delay
       position:(NPTipPosition)position;



/**
 *  显示某个状态的提示框
 *
 *  @param status 状态
 *  @param msg    提示语
 */
+ (void)showWithStatus:(NPTipStatus)status
                   msg:(NSString *)msg;

@end
