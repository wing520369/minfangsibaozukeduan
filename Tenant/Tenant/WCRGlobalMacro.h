//
//  WCRGlobalMacro.h
//  WCRCustomProject
//
//  Created by eliot on 2017/4/19.
//  Copyright © 2017年 aleven. All rights reserved.
//

/// 全局 宏
/// 常用的尺寸，颜色

#ifndef WCRGlobalMacro_h
#define WCRGlobalMacro_h

/// 服务器的链接
#define SERVERCE_HOST @"http://119.23.127.189:80/rent"

#define kMainWidth    [[UIScreen mainScreen] bounds].size.width
#define kMainHeight   [[UIScreen mainScreen] bounds].size.height

/// 自动适配宽度,width为原始iphone4/5的宽度
#define AUTO_MATE_WIDTH(width) ((width) * kMainWidth / 320.0)
/// 自动适配宽度,width为原始iphone4/5的高度
#define AUTO_MATE_HEIGHT(height) ((height) * kMainHeight / 568.0)

/// 一般的按钮，单行Cell的高度
#define kDefaultHeight 45.0
/// 距离屏幕左边的距离
#define kSpaceX AUTO_MATE_WIDTH(12)

/// 右边箭头的宽高（一般用于Cell）
#define kArrowImageHeight 14
#define kArrowImageWidth 14

/// 默认占位图名
#define kPlaceholderImageName @"logo"

/// 控制器默认的背景颜色
#define kViewControllerBgColor @"#F2F2F2"
/// 导航栏的颜色
#define kNavigationBarColor @"#37C0BA"
/// 灰色
#define kCustomGrayColor @"#666666"
/// 蓝色
#define kCustomBlueColor @"#39aee8"
/// app主调色：深橙色
#define kMainColor @"#eb6818"

/// 在调试模式（DEBUG）输出日志，发布应用就没有了
#ifdef DEBUG
#define WCRLog(...) NSLog(__VA_ARGS__)
#else
#define WCRLog(...)
#endif

#endif /* WCRGlobalMacro_h */
