//
//  WCRLoginViewModel.m
//  WCRCustomProject
//
//  Created by eliot on 2017/4/19.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRLoginViewModel.h"
#import "NPTip.h"
#import "RegExpValidate.h"
#import "NPGeneralApi.h"
#import "NSString+JKHash.h"
#import "Tools.h"
#import "WCRUserModel.h"

#import "RACSignal.h"
#import "RACSubscriber.h"
#import "RACCommand.h"

@implementation WCRLoginViewModel

- (instancetype)init {
    if (self = [super init]) {
        [self initialBind];
    }
    return self;
}

- (void)initialBind {
    
    @weakify(self);
    _command = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        @strongify(self);
        return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
           
            BOOL flag = [self checkPhoneAndPassword];
            if (flag) {
                // 可以执行登录动作
                NSString *password = self.password;
                NSDictionary *dict = @{@"phone":self.phone,@"password":[password.jk_sha1String uppercaseString],@"tokenName":[Tools tokenName]};
                [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/loginApiController/appTenantLogin" ResponseBlock:^(NPGeneralResponse *responseMd) {
                    if (responseMd.isOK) {
                        WCRUserModel *userModel = [WCRUserModel jsonWithDictionary:responseMd.response];
                        // 保存未加密的密码
                        userModel.noSecPwd = password;
                        [subscriber sendNext:userModel];
                        [subscriber sendCompleted];
                    }
                    else {
                        [subscriber sendError:nil];
                    }
                }];
            }
            else {
                [subscriber sendError:nil];
            }
            
            return nil;
        }];
    }];
}

- (BOOL)checkPhoneAndPassword {
    // 1.手机号是否为空
    if (!self.phone.length) {
        [NPTip showTip:@"请输入手机号"];
        return NO;
    }
    if (![RegExpValidate validateMobile:self.phone]) {
        [NPTip showTip:@"手机号码格式不正确"];
        return NO;
    }
    if (!_password.length) {
        [NPTip showTip:@"请输入密码"];
        return NO;
    }
    if (_password.length < 6 || _password.length > 16) {
        [NPTip showTip:@"密码应该为6到16位"];
        return NO;
    }
    return YES;
}

@end
