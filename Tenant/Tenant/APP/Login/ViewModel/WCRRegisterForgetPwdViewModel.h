//
//  WCRRegisterForgetPwdViewModel.h
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ViewModelType) {
    ViewModelTypeRegister = 336,
    ViewModelTypeForgetPwd,
};

@class RACSignal, RACCommand;
@interface WCRRegisterForgetPwdViewModel : NSObject
@property (nonatomic, assign) ViewModelType type;
/// 手机号
@property (nonatomic, copy) NSString *phone;
/// 验证码
@property (nonatomic, copy) NSString *code;
/// 密码
@property (nonatomic, copy) NSString *password;
/// 确认密码
@property (nonatomic, copy) NSString *rePassword;
/// 推荐人手机号
@property (nonatomic, copy) NSString *recommendPhone;
/// 确定修改密码 Or 注册 的命令
@property (nonatomic, strong) RACCommand *command;
/// 获取验证码的命令
@property (nonatomic, strong) RACCommand *codeCommand;

@end
