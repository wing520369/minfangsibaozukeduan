//
//  WCRLoginViewModel.h
//  WCRCustomProject
//
//  Created by eliot on 2017/4/19.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACCommand;
@interface WCRLoginViewModel : NSObject
/// 是否可以登录命令
@property (nonatomic, strong) RACCommand *command;
/// 账号
@property (nonatomic, copy) NSString *phone;
/// 密码
@property (nonatomic, copy) NSString *password;
@end
