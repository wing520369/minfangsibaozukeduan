//
//  WCRRegisterForgetPwdViewModel.m
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRegisterForgetPwdViewModel.h"
#import "NPTip.h"
#import "RegExpValidate.h"
#import "NPGeneralApi.h"
#import "NSString+JKHash.h"
#import "Tools.h"
#import "RACSignal.h"
#import "RACSignal+Operations.h"
#import "NSObject+RACPropertySubscribing.h"
#import "RACCommand.h"
#import "RACSubscriber.h"

@interface WCRRegisterForgetPwdViewModel ()
/// 保存获取验证码的手机号，防止获取验证码后修改手机号，与后台不匹配
@property (nonatomic, strong) NSString *finallyPhone;
@end

@implementation WCRRegisterForgetPwdViewModel

- (instancetype)init {
    if (self = [super init]) {
        [self initialBind];
    }
    return self;
}

- (void)initialBind {
    
    @weakify(self);
    
    // 获取验证码的命令
    _codeCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
            @strongify(self);
            if ([self checkPhone]) {
                // 获取验证码
                self.finallyPhone = self.phone;
                NSString *type = self.type == ViewModelTypeRegister ? @"1" : @"2";
                NSDictionary *dict = @{@"phone":self.finallyPhone,@"type":type};
                [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/loginApiController/smsCode" ResponseBlock:^(NPGeneralResponse *responseMd) {
                    if (responseMd.isOK) {
                        [subscriber sendNext:@(YES)];
                    }
                    else {
                        [subscriber sendNext:@(NO)];
                    }
                    [subscriber sendCompleted];
                }];
            }
            else {
                [subscriber sendError:nil];
            }
            return nil;
        }];
    }];
    
    _command = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
            @strongify(self);
            if ([self checkComplete]) {
                NSString *url = nil;
                NSDictionary *dict = nil;
                // userType 用户类型，必填 1房东 2租客
                if (self.type == ViewModelTypeRegister) {
                    // refPhone
                    NSString *refPhone = self.recommendPhone.length ? self.recommendPhone : @"";
                    url = @"/api/loginApiController/regist";
                    dict = @{@"phone":self.finallyPhone,@"code":self.code,@"userType":@"2",@"password":[self.password.jk_sha1String uppercaseString],@"refPhone":refPhone};
                }
                else {
                    url = @"/api/loginApiController/resetPassword";
                    dict = @{@"phone":self.finallyPhone,@"code":self.code,@"tokenName":[Tools tokenName],@"password":[self.password.jk_sha1String uppercaseString]};
                }
                
                [NPGeneralApi WCRApiWithParameters:dict tailUrl:url ResponseBlock:^(NPGeneralResponse *responseMd) {
                    if (responseMd.isOK) {
                        NSString *showTitle = self.type == ViewModelTypeRegister ? @"注册成功，请重新登录" : @"重新设置密码成功，请重新登录";
                        [NPTip showTip:showTitle];
                        [subscriber sendNext:@(YES)];
                    }
                    [subscriber sendCompleted];
                }];
            }
            else {
                [subscriber sendError:nil];
            }
            return nil;
        }];
    }];
    
}

- (BOOL)checkPhone {
    if (!self.phone.length) {
        [NPTip showTip:@"请输入手机号码"];
        return NO;
    }
    if (![RegExpValidate validateMobile:self.phone]) {
        [NPTip showTip:@"手机号码格式不正确"];
        return NO;
    }
    return YES;
}

- (BOOL)checkComplete {
    
    if (!self.finallyPhone.length) {
        [NPTip showTip:@"请先获取验证码"];
        return NO;
    }
    if (!_code.length) {
        [NPTip showTip:@"请输入验证码"];
        return NO;
    }
    if (!_password.length) {
        [NPTip showTip:@"请输入密码"];
        return NO;
    }
    if (_password.length < 6 || _password.length > 16) {
        [NPTip showTip:@"密码应该为6到16位"];
        return NO;
    }
    if (![_password isEqualToString:_rePassword]) {
        [NPTip showTip:@"两次输入的密码不一致"];
        return NO;
    }
    if (self.type == ViewModelTypeRegister) {
        if (self.recommendPhone.length > 0) {
            if (![RegExpValidate validateMobile:self.recommendPhone]) {
                [NPTip showTip:@"推荐人手机号码格式不正确"];
                return NO;
            }
        }
    }
    return YES;
}

@end
