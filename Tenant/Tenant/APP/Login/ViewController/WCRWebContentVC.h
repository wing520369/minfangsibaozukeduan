//
//  WCRWebContentVC.h
//  Tenant
//
//  Created by HzB on 2017/10/12.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

@interface WCRWebContentVC : WCRBasicViewController

@property (nonatomic,copy) NSString *text;
@property (nonatomic,copy) NSString *url;

@end
