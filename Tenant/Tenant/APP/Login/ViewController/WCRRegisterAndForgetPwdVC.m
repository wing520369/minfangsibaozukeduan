//
//  WCRRegisterAndForgetPwdVC.m
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRegisterAndForgetPwdVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UITextField+WCRCustom.h"
#import "UILabel+YBAttributeTextTapAction.h"
#import "WCRAppointmentVC.h"
#import "WCRRegisterForgetPwdViewModel.h"
#import "NSString+WCRCustom.h"
#import "NSMutableAttributedString+WCRCustom.h"
#import "RACSignal.h"
#import "RACDisposable.h"
#import "UITextField+RACSignalSupport.h"
#import "UIControl+RACSignalSupport.h"
#import "RACCommand.h"
#import "RACSubscriptingAssignmentTrampoline.h"
#import "MSWeakTimer.h"
#import "RACSignal+Operations.h"
#import "RACScheduler.h"
#import "NPGeneralApi.h"
#import "MSWeakTimer.h"

#import "WCRWebContentVC.h"
#import "YYText.h"

@interface WCRRegisterAndForgetPwdVC ()
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *codeTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *rePasswordTextField;
@property (nonatomic, strong) UIButton *getCodeButton;
@property (nonatomic, strong) UIButton *completeButton;
@property (nonatomic, strong) WCRRegisterForgetPwdViewModel *viewModel;
@property (nonatomic, strong) UITextField *recommendPhoneTF;
@property (nonatomic, strong) MSWeakTimer *timer;
@property (nonatomic, assign) NSInteger time;
@end

@implementation WCRRegisterAndForgetPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.time = 60;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    NSString *title = _type == ViewControllerTypeRegister ? @"注册账号" : @"修改密码";
    [self addLeftGoBackItemWithTitle:title];
    [self createUI];
    [self bindViewModel];
}

- (void)bindViewModel {
    
    @weakify(self);
    
    _viewModel = [[WCRRegisterForgetPwdViewModel alloc] init];
    _viewModel.type = _type == ViewControllerTypeRegister ? ViewModelTypeRegister : ViewModelTypeForgetPwd;
    
    [_phoneTextField.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        self.viewModel.phone = x;
        [self.phoneTextField textFieldlimitInputWithNumber:11 AlertMessage:@"手机号最多只能输入11位"];
    }];
    RAC(self.viewModel, code) = _codeTextField.rac_textSignal;
    [_passwordTextField.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        self.viewModel.password = x;
        [self.passwordTextField textFieldlimitInputWithNumber:16 AlertMessage:@"密码最多只能输入16位"];
    }];
    [_rePasswordTextField.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        self.viewModel.rePassword = x;
        [self.passwordTextField textFieldlimitInputWithNumber:16 AlertMessage:@"密码最多只能输入16位"];
    }];
    
    if (_type == ViewControllerTypeRegister) {
        RAC(self.viewModel, recommendPhone) = _recommendPhoneTF.rac_textSignal;
    }
    
    /**/
    // 获取验证码
    [[_getCodeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {
        @strongify(self);
        [self.view endEditing:YES];
        [[self.viewModel.codeCommand execute:nil] subscribeNext:^(id  _Nullable x) {
            if ([x boolValue] == YES) {
                // 获取验证码请求成功，倒计时
                button.enabled = NO;
                [self racTimer];
            }
        }];
    }];
    
    
    // 修改密码 or 注册
    [[_completeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        [self.view endEditing:YES];
        [[self.viewModel.command execute:nil] subscribeNext:^(id  _Nullable x) {
            if ([x boolValue] == YES) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }];
    }];
}

- (void)racTimer {
    self.timer = [MSWeakTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(forgetOrRegisterCutTimeDown:) userInfo:nil repeats:YES dispatchQueue:dispatch_get_main_queue()];
}
- (void)forgetOrRegisterCutTimeDown:(MSWeakTimer *)timer {
    [_getCodeButton setTitle:[NSString stringWithFormat:@"%ld",(long)self.time] forState:UIControlStateDisabled];
    self.time--;
    if (self.time < 0) {
        [timer invalidate];
        timer = nil;
        self.time = 60;
        [_getCodeButton setTitle:@"重新获取" forState:UIControlStateDisabled];
        [_getCodeButton setTitle:@"重新获取" forState:0];
        _getCodeButton.enabled = YES;
    }
}

- (void)createUI {
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 15, kMainWidth, 0.1)];
    contentView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:contentView];
    
    CGFloat border = 10;
    CGFloat y = border;
    UIFont *font = [UIFont CustomFontWithSize:15.0];
    // 手机号
    _phoneTextField = [self getTextFieldWithTitle:@"手机号码" Placeholder:@"输入手机号码" Ypoint:y Font:font SuperView:contentView];
    _phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    // 验证码
    y = _phoneTextField.bottom_wcr + border;
    _codeTextField = [self getTextFieldWithTitle:@"验证码" Placeholder:@"输入验证码" Ypoint:y Font:font SuperView:contentView];
    _codeTextField.keyboardType = UIKeyboardTypeNumberPad;
    // 右视图发送验证码
    _getCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _getCodeButton.frame = CGRectMake(0, 5, 75, _codeTextField.height_wcr - 10);
    [_getCodeButton buttonWithTitleColor:[UIColor colorWithHexString:kMainColor] Background:nil Font:[UIFont CustomFontWithSize:12.0] Title:@"发送验证码"];
    [_getCodeButton setLayerWithBorderWidth:1.0 Corner:3.0 BorderColor:[UIColor colorWithHexString:kMainColor]];
    _codeTextField.rightView = _getCodeButton;
    _codeTextField.rightViewMode = UITextFieldViewModeAlways;
    
    y = _codeTextField.bottom_wcr + border;
    _passwordTextField = [self getTextFieldWithTitle:@"新密码" Placeholder:@"密码6-16位" Ypoint:y Font:font SuperView:contentView];
    _passwordTextField.secureTextEntry = YES;
    
    y = _passwordTextField.bottom_wcr + border;
    _rePasswordTextField = [self getTextFieldWithTitle:@"确认新密码" Placeholder:@"密码6-16位" Ypoint:y Font:font SuperView:contentView];
    _rePasswordTextField.secureTextEntry = YES;
    
    y = _rePasswordTextField.maxY_wcr;
    // 注册协议
    if (_type == ViewControllerTypeRegister) {
        y += border;
        _recommendPhoneTF = [self getTextFieldWithTitle:@"推广人手机号" Placeholder:@"请输入推广人手机号（选填）" Ypoint:y Font:font SuperView:contentView];
        _recommendPhoneTF.keyboardType = UIKeyboardTypeNumberPad;
        
        y = _recommendPhoneTF.bottom_wcr + 8;
        
        
        CGFloat fontsize = 15.0;
        NSString *text = @"注册账号即表示我同意民房四宝的服务条款、非歧视政策、支付服务条款、隐私政策、客房退款政策和房东保障金计划条款。";
        NSArray *textList = @[@"服务条款",@"非歧视政策",@"支付服务条款",@"隐私政策",@"客房退款政策和房东保障金计划条款"];
        NSMutableAttributedString *attText = [[NSMutableAttributedString alloc]initWithString:text];
        attText.yy_font = [UIFont systemFontOfSize:fontsize];
        attText.yy_lineSpacing = 2.0;
        attText.yy_color = [UIColor blackColor];
        
        NSArray *urlList = @[@"/api/publicApiController/getService",
                             @"/api/publicApiController/getNonDiscrimination",
                             @"/api/publicApiController/getPayment",
                             @"/api/publicApiController/getPrivacy",
                              @"/api/publicApiController/getRefund"];
         __weak typeof(self) weakSelf = self;
        for (NSString *subText in textList) {
            NSRange range = [text rangeOfString:subText];
            [attText yy_setTextHighlightRange:range color:[UIColor colorWithRGB:50 G:164 B:233] backgroundColor:[UIColor colorWithRGB:40 G:154 B:223] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                
                NSString *titleText = [text.string substringWithRange:range];
                NSInteger index = [textList indexOfObject:titleText];
                WCRWebContentVC *vc = [[WCRWebContentVC alloc]init];
                vc.text = titleText;
                vc.url = [urlList objectAtIndex:index];
                [weakSelf.navigationController pushViewController:vc animated:YES];
//                NSLog(@"tap text range:%@ \n %@",text.string,[text.string substringWithRange:range]);
            }];
        }

        YYLabel *label = [[YYLabel alloc]initWithFrame:CGRectMake(_rePasswordTextField.x_wcr, y, _rePasswordTextField.width_wcr, 0)];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.font = [UIFont systemFontOfSize:fontsize];
        label.textColor = [UIColor blackColor];


        label.attributedText = attText;
        CGSize size = CGSizeMake(label.width_wcr, CGFLOAT_MAX);
        YYTextLayout *layout = [YYTextLayout layoutWithContainerSize:size text:attText];
        label.height_wcr = layout.textBoundingSize.height;
        label.textLayout = layout;
        
        [contentView addSubview:label];
        y = label.bottom_wcr + 10.0;


//        UILabel *agreementLabel = [[UILabel alloc] initWithFrame:CGRectMake(_rePasswordTextField.x_wcr, y, _rePasswordTextField.width_wcr, 37)];
//        [agreementLabel labelWithTextColor:[UIColor blackColor] Font:[UIFont CustomFontWithSize:13.0] BackgroundColor:nil Text:nil];
//        agreementLabel.numberOfLines = 0;
//        
//        NSString *text = @"注册账号即表示我同意民房四宝的服务条款、非歧视政策、支付服务条款、隐私政策、客房退款政策和房东保障金计划条款。";
//        CGFloat labelHeight = [NSString calculateWithContent:text MaxWidth:agreementLabel.width_wcr Font:agreementLabel.font];
//        agreementLabel.height_wcr = labelHeight;
//        
//        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:text];
//        NSArray *textList = @[@"服务条款",@"非歧视政策",@"支付服务条款",@"隐私政策",@"客房退款政策和房东保障金计划条款"];
//        for (NSString *subText in textList) {
//            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRGB:50 G:164 B:233] range:[text rangeOfString:subText]];
//        }
//        agreementLabel.attributedText = attributedString;
//     
//        [contentView addSubview:agreementLabel];
//        y = agreementLabel.bottom_wcr + 5;
//        
//        __weak typeof(self) weakSelf = self;
//        NSArray *urlList = @[@"/api/publicApiController/getService",
//                             @"/api/publicApiController/getNonDiscrimination ",
//                             @"/api/publicApiController/getRefund ",
//                             @"/api/publicApiController/getPayment",
//                             @"/api/publicApiController/getPrivacy"];
//        
//        [agreementLabel yb_addAttributeTapActionWithStrings:textList tapClicked:^(NSString *string, NSRange range, NSInteger index) {
//    
//            WCRWebContentVC *vc = [[WCRWebContentVC alloc]init];
//            vc.text = [textList objectAtIndex:index];
//            vc.url = [urlList objectAtIndex:index];
//            [weakSelf.navigationController pushViewController:vc animated:YES];
////            WCRAppointmentVC *appointmentVC = [[WCRAppointmentVC alloc] init];
////            [weakSelf.navigationController pushViewController:appointmentVC animated:YES];
//        }];
    }
    
    contentView.height_wcr = y;
    
    y = contentView.bottom_wcr + 30;
    _completeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _completeButton.frame = CGRectMake(_phoneTextField.x_wcr, y, _phoneTextField.width_wcr, kDefaultHeight);
    NSString *buttonTitle = _type == ViewControllerTypeRegister ? @"注册" : @"提交";
    [_completeButton buttonWithTitleColor:[UIColor whiteColor] Background:[UIColor colorWithHexString:kCustomBlueColor] Font:font Title:buttonTitle];
    [_completeButton setViewCorner:kDefaultHeight / 2.0];
    [scrollView addSubview:_completeButton];
    
    if (_completeButton.bottom_wcr > scrollView.height_wcr) {
        scrollView.contentSize = CGSizeMake(kMainWidth, _completeButton.bottom_wcr + 30);
    }
}
- (UITextField *)getTextFieldWithTitle:(NSString *)title Placeholder:(NSString *)placeholder Ypoint:(CGFloat)yPoint Font:(UIFont *)font SuperView:(UIView *)superView {
    // 标题
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, yPoint, superView.width_wcr - 40, 20)];
    [label labelWithTextColor:[UIColor blackColor] Font:[UIFont CustomFontWithSize:15.0] BackgroundColor:nil Text:title];
    [superView addSubview:label];
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(label.x_wcr, label.bottom_wcr, label.width_wcr, 40)];
    [textField textFieldWithPlaceholder:placeholder Font:[UIFont CustomFontWithSize:15.0]];
    // 线
    UIView *lineView = [UIView lineView];
    lineView.frame = CGRectMake(0, textField.height_wcr - 1, textField.width_wcr, 1);
    [textField addSubview:lineView];
    [superView addSubview:textField];
    
    return textField;
}

@end
