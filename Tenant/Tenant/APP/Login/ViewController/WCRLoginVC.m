//
//  WCRLoginVC.m
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRLoginVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UITextField+WCRCustom.h"
#import "WCRTabBarController.h"
#import "WCRRegisterAndForgetPwdVC.h"
#import "WCRLoginViewModel.h"
#import "RACSignal.h"
#import "RACDisposable.h"
#import "UITextField+RACSignalSupport.h"
#import "UIControl+RACSignalSupport.h"
#import "RACCommand.h"
#import "RACSubscriptingAssignmentTrampoline.h"

@interface WCRLoginVC ()
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UIButton *loginButton;
@property (nonatomic, strong) UIButton *registerButton;
@property (nonatomic, strong) UIButton *forgetPwdButton;
@property (nonatomic, strong) WCRLoginViewModel *loginViewModel;
@end

@implementation WCRLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"登录"];
    [self createUI];
    [self bindViewModel];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
- (void)bindViewModel {
    _loginViewModel = [[WCRLoginViewModel alloc] init];
    @weakify(self);
    [_phoneTextField.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        self.loginViewModel.phone = x;
        [self.phoneTextField textFieldlimitInputWithNumber:11 AlertMessage:@"手机号最多只能输入11位"];
    }];
    [_passwordTextField.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        self.loginViewModel.password = x;
        [self.passwordTextField textFieldlimitInputWithNumber:16 AlertMessage:@"密码最多只能输入16位"];
    }];
    
    // 登录
    [[_loginButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        [self.view endEditing:YES];
        // 执行登录的命令
        [[self.loginViewModel.command execute:nil] subscribeNext:^(WCRUserModel *userModel) {
            if (userModel && userModel.ID.length) {
                // 登录成功
                // 保存用户信息
                [WCRUserModelTool saveUserModel:userModel UserID:userModel.ID AccountName:userModel.phone Password:userModel.noSecPwd LoginType:kAccoutLogin_Type];
                WCRTabBarController *tabBarController = [[WCRTabBarController alloc] init];
                [[UIApplication sharedApplication] keyWindow].rootViewController = tabBarController;
            }
        }];
    }];
    // 注册
    [[_registerButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        [self skipToRegisterOrModifyPwd:ViewControllerTypeRegister];
    }];
    // 忘记密码
    [[_forgetPwdButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        [self skipToRegisterOrModifyPwd:ViewControllerTypeForgetPwd];
    }];
}
- (void)skipToRegisterOrModifyPwd:(ViewControllerType)type {
    WCRRegisterAndForgetPwdVC *forgetVC = [[WCRRegisterAndForgetPwdVC alloc] init];
    forgetVC.type = type;
//    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:forgetVC];
//    [self presentViewController:navigation animated:YES completion:nil];
    [self.navigationController pushViewController:forgetVC animated:YES];
}

- (void)createUI {
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bgImageView imageViewWithImageName:@"登录bg"];
    [self.view addSubview:bgImageView];
    
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    // logo
    UIImage *image = [UIImage imageFileNamed:@"logo"];
    CGFloat width = (320 / 750.0) * kMainWidth;
    CGFloat height = [image InProportionAtWidth:width];
    CGFloat x = (kMainWidth - width) / 2.0;
    CGFloat y = (147 / 1334.0) * kMainHeight;
    UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [logoImageView setContentModeToScaleAspectFill];
    logoImageView.image = image;
    [scrollView addSubview:logoImageView];
    
    UIFont *font = [UIFont CustomFontWithSize:15.0];
    // 手机号
    y = logoImageView.bottom_wcr + 20;
    _phoneTextField = [self getTextFieldWithTitle:@"手机号码" Placeholder:@"输入手机号码" Ypoint:y Font:font SuperView:scrollView];
    _phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    y = _phoneTextField.bottom_wcr + 20;
    _passwordTextField = [self getTextFieldWithTitle:@"密码" Placeholder:@"密码6-16位" Ypoint:y Font:font SuperView:scrollView];
    _passwordTextField.secureTextEntry = YES;
    // 添加右视图：忘记密码
    _forgetPwdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _forgetPwdButton.frame = CGRectMake(0, 0, 80, _passwordTextField.height_wcr);
    [_forgetPwdButton buttonWithTitleColor:[UIColor whiteColor] Background:nil Font:[UIFont CustomFontWithSize:13.0] Title:@"忘记密码?"];
    _forgetPwdButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    _passwordTextField.rightView = _forgetPwdButton;
    _passwordTextField.rightViewMode = UITextFieldViewModeAlways;
    
    // 登录
    y = _passwordTextField.bottom_wcr + 30;
    x = _passwordTextField.x_wcr;
    width = _passwordTextField.width_wcr;
    height = kDefaultHeight;
    _loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _loginButton.frame = CGRectMake(x, y, width, height);
    [_loginButton buttonWithTitleColor:[UIColor colorWithHexString:kMainColor] Background:[UIColor whiteColor] Font:font Title:@"登录"];
    [_loginButton setViewCorner:height / 2.0];
    [scrollView addSubview:_loginButton];
    
    // 注册
    y = _loginButton.bottom_wcr + 15;
    x = _passwordTextField.x_wcr;
    width = _passwordTextField.width_wcr;
    height = kDefaultHeight;
    _registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _registerButton.frame = CGRectMake(x, y, width, height);
    [_registerButton buttonWithTitleColor:[UIColor whiteColor] Background:[UIColor colorWithHexString:kCustomBlueColor] Font:font Title:@"注册"];
    [_registerButton setViewCorner:height / 2.0];
    [scrollView addSubview:_registerButton];
    
    if (_registerButton.bottom_wcr > scrollView.height_wcr + 30) {
        scrollView.contentSize = CGSizeMake(kMainWidth, _registerButton.bottom_wcr + 30);
    }
    
    
    // 返回按钮
    image = [UIImage imageNamed:@"0522icon_xiangzuo"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 20, 50, 40);
    [button setImage:image forState:0];
    [button.imageView setContentMode:UIViewContentModeScaleAspectFill];
    [button addTarget:self action:@selector(leftBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    button.adjustsImageWhenHighlighted = NO;
    [self.view addSubview:button];
    
}
- (void)leftBarButtonItemAction:(UIButton *)button {
    [self.view endEditing:YES];
    if (self.backToHomePage) {
        WCRTabBarController *tabBarController = [[WCRTabBarController alloc] init];
        [[UIApplication sharedApplication] keyWindow].rootViewController = tabBarController;
    }
    else {
        if (self.navigationController) {
            if (self.navigationController.viewControllers.count > 1) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
- (UITextField *)getTextFieldWithTitle:(NSString *)title Placeholder:(NSString *)placeholder Ypoint:(CGFloat)yPoint Font:(UIFont *)font SuperView:(TPKeyboardAvoidingScrollView *)superView {
    // 标题
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, yPoint, superView.width_wcr - 40, 20)];
    [label labelWithTextColor:[UIColor whiteColor] Font:[UIFont CustomFontWithSize:15.0] BackgroundColor:nil Text:title];
    [superView addSubview:label];
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(label.x_wcr, label.bottom_wcr, label.width_wcr, 40)];
    [textField textFieldWithPlaceholder:placeholder Font:[UIFont CustomFontWithSize:15.0]];
    [textField setPlaceholderColor:[UIColor colorWithRGB:244 G:190 B:154]];
    // 线
    UIView *lineView = [UIView lineView];
    lineView.backgroundColor = [UIColor whiteColor];
    lineView.frame = CGRectMake(0, textField.height_wcr - 1, textField.width_wcr, 1);
    [textField addSubview:lineView];
    [superView addSubview:textField];
    // 字体显示白色
    textField.textColor = [UIColor whiteColor];
    
    return textField;
}

@end
