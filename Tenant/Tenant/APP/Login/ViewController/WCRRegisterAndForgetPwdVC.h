//
//  WCRRegisterAndForgetPwdVC.h
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

typedef NS_ENUM(NSInteger, ViewControllerType) {
    ViewControllerTypeRegister = 237,
    ViewControllerTypeForgetPwd,
};

/// 注册和忘记密码
@interface WCRRegisterAndForgetPwdVC : WCRBasicViewController
@property (nonatomic, assign) ViewControllerType type;
@end
