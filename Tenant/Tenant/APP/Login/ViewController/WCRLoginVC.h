//
//  WCRLoginVC.h
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

@interface WCRLoginVC : WCRBasicViewController
/// 返回到首页
@property (nonatomic, assign) BOOL backToHomePage;
@end
