//
//  WCRAppointmentVC.m
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRAppointmentVC.h"

@interface WCRAppointmentVC ()

@end

@implementation WCRAppointmentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"服务条款"];
    //设置协议内容
    [self setupAgreement];
}

#pragma mark - 设置协议内容

- (void)setupAgreement {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"用户注册协议.txt" ofType:nil];
    
    NSString *agreemContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    textView.text = agreemContent;
    textView.font = [UIFont CustomFontWithSize:14.0];
    textView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:textView];
    
}

@end
