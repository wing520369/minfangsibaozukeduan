//
//  WCRMyVC.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyVC.h"
#import "WCRMyInfoCell.h"
#import "WCRMyModel.h"
#import "UIImageView+WebCache.h"
#import "WCRMyInfoDetailVC.h"
#import "UINavigationController+WCRCustom.h"
#import "UIScrollView+NPRefresh.h"
#import "NPGeneralApi.h"
#import "WCRIntegralMallVC.h"
#import "Tools.h"

typedef NS_ENUM(NSInteger, ButtonActionType) {
    ButtonActionTypeMessageTip = 1005,
    ButtonActionTypeSystemSetting,
};

static NSString *identifier = @"cell";

@interface WCRMyVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) UILabel *nickNameLabel;
@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *buttons;
@property (nonatomic, strong) UIView *tipRedPointView;
@end

@implementation WCRMyVC

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _buttons = [NSMutableArray arrayWithCapacity:5];
    _dataSource = [WCRMyModel getMyInfoModels];
    [self createUI];
    [self updateUserInfoShow];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserInfo:) name:@"updateUserInfo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserInfo:) name:@"updateUserAccount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccessUpdateUserInfo:) name:@"paySuccessUpdateUserInfo" object:nil];
}
- (void)updateUserInfo:(NSNotification *)noti {
    [self updateUserInfoShow];
}
- (void)paySuccessUpdateUserInfo:(NSNotification *)noti {
    [self getUserInfoFromNetwork];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.mj_header.hidden = [WCRUserModelTool isLogin] ? NO : YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self getHaveNoReadInfoFromNetwork];
    [self.navigationController setDefaultStyle];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
- (void)getHaveNoReadInfoFromNetwork {
   
    if (![WCRUserModelTool isLogin] || [WCRUserModelTool getUserId].length < 1) {
        self.tipRedPointView.hidden = YES;
        return;
    }
    
    @weakify(self);
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiNoShowMessageViewWithParameters:dict tailUrl:@"/api/tenantApiController/getSendMessageIsRead" ResponseBlock:^(NPGeneralResponse *responseMd) {
        @strongify(self);
        if ([responseMd.response isKindOfClass:[NSDictionary class]]) {
            NSInteger value = [[responseMd.response objectForKey:@"value"] integerValue];
        
            if (value > 0) {
                // 显示红点
                self.tipRedPointView.hidden = NO;
            }
            else {
                self.tipRedPointView.hidden = YES;
            }
        }
    }];
}
- (void)updateUserInfoShow {
    // 刷新个人信息
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    [_dataSource enumerateObjectsUsingBlock:^(WCRMyModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.type == MyInfoTypeMyAccount) {
            obj.content = [NSString stringWithFormat:@"%.2f",[userModel.amount floatValue]];
        }
        else if (obj.type == MyInfoTypeMerchant) {
            obj.content = userModel.score;
        }
    }];
    [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:userModel.avatar] placeholderImage:[UIImage imageFileNamed:@"icon_geren"]];
    _nickNameLabel.text = userModel.nickname.length ? userModel.nickname : userModel.phone;
    for (NSInteger i = 0; i < _buttons.count; i++) {
        UIButton *button = [_buttons objectAtIndex:i];
        button.selected = i < [userModel.level integerValue] ? YES : NO;
    }
    [_tableView reloadData];
}
- (void)createUI {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 49) style:UITableViewStylePlain];
    _tableView = tableView;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    [tableView registerClass:[WCRMyInfoCell class] forCellReuseIdentifier:identifier];
    // 头视图
    CGFloat height = kMainWidth * (440 / 750.0);
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, height)];
    bgImageView.userInteractionEnabled = YES;
    [bgImageView imageViewWithImageName:@"userinfobg"];
    
    // 头像
    height = height * (130 / 375.0);
    CGFloat width = height;
    CGFloat x = (kMainWidth - width) / 2.0;
    CGFloat y = (bgImageView.height_wcr - height) / 2.0;
    UIImageView *avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [avatarImageView setContentModeToScaleAspectFill];
    avatarImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAvatar:)];
    [avatarImageView addGestureRecognizer:tap];
    [avatarImageView setViewCorner:height / 2.0];
    _avatarImageView = avatarImageView;
    [bgImageView addSubview:avatarImageView];
    
    // 等级
    CGFloat border = 0;
    CGFloat left = 8;
    width = (avatarImageView.width_wcr - border * 4 - left * 2) / 5.0;
    height = width;
    x = avatarImageView.x_wcr + left;
    y = avatarImageView.y_wcr - height - 8;
    for (NSInteger i = 0; i < 5; i++) {
        UIButton *startButton = [[UIButton alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [startButton buttonWithImageName:@"icon_stars_default" SelectedImageName:@"icon_stars_selected"];
        startButton.userInteractionEnabled = NO;
        [bgImageView addSubview:startButton];
        [_buttons addObject:startButton];
        x = startButton.right_wcr + border;
    }
    
    // 昵称
    UILabel *nickNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, avatarImageView.bottom_wcr + 8, kMainWidth - 20, 25)];
    [nickNameLabel labelWithTextColor:[UIColor whiteColor] Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
    nickNameLabel.textAlignment = NSTextAlignmentCenter;
    _nickNameLabel = nickNameLabel;
    [bgImageView addSubview:nickNameLabel];
    
    // 消息提醒 系统设置
    height = 30;
    width = 50;
    y = 15;
    NSArray *imageNames = @[@"icon_message",@"icon_xitongshezhi"];
    NSArray *tags = @[@(ButtonActionTypeMessageTip),@(ButtonActionTypeSystemSetting)];
    for (NSInteger i = 0; i < imageNames.count; i++) {
        UIButton *tipButton = [UIButton buttonWithType:UIButtonTypeCustom];
        x = i == 0 ? 0 : kMainWidth - width;
        tipButton.frame = CGRectMake(x, y, width, width);
        [tipButton setImage:[UIImage imageFileNamed:[imageNames objectAtIndex:i]] forState:0];
        [tipButton.imageView setContentModeToScaleAspectFill];
        [tipButton setEnlargeEdgeWithTop:20 Right:20 Bottom:20 Left:20];
        tipButton.tag = [[tags objectAtIndex:i] integerValue];
        [tipButton addTarget:self action:@selector(buttonAction:)];
        [bgImageView addSubview:tipButton];
        if (i == 0) {
            // 红点
            _tipRedPointView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(tipButton.imageView.frame) - 4, CGRectGetMinY(tipButton.imageView.frame), 6, 6)];
            _tipRedPointView.backgroundColor = [UIColor redColor];
            [_tipRedPointView setViewCorner:_tipRedPointView.height_wcr / 2.0];
            _tipRedPointView.hidden = YES;
            [tipButton addSubview:_tipRedPointView];
        }
    }
    
    tableView.tableHeaderView = bgImageView;
    
    @weakify(self);
    [tableView MJHeaderWithRefreshingBlock:^{
        @strongify(self);
        [self getUserInfoFromNetwork];
    }];
    
    tableView.mj_header.hidden = [WCRUserModelTool isLogin] ? NO : YES;
}

#pragma mark - 消息提醒 系统设置
- (void)buttonAction:(UIButton *)button {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    NSString *className = nil;
    if (button.tag == ButtonActionTypeMessageTip) {
        className = @"WCRMessageTipVC";
        if (![Tools checkIsLogin]) {
            return;
        }
    }
    else if (button.tag == ButtonActionTypeSystemSetting) {
        className = @"WCRSystemSettingVC";
    }
    UIViewController *vc = [[NSClassFromString(className) alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getUserInfoFromNetwork {
    @weakify(self);
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiNoShowMessageViewWithParameters:dict tailUrl:@"/api/userApiController/getUserInfo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            WCRUserModel *userModel = [WCRUserModel jsonWithDictionary:responseMd.response];
            if (userModel && userModel.ID.length) {
                [WCRUserModelTool saveUserInfo:userModel];
                [self updateUserInfoShow];
            }
        }
        [self.tableView.mj_header endRefreshing];
        [self.tableView reloadData];
    }];
}

#pragma mark - 点击头像，跳转到个人信息
- (void)tapAvatar:(UITapGestureRecognizer *)tap {
    
    if (![Tools checkIsLogin]) {
        return;
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    WCRMyInfoDetailVC *detailVC = [[WCRMyInfoDetailVC alloc] init];
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}
#pragma mark - TableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRMyInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.model = [_dataSource objectAtIndex:indexPath.row];
    return cell;
}
#pragma mark - TableView delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRMyModel *model = [_dataSource objectAtIndex:indexPath.row];
    return model.cellHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    WCRMyModel *model = [_dataSource objectAtIndex:indexPath.row];
    if (model.type == MyInfoTypeCustomer || model.type == MyInfoTypeLifeService || model.type == MyInfoTypeGuide) {
        
    }
    else {
        if (![Tools checkIsLogin]) {
            return;
        }
    }
    
    UIViewController *vc = [[NSClassFromString(model.className) alloc] init];
    if ([model.className isEqualToString:@"WCRIntegralMallVC"]) {
        ((WCRIntegralMallVC *)vc).type = @"1";
    }
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
