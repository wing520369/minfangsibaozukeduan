//
//  WCRMyCustomerVC.m
//  Landlord
//
//  Created by eliot on 2017/4/28.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyCustomerVC.h"
#import "UINavigationController+WCRCustom.h"
#import "Tools.h"
#import "WCRSuggestVC.h"
#import "NPGeneralApi.h"

typedef NS_ENUM(NSInteger, TextFieldType) {
    TextFieldTypeQQ = 2131,
    TextFieldTypePhone,
    TextFieldTypeSuggest,
};

@interface WCRMyCustomerVC ()<UITextFieldDelegate>
@property (nonatomic, strong) NSString *qqNo;
@property (nonatomic, strong) NSString *phone;
@end

@implementation WCRMyCustomerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"我的客服"];
    [self getDataFromNetwork];
}

- (void)getDataFromNetwork {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = nil;//@{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/publicApiController/getCustomerService" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            if ([responseMd.response isKindOfClass:[NSDictionary class]]) {
                weakSelf.qqNo = [responseMd.response objectForKey:@"qqNo"];
                weakSelf.phone = [responseMd.response objectForKey:@"phone"];
            }
            [weakSelf createUI];
        }
    }];
}

- (void)createUI {
    UIView *whiteBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kMainWidth, kDefaultHeight * 2)];
    whiteBgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:whiteBgView];
    
    UITextField *QQTextField = [self getTextFieldWithYpoint:0 SuperView:whiteBgView LeftTitle:@"客服QQ：" Text:self.qqNo Tag:TextFieldTypeQQ];
    QQTextField.userInteractionEnabled = NO;
    [QQTextField addLineViewPositionType:kBottom_Type];
    
    UITextField *phoneTextField = [self getTextFieldWithYpoint:QQTextField.bottom_wcr SuperView:whiteBgView LeftTitle:@"客服热线：" Text:self.phone Tag:TextFieldTypePhone];
    
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, phoneTextField.bottom_wcr, kMainWidth, 10)];
    grayView.backgroundColor = self.view.backgroundColor;
    [whiteBgView addSubview:grayView];
    
    UITextField *suggestTextField = [self getTextFieldWithYpoint:grayView.bottom_wcr SuperView:whiteBgView LeftTitle:@"意见反馈" Text:nil Tag:TextFieldTypeSuggest];
    UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, (suggestTextField.height_wcr - 14) / 2.0, 14, 14)];
    [arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
    suggestTextField.rightView = arrowImageView;
    suggestTextField.rightViewMode = UITextFieldViewModeAlways;
    
    whiteBgView.height_wcr = suggestTextField.bottom_wcr;
}

- (UITextField *)getTextFieldWithYpoint:(CGFloat)yPoint SuperView:(UIView *)superView LeftTitle:(NSString *)leftTitle Text:(NSString *)text Tag:(TextFieldType)tag {
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(kSpaceX, yPoint, superView.width_wcr - kSpaceX * 2, kDefaultHeight)];
    textField.tag = tag;
    textField.font = [UIFont CustomFontWithSize:14.0];
    textField.text = text;
    textField.delegate = self;
    textField.textAlignment = NSTextAlignmentRight;
    // 左视图
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, kDefaultHeight)];
    [label labelWithTextColor:nil Font:textField.font BackgroundColor:nil Text:leftTitle];
    textField.leftView = label;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    [superView addSubview:textField];
    
    return textField;
}

#pragma mark - 拨打电话
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.tag == TextFieldTypePhone) {
        // 拨打电话
        [Tools dialPhoneNumber:textField.text];
        return NO;
    }
    else if (textField.tag == TextFieldTypeSuggest) {
        // 意见反馈
        [self skipToSuggetVC];
        return NO;
    }
    return YES;
}
- (void)skipToSuggetVC {
    if (![Tools checkIsLogin]) {
        return;
    }
    
    WCRSuggestVC *suggestVC = [[WCRSuggestVC alloc] init];
    [self.navigationController pushViewController:suggestVC animated:YES];
}
@end
