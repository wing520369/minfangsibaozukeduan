//
//  WCRPaymentVC.m
//  Landlord
//
//  Created by eliot on 2017/5/16.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRPaymentListVC.h"
#import "WCRPaymentListCell.h"
#import "WCRPaymentListModel.h"
#import "BasicListViewModel.h"

static NSString *identifier = @"cell";

@interface WCRPaymentListVC ()
@property (nonatomic, strong) BasicListViewModel *viewModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRPaymentListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"收支明细"];
    [self createUI];
}

- (void)createUI {
    
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    _viewModel = [[BasicListViewModel alloc] initWithParameters:dict tailUrl:@"/api/userApiController/getAccountInfo"];
    
    @weakify(self);
    _tableView = [_viewModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) CellIdentifier:identifier CellClassName:@"WCRPaymentListCell" ModelClassName:@"WCRPaymentListModel" ConfigureCellBlock:^(WCRPaymentListCell *cell, NSIndexPath *indexPath, WCRPaymentListModel *model) {
        @strongify(self);
        cell.model = model;
        cell.lineView.hidden = self.viewModel.dataSource.count - 1 == indexPath.row ? YES : NO;
    }];
    _tableView.rowHeight = [WCRPaymentListCell cellHeight];
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    [self.view addSubview:_tableView];
    
    [_viewModel tableViewSetRefresh:_tableView HeaderRefresh:^{
        @strongify(self);
        [self getPaymentListFromNetwork:LoadListType_First];
    } FooterRefresh:^{
        @strongify(self);
        [self getPaymentListFromNetwork:LoadListType_Next];
    }];
    
    [self getPaymentListFromNetwork:LoadListType_First];
}

- (void)getPaymentListFromNetwork:(LoadListType)type {
    @weakify(self);
    [_viewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        @strongify(self);
        [self.viewModel loadListDataCompleteReload:self.tableView IsEnd:isEnd];
    }];
}

@end
