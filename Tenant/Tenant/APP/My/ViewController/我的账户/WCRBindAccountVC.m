//
//  WCRBindAccountVC.m
//  Landlord
//
//  Created by eliot on 2017/5/16.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBindAccountVC.h"
#import "WCRBindAccountTypeVC.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"
#import "WCRPasswordManageVC.h"
#import "UIViewController+WCRCustom.h"
#import "UITextField+WCRCustom.h"
#import "WCRBindAccountDetailVC.h"

@interface WCRBindAccountVC ()
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *skipButton;
@end

@implementation WCRBindAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"绑定账号"];
    [self createUI];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBindAccount:) name:@"updateBindAccount" object:nil];
}

- (void)updateBindAccount:(NSNotification *)noti {
    WCRUserModel *userModel = [noti.userInfo objectForKey:@"userModel"];
    [self createAccountUIWithUserModel:userModel];
}

- (void)createUI {
    
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    if (!userModel.bindType.length) {
        // 暂未绑定
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 120)];
        label.textAlignment = NSTextAlignmentCenter;
        [label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:@"还没有绑定账号"];
        [self.view addSubview:label];
        
        UIButton *bindButton = [UIButton getButtonWithYpoint:label.bottom_wcr Title:@"去绑定"];
        [bindButton addTarget:self action:@selector(bindAccount:)];
        [self.view addSubview:bindButton];
    }
    else {
        // 已经绑定
        [self addRightBarButtonItemTitle:@"修改"];
        [self createAccountUIWithUserModel:userModel];
    }
    
}

- (void)createAccountUIWithUserModel:(WCRUserModel *)userModel {
    
    if (_textField) {
        [_textField removeFromSuperview];
        [self.skipButton removeFromSuperview];
        self.skipButton = nil;
        _textField = nil;
    }
    
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, kMainWidth, kDefaultHeight)];
    _textField.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_textField];
    
    NSString *imageName = @"icon_zhifubao";
    NSString *text = @"支付宝";
    if ([userModel.bindType integerValue] == 2) {
        // 银行卡
        imageName = @"icon_Solid";
        text = @"银行卡";
    }
    _textField.text = text;
    [_textField textFieldWithFont:[UIFont CustomFontWithSize:14.0] Placeholder:nil LeftImageName:imageName ImageWidth:20 Border:kSpaceX];
    
    // 右视图
    text = userModel.moneyNo;
    UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, _textField.height_wcr)];
    [rightLabel labelWithTextColor:nil Font:_textField.font BackgroundColor:nil Text:text];
    rightLabel.textAlignment = NSTextAlignmentRight;
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rightLabel.width_wcr + kSpaceX, _textField.height_wcr)];
    [rightView addSubview:rightLabel];
    _textField.rightView = rightView;
    _textField.rightViewMode = UITextFieldViewModeAlways;
    
    _textField.userInteractionEnabled = NO;
    
    self.skipButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.skipButton.frame = _textField.frame;
    self.skipButton.backgroundColor = [UIColor clearColor];
    [self.skipButton addTarget:self action:@selector(skipToDetail)];
    [self.view addSubview:self.skipButton];
}

- (void)rightBarButtonItemAction:(UIButton *)button {
    // 重新绑定
    WCRBindAccountTypeVC *bindVC = [[WCRBindAccountTypeVC alloc] init];
    bindVC.type = @"2";
    [self.navigationController pushViewController:bindVC animated:YES];
}

#pragma mark - 绑定账号
- (void)bindAccount:(UIButton *)button {
    // 判断是否设置了交易密码
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    if (!userModel.passwordPay.length) {
        @weakify(self);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"请先设置交易密码" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
            if ([x integerValue] == 1) {
                // 设置交易密码
                @strongify(self);
                WCRPasswordManageVC *passwordManageVC = [[WCRPasswordManageVC alloc] init];
                passwordManageVC.type = @"0";
                passwordManageVC.titleString = @"设置支付密码";
                [self.navigationController pushViewController:passwordManageVC animated:YES];
            }
        }];
        [alertView show];
        return;
    }
    WCRBindAccountTypeVC *bindVC = [[WCRBindAccountTypeVC alloc] init];
    bindVC.type = @"2";
    [self.navigationController pushViewController:bindVC animated:YES];
}

- (void)skipToDetail {
    WCRBindAccountDetailVC *bindAccountDetailVC = [[WCRBindAccountDetailVC alloc] init];
    [self.navigationController pushViewController:bindAccountDetailVC animated:YES];
}

@end
