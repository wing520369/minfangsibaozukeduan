//
//  WCRMyAccountVC.m
//  Landlord
//
//  Created by eliot on 2017/4/28.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyAccountVC.h"
#import "UINavigationController+WCRCustom.h"
#import "NSMutableAttributedString+WCRCustom.h"
#import "UITextField+WCRCustom.h"
#import "NPGeneralApi.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UIScrollView+NPRefresh.h"

typedef NS_ENUM(NSInteger, AccountActionType) {
    /// 充值
    AccountActionTypeRecharge = 1048,
    /// 提现
    AccountActionTypeWithdrawals,
    /// 绑定账号
    AccountActionTypeBlindAccount,
    /// 收支明细
    AccountActionTypePayDetail,
};

@interface WCRMyAccountVC ()<UITextFieldDelegate>
@property (nonatomic, strong) UILabel *moneyLabel;
@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollView;
@end

@implementation WCRMyAccountVC

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"我的账户"];
    [self createUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserAccount) name:@"updateUserAccount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rechargeSuccess) name:@"rechargeSuccess" object:nil];
}

- (void)rechargeSuccess {
    [self getUserInfoFromNetwork];
}

- (void)getUserInfoFromNetwork {
    
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiNoShowMessageViewWithParameters:dict tailUrl:@"/api/userApiController/getUserInfo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            WCRUserModel *userModel = [WCRUserModel jsonWithDictionary:responseMd.response];
            if (userModel && userModel.ID.length) {
                [WCRUserModelTool saveUserInfo:userModel];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateUserAccount" object:nil userInfo:@{@"userModel":userModel}];
            }
        }
        [self.scrollView.mj_header endRefreshing];
    }];
}

- (void)updateUserAccount {
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    NSString *text = [NSString stringWithFormat:@"%.2f元",[userModel.amount floatValue]];
    NSMutableAttributedString *mutableStr = [[NSMutableAttributedString alloc] initWithString:text];
    [mutableStr setFont:[UIFont CustomFontWithSize:15.0] range:NSMakeRange(text.length - 1, 1)];
    _moneyLabel.attributedText = mutableStr;
}

- (void)createUI {
    
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    self.scrollView = scrollView;
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    @weakify(self);
    [scrollView MJHeaderWithRefreshingBlock:^{
        @strongify(self);
        [self getUserInfoFromNetwork];
    }];
    
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    
    // 背景View
    CGFloat x = kSpaceX;
    CGFloat y = 20;
    CGFloat width = kMainWidth - kSpaceX * 2;
    CGFloat height = width * (365 / 690.0);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [imageView imageViewWithImageName:@"bg"];
    [imageView setViewCorner:6.0];
    [scrollView addSubview:imageView];
    
    UIFont *font = [UIFont CustomFontWithSize:30];
    
    NSString *text = [NSString stringWithFormat:@"%.2f元",[userModel.amount floatValue]];
    height = imageView.height_wcr * (240 / 365.0);
    UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [moneyLabel labelWithTextColor:[UIColor whiteColor] Font:font BackgroundColor:nil Text:nil];
    _moneyLabel = moneyLabel;
    moneyLabel.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:moneyLabel];
    NSMutableAttributedString *mutableStr = [[NSMutableAttributedString alloc] initWithString:text];
    [mutableStr setFont:[UIFont CustomFontWithSize:15.0] range:NSMakeRange(text.length - 1, 1)];
    moneyLabel.attributedText = mutableStr;
    
    // 充值 提现
    height = 30;
    width = 80;
    y = moneyLabel.bottom_wcr;
    x = kMainWidth / 2.0 - width;
    UIButton *rechargeButton = [self getButtonWithFrame:CGRectMake(x, y, width, height) IsLeftCorner:YES Tag:AccountActionTypeRecharge Title:@"充值"];
    [scrollView addSubview:rechargeButton];
    
    x = rechargeButton.right_wcr;
    UIButton *withdrawalsButton = [self getButtonWithFrame:CGRectMake(x, y, width, height) IsLeftCorner:NO Tag:AccountActionTypeWithdrawals Title:@"提现"];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.3, withdrawalsButton.height_wcr)];
    lineView.backgroundColor = [UIColor whiteColor];
    [withdrawalsButton addSubview:lineView];
    [scrollView addSubview:withdrawalsButton];
    
    CGFloat border = 10;
    x = 0;
    y = imageView.bottom_wcr + border + 10;
    width = kMainWidth;
    height = kDefaultHeight;
    UITextField *textField = [self getTextFieldWithFrame:CGRectMake(x, y, width, height) Tag:AccountActionTypeBlindAccount Title:@"绑定账号"];
    [scrollView addSubview:textField];
    
    y = textField.bottom_wcr + border;
    UITextField *detailTextField = [self getTextFieldWithFrame:CGRectMake(x, y, width, height) Tag:AccountActionTypePayDetail Title:@"收支明细"];
    [scrollView addSubview:detailTextField];
}

- (UITextField *)getTextFieldWithFrame:(CGRect)frame Tag:(AccountActionType)tag Title:(NSString *)title {
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.text = title;
    //
    [textField textFieldWithFont:[UIFont CustomFontWithSize:14.0] PlaceHolder:nil LeftLabelText:nil Border:0];
    [textField textFieldAddRightImage:@"icon-xiangyoujiantou" Width:14 RightBorder:kSpaceX];
    textField.tag = tag;
    textField.delegate = self;
    //    [self.view addSubview:textField];
    textField.backgroundColor = [UIColor whiteColor];
    return textField;
}

- (UIButton *)getButtonWithFrame:(CGRect)frame IsLeftCorner:(BOOL)isLeftCorner Tag:(AccountActionType)tag Title:(NSString *)title {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button buttonWithTitleColor:[UIColor whiteColor] Background:nil Font:[UIFont CustomFontWithSize:14.0] Title:title];
    button.frame = frame;
    [button setLayerWithBorderWidth:1.0 Corner:0.0 BorderColor:[UIColor whiteColor]];
    
    CGFloat corner = button.height_wcr / 2.0;
    UIBezierPath *maskPath = nil;
    if (isLeftCorner) {
        maskPath = [UIBezierPath bezierPathWithRoundedRect:button.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii:CGSizeMake(corner, corner)];
    }
    else {
        maskPath = [UIBezierPath bezierPathWithRoundedRect:button.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(corner, corner)];
    }
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = button.bounds;
    maskLayer.path = maskPath.CGPath;
    button.layer.mask = maskLayer;
    
    CAShapeLayer *borderLayer=[CAShapeLayer layer];
    borderLayer.path    =   maskPath.CGPath;
    borderLayer.strokeColor    = [UIColor whiteColor].CGColor;
    borderLayer.lineWidth      = 1.0;
    borderLayer.frame = button.bounds;
    borderLayer.fillColor = [UIColor clearColor].CGColor;
    [button.layer addSublayer:borderLayer];
    
    button.tag = tag;
    [button addTarget:self action:@selector(AccountActionAction:)];
    //    [self.view addSubview:button];
    return button;
}

- (void)AccountActionAction:(UIButton *)button {
    [self skipToVCWithTag:button.tag];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self skipToVCWithTag:textField.tag];
    return NO;
}

- (void)skipToVCWithTag:(AccountActionType)tag {
    NSString *className = nil;
    switch (tag) {
        case AccountActionTypeRecharge:
            // 充值
            className = @"WCRRechargeVC";
            break;
        case AccountActionTypeWithdrawals:
            // 提现
            className = @"WCRWithdrawalsVC";
            break;
        case AccountActionTypeBlindAccount:
            className = @"WCRBindAccountVC";
            break;
        case AccountActionTypePayDetail:
            className = @"WCRPaymentListVC";
            break;
    }
    if (!className) {
        return;
    }
    UIViewController *vc = [[NSClassFromString(className) alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
