//
//  WCRRechargeVC.m
//  Landlord
//
//  Created by eliot on 2017/5/16.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRechargeVC.h"
#import "UITextField+WCRCustom.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "Tools.h"
#import "NPGeneralApi.h"
#import "NPTip.h"
#import "WCRAlipayTools.h"
#import "WCRWXTools.h"

typedef NS_ENUM(NSInteger, PayType) {
    PayTypeAlipay = 1216,
    PayTypeWXpay,
    PayTypeMoney,
    PayTypeSelectedAlipay,
    PayTypeSelectedWX,
};

@interface WCRRechargeVC ()<UITextFieldDelegate>
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UITextField *typeTextField;
@property (nonatomic, strong) UITextField *moneyTextField;
@property (nonatomic, strong) UIView *bgView;
@end

@implementation WCRRechargeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"充值"];
    [self createUI];
    // 支付宝支付成功的（另外）回调
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AlipayResult:) name:@"AlipayResult" object:nil];
}
#pragma mark - 支付宝支付成功的（另外）回调
- (void)AlipayResult:(NSNotification *)noti {
    //【callback处理支付结果】,弹出提示框
    NSDictionary *dict = noti.userInfo;
    NSString *message = nil;
    NSString *status = [dict objectForKey:@"resultStatus"];
    if ([status isEqualToString:@"9000"]) {
        // 恭喜您，支付成功！
        message = @"充值成功！";
        [[NSNotificationCenter defaultCenter] postNotificationName:@"rechargeSuccess" object:nil userInfo:nil];
    }else{
        // @"未完成支付";
        message = @"充值失败！";
    }
    [NPTip showTip:message];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)createUI {
    
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    _typeTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 20, kMainWidth, kDefaultHeight)];
    _typeTextField.font = font;
    
    _leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kSpaceX, (_typeTextField.height_wcr - 20) / 2.0, 20, 20)];
//    [_leftImageView imageViewWithImageName:@"icon_zhifubao"];
    [_leftImageView imageViewWithImageName:@"icon_wepay"];
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20 + kSpaceX * 2, _typeTextField.height_wcr)];
    [leftView addSubview:_leftImageView];
    _typeTextField.leftView = leftView;
    _typeTextField.leftViewMode = UITextFieldViewModeAlways;
    
    [_typeTextField textFieldAddRightImage:@"icon-xiangyoujiantou" Width:14 RightBorder:kSpaceX];
//    _typeTextField.text = @"    支付宝支付";
//    _typeTextField.tag = PayTypeAlipay;
    _typeTextField.text = @"    微信支付支付";
    _typeTextField.tag =  PayTypeWXpay;
    _typeTextField.delegate = self;
    _typeTextField.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:_typeTextField];
    _typeTextField.userInteractionEnabled = NO;
    
    UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    actionButton.backgroundColor = [UIColor clearColor];
    actionButton.frame = _typeTextField.frame;
    [actionButton addTarget:self action:@selector(selectPayType:)];
    [scrollView addSubview:actionButton];
    
    _moneyTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, _typeTextField.bottom_wcr + 10, kMainWidth, kDefaultHeight)];
    [_moneyTextField textFieldWithFont:font PlaceHolder:@"充值单笔最多20000元" LeftLabelText:@"金额(¥)" Border:kSpaceX];
    [_moneyTextField textFieldAddRightImage:nil Width:kSpaceX RightBorder:0];
    _moneyTextField.keyboardType = UIKeyboardTypeDecimalPad;
    _moneyTextField.delegate = self;
    _moneyTextField.tag = PayTypeMoney;
    _moneyTextField.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:_moneyTextField];
    
    // 下一步
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(kSpaceX, _moneyTextField.bottom_wcr + 35, kMainWidth - kSpaceX * 2, 40);
    [nextButton buttonWithTitleColor:[UIColor whiteColor] Background:[UIColor colorWithHexString:kMainColor] Font:[UIFont CustomFontWithSize:15.0] Title:@"下一步"];
    [nextButton setViewCorner:nextButton.height_wcr / 2.0];
    [nextButton addTarget:self action:@selector(nextButtonAction)];
    [scrollView addSubview:nextButton];
}

#pragma mark - 选择支付方式
- (void)selectPayType:(UIButton *)button {
    [self popView];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    switch (textField.tag) {
            /*
        case PayTypeAlipay:
            [self popView];
            return NO;
            break;
        case PayTypeWXpay:
            [self popView];
            return NO;
            break;
             */
        case PayTypeMoney:
            
            break;
        case PayTypeSelectedWX:
            _typeTextField.text = textField.text;
            _typeTextField.tag = PayTypeWXpay;
            _leftImageView.image = [UIImage imageFileNamed:@"icon_wepay"];
            [_bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            [_bgView removeFromSuperview];
            break;
        case PayTypeSelectedAlipay:
            _typeTextField.text = textField.text;
            _typeTextField.tag = PayTypeAlipay;
            _leftImageView.image = [UIImage imageFileNamed:@"icon_zhifubao"];
            [_bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            [_bgView removeFromSuperview];
            break;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == PayTypeMoney) {
        return [Tools checkTrueNumberWithTextField:textField InRange:range CurrentString:string];
    }
    return YES;
}

#pragma mark - 下一步
- (void)nextButtonAction {
    
    [self.view endEditing:YES];
    if (!_moneyTextField.text.length) {
        [NPTip showTip:@"请输入金额"];
        return;
    }
    if ([_moneyTextField.text floatValue] <= 0) {
        [NPTip showTip:@"请输入正确的金额"];
        return;
    }
    if ([_moneyTextField.text floatValue] > 20000) {
        [NPTip showTip:@"充值单笔最多20000元"];
        return;
    }
    
    NSString *requestUrl = self.typeTextField.tag == PayTypeAlipay ? @"/api/payApiController/chargeAlipay" : @"/api/payApiController/chargeTenpayTenant";
    
    /**/
    @weakify(self);
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"amount":_moneyTextField.text};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:requestUrl ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            if (self.typeTextField.tag == PayTypeAlipay) {
                // 支付宝
                NSString *orderStr = [NSString stringWithFormat:@"%@",responseMd.response];
                if (orderStr.length) {
                    [WCRAlipayTools AlipayWithpayOrder:orderStr CompleteResult:^(NSDictionary *resultDictionary) {
                        NSString *message = nil;
                        NSString *status = [resultDictionary objectForKey:@"resultStatus"];
                        if ([status isEqualToString:@"9000"]) {
                            // 支付成功！
                            message = @"充值成功！";
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"rechargeSuccess" object:nil userInfo:nil];
                        }else{
                            // @"未完成支付";
                            message = @"充值失败！";
                        }
                        [NPTip showTip:message];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
                else {
                    [NPTip showTip:@"充值失败！"];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            else if (self.typeTextField.tag == PayTypeWXpay) {
                // 微信
                if ([responseMd.response isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *responseDict = responseMd.response;
                    [WCRWXTools WXPayAction:responseDict PayResultBlock:^(int errCode) {
                        NSString *result = @"充值失败！";
                        switch (errCode) {
                            case WXSuccess:{
                                result = @"充值成功！";
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"rechargeSuccess" object:nil userInfo:nil];
                            }
                                break;
                            default:
                                
                                break;
                        }
                        [NPTip showTip:result];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
                else {
                    [NPTip showTip:@"充值失败！"];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
    }];
}

- (void)popView {
    
    [self.view endEditing:YES];
    
    UIView *bgView = [UIView popBackgroudViewInWindowWithColor:nil];
    _bgView = bgView;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissSuperView:)];
    [bgView addGestureRecognizer:tap];
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, kMainHeight, kMainWidth, 150)];
    contentView.backgroundColor = [UIColor whiteColor];
    [contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] init]];
    [bgView addSubview:contentView];
    
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    NSString *text = @"选择支付方式";
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, 50)];
    [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    label.textAlignment = NSTextAlignmentCenter;
    [label addLineViewPositionType:kBottom_Type];
    [contentView addSubview:label];
    
    
//    UITextField *alipayTF = [self getTextFiledWithYpoint:label.bottom_wcr Font:font ImageName:@"icon_zhifubao" Tag:PayTypeSelectedAlipay Title:@"    支付宝支付" SuperView:contentView];
//    [alipayTF addLineViewPositionType:kBottom_Type];
//    
//    [self getTextFiledWithYpoint:alipayTF.bottom_wcr Font:font ImageName:@"icon_wepay" Tag:PayTypeSelectedWX Title:@"  微信支付" SuperView:contentView];

    UITextField *wxTF = [self getTextFiledWithYpoint:label.bottom_wcr Font:font ImageName:@"icon_wepay" Tag:PayTypeSelectedWX Title:@"  微信支付" SuperView:contentView];
    [wxTF addLineViewPositionType:kBottom_Type];
    
    [self getTextFiledWithYpoint:wxTF.bottom_wcr Font:font ImageName:@"icon_zhifubao" Tag:PayTypeSelectedAlipay Title:@"    支付宝支付" SuperView:contentView];
  
    [UIView animateWithDuration:0.1 animations:^{
        contentView.y_wcr = kMainHeight - contentView.height_wcr;
    }];
    
}

- (UITextField*)getTextFiledWithYpoint:(CGFloat)yPoint Font:(UIFont *)font ImageName:(NSString *)imageName Tag:(PayType)tag Title:(NSString *)title SuperView:(UIView *)superView {
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(kSpaceX, yPoint, kMainWidth - kSpaceX * 2, kDefaultHeight)];
    textField.tag = tag;
    textField.text = title;
    textField.delegate = self;
    [textField textFieldWithFont:font Placeholder:nil LeftImageName:imageName ImageWidth:20 Border:0];
    [superView addSubview:textField];
    return textField;
}

- (void)dismissSuperView:(UITapGestureRecognizer *)tap {
    [_bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_bgView removeFromSuperview];
}

@end
