//
//  WCRWithdrawalsVC.m
//  Landlord
//
//  Created by eliot on 2017/5/16.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRWithdrawalsVC.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"
#import "WCRBindAccountVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UITextField+WCRCustom.h"
#import "NPGeneralApi.h"
#import "NPTip.h"
#import "Tools.h"
#import "NSString+JKHash.h"
#import "NSString+WCRCustom.h"

@interface WCRWithdrawalsVC ()<UITextFieldDelegate>
@property (nonatomic, strong) UITextField *moneyTextField;
@property (nonatomic, strong) UITextField *payPwdTextField;
@end

@implementation WCRWithdrawalsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"提现"];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    if (!userModel.bindType.length) {
        // 暂未绑定账号
        @weakify(self);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"还没有绑定账号，请先绑定账号" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
            @strongify(self);
            if ([x integerValue] == 1) {
                WCRBindAccountVC *bindVC = [[WCRBindAccountVC alloc] init];
                [self.navigationController pushViewController:bindVC animated:YES];
            }
            else {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
        [alertView show];
    }
}

- (void)createUI {
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    UIFont *font = [UIFont CustomFontWithSize:13.0];
    _moneyTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, kMainWidth, kDefaultHeight)];
    [_moneyTextField textFieldWithPlaceholder:@"提现500元起" Font:font];
    [self addLeftViewWithTitle:@"金额(¥)" TextField:_moneyTextField];
    [_moneyTextField textFieldAddRightImage:nil Width:kSpaceX RightBorder:0];
    _moneyTextField.keyboardType = UIKeyboardTypeDecimalPad;
    _moneyTextField.delegate = self;
    _moneyTextField.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:_moneyTextField];
    
    _payPwdTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, _moneyTextField.bottom_wcr + 10, kMainWidth, kDefaultHeight)];
    [_payPwdTextField textFieldWithPlaceholder:@"请输入支付密码" Font:font];
    [self addLeftViewWithTitle:@"支付密码" TextField:_payPwdTextField];
    [_payPwdTextField textFieldAddRightImage:nil Width:kSpaceX RightBorder:0];
    _payPwdTextField.secureTextEntry = YES;
    _payPwdTextField.delegate = self;
    _payPwdTextField.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:_payPwdTextField];
    
    // 下一步
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(kSpaceX, _payPwdTextField.bottom_wcr + 35, kMainWidth - kSpaceX * 2, 40);
    [nextButton buttonWithTitleColor:[UIColor whiteColor] Background:[UIColor colorWithHexString:kMainColor] Font:[UIFont CustomFontWithSize:15.0] Title:@"申请提现"];
    [nextButton setViewCorner:nextButton.height_wcr / 2.0];
    [nextButton addTarget:self action:@selector(nextButtonAction)];
    [scrollView addSubview:nextButton];
}

- (void)addLeftViewWithTitle:(NSString *)title TextField:(UITextField *)textField {
    UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 75, textField.height_wcr)];
    //标签
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(kSpaceX, 0, leftView.width_wcr - kSpaceX, leftView.height_wcr)];
    label.font = textField.font;
    label.text = title;
    [leftView addSubview:label];
    
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark - TextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _moneyTextField) {
        return [Tools checkTrueNumberWithTextField:textField InRange:range CurrentString:string];
    }
    return YES;
}


- (void)nextButtonAction {
    
    WCRUserModel *user = [WCRUserModelTool getUserInfo];
    if (!_moneyTextField.text.length) {
        [NPTip showTip:@"请输入提现金额"];
        return;
    }
    if ([_moneyTextField.text floatValue] < 500) {
        [NPTip showTip:_moneyTextField.placeholder];
        return;
    }
    if ([_moneyTextField.text floatValue] > [user.amount floatValue]) {
        [NPTip showTip:@"提现金额不能大于账户余额"];
        return;
    }
    if (!_payPwdTextField.text.length) {
        [NPTip showTip:_payPwdTextField.placeholder];
        return;
    }
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"amount":_moneyTextField.text,@"passwordPay":[_payPwdTextField.text.jk_sha1String uppercaseString]};
    __weak typeof(self) weakSelf = self;
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/userApiController/fetch" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            [NPTip showTip:@"提现申请成功，等待审核！"];
            WCRUserModel *userModel = [WCRUserModel jsonWithDictionary:responseMd.response];
            [WCRUserModelTool saveUserInfo:userModel];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateUserInfo" object:nil userInfo:@{@"userModel":userModel}];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateUserAccount" object:nil];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

@end
