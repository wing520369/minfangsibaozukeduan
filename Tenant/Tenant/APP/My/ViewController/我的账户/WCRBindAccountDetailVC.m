//
//  WCRBindAccountDetailVC.m
//  Landlord
//
//  Created by eliot on 2017/7/27.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBindAccountDetailVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UITextField+WCRCustom.h"

@interface WCRBindAccountDetailVC ()

@end

@implementation WCRBindAccountDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"绑定账号"];
    [self createUI];
}

- (void)createUI {
 
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kMainWidth, 0.1)];
    contentView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:contentView];
    
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    NSString *text = [userModel.bindType integerValue] == 1 ? @"支付宝" : @"银行卡";
    CGFloat y = 0;
    UITextField *accountTF = [self getTextFieldWithYpoint:y Font:font HaveLine:YES LeftTitle:text SuperView:contentView];
    accountTF.text = userModel.moneyNo;
    
    text = @"真实姓名";
    y = accountTF.bottom_wcr;
    UITextField *nameTF = [self getTextFieldWithYpoint:y Font:font HaveLine:NO LeftTitle:text SuperView:contentView];
    nameTF.text = userModel.trueName;
    
    y = nameTF.bottom_wcr;
    if ([userModel.bindType integerValue] == 2) {
        
        [nameTF addLineViewPositionType:kBottom_Type];
        
        text = @"开户银行";
        
        UITextField *bankAddressTF = [self getTextFieldWithYpoint:y Font:font HaveLine:NO LeftTitle:text SuperView:contentView];
        bankAddressTF.text = userModel.bankName;
        y = bankAddressTF.bottom_wcr;
    }
    
    contentView.height_wcr = y;
}

- (UITextField *)getTextFieldWithYpoint:(CGFloat)yPoint Font:(UIFont *)font HaveLine:(BOOL)haveLine LeftTitle:(NSString *)leftTitle SuperView:(UIView *)superView {
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(kSpaceX, yPoint, kMainWidth - kSpaceX * 2, kDefaultHeight)];
    [textField textFieldWithPlaceholder:nil Font:font];
    
    // 左视图
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 75, textField.height_wcr)];
    label.font = font;
    label.text = leftTitle;
    
    textField.leftView = label;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    
    if (haveLine) {
        [textField addLineViewPositionType:kBottom_Type];
    }
    
    textField.userInteractionEnabled = NO;
    
    
    [superView addSubview:textField];
    
    return textField;
}

@end
