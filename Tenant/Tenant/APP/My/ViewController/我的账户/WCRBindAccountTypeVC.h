//
//  WCRBindAccountTypeVC.h
//  Landlord
//
//  Created by eliot on 2017/5/16.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

/// 选择绑定的账号
@interface WCRBindAccountTypeVC : WCRBasicViewController
/// 0：选择绑定的类型  1：支付宝  2：银行卡
@property (nonatomic, strong) NSString *type;
@end
