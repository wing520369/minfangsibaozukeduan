//
//  WCRBindAccountTypeVC.m
//  Landlord
//
//  Created by eliot on 2017/5/16.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBindAccountTypeVC.h"
#import "UITextField+WCRCustom.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "NPGeneralApi.h"
#import "MSWeakTimer.h"
#import "NPTip.h"
#import "NSString+JKHash.h"
#import "UIViewController+WCRCustom.h"
#import "WCRPasswordManageVC.h"
#import "WCRSingleModelPickerView.h"

typedef NS_ENUM(NSInteger, BindType) {
    /// 选择支付宝
    BindTypeAlipay = 408,
    /// 选择银行卡
    BindTypeBank,
    /// 输入支付宝 or 银行卡 账号
    BindTypeAccount,
    /// 真实姓名
    BindTypeName,
    /// 开户银行
    BindTypeBankAddress,
    /// 支付密码
    BindTypePayPwd,
    /// 验证码
    BindTypeCode,
};

@interface WCRBindAccountTypeVC ()<UITextFieldDelegate>
@property (nonatomic, strong) UITextField *accountTF;
@property (nonatomic, strong) UITextField *nameTF;
@property (nonatomic, strong) UITextField *bankAddressTF;
@property (nonatomic, strong) UITextField *payPwdTF;
@property (nonatomic, strong) UITextField *codeTF;
@property (nonatomic, strong) UIButton *getCodeButton;
@property (nonatomic, assign) NSInteger time;
@property (nonatomic, strong) MSWeakTimer *timer;
@property (nonatomic, strong) NSArray *banks;
@end

@implementation WCRBindAccountTypeVC
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"绑定账号"];
    [self createUI];
}
- (void)createUI {
    
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    
    if ([_type integerValue] == 0) {
        // 选择绑定的方式
        CGFloat y = 10;
        NSArray *titles = @[@"支付宝",@"银行卡"];
        NSArray *imageNames = @[@"icon_zhifubao",@"icon_Solid"];
        NSArray *tags = @[@(BindTypeAlipay),@(BindTypeBank)];
        for (NSInteger i = 0; i < titles.count; i++) {
            UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(0, y, kMainWidth, kDefaultHeight)];
            [textfield textFieldWithFont:font Placeholder:nil LeftImageName:[imageNames objectAtIndex:i] ImageWidth:20 Border:kSpaceX];
            textfield.text = [titles objectAtIndex:i];
            textfield.tag = [[tags objectAtIndex:i] integerValue];
            textfield.delegate = self;
            textfield.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:textfield];
            y = textfield.bottom_wcr + 10;
        }
    }
    else {
        
        self.time = 60;
        
        TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
        scrollView.backgroundColor = [UIColor clearColor];
        scrollView.opaque = YES;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:scrollView];
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kMainWidth, 0.1)];
        contentView.backgroundColor = [UIColor whiteColor];
        [scrollView addSubview:contentView];
        
        CGFloat y = 0;
        _accountTF = [self getTextFieldWithPlaceholder:@"请输入支付宝账号" Ypoint:y Font:font HaveLine:YES Tag:BindTypeAccount SuperView:contentView];
        
        y = _accountTF.bottom_wcr;
        _nameTF = [self getTextFieldWithPlaceholder:@"请输入真实姓名" Ypoint:y Font:font HaveLine:YES Tag:BindTypeName SuperView:contentView];
        
        y = _nameTF.bottom_wcr;
        if ([_type integerValue] == 2) {
            _accountTF.placeholder = @"请输入银行卡卡号";
            _bankAddressTF = [self getTextFieldWithPlaceholder:@"请选择银行" Ypoint:y Font:font HaveLine:YES Tag:BindTypeBankAddress SuperView:contentView];
            y = _bankAddressTF.bottom_wcr;
        }
        
        _payPwdTF = [self getTextFieldWithPlaceholder:@"请输入交易密码" Ypoint:y Font:font HaveLine:NO Tag:BindTypePayPwd SuperView:contentView];
        _payPwdTF.secureTextEntry = YES;
        // 右视图 忘记密码
        UIButton *forgetPwdButton = [UIButton buttonWithType:UIButtonTypeCustom];
        forgetPwdButton.frame = CGRectMake(0, 5, 75, _payPwdTF.height_wcr - 14);
        [forgetPwdButton buttonWithTitleColor:[UIColor colorWithHexString:kMainColor] Background:nil Font:[UIFont CustomFontWithSize:12.0] Title:@"忘记密码"];
        [forgetPwdButton setLayerWithBorderWidth:1.0 Corner:3.0 BorderColor:[UIColor colorWithHexString:kMainColor]];
        [forgetPwdButton addTarget:self action:@selector(forgetPayPwd:)];
        _payPwdTF.rightView = forgetPwdButton;
        _payPwdTF.rightViewMode = UITextFieldViewModeAlways;
        
        y = _payPwdTF.bottom_wcr;
        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, 10)];
        grayView.backgroundColor = self.view.backgroundColor;
        [contentView addSubview:grayView];
        
        // 验证码
        y = grayView.bottom_wcr + 10;
        _codeTF = [self getTextFieldWithPlaceholder:@"请输入验证码" Ypoint:y Font:font HaveLine:NO Tag:BindTypeCode SuperView:contentView];
        _codeTF.keyboardType = UIKeyboardTypeNumberPad;
        // 右视图发送验证码
        _getCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _getCodeButton.frame = CGRectMake(0, 5, 75, _codeTF.height_wcr - 14);
        [_getCodeButton buttonWithTitleColor:[UIColor colorWithHexString:kMainColor] Background:nil Font:[UIFont CustomFontWithSize:12.0] Title:@"发送验证码"];
        [_getCodeButton setLayerWithBorderWidth:1.0 Corner:3.0 BorderColor:[UIColor colorWithHexString:kMainColor]];
        [_getCodeButton addTarget:self action:@selector(bindGetCode:)];
        _codeTF.rightView = _getCodeButton;
        _codeTF.rightViewMode = UITextFieldViewModeAlways;
        
        contentView.height_wcr = _codeTF.bottom_wcr;
        
        y = contentView.bottom_wcr + 30;
        // 绑定
        UIButton *bindButton = [UIButton getButtonWithYpoint:y Title:@"绑定"];
        [bindButton addTarget:self action:@selector(bindAccountAction)];
        [scrollView addSubview:bindButton];
        
        if (bindButton.bottom_wcr > scrollView.height_wcr) {
            scrollView.contentSize = CGSizeMake(kMainWidth, bindButton.bottom_wcr + 30);
        }
    }
}


- (UITextField *)getTextFieldWithPlaceholder:(NSString *)placeholder Ypoint:(CGFloat)yPoint Font:(UIFont *)font HaveLine:(BOOL)haveLine Tag:(BindType)tag SuperView:(UIView *)superView {

    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(kSpaceX, yPoint, kMainWidth - kSpaceX * 2, kDefaultHeight)];
    [textField textFieldWithPlaceholder:placeholder Font:font];
    textField.tag = tag;
    textField.delegate = self;
    
    if (haveLine) {
        [textField addLineViewPositionType:kBottom_Type];
    }
    
    [superView addSubview:textField];
    
    return textField;
}

#pragma mark - TextField Delegate 
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.tag == BindTypeAlipay || textField.tag == BindTypeBank) {
        NSString *type = textField.tag == BindTypeAlipay ? @"1" : @"2";
        WCRBindAccountTypeVC *bindVC = [[WCRBindAccountTypeVC alloc] init];
        bindVC.type = type;
        [self.navigationController pushViewController:bindVC animated:YES];
        return NO;
    }
    else if (textField.tag == BindTypeBankAddress) {
        
        [WCRSingleModelPickerView popSinglePickViewWithModels:self.banks SelectedActionBlock:^(WCRSingleModel *model) {
            textField.text = model.name;
        }];
        return NO;
    }
    return YES;
}

- (NSArray *)banks {
    if (!_banks) {
        NSArray *titles = @[@"工商银行",@"建设银行",@"农业银行",@"中国银行",@"交通银行",@"民生银行",@"邮储银行",@"招商银行",@"光大银行",@"中信银行",@"华夏银行",@"浦发银行",@"平安银行",@"兴业银行",@"广发银行",@"北京银行",@"上海银行",@"厦门银行"];
        NSMutableArray *models = [NSMutableArray arrayWithCapacity:titles.count];
        for (NSInteger i = 0; i < titles.count; i++) {
            WCRSingleModel *model = [[WCRSingleModel alloc] init];
            model.name = [titles objectAtIndex:i];
            [models addObject:model];
        }
        _banks = [models copy];
    }
    return _banks;
}

#pragma mark - 获取验证码
- (void)bindGetCode:(UIButton *)button {
    
    [self.view endEditing:YES];
    button.enabled = NO;
    // 1注册，2忘记密码，3其他
    NSString *type = @"3";
    NSDictionary *dict = @{@"phone":[WCRUserModelTool getCurrentAccount],@"type":type};
    __weak typeof(self) weakSelf = self;
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/loginApiController/smsCode" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            weakSelf.timer = [MSWeakTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(forgetOrRegisterCutTimeDown:) userInfo:nil repeats:YES dispatchQueue:dispatch_get_main_queue()];
        }
        else {
            button.enabled = YES;
        }
    }];
}
- (void)forgetOrRegisterCutTimeDown:(MSWeakTimer *)timer {
    [_getCodeButton setTitle:[NSString stringWithFormat:@"%ld",(long)self.time] forState:UIControlStateDisabled];
    self.time--;
    if (self.time < 0) {
        [timer invalidate];
        timer = nil;
        self.time = 60;
        [_getCodeButton setTitle:@"重新获取" forState:UIControlStateDisabled];
        [_getCodeButton setTitle:@"重新获取" forState:0];
        _getCodeButton.enabled = YES;
    }
}

#pragma mark - 忘记支付密码（修改）
- (void)forgetPayPwd:(UIButton *)button {
    WCRPasswordManageVC *passwordVC = [[WCRPasswordManageVC alloc] init];
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    passwordVC.type = userModel.passwordPay.length ? @"1" : @"0";
    passwordVC.titleString = @"支付密码";
    [self.navigationController pushViewController:passwordVC animated:YES];
}

#pragma mark - 绑定
- (void)bindAccountAction {
    if (!_accountTF.text.length) {
        [NPTip showTip:_accountTF.placeholder];
        return;
    }
    if (!_nameTF.text.length) {
        [NPTip showTip:_nameTF.placeholder];
        return;
    }
    if ([_type integerValue] == 2) {
        if (!_bankAddressTF.text.length) {
            [NPTip showTip:_bankAddressTF.placeholder];
            return;
        }
    }
    if (!_payPwdTF.text.length) {
        [NPTip showTip:_payPwdTF.placeholder];
        return;
    }
    if (!_codeTF.text.length) {
        [NPTip showTip:_codeTF.placeholder];
        return;
    }
    
    NSString *bindType = [_type integerValue] == 1 ? @"1" : @"2";
    NSString *bankName = _bankAddressTF.text.length ? _bankAddressTF.text : @"";
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],
                           @"tokenName":[WCRUserModelTool getToken],
                           @"bindType":bindType,
                           @"moneyNo":_accountTF.text,
                           @"passwordPay":[_payPwdTF.text.jk_sha1String uppercaseString],
                           @"bankName":bankName,
                           @"trueName":_nameTF.text,
                           @"code":_codeTF.text};
    @weakify(self);
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/userApiController/bindMoneyNo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            
            WCRUserModel *userModel = [WCRUserModel jsonWithDictionary:responseMd.response];
            [WCRUserModelTool saveUserInfo:userModel];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateBindAccount" object:nil userInfo:@{@"userModel":userModel}];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateUserInfo" object:nil userInfo:@{@"userModel":userModel}];
            
            UIViewController *vc = [UIViewController FindSpecificViewController:self.navigationController.viewControllers outViewController:NSClassFromString(@"WCRBindAccountVC")];
            [self.navigationController popToViewController:vc animated:YES];
        }
    }];
}

@end
