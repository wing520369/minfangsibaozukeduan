//
//  WCRPasswordManageVC.h
//  Landlord
//
//  Created by eliot on 2017/4/28.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class RACSubject;
/// 0：设置支付密码  1：修改支付密码  2：修改登录密码
@interface WCRPasswordManageVC : WCRBasicViewController
/// 0：设置支付密码  1：修改支付密码  2：修改登录密码
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, strong) RACSubject *pwdOperationSignal;
@end
