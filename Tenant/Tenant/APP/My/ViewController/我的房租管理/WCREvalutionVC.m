//
//  WCREvalutionVC.m
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCREvalutionVC.h"
#import "UITableView+FDTemplateLayoutCell.h"
#import "WCREvalutionCell.h"
#import "WCREvalutionModel.h"
#import "BasicListViewModel.h"

static NSString *identifier = @"cell";

@interface WCREvalutionVC ()<UITableViewDelegate>
@property (nonatomic, strong) BasicListViewModel *viewModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCREvalutionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)updateDepositList {
    [self getDataFromNetwork:LoadListType_First];
}

- (void)createUI {
    
    NSString *url = @"/api/commentApiController/tenantCommentList";
    NSString *type = _type == EvalutionVCTypeEvaluteOther ? @"1" : @"2";
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"type":type};
    _viewModel = [[BasicListViewModel alloc] initWithParameters:dict tailUrl:url];
    
    @weakify(self);
    self.tableView = [self.viewModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64 - kDefaultHeight) CellIdentifier:identifier CellClassName:@"WCREvalutionCell" ModelClassName:@"WCREvalutionModel" ConfigureCellBlock:^(WCREvalutionCell *cell, NSIndexPath *indexPath, WCREvalutionModel *model) {
        @strongify(self);
        [self configureCell:cell IndexPath:indexPath];
    }];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    
    [self.viewModel tableViewSetRefresh:self.tableView HeaderRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_First];
    } FooterRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_Next];
    }];
    
    [self getDataFromNetwork:LoadListType_First];
}

- (void)getDataFromNetwork:(LoadListType)type {
    @weakify(self);
    [self.viewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        @strongify(self);
        [self.viewModel loadListDataCompleteReload:self.tableView IsEnd:isEnd];
    }];
}

- (void)configureCell:(WCREvalutionCell *)cell IndexPath:(NSIndexPath *)indexPath {
    cell.model = [self.viewModel.dataSource objectAtIndex:indexPath.row];
    cell.fd_enforceFrameLayout = YES;
}

#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    __block NSIndexPath *blockIndexPath = indexPath;
    return [tableView fd_heightForCellWithIdentifier:identifier cacheByIndexPath:indexPath configuration:^(WCREvalutionCell *cell) {
        [weakSelf configureCell:cell IndexPath:blockIndexPath];
    }];
}

@end
