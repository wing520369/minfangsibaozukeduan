//
//  WCRRentDepositVC.m
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRentDepositVC.h"
#import "WCRDepositCell.h"
#import "WCRRentDepositModel.h"
#import "BasicListViewModel.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"
#import "NPTip.h"
#import "NPGeneralApi.h"
#import "WCRRentDepositDetailVC.h"

static NSString *identifier = @"cell";

@interface WCRRentDepositVC ()<UITableViewDelegate,WCRTenenmentCellDelegate>
@property (nonatomic, strong) BasicListViewModel *viewModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRRentDepositVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDepositList) name:@"updateDepositList" object:nil];
}

- (void)updateDepositList {
    if (self.type == RentDepositVCTypeNoPay) {
        [self getDataFromNetwork:LoadListType_First];
    }
    else {
        @weakify(self);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            @strongify(self);
            [self getDataFromNetwork:LoadListType_First];
        });
    }
}

- (void)createUI {
    
    NSString *url = @"/api/tenantApiController/getSendMessageList";
    NSString *billStatus = _type == RentDepositVCTypeNoPay ? @"1" : @"3";
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"status":billStatus};
    _viewModel = [[BasicListViewModel alloc] initWithParameters:dict tailUrl:url];
    
    @weakify(self);
    self.tableView = [self.viewModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64 - kDefaultHeight) CellIdentifier:identifier CellClassName:@"WCRDepositCell" ModelClassName:@"WCRRentDepositModel" ConfigureCellBlock:^(WCRDepositCell *cell, NSIndexPath *indexPath, WCRRentDepositModel *model) {
        @strongify(self);
        cell.rentDepositModel = model;
        cell.delegate = self;
    }];
    self.tableView.rowHeight = [WCRDepositCell cellHeight];
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    
    [self.viewModel tableViewSetRefresh:self.tableView HeaderRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_First];
    } FooterRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_Next];
    }];
    
    [self getDataFromNetwork:LoadListType_First];
}

- (void)getDataFromNetwork:(LoadListType)type {
    @weakify(self);
    [self.viewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        @strongify(self);
        [self.viewModel loadListDataCompleteReload:self.tableView IsEnd:isEnd];
    }];
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self skipToDetailVC:[self.viewModel modelAtIndexPath:indexPath]];
}

- (void)skipToDetailVC:(WCRRentDepositModel *)model {
    WCRRentDepositDetailVC *detailVC = [[WCRRentDepositDetailVC alloc] init];
    detailVC.isFromTipMsg = NO;
    detailVC.model = model;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - 去支付，确认，删除
- (void)tenenmentCellDepositClickAction:(ActionType)type DepositModel:(WCRRentDepositModel *)model {
    @weakify(self);
    switch (type) {
        case ActionTypePay:{
            // 去支付
            [self skipToDetailVC:model];
        }
            break;
        case ActionTypeConfirmExit:{
            // 确认
            [self skipToDetailVC:model];
        }
            break;
        case ActionTypeOnReceiveMoney:{
            // 待收款
        }
            break;
        case ActionTypeDelete:{
            // 删除
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定删除此缴费单吗？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
                if ([x integerValue] == 1) {
                    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"messageId":model.ID};
                    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/tenantApiController/delFeeBill" ResponseBlock:^(NPGeneralResponse *responseMd) {
                        if (responseMd.isOK) {
                            @strongify(self);
                            [NPTip showTip:@"删除成功"];
                            if ([self.viewModel.dataSource containsObject:model]) {
                                [self.viewModel.dataSource removeObject:model];
                            }
                            [self.tableView reloadData];
                        }
                    }];
                }
            }];
            [alertView show];
            
        }
            break;
        default:
            break;
    }
}

@end
