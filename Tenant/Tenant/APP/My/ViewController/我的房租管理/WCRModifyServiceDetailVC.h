//
//  WCRModifyInfoDetailVC.h
//  Landlord
//
//  Created by eliot on 2017/5/18.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class WCRModifyServiceListModel;
/// 报修信息 详情
@interface WCRModifyServiceDetailVC : WCRBasicViewController
@property (nonatomic, strong) WCRModifyServiceListModel *modifyModel;
@end
