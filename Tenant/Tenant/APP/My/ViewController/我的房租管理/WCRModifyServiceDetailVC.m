//
//  WCRModifyInfoDetailVC.m
//  Landlord
//
//  Created by eliot on 2017/5/18.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRModifyServiceDetailVC.h"
#import "UIViewController+WCRCustom.h"
#import "NPGeneralApi.h"
#import "WCRModifyServiceListModel.h"
#import "NSString+WCRCustom.h"
#import "WCRPhotoBrowser.h"
#import "UIImageView+WebCache.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"
#import "NPTip.h"
#import "WCRMutilImageViews.h"
#import "WCRPhotoBrowser.h"

@interface WCRModifyServiceDetailVC ()<WCRMutilImageViewsDelegate>
@property (nonatomic, strong) WCRMutilImageViews *imageViews;
@property (nonatomic, strong) UIScrollView *bgView;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) NSMutableArray *images;
@end

@implementation WCRModifyServiceDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"报修服务"];
    [self addRightBarButtonItemTitle:@"删除"];
    [self getDetailDataFromNetwork];
}

// 删除
- (void)rightBarButtonItemAction:(UIButton *)button {
    
    __weak typeof(self) weakSelf = self;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定删除？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
        if ([x integerValue] == 1) {
            NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"repairIds":weakSelf.modifyModel.ID};
            [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/roomApiController/delRepair" ResponseBlock:^(NPGeneralResponse *responseMd) {
                if (responseMd.isOK) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateInfoList" object:nil];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }];
    [alertView show];
}

- (void)getDetailDataFromNetwork {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"repairId":_modifyModel.ID};
    [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/roomApiController/getRepairInfo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            WCRModifyServiceListModel *model = [WCRModifyServiceListModel jsonWithDictionary:responseMd.response];
            weakSelf.modifyModel.status = model.status;
            weakSelf.modifyModel.isRead = @"1";
            [weakSelf createUI];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:nil];
        }
    }];
}

- (void)createUI {

    UIScrollView *bgView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kSpaceX, kMainWidth, 0.1)];
    _bgView = bgView;
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:bgView];
    
    UIFont *font = [UIFont CustomFontWithSize:15.0];
    
    NSString *text = _modifyModel.title;
    CGFloat border = 7;
    // 标题
    CGFloat x = kSpaceX;
    CGFloat y = border;
    CGFloat width = bgView.width_wcr - x * 2;
    CGFloat height = [NSString calculateWithContent:text MaxWidth:width Font:font] + border;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [titleLabel labelWithTextColor:[UIColor colorWithHexString:kMainColor] Font:font BackgroundColor:nil Text:text];
    titleLabel.numberOfLines = 0;
    [bgView addSubview:titleLabel];
    
    // 房产
    font = [UIFont CustomFontWithSize:12.0];
    text = [NSString stringWithFormat:@"房产：%@ - 房号：%@",_modifyModel.houseName,_modifyModel.roomNo];
    y = titleLabel.bottom_wcr + border;
    height = font.lineHeight;
    UILabel *houseLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [houseLabel labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:text];
    [bgView addSubview:houseLabel];
    
    // 租客
    text = [NSString stringWithFormat:@"联系人：%@   电话：%@",_modifyModel.name,_modifyModel.phone];
    y = houseLabel.bottom_wcr + border;
    UILabel *tenantLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [tenantLabel labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:text];
    [bgView addSubview:tenantLabel];
    
    // 内容
    font = [UIFont CustomFontWithSize:13.0];
    text = _modifyModel.content;
    y = tenantLabel.bottom_wcr + border;
    height = [NSString calculateWithContent:text MaxWidth:width Font:font];
    UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [contentLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    contentLabel.numberOfLines = 0;
    [bgView addSubview:contentLabel];
    
    // 图片
    y = contentLabel.bottom_wcr + 10;
    __block NSMutableArray *images = [NSMutableArray arrayWithCapacity:_modifyModel.repairImgs.count];
    [_modifyModel.repairImgs enumerateObjectsUsingBlock:^(WCRRepairImgsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [images addObject:obj.img];
    }];
    WCRMutilImageViews *imageViews = [[WCRMutilImageViews alloc] initWithFrame:CGRectMake(x, y, width, height)];
    imageViews.hiddenAddButton = YES;
    [imageViews WCRMutilImageViewsReloadWithImages:images];
    imageViews.delegate = self;
    _imageViews = imageViews;
    [bgView addSubview:imageViews];
    
    // 时间
    y = imageViews.bottom_wcr + 10;
    text = [_modifyModel.createDate substringWithRange:NSMakeRange(0, _modifyModel.createDate.length - 3)];
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:houseLabel.frame];
    timeLabel.y_wcr = y;
    _timeLabel = timeLabel;
    [timeLabel labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:houseLabel.font BackgroundColor:nil Text:text];
    [bgView addSubview:timeLabel];
    
    bgView.height_wcr = timeLabel.bottom_wcr + 15;
    if (bgView.bottom_wcr > kMainHeight - 64) {
        bgView.contentSize = CGSizeMake(bgView.width_wcr, bgView.bottom_wcr);
    }
    
    
    _images = [NSMutableArray arrayWithCapacity:images.count];
    [_images addObjectsFromArray:images];
}

- (void)mutilImageViewsUpdateImages:(NSMutableArray *)images IsNeedRefreshHeight:(BOOL)isRefresh {
    _timeLabel.y_wcr = _imageViews.bottom_wcr + 10;
    _bgView.height_wcr = _timeLabel.bottom_wcr + 15;
    if (_bgView.bottom_wcr > kMainHeight - 64) {
        _bgView.contentSize = CGSizeMake(_bgView.width_wcr, _bgView.bottom_wcr);
    }
}

- (void)mutilImageViewsClickImageWithIndex:(NSInteger)index {
    [WCRPhotoBrowser showWithPhotoUrlArray:_images currentIndex:index];
}

@end
