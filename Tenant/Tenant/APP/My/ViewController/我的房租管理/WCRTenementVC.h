//
//  WCRTenementVC.h
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

typedef NS_ENUM(NSInteger, TenementVCType) {
    TenementVCTypeOnRent = 242,
    TenementVCTypeNoEvalution,
    TenementVCTypeExitRent,
};

/// 我的租房
@interface WCRTenementVC : WCRBasicViewController
@property (nonatomic, assign) TenementVCType type;
@end
