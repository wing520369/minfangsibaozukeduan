//
//  WCRRentDepositVC.h
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

typedef NS_ENUM(NSInteger, RentDepositVCType) {
    /// 未支付
    RentDepositVCTypeNoPay = 352,
    /// 已完成
    RentDepositVCTypeComplete,
};

/// 缴费单
@interface WCRRentDepositVC : WCRBasicViewController
@property (nonatomic, assign) RentDepositVCType type;
@end
