//
//  WCRPublishEvalutionVC.h
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class WCRTenementModel;
/// 发布 评价
@interface WCRPublishEvalutionVC : WCRBasicViewController
@property (nonatomic, strong) WCRTenementModel *model;
@end
