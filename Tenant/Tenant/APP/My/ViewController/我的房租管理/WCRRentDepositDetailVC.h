//
//  WCRRentDepositDetailVC.h
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class WCRRentDepositModel;
/// 缴费单详情
@interface WCRRentDepositDetailVC : WCRBasicViewController
@property (nonatomic, strong) WCRRentDepositModel *model;
/// YES : 从消息提醒   NO：缴费单
@property (nonatomic, assign) BOOL isFromTipMsg;
@end
