//
//  WCRRentDepositDetailVC.m
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRentDepositDetailVC.h"
#import "WCRRentDepositHouseInfoCell.h"
#import "WCRRentDepositModel.h"
#import "myUILabel.h"
#import "NSString+WCRCustom.h"
#import "WCRRentDepositCostCell.h"
#import "WCRDepositCostModel.h"
#import "NPGeneralApi.h"
#import "NPTip.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"
#import "WCRPasswordManageVC.h"
#import "NSString+JKHash.h"
#import "UITextField+WCRCustom.h"
#import "WCRRechargeVC.h"

static NSString *identifier = @"cell";
static NSString *costIdentifier = @"costCell";

@interface WCRRentDepositDetailVC ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataSource;
@end

@implementation WCRRentDepositDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"缴费单详情"];
    [self initialData];
//    [self createUI];
}
- (void)initialData {

    if (self.isFromTipMsg) {
        [self analysisData];
        [self createUI];
    }
    else {
        @weakify(self);
        NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"sendMessageId":self.model.ID};
        [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/tenantApiController/getSendMessageInfo" ResponseBlock:^(NPGeneralResponse *responseMd) {
            if (responseMd.isOK) {
                
                @strongify(self);
                
                [self analysisData];
                [self createUI];
            }
        }];
    }
}

- (void)analysisData {
    NSArray *array = [self.model.content jsonObject];
    NSArray *models = [WCRDepositCostModel jsonWithArray:array];
    
    NSArray *sortArray = [models sortedArrayUsingComparator:^NSComparisonResult(WCRDepositCostModel *obj1, WCRDepositCostModel *obj2) {
        if (obj1.order < obj2.order) {
            return NSOrderedAscending;
        }
        else {
            return NSOrderedDescending;
        }
    }];
    
    self.dataSource = [NSMutableArray arrayWithCapacity:models.count];
    [self.dataSource addObjectsFromArray:sortArray];
}

- (void)createUI {
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64 - kDefaultHeight) style:UITableViewStylePlain];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    
    [self createTableViewFooter:tableView];
    tableView.sectionHeaderHeight = 10;
    [tableView registerClass:[WCRRentDepositHouseInfoCell class] forCellReuseIdentifier:identifier];
    [tableView registerClass:[WCRRentDepositCostCell class] forCellReuseIdentifier:costIdentifier];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, tableView.bottom_wcr, kMainWidth, kDefaultHeight);
    UIColor *titleColor = [UIColor whiteColor];
    UIColor *bgColor = [UIColor colorWithHexString:kMainColor];
    NSString *text = @"未知的状态";
    // 缴费单状态 0-取消 1-待支付 2-待收款 3-支付完成
    // 账单状态 0-未支付 1-待支付 2-到账 3-未清算 4-清算 5-租客确认 6-发送清算账单
    if ([_model.status integerValue] == 1) {
        text = @"支付";
        if ([_model.amount integerValue] <= 0) {
            // 退租，房东给租客钱
            if ([_model.billStatus integerValue] == 6) {
                text = @"确认";
            }
            else if ([_model.billStatus integerValue] == 5) {
                text = @"待收款";
                bgColor = [UIColor lineColor];
                button.userInteractionEnabled = NO;
            }
        }
        else {
            if ([_model.billStatus integerValue] == 6) {
                // 租客退租，给房东钱
                text = @"支付";
            }
        }
    }
    else if ([_model.status integerValue] == 2) {
        // 目前没有用到
        // 待款
        titleColor = [UIColor colorWithHexString:kCustomGrayColor];
        bgColor = [UIColor whiteColor];
        text = @"待收款";
    }
    else if ([_model.status integerValue] == 3) {
        // 已完成
        titleColor = [UIColor colorWithHexString:kCustomGrayColor];
        bgColor = [UIColor whiteColor];
        text = @"删除";
    }
    [button buttonWithTitleColor:titleColor Background:bgColor Font:[UIFont CustomFontWithSize:14.0] Title:text];
    [button addTarget:self action:@selector(detailButtonAction)];
    [self.view addSubview:button];
}
- (void)createTableViewFooter:(UITableView *)tableView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 0.1)];
    footerView.backgroundColor = [UIColor colorWithRGB:223 G:223 B:223];
    
    NSString *text = [NSString stringWithFormat:@"总计：%.2f",[self.model.amount floatValue]];
    CGFloat x = kSpaceX;
    CGFloat y = 0;
    CGFloat width = kMainWidth - kSpaceX * 2;
    CGFloat height = 40;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label labelWithTextColor:[UIColor colorWithHexString:kMainColor] Font:[UIFont CustomFontWithSize:11.0] BackgroundColor:nil Text:text];
    label.textAlignment = NSTextAlignmentRight;
    [footerView addSubview:label];
    
    // 备注
    x = 0;
    y = label.bottom_wcr;
    UIView *markView = [[UIView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, 0.1)];
    markView.backgroundColor = [UIColor whiteColor];
    
    x = kSpaceX;
    y = 0;
    width = 100;
    height = 40;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [titleLabel labelWithTextColor:nil Font:[UIFont CustomFontWithSize:13.0] BackgroundColor:nil Text:@"备注"];
    [markView addSubview:titleLabel];
    
    y = titleLabel.bottom_wcr;
    width = kMainWidth - x * 2;
    height = 130;
    UIView *borderView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [borderView setLayerWithBorderWidth:1.0 Corner:0 BorderColor:[UIColor colorWithHexString:kCustomGrayColor]];
    [markView addSubview:borderView];
    
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(borderView.x_wcr + 5, borderView.y_wcr + 5, borderView.width_wcr - 10, borderView.height_wcr - 15)];
    textView.showsVerticalScrollIndicator = NO;
    textView.backgroundColor = [UIColor clearColor];
    textView.editable = NO;
    textView.font = [UIFont CustomFontWithSize:12.0];
    [markView addSubview:textView];
    
    markView.height_wcr = borderView.bottom_wcr + 10;
    [footerView addSubview:markView];
    
    footerView.height_wcr = markView.bottom_wcr;
    tableView.tableFooterView = footerView;
    
    if (self.model.remarks.length) {
        textView.text = self.model.remarks;
    }
}
#pragma mark - Table View DataSorce
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        WCRRentDepositHouseInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        cell.rentDepositModel = self.model;
        return cell;
    }
    else {
        WCRRentDepositCostCell *cell = [tableView dequeueReusableCellWithIdentifier:costIdentifier forIndexPath:indexPath];
        cell.model = [self.dataSource objectAtIndex:indexPath.row];
        return cell;
    }
}
#pragma mark - Table View Delegate 
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [WCRRentDepositHouseInfoCell cellHeight];
    }
    return [WCRRentDepositCostCell cellHeight];
}
#pragma mark - 支付(确认) or 删除
- (void)detailButtonAction {
    
    // 缴费单状态 0-取消 1-待支付 2-待收款 3-支付完成
    if ([self.model.status integerValue] == 1) {
        if ([_model.amount floatValue] > 0) {
            // 支付
            [self goToPay];
        }
        else {
           // 确认
            [self confirmExitRent];
        }
    }
    else if ([self.model.status integerValue] == 3){
        // 删除
        [self deleteDepositBill];
    }
}
- (void)goToPay {
    @weakify(self);
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    // 支付
    // 需要支付密码
    // 判断是否设置了交易密码
    if (!userModel.passwordPay.length) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"请先设置交易密码" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
            if ([x integerValue] == 1) {
                // 设置交易密码
                @strongify(self);
                WCRPasswordManageVC *passwordManageVC = [[WCRPasswordManageVC alloc] init];
                passwordManageVC.type = @"0";
                passwordManageVC.titleString = @"设置支付密码";
                [self.navigationController pushViewController:passwordManageVC animated:YES];
            }
        }];
        [alertView show];
        return;
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"请输入交易密码" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *textField = [alertView textFieldAtIndex:0];
    textField.secureTextEntry = YES;
    [textField textFieldWithFont:[UIFont CustomFontWithSize:12.0] PlaceHolder:@"请输入交易密码" LeftLabelText:nil Border:0];
    [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
        if ([x integerValue] == 1) {
            @strongify(self);
            NSString *requestUrl = @"/api/tenantApiController/payTenantBill";
            NSString *tenantBillId = @"";
            if ([self.model.billStatus integerValue] == 6) {
                // 清算账单
                requestUrl = @"/api/leaveApiController/tenantPalyRent";
                tenantBillId = self.model.billId;
            }
            NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"messageId":self.model.ID,@"passwordPay":[textField.text.jk_sha1String uppercaseString],@"tenantBillId":tenantBillId};
            [NPGeneralApi WCRApiWithParameters:dict tailUrl:requestUrl ResponseBlock:^(NPGeneralResponse *responseMd) {
                if (responseMd.isOK) {
                    [NPTip showTip:@"支付成功"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateDepositList" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"paySuccessUpdateUserInfo" object:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else {
                    NSString *message = [NSString stringWithFormat:@"%@",responseMd.msg];
                    if ([message isEqualToString:@"账户余额不足，请充值！"]) {
                        [self.view endEditing:YES];
                        UIAlertView *noMoneyAlertView = [[UIAlertView alloc] initWithTitle:nil message:@"账户余额不足，请充值！" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                        [noMoneyAlertView.rac_buttonClickedSignal subscribeNext:^(NSNumber *noMoney) {
                            if ([noMoney integerValue] == 1) {
                                // 跳转到充值的页面
                                WCRRechargeVC *rechargeVC = [[WCRRechargeVC alloc] init];
                                [self.navigationController pushViewController:rechargeVC animated:YES];
                            }
                        }];
                        [noMoneyAlertView show];
                    }
                }
            }];
        }
    }];
    [alertView show];
}
- (void)confirmExitRent {
    @weakify(self);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确认与房东办理好退租手续？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
        if ([x integerValue] == 1) {
            NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"tenantBillId":self.model.billId};
            [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/leaveApiController/tenantConLeave" ResponseBlock:^(NPGeneralResponse *responseMd) {
                if (responseMd.isOK) {
                    @strongify(self);
                    [NPTip showTip:@"确认退租成功"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateDepositList" object:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }];
    [alertView show];
}
- (void)deleteDepositBill {
    @weakify(self);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定删除此缴费单吗？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
        if ([x integerValue] == 1) {
            @strongify(self);
            NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"messageId":self.model.ID};
            [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/tenantApiController/delFeeBill" ResponseBlock:^(NPGeneralResponse *responseMd) {
                if (responseMd.isOK) {
                    @strongify(self);
                    [NPTip showTip:@"删除成功"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateDepositList" object:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }];
    [alertView show];
}

@end
