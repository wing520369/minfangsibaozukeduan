//
//  WCRTenementVC.m
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRTenementVC.h"
#import "WCRTenementModel.h"
#import "WCRRoomModel.h"
#import "WCRTenenmentCell.h"
#import "BasicListViewModel.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"
#import "NPTip.h"
#import "NPGeneralApi.h"
#import "WCRPublishEvalutionVC.h"

static NSString *identifier = @"cell";

@interface WCRTenementVC ()<UITableViewDelegate,WCRTenenmentCellDelegate>
@property (nonatomic, strong) BasicListViewModel *viewModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRTenementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateList) name:@"updateList" object:nil];
}

- (void)updateList {
    [self getDataFromNetwork:LoadListType_First];
}

- (void)createUI {
    
    NSString *url = @"/api/leaveApiController/getRentRoom";
    switch (self.type) {
        case TenementVCTypeOnRent:
            url = @"/api/leaveApiController/getRentRoom";
            break;
        case TenementVCTypeNoEvalution:
            url = @"/api/leaveApiController/getTenantCommentRoom";
            break;
        case TenementVCTypeExitRent:
            url = @"/api/leaveApiController/getTenantLeaveRoom";
            break;
        default:
            break;
    }
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    _viewModel = [[BasicListViewModel alloc] initWithParameters:dict tailUrl:url];
    
    @weakify(self);
    self.tableView = [self.viewModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64 - kDefaultHeight) CellIdentifier:identifier CellClassName:@"WCRTenenmentCell" ModelClassName:@"WCRTenementModel" ConfigureCellBlock:^(WCRTenenmentCell *cell, NSIndexPath *indexPath, WCRTenementModel *model) {
        @strongify(self);
        cell.model = model;
        cell.delegate = self;
    }];
    self.tableView.rowHeight = [WCRTenenmentCell cellHeight];
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    
    [self.viewModel tableViewSetRefresh:self.tableView HeaderRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_First];
    } FooterRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_Next];
    }];
    
    [self getDataFromNetwork:LoadListType_First];
}

- (void)getDataFromNetwork:(LoadListType)type {
    @weakify(self);
    [self.viewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        @strongify(self);
        [self.viewModel.dataSource enumerateObjectsUsingBlock:^(WCRTenementModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
            switch (self.type) {
                case TenementVCTypeOnRent:
                    model.room.type = @"0";
                    break;
                case TenementVCTypeNoEvalution:
                    model.room.type = @"1";
                    break;
                case TenementVCTypeExitRent:
                    model.room.type = @"2";
                    break;
                default:
                    break;
            }
        }];
        [self.viewModel loadListDataCompleteReload:self.tableView IsEnd:isEnd];
    }];
}

#pragma mark - 退租，删除，去评价
- (void)tenenmentCellClickAction:(ActionType)type Model:(WCRTenementModel *)model {
    switch (type) {
        case ActionTypeExitRent:{
            // 退租
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定退租吗？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
                if ([x integerValue] == 1) {
                    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"roomId":model.room.ID};
                    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/leaveApiController/applyLeave" ResponseBlock:^(NPGeneralResponse *responseMd) {
                        if (responseMd.isOK) {
                            [NPTip showTip:@"退租申请提交成功"];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateList" object:nil];
                        }
                    }];
                }
            }];
            [alertView show];
        }
            break;
        case ActionTypeOnExitRent:{
            // 退租中
            
        }
            break;
        case ActionTypeGoToEvalution:{
            // 去评价
            WCRPublishEvalutionVC *evalutionVC = [[WCRPublishEvalutionVC alloc] init];
            evalutionVC.model = model;
            [self.navigationController pushViewController:evalutionVC animated:YES];
        }
            break;
        case ActionTypeDelete:{
            // 删除
            [self deleteHaveRent:model];
        }
            break;
        default:
            break;
    }
}

- (void)deleteHaveRent:(WCRTenementModel *)model {
    @weakify(self);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"你确定删除此记录？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
        if ([x integerValue] == 1) {
            @strongify(self);
            NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"applyId":model.ID};
            [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/leaveApiController/delTenantCommentRoom" ResponseBlock:^(NPGeneralResponse *responseMd) {
                if (responseMd.isOK) {
                    if ([self.viewModel.dataSource containsObject:model]) {
                        [self.viewModel.dataSource removeObject:model];
                    }
                    [self.tableView reloadData];
                    if (self.type == TenementVCTypeNoEvalution) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateList" object:nil];
                    }
                }
            }];
        }
    }];
    [alertView show];
}


@end
