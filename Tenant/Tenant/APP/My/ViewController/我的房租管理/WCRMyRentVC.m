//
//  WCRMyRentVC.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyRentVC.h"
#import "WCRMyRentCell.h"
#import "WCRMyRentModel.h"

static NSString *identifier = @"cell";

@interface WCRMyRentVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSArray *dataSource;
@end

@implementation WCRMyRentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"我的房租管理"];
    _dataSource = [WCRMyRentModel getMyRentModels];
    [self createUI];
}

- (void)createUI {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64 - 49) style:UITableViewStyleGrouped];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.rowHeight = [WCRMyRentCell cellHeight];
    tableView.sectionHeaderHeight = 10;
    tableView.sectionFooterHeight = 0.001;
    tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    [tableView registerClass:[WCRMyRentCell class] forCellReuseIdentifier:identifier];
    [self.view addSubview:tableView];
}

#pragma mark - Table View DataSorce
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRMyRentCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.model = [_dataSource objectAtIndex:indexPath.section];
    return cell;
}
#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRMyRentModel *model = [_dataSource objectAtIndex:indexPath.section];
    UIViewController *vc = [[NSClassFromString(model.className) alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
