//
//  WCREvalutionVC.h
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

typedef NS_ENUM(NSInteger, EvalutionVCType) {
    /// 评论别人
    EvalutionVCTypeEvaluteOther,
    /// 别人评论我
    EvalutionVCTypeEvaluteMe,
};

/// 评分星级
@interface WCREvalutionVC : WCRBasicViewController
@property (nonatomic, assign) EvalutionVCType type;
@end
