//
//  WCRRentDepositMainVC.m
//  Tenant
//
//  Created by eliot on 2017/6/5.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRentDepositMainVC.h"
#import "WCRRentDepositVC.h"
#import "WCRTopMultiButtonsView.h"

@interface WCRRentDepositMainVC ()<UIPageViewControllerDataSource, UIPageViewControllerDelegate>
@property (nonatomic, strong) UIPageViewController *pageViewController;
//控制器数组
@property (nonatomic, strong) NSMutableArray *controllerList;
//当前下标
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) WCRTopMultiButtonsView *topMultipleButtonView;
@end

@implementation WCRRentDepositMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"缴费单"];
    [self createUI];
}

- (void)createUI {
    
    NSArray *titleArray = @[@"未支付",@"已完成"];
    
    CGFloat x = 0;
    CGFloat y =  0;
    CGFloat width = kMainWidth;
    CGFloat height = kDefaultHeight;
    __weak typeof(self) weakSelf = self;
    _topMultipleButtonView = [[WCRTopMultiButtonsView alloc]initWithFrame:CGRectMake(x, y, width, height) buttonTitleArray:titleArray actionBlock:^(NSInteger index) {
        [weakSelf pageControlValueChanged:index];
    }];
    [self.view addSubview:_topMultipleButtonView];
    
    //
    _controllerList = [[NSMutableArray alloc]init];
    NSArray *tags = @[@(RentDepositVCTypeNoPay),@(RentDepositVCTypeComplete)];
    for (int i = 0; i < titleArray.count; i++) {
        WCRRentDepositVC *rentDepositVC = [[WCRRentDepositVC alloc]init];
        rentDepositVC.type = [[tags objectAtIndex:i] integerValue];
        [_controllerList addObject:rentDepositVC];
    }
    
    //
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    y = _topMultipleButtonView.bottom_wcr;
    x = 0;
    height = self.view.height_wcr - y;
    self.pageViewController.view.frame = CGRectMake(x,y,width,height);
    [self.pageViewController setDataSource:self];
    [self.pageViewController setDelegate:self];
    self.pageViewController.view.backgroundColor = [UIColor clearColor];
    [self.pageViewController.view setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    
    [_topMultipleButtonView selectAtIndex:_currentIndex];
    [self pageControlValueChanged:_currentIndex];
    
}

//顶部切换按钮响应
- (void)pageControlValueChanged:(NSInteger)index {
    UIPageViewControllerNavigationDirection direction = index > [self.controllerList indexOfObject:[self.pageViewController.viewControllers lastObject]] ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
    [self.pageViewController setViewControllers:@[self.controllerList[index]] direction:direction animated:YES completion:NULL];
}

#pragma mark - UIPageViewControllerDataSource
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger index = [self.controllerList indexOfObject:viewController];
    if ((index == NSNotFound) || (index == 0)) {
        return nil;
    }
    return self.controllerList[--index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSUInteger index = [self.controllerList indexOfObject:viewController];
    
    if ((index == NSNotFound)||(index+1 >= [self.controllerList count])) {
        return nil;
    }
    return self.controllerList[++index];
}

- (void)pageViewController:(UIPageViewController *)viewController didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    
    if (completed) {
        NSInteger index = [self.controllerList indexOfObject:[viewController.viewControllers lastObject]];
        [_topMultipleButtonView selectAtIndex:index];
    }
}
@end
