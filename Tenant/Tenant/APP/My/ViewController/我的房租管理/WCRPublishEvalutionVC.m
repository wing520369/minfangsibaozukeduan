//
//  WCRPublishEvalutionVC.m
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRPublishEvalutionVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "SAMTextView.h"
#import "UIViewController+WCRCustom.h"
#import "NPTip.h"
#import "NPGeneralApi.h"
#import "NSString+WCRCustom.h"
#import "WCRTenementModel.h"

@interface WCRPublishEvalutionVC ()
@property (nonatomic, strong) SAMTextView *textView;
@property (nonatomic, strong) NSMutableArray *buttons;
@end

@implementation WCRPublishEvalutionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _buttons = [NSMutableArray arrayWithCapacity:5];
    [self addLeftGoBackItemWithTitle:@"评价"];
    [self addRightBarButtonItemTitle:@"发布"];
    [self createUI];
}

#pragma mark - 发布
- (void)rightBarButtonItemAction:(UIButton *)button {
    NSInteger level = 0;
    for (UIButton *starButton in self.buttons) {
        if (starButton.selected) {
            level++;
        }
    }
    if (level == 0) {
        [NPTip showTip:@"请选择评价星级"];
        return;
    }
    if (!_textView.text.length) {
        [NPTip showTip:@"请输入对房东的评价"];
        return;
    }
    
    @weakify(self);
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"applyId":self.model.ID,@"level":@(level),@"content":_textView.text};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/commentApiController/tenantCommentHost" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            [NPTip showTip:@"发表评论成功"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateList" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)createUI {
 
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    CGFloat x = kSpaceX;
    CGFloat y = 15;
    CGFloat width = kMainWidth - x * 2;
    CGFloat height = 0.1;
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    bgView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:bgView];
    
    UIFont *font = [UIFont CustomFontWithSize:13.0];
    NSString *text = @"评价星级";
    x = kSpaceX;
    width = [NSString calculateWithFont:font Content:text];
    height = 40;
    y = 0;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    [bgView addSubview:label];
    
    // 评价星
    width = 25;
    x = label.right_wcr + 8;
    NSString *imageName = @"icon_stars_default";
    NSString *selectImageName = @"icon_stars_selected";
    for (NSInteger i = 0; i < 5; i++) {
        UIButton *starButton = [UIButton buttonWithType:UIButtonTypeCustom];
        starButton.frame = CGRectMake(x, y, width, height);
        [starButton buttonWithImageName:imageName SelectedImageName:selectImageName];
        [starButton addTarget:self action:@selector(clickStarButtonAction:)];
        starButton.tag = i;
        [bgView addSubview:starButton];
        [self.buttons addObject:starButton];
        x = starButton.right_wcr;
    }
    
    UIView *lineView = [UIView lineView];
    lineView.frame = CGRectMake(0, label.bottom_wcr, bgView.width_wcr, 1);
    [bgView addSubview:lineView];
    
    y = lineView.bottom_wcr + 7;
    x  = label.x_wcr;
    width = bgView.width_wcr - x * 2;
    height = 150;
    _textView = [[SAMTextView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    _textView.font = font;
    _textView.placeholder = @"评价房东";
    [bgView addSubview:_textView];
    
    bgView.height_wcr = _textView.bottom_wcr + 15;
    [bgView setViewCorner:4.0];
}

- (void)clickStarButtonAction:(UIButton *)button {
    
    for (NSInteger i = 0; i < self.buttons.count; i++) {
        UIButton *starButton = [self.buttons objectAtIndex:i];
        if (i <= button.tag) {
            starButton.selected = YES;
        }
        else {
            starButton.selected = NO;
        }
    }
}

@end
