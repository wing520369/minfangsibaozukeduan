//
//  WCRSystemSettingVC.m
//  Landlord
//
//  Created by eliot on 2017/4/28.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRSystemSettingVC.h"
#import "WCRPasswordManageVC.h"
#import "WCRLoginVC.h"
#import "NPGeneralApi.h"
#import "WCRAboutUsVC.h"
#import "Tools.h"

@interface WCRSettingCell : UITableViewCell
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UILabel *versionLabel;
@property (nonatomic, strong) UIImageView *arrowImageView;
@end
@implementation WCRSettingCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, kDefaultHeight)];
        [_label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_label];
        
        _arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kMainWidth - 14 - kSpaceX, (kDefaultHeight - 14) / 2.0, 14, 14)];
        [_arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
        [self.contentView addSubview:_arrowImageView];
        
        _versionLabel = [[UILabel alloc] initWithFrame:_label.frame];
        [_versionLabel labelWithTextColor:[UIColor colorWithHexString:kCustomBlueColor] Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
        _versionLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_versionLabel];
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
@end

static NSString *identifier = @"cell";

@interface WCRSystemSettingVC ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSArray *dataSource;
@end

@implementation WCRSystemSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"系统设置"];
    _dataSource = @[@"版本信息",@"登录密码",@"关于民房四宝"];
    [self createUI];
}

- (void)createUI {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) style:UITableViewStyleGrouped];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    
    tableView.rowHeight = kDefaultHeight;
    [tableView registerClass:[WCRSettingCell class] forCellReuseIdentifier:identifier];
    tableView.sectionHeaderHeight = 10;
    tableView.sectionFooterHeight = 0.001;
    tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    
    // 退出登录
    CGFloat height = 40;
    CGFloat x = kSpaceX;
    CGFloat width = kMainWidth - x * 2;
    CGFloat y = kMainHeight - 64 - 80;
    UIButton *logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    logoutButton.frame = CGRectMake(x, y, width, height);
    [logoutButton buttonWithTitleColor:[UIColor colorWithHexString:kMainColor] Background:[UIColor whiteColor] Font:[UIFont CustomFontWithSize:16.0] Title:@"退出"];
    [logoutButton setLayerWithBorderWidth:1.0 Corner:height / 2.0 BorderColor:[UIColor lineColor]];
    [logoutButton addTarget:self action:@selector(logoutAction)];
    [self.view addSubview:logoutButton];
}

#pragma mark -  退出登录
- (void)logoutAction {
    
    if (![Tools checkIsLogin]) {
        return;
    }
    
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/loginApiController/appLogout" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            [WCRUserModelTool logOut];
            WCRLoginVC *loginVC = [[WCRLoginVC alloc] init];
            loginVC.backToHomePage = YES;
            [[UIApplication sharedApplication] keyWindow].rootViewController = [[WCRLoginVC alloc] init];
        }
    }];
}

#pragma mark - Table View DataSorce
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.label.text = [_dataSource objectAtIndex:indexPath.section];
    if (indexPath.section == 0) {
        cell.versionLabel.hidden = NO;
        cell.arrowImageView.hidden = YES;
        // app版本，版本号
        NSString *text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        cell.versionLabel.text = text;
    }
    else {
        cell.versionLabel.hidden = YES;
        cell.arrowImageView.hidden = NO;
    }
    return cell;
}
#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            
            break;
        case 1:{
            // 登录密码
            
            if (![Tools checkIsLogin]) {
                return;
            }
            
            WCRPasswordManageVC *passwordVC = [[WCRPasswordManageVC alloc] init];
            passwordVC.type = @"2";
            passwordVC.titleString = @"登录密码";
            [self.navigationController pushViewController:passwordVC animated:YES];
        }
            break;
        case 2:{
            // 关于民房四宝
            WCRAboutUsVC *usVC = [[WCRAboutUsVC alloc] init];
            [self.navigationController pushViewController:usVC animated:YES];
        }
            break;
        default:
            break;
    }
}

@end
