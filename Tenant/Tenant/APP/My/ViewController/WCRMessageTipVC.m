//
//  WCRMessageTipVC.m
//  Tenant
//
//  Created by eliot on 2017/5/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMessageTipVC.h"
#import "BasicListViewModel.h"
#import "WCRInfoListCell.h"
#import "WCRRentDepositModel.h"
#import "WCRRentDepositDetailVC.h"
#import "NPGeneralApi.h"
#import "UINavigationController+WCRCustom.h"

static NSString *identifier = @"cell";

@interface WCRMessageTipVC ()<UITableViewDelegate>
@property (nonatomic, strong) BasicListViewModel *viewModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRMessageTipVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"消息提醒"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDepositList) name:@"updateDepositList" object:nil];
    [self createUI];
}

- (void)updateDepositList {
    [self getDataFromNetwork:LoadListType_First];
}

- (void)createUI {
    
    NSString *url = @"/api/tenantApiController/getSendMessageList";
    NSString *status = @"1";
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"status":status};
    _viewModel = [[BasicListViewModel alloc] initWithParameters:dict tailUrl:url];
    
    @weakify(self);
    self.tableView = [self.viewModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) CellIdentifier:identifier CellClassName:@"WCRInfoListCell" ModelClassName:@"WCRRentDepositModel" ConfigureCellBlock:^(WCRInfoListCell *cell, NSIndexPath *indexPath, WCRRentDepositModel *model) {
        @strongify(self);
        cell.model = model;
        cell.lineView.hidden = self.viewModel.dataSource.count - 1 == indexPath.row ? YES : NO;
    }];
    self.tableView.rowHeight = [WCRInfoListCell cellHeight];
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    
    [self.viewModel tableViewSetRefresh:self.tableView HeaderRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_First];
    } FooterRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_Next];
    }];
    
    [self getDataFromNetwork:LoadListType_First];
}

- (void)getDataFromNetwork:(LoadListType)type {
    @weakify(self);
    [self.viewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        @strongify(self);
        [self.viewModel loadListDataCompleteReload:self.tableView IsEnd:isEnd];
    }];
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRRentDepositModel *model = [self.viewModel modelAtIndexPath:indexPath];
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"sendMessageId":model.ID};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/tenantApiController/getSendMessageInfo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            model.isRead = @"1";
            WCRRentDepositModel *newModel = [WCRRentDepositModel jsonWithDictionary:responseMd.response];
            WCRRentDepositDetailVC *detailVC = [[WCRRentDepositDetailVC alloc] init];
            detailVC.isFromTipMsg = YES;
            detailVC.model = newModel;
            [self.navigationController pushViewController:detailVC animated:YES];
            [self.tableView reloadData];
        }
    }];
}

@end
