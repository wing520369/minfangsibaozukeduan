//
//  WCRSuggestVC.m
//  Landlord
//
//  Created by 吴传荣 on 2017/5/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRSuggestVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UITextView+WCRCustom.h"
#import "SAMTextView.h"
#import "UIViewController+WCRCustom.h"
#import "NPTip.h"
#import "NPGeneralApi.h"

@interface WCRSuggestVC ()<UITextViewDelegate>
@property (nonatomic, strong) SAMTextView *textView;
@end

@implementation WCRSuggestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"意见反馈"];
    [self addRightBarButtonItemTitle:@"提交"];
    [self createUI];
}

- (void)rightBarButtonItemAction:(UIButton *)button {
    if (!_textView.text.length) {
        [NPTip showTip:@"请输入你想说的话"];
        return;
    }
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"content":_textView.text};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/publicApiController/feedBack" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            [NPTip showTip:@"意见反馈提交成功"];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)createUI {
    
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kMainWidth, 0.1)];
    bgView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:bgView];
    
    CGFloat x = kSpaceX;
    CGFloat y = 0;
    CGFloat width = kMainWidth - kSpaceX * 2;
    CGFloat height = 45;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:15.0] BackgroundColor:nil Text:@"意见反馈"];
    [bgView addSubview:label];
    
    y = label.bottom_wcr;
    height = 130;
    UIView *borderView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [borderView setLayerWithBorderWidth:1.0 Corner:3.0 BorderColor:[UIColor lineColor]];
    [bgView addSubview:borderView];
    
    _textView = [[SAMTextView alloc] initWithFrame:CGRectMake(borderView.x_wcr + 5, borderView.y_wcr + 5, borderView.width_wcr - 10 , borderView.height_wcr - 10)];
    _textView.font = [UIFont CustomFontWithSize:13.0];
    _textView.placeholder = @"说点什么吧！";
    _textView.delegate = self;
    [bgView addSubview:_textView];
    
    bgView.height_wcr = borderView.bottom_wcr + 30;
    
}

- (void)textViewDidChange:(UITextView *)textView {
    [textView textViewlimitInputWithNumber:1000 AlertMessage:@"最多只能输入1000个字"];
}

@end
