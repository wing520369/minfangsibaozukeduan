//
//  WCRAppointLookRoomVC.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRAppointLookRoomVC.h"
#import "BasicListViewModel.h"
#import "WCRDeleteBottomView.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"
#import "NSObject+RACPropertySubscribing.h"
#import "RACSubject.h"
#import "UIViewController+WCRCustom.h"
#import "WCRHousingSourcesModel.h"
#import "WCRAppointmentLookRoomCell.h"
#import "WCRAppointLookRoomModel.h"
#import "NPTip.h"
#import "NPGeneralApi.h"
#import "WCRAppointLookRoomDetailVC.h"

static NSString *identifier = @"cell";

@interface WCRAppointLookRoomVC ()<UITableViewDelegate>
@property (nonatomic, strong) BasicListViewModel *viewModel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *selectedModel;
@property (nonatomic, assign) BOOL isDelete;
@property (nonatomic, strong) WCRDeleteBottomView *bottomView;
@end

@implementation WCRAppointLookRoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"预约看房"];
    
    self.isDelete = NO;
    [self createUIAndInitialData];
    [self deleteOperation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInfoList) name:@"updateInfoList" object:nil];
}

- (void)updateInfoList {
    [self getInfoListFromNetworkWithType:LoadListType_First];
}

- (void)deleteOperation {
    
    _selectedModel = [NSMutableArray array];
    UIButton *rightButton = [self addRightBarButtonItemTitle:@"删除"];
    [rightButton setTitle:@"取消" forState:UIControlStateSelected];
    
    @weakify(self);
    [RACObserve(self, isDelete) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        BOOL delete = [x boolValue];
        [self.viewModel.dataSource enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
            WCRAppointLookRoomModel *appointmentModel = obj;
            appointmentModel.hidden = !delete;
        }];
        if (delete) {
            [UIView animateWithDuration:0.3 animations:^{
                self.bottomView.y_wcr = kMainHeight - 64 - self.bottomView.height_wcr;
                self.tableView.height_wcr = kMainHeight - 64 - self.bottomView.height_wcr;
            }];
        }
        else {
            [UIView animateWithDuration:0.3 animations:^{
                self.bottomView.y_wcr = kMainHeight;
                self.tableView.height_wcr = kMainHeight - 64;
            }];
            [self.selectedModel enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
                WCRAppointLookRoomModel *appointmentModel = obj;
                appointmentModel.isSelected = NO;
            }];
            [self.selectedModel removeAllObjects];
        }
        self.viewModel.isTableViewEdit = delete;
        self.tableView.editing = delete;
        rightButton.selected = delete;
        [self.tableView reloadData];
    }];
}
#pragma mark - 删除
- (void)rightBarButtonItemAction:(UIButton *)button {
    button.selected = !button.selected;
    self.isDelete = button.selected;
}

- (void)createUIAndInitialData {
    
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    _viewModel = [[BasicListViewModel alloc] initWithParameters:dict tailUrl:@"/api/roomApiController/getOrderTableList"];
    
    // 创建tableView
    @weakify(self);
    self.tableView = [_viewModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) CellIdentifier:identifier CellClassName:@"WCRAppointmentLookRoomCell" ModelClassName:@"WCRAppointLookRoomModel" ConfigureCellBlock:^(WCRAppointmentLookRoomCell *cell, NSIndexPath *indexPath, WCRAppointLookRoomModel *model) {
        @strongify(self);
        cell.model = model;
        cell.lineView.hidden = indexPath.row == self.viewModel.dataSource.count - 1 ? YES : NO;
    }];
    
    // 刷新
    [self.viewModel tableViewSetRefresh:_tableView HeaderRefresh:^{
        @strongify(self);
        [self getInfoListFromNetworkWithType:LoadListType_First];
    } FooterRefresh:^{
        @strongify(self);
        [self getInfoListFromNetworkWithType:LoadListType_Next];
    }];
    
    _tableView.delegate = self;
    _tableView.rowHeight = kDefaultHeight;
    [self.view addSubview:_tableView];
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    
    [self getInfoListFromNetworkWithType:LoadListType_First];
    
    _tableView.allowsSelectionDuringEditing = YES;
    [self createDeleteView];
}
#pragma mark - 删除 取消 or 确定
- (void)createDeleteView {
    _bottomView = [[WCRDeleteBottomView alloc] init];
    [self.view addSubview:_bottomView];
    
    self.bottomView.deleteActionSignal = [RACSubject subject];
    @weakify(self);
    [self.bottomView.deleteActionSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if ([x boolValue]) {
            // 确定
            if (self.selectedModel.count == 0) {
                [NPTip showTip:@"请选择要删除的数据"];
                return;
            }
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定删除所选的数据吗？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
                if ([x integerValue] == 1) {
                    NSMutableArray *ids = [NSMutableArray arrayWithCapacity:self.selectedModel.count];
                    [self.selectedModel enumerateObjectsUsingBlock:^(WCRAppointLookRoomModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        [ids addObject:obj.ID];
                    }];
                    NSString *Ids = [ids componentsJoinedByString:@","];
                    
                    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"orderTableIds":Ids};
                    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/roomApiController/delOrderTable" ResponseBlock:^(NPGeneralResponse *responseMd) {
                        if (responseMd.isOK) {
                            [self.viewModel.dataSource removeObjectsInArray:self.selectedModel];
                            [self.selectedModel removeAllObjects];
                            [self.tableView reloadData];
                            self.isDelete = NO;
                        }
                    }];
                }
            }];
            [alertView show];
        }
        else {
            // 取消
            self.isDelete = NO;
        }
    }];
}
#pragma mark - 获取网络数据
- (void)getInfoListFromNetworkWithType:(LoadListType)type {
    @weakify(self);
    [_viewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        @strongify(self);
        [self.viewModel loadListDataCompleteReload:self.tableView IsEnd:isEnd];
    }];
}

#pragma mark - Table View Delegate
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.isDelete ? UITableViewCellEditingStyleDelete & UITableViewCellEditingStyleInsert : UITableViewCellEditingStyleNone;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isDelete) {
        id model = [self.viewModel modelAtIndexPath:indexPath];
        BOOL isSelect = NO;
        WCRAppointLookRoomModel *appointmentModel = model;
        appointmentModel.isSelected = !appointmentModel.isSelected;
        isSelect = appointmentModel.isSelected;
        
        WCRAppointmentLookRoomCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell setButtonSelected:isSelect];
        if (isSelect) {
            [_selectedModel addObject:model];
        }
        else {
            [_selectedModel removeObject:model];
        }
    }
    else {
        WCRAppointLookRoomDetailVC *detailVC = [[WCRAppointLookRoomDetailVC alloc] init];
        detailVC.appointmentModel = [_viewModel modelAtIndexPath:indexPath];
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}



@end
