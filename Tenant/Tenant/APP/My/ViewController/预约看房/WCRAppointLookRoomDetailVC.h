//
//  WCRAppointmentInfoDetailVC.h
//  Landlord
//
//  Created by eliot on 2017/5/18.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class WCRAppointLookRoomModel;

/// 预约看房 详情
@interface WCRAppointLookRoomDetailVC : WCRBasicViewController
@property (nonatomic, strong) WCRAppointLookRoomModel *appointmentModel;
@end
