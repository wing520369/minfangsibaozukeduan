//
//  WCRAppointmentInfoDetailVC.m
//  Landlord
//
//  Created by eliot on 2017/5/18.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRAppointLookRoomDetailVC.h"
#import "WCRAppointLookRoomModel.h"
#import "UIViewController+WCRCustom.h"
#import "NPGeneralApi.h"
#import "NSString+WCRCustom.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"

@interface WCRAppointLookRoomDetailVC ()

@end

@implementation WCRAppointLookRoomDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"预约看房"];
    [self addRightBarButtonItemTitle:@"删除"];
    [self getDetailDataFromNetwork];
}

// 删除
- (void)rightBarButtonItemAction:(UIButton *)button {
    
    __weak typeof(self) weakSelf = self;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定删除？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
        if ([x integerValue] == 1) {
            NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"orderTableIds":_appointmentModel.ID};
            [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/roomApiController/delOrderTable" ResponseBlock:^(NPGeneralResponse *responseMd) {
                if (responseMd.isOK) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateInfoList" object:nil];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }];
    [alertView show];
}

- (void)getDetailDataFromNetwork {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"orderTableId":_appointmentModel.ID};
    [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/roomApiController/getOrderTableInfo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            weakSelf.appointmentModel.isRead = @"1";
            [weakSelf createUI];
        }
    }];
}

- (void)createUI {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(kSpaceX, kSpaceX, kMainWidth - kSpaceX * 2, 0.1)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    
    UIFont *font = [UIFont CustomFontWithSize:15.0];
    NSString *text = [NSString stringWithFormat:@"预约看房：房产 %@ - 房号 %@",_appointmentModel.houseName,_appointmentModel.roomNo];
    
    CGFloat x = kSpaceX;
    CGFloat y = kSpaceX;
    CGFloat width = bgView.width_wcr - x * 2;
    CGFloat height = [NSString calculateWithContent:text MaxWidth:width Font:font];
    UILabel *houseLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    houseLabel.numberOfLines = 0;
    [houseLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    [bgView addSubview:houseLabel];
    
    
    NSArray *titles = @[
                        [NSString stringWithFormat:@"联系人：%@",_appointmentModel.name],
                        [NSString stringWithFormat:@"联系电话：%@",_appointmentModel.phone],
                        [NSString stringWithFormat:@"看房时间：%@",[_appointmentModel.time formatAllDateString]]
                        ];
    font = [UIFont CustomFontWithSize:13.0];
    CGFloat border = 7;
    y = houseLabel.bottom_wcr + border;
    height = font.lineHeight + 5;
    for (NSInteger i = 0; i < titles.count; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [label labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:[titles objectAtIndex:i]];
        [bgView addSubview:label];
        y = label.bottom_wcr + border;
    }
    
    bgView.height_wcr = y;
    [bgView setViewCorner:6.0];
}


@end
