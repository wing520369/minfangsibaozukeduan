//
//  SXPersonInfoEntity.h
//  SXEasyAddressBookDemo
//
//  Created by dongshangxian on 16/5/23.
//  Copyright © 2016年 Sankuai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SXPersonInfoEntity : NSObject

@property(nonatomic,copy)NSString *firstname;
@property(nonatomic,copy)NSString *lastname;
@property(nonatomic,copy)NSString *fullname;
@property(nonatomic,copy)NSString *phoneNumber;

///读取birthday生日
@property (nonatomic, copy) NSString *birthday;

///读取organization公司
@property (nonatomic, copy) NSString *organization;
///读取jobtitle工作
@property (nonatomic, copy) NSString *jobtitle;
///读取department部门
@property (nonatomic, copy) NSString *department;

///读取note备忘录
@property (nonatomic, copy) NSString *note;

/** 联系人电话数组,因为一个联系人可能存储多个号码*/
@property (nonatomic, strong) NSMutableArray *mobileArray;
///邮箱
@property (nonatomic, strong) NSMutableArray *emailList;
///地址
@property (nonatomic, strong) NSMutableArray *addressList;

- (NSDictionary *)updateDict;

+ (NSString *)jsonListTextOfList:(NSArray<SXPersonInfoEntity*> *)list;

- (NSString *)removePhoneSpecialSubString: (NSString *)string;



///获取URL多值
//@property (nonatomic, strong) NSMutableArray *urlList;
/** 联系人头像*/
//@property (nonatomic, strong) UIImage *headerImage;

//@property (nonatomic, copy) NSString *phone;

@end
