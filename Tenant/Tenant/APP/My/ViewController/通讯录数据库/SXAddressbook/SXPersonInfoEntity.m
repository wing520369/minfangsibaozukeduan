//
//  SXPersonInfoEntity.m
//  SXEasyAddressBookDemo
//
//  Created by dongshangxian on 16/5/23.
//  Copyright © 2016年 Sankuai. All rights reserved.
//

#import "SXPersonInfoEntity.h"

@implementation SXPersonInfoEntity

- (NSMutableArray *)mobileArray{
    if(!_mobileArray){
        _mobileArray = [NSMutableArray array];
    }
    return _mobileArray;
}

- (NSMutableArray *)emailList{
    if (!_emailList) {
        _emailList = [[NSMutableArray alloc]init];
    }
    return _emailList;
}

- (NSMutableArray *)addressList{
    if (!_addressList) {
        _addressList =  [[NSMutableArray alloc]init];
    }
    return _addressList;
}

- (NSDictionary *)updateDict{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"" forKey:@"phone"];
    [dict setObject:@"" forKey:@"name"];
    [dict setObject:@"" forKey:@"gender"];
    [dict setObject:@"" forKey:@"age"];
    [dict setObject:@"" forKey:@"birthday"];
    [dict setObject:@"" forKey:@"identifyCode"];
    [dict setObject:@"" forKey:@"identifyFace"];
    [dict setObject:@"" forKey:@"identifyBack"];
    [dict setObject:@"" forKey:@"otherInfo"];
    
    ///
    if (_fullname) {
        [dict setObject:_fullname forKey:@"name"];
    }
    //
    if (_birthday) {
        [dict setObject:_birthday forKey:@"birthday"];
    }
    //
    if (_mobileArray.count) {
        NSString *phone = [_mobileArray firstObject];
        [dict setObject:phone forKey:@"phone"];
    }
    /////
    NSMutableDictionary *otherInfo = [[NSMutableDictionary alloc]init];
    if (_organization) {
        [otherInfo setObject:_organization forKey:@"organization"];
    }
    //
    if (_jobtitle) {
        [otherInfo setObject:_jobtitle forKey:@"jobtitle"];
    }
    //
    if (_department) {
        [otherInfo setObject:_department forKey:@"department"];
    }
    //
    if (_note) {
        [otherInfo setObject:_note forKey:@"note"];
    }
    
    //
    if (_mobileArray.count > 1) {
        NSString *phoneList = [_mobileArray componentsJoinedByString:@","];
        [otherInfo setObject:phoneList forKey:@"phoneList"];
    }
    //
    if (_emailList.count) {
        NSString *emailList = [_emailList componentsJoinedByString:@","];
        [otherInfo setObject:emailList forKey:@"emailList"];
    }
    //
    if (_addressList.count) {
        NSString *addressList = [_addressList componentsJoinedByString:@","];
        [otherInfo setObject:addressList forKey:@"addressList"];
    }
    if (otherInfo.allKeys.count) {
        NSString *otherInfoJsonText = [SXPersonInfoEntity stringHanldToJsonObject:otherInfo];
        if (otherInfoJsonText) {
            [dict setObject:otherInfoJsonText forKey:@"otherInfo"];
        }
    }
    /////
    return dict;
}

+ (NSString *)jsonListTextOfList:(NSArray<SXPersonInfoEntity *> *)list{
    if (list.count < 1) {
        return nil;
    }
    //
    NSMutableArray *dictList = [[NSMutableArray alloc]init];
    for (SXPersonInfoEntity *model in list) {
        NSDictionary *dict = model.updateDict;
        
        [dictList addObject:dict];
    }
    //
    return [self stringHanldToJsonObject:dictList];
}

+ (NSString *)stringWithJsonObject:(id)obj{
    
    if([NSJSONSerialization isValidJSONObject:obj]){
        NSError *error;
        NSData *dictionaryData = [NSJSONSerialization dataWithJSONObject:obj options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonListText = [[NSString alloc]initWithData:dictionaryData encoding:NSUTF8StringEncoding];
        
        return jsonListText;
    }
    return nil;
}

+ (NSString *)stringHanldToJsonObject:(id)obj{
    NSString *jsonListText = [self stringWithJsonObject:obj];
    if (!jsonListText) {
        return nil;
    }
    jsonListText = [jsonListText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    jsonListText = [jsonListText stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    jsonListText =  [jsonListText stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    jsonListText =  [jsonListText stringByReplacingOccurrencesOfString:@" " withString:@""];
    return jsonListText;
}

//过滤指定字符串(可自定义添加自己过滤的字符串)
- (NSString *)removePhoneSpecialSubString:(NSString *)string
{
    //    string = [string stringByReplacingOccurrencesOfString:@"+86" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"-" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"(" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@")" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    return string;
}

//- (NSMutableArray *)urlList{
//    if (!_urlList) {
//        _urlList = [[NSMutableArray alloc]init];
//    }
//    return _urlList;
//
//}
@end
