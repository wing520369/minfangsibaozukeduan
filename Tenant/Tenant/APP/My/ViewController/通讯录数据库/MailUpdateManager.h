//
//  MailUpdateManager.h
//  HaiYun
//
//  Created by aleven1 on 2017/12/26.
//  Copyright © 2017年 Aleven. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MailUpdateManager : NSObject

+ (instancetype)sharedManager;

- (void)star;

@end
