//
//  NPSYSAuthorizatManager.m
//  MyTool
//
//  Created by HzB on 2017/2/15.
//  Copyright © 2017年 Aleven. All rights reserved.
//

#import "NPSYSAuthorizatManager.h"

#import <AddressBook/AddressBook.h>
#import <Contacts/Contacts.h>

#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>

#import <MediaPlayer/MediaPlayer.h>
#import <CoreLocation/CLLocationManager.h>

///系统的版本号
#define SYSTEM_VERSION_NPSYSAUTH [[[UIDevice currentDevice] systemVersion] floatValue]

#pragma mark -  NPSYSAuthorizatInfo
@interface NPSYSAuthorizatInfo : NSObject

///名称标题
@property (nonatomic,copy) NSString *name;
///设置路径
@property (nonatomic,copy) NSString *setupPath;
///提醒语句
@property (nonatomic,copy) NSString *setupPathTip;
///类型
@property (nonatomic,assign) NPSYSAuthorizat authType;

@end

@implementation NPSYSAuthorizatInfo

+ (instancetype)infoWithAuthType:(NPSYSAuthorizat)authType{
    return [[self alloc] initWithAuthType:authType];
}

- (instancetype)initWithAuthType:(NPSYSAuthorizat)authType{

    if (self = [self init]) {
        _authType = authType;
        switch (authType) {
            case NPSYSAuthorizat_ABAddress:{
                _name = @"通讯录";
                _setupPathTip = @"请在手机的""设置-隐私-通讯录""中允许访问通讯录";
                break;
            }
            case NPSYSAuthorizat_Camera:{
                _name = @"相机";
                _setupPathTip = @"请在手机的""设置-隐私-相机""中允许访问相机";
                break;
            }
            case NPSYSAuthorizat_PhotoLibrary:{
                _name = @"相册";
                _setupPathTip = @"请在手机的""设置-隐私-照片""中允许访问照片";
                break;
            }
            case NPSYSAuthorizat_Location:{
                _name = @"定位";
                _setupPathTip = @"请在手机的""设置-隐私-位置""中允许访问定位";
                break;
            }
            case NPSYSAuthorizat_iTuneLibrary:{
            
                _name = @"音乐库";
                _setupPathTip = @"请在手机的""设置-隐私-媒体与AppleMusic""中允许访问";
                break;
            }
        }
    }
    return self;
}

@end


#pragma mark -
#pragma mark -  NPSYSAuthorizatManager
@interface NPSYSAuthorizatManager()<UIAlertViewDelegate>

@property (nonatomic,strong) NPSYSAuthorizatInfo *authInfo;
@property (nonatomic,copy) NPSYSAuthResult result;

@end

@implementation NPSYSAuthorizatManager

+ (instancetype)sharedManager{
    static NPSYSAuthorizatManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc]init];
    });
    return manager;
}

- (void)requestAuthorizationWithType:(NPSYSAuthorizat)type
                              result:(NPSYSAuthResult)result{
    self.authInfo = [NPSYSAuthorizatInfo infoWithAuthType:type];
    self.result = result;
    [self _requestAuthority];
}

+ (void)requestAuthorizationWithType:(NPSYSAuthorizat)type
                              result:(NPSYSAuthResult)result{
    NPSYSAuthorizatManager *mananger = [NPSYSAuthorizatManager sharedManager];
    [mananger requestAuthorizationWithType:type result:result];
}

#pragma mark - requestAuthorization
- (void)_requestAuthority{
    switch (_authInfo.authType) {
        case NPSYSAuthorizat_ABAddress:{
            [self _requestABAddressAuthority];
            break;
        }
        case NPSYSAuthorizat_Camera:{
            [self _requestCameraAuthority];
            break;
        }
        case NPSYSAuthorizat_PhotoLibrary:{
            [self _requestPhotoLibraryAuthority];
            break;
        }
        case NPSYSAuthorizat_Location:{
            [self _requestLocationAuthority];
            break;
        }case NPSYSAuthorizat_iTuneLibrary:{
            [self _requestiTuneLibraryAuthority];
            break;
        }
    }
}

- (void)sendResultStatus:(NPSYSAuthorizatStatus)status
                   error:(NSError *)error{


    
    if ([NSThread isMainThread]) {
        
        if (_result) {
            _result(status,error);
        }
        self.authInfo = nil;
        self.result = nil;

    }else {
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (_result) {
                _result(status,error);
            }
            self.authInfo = nil;
            self.result = nil;
        });
    }
}

///注册音乐库
- (void)_requestiTuneLibraryAuthority{

    if (SYSTEM_VERSION_NPSYSAUTH < 9.3) {
        [self sendResultStatus:NPSYSAuthorizatStatusAuthorized error:nil];
    }else{
        __weak typeof(self) weakSelf = self;
        MPMediaLibraryAuthorizationStatus status =  [MPMediaLibrary authorizationStatus];
        if (status == MPMediaLibraryAuthorizationStatusNotDetermined) {
            [MPMediaLibrary requestAuthorization:^(MPMediaLibraryAuthorizationStatus status) {
                [weakSelf _requestiTuneLibraryAuthority];
                
            }];
        }else{
            NPSYSAuthorizatStatus npstatu ;
            if (status == MPMediaLibraryAuthorizationStatusRestricted) {
                npstatu = NPSYSAuthorizatStatusRestricted;
            }else if(status == MPMediaLibraryAuthorizationStatusDenied){
                npstatu = NPSYSAuthorizatStatusDenied;
            }else{
               npstatu =(NPSYSAuthorizatStatus)status;
            }
            [self _handleResultAuthStatu:npstatu];
        }
    }
}

///注册通讯录权限
- (void)_requestABAddressAuthority{
    
    if (SYSTEM_VERSION_NPSYSAUTH >= 9.0) {
        
        // 1.获取授权的状态
        CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
        
        if (status == CNAuthorizationStatusNotDetermined) {
             __weak typeof(self) weakSelf = self;
            CNContactStore  *contactStore = [[CNContactStore alloc] init];
            [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if (granted) {
                    //授权成功
                } else {
                    //授权失败
                }
                [weakSelf _requestABAddressAuthority];
            }];
            
        }else{
            
            NPSYSAuthorizatStatus npstatu = (NPSYSAuthorizatStatus)status;
            [self _handleResultAuthStatu:npstatu];
        }
        
    }else{
    
        // 1.获取授权的状态
        ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
        // 2.判断授权状态,如果是未决定状态,才需要请求
        if (status == kABAuthorizationStatusNotDetermined) {
            // 用户还没有决定是否授权你的程序进行访问
            // 创建通讯录进行授权
            __weak typeof(self) weakSelf = self;
            ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                if (granted) {
                    //授权成功
                } else {
                    //授权失败
                }
                [weakSelf _requestABAddressAuthority];
            });
            
        }else{
            NPSYSAuthorizatStatus npstatu = (NPSYSAuthorizatStatus)status;
            [self _handleResultAuthStatu:npstatu];
        }
    }
}

///注册相机权限
- (void)_requestCameraAuthority{
    //
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    // 2.判断授权状态,如果是未决定状态,才需要请求
    if (authStatus == AVAuthorizationStatusNotDetermined) {
        // 用户还没有决定是否授权你的程序进行访问
        __weak typeof(self) weakSelf = self;
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            [weakSelf _requestCameraAuthority];
        }];
        
    }else{
        NPSYSAuthorizatStatus npstatu = (NPSYSAuthorizatStatus)authStatus;
        [self _handleResultAuthStatu:npstatu];
    }
}

///注册相册权限
- (void)_requestPhotoLibraryAuthority{

    if (SYSTEM_VERSION_NPSYSAUTH >= 8.0) {
        PHAuthorizationStatus nowStatus = [PHPhotoLibrary authorizationStatus];
        // 2.判断授权状态,如果是未决定状态,才需要请求
        if (nowStatus == PHAuthorizationStatusNotDetermined) {
            // 用户还没有决定是否授权你的程序进行访问
            __weak typeof(self) weakSelf = self;
            // 用户尚未做出了选择这个应用程序的问候
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                NPSYSAuthorizatStatus npstatu = (NPSYSAuthorizatStatus)status;
                [weakSelf _handleResultAuthStatu:npstatu];
            }];

        }else{
            NPSYSAuthorizatStatus statu = (NPSYSAuthorizatStatus)nowStatus;
            [self _handleResultAuthStatu:statu];
        }

    }else{
        __weak typeof(self) weakSelf = self;
        ALAuthorizationStatus nowStatus = [ALAssetsLibrary authorizationStatus];
        if (nowStatus == ALAuthorizationStatusNotDetermined) {
            ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
            [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                if (*stop) {
                    return;
                }
                *stop = TRUE;//不能省略
                [weakSelf _requestPhotoLibraryAuthority];
            } failureBlock:^(NSError *error) {
                [weakSelf _requestPhotoLibraryAuthority];
            }];
        }else{
            NPSYSAuthorizatStatus statu = (NPSYSAuthorizatStatus)nowStatus;
            [self _handleResultAuthStatu:statu];
        }
    }
}

///注册定位比较麻烦，
///这里不注册只检测，所以验证定位时，要另外判断error
- (void)_requestLocationAuthority{
    
    NSString *title = @"获取定位权限失败";
    NSString *message = @"请在手机的“设置-隐私-定位”中允许访问定位";
    if (![CLLocationManager locationServicesEnabled]) {
        //这里是总的定位开关，如关了提示调到隐私哪里打开
        
        if (SYSTEM_VERSION_NPSYSAUTH >= 8.0) {
            
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertVC addAction:action];
            UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
            [controller presentViewController:alertVC animated:YES completion:nil];
            
        }else{
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message  delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
            [alertView show];
        }
        //
        NSError *error = [NSError errorWithDomain:@"获取定位权限失败" code:kCLAuthorizationStatusDenied userInfo:nil];
        [self sendResultStatus:NPSYSAuthorizatStatusDenied error:error];
        
    }else{
    
        NSError *error = nil;
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        NPSYSAuthorizatStatus npStatus = NPSYSAuthorizatStatusNotDetermined;
        if (status == kCLAuthorizationStatusNotDetermined) {
            npStatus = NPSYSAuthorizatStatusNotDetermined;
//            error = [NSError errorWithDomain:@"还没请求获取定位权限" code:status userInfo:nil];
            
        }else if (status == kCLAuthorizationStatusAuthorizedWhenInUse ||
                  status == kCLAuthorizationStatusAuthorizedAlways) {
           
            npStatus = NPSYSAuthorizatStatusAuthorized;
            
        }else if (status == kCLAuthorizationStatusDenied){
        
            npStatus = NPSYSAuthorizatStatusDenied;
            
            NSString *domain = [NSString stringWithFormat:@"获取%@权限失败",_authInfo.name];
            error = [NSError errorWithDomain:domain code:2 userInfo:nil];
            [self showDeniedStatusAlertWithError:error];
        
        }else if (status == kCLAuthorizationStatusRestricted){
            
            npStatus =  NPSYSAuthorizatStatusRestricted;
            
            NSString *domain = [NSString stringWithFormat:@"家长控制或其它一些许可配置阻止程序与%@进行交互",_authInfo.name];
            error = [NSError errorWithDomain:domain code:1 userInfo:nil];
            [self showDeniedStatusAlertWithError:error];
        }
        //
        [self sendResultStatus:npStatus error:error];
    }
}

///处理权限结果
- (void)_handleResultAuthStatu:(NPSYSAuthorizatStatus)authStatu{
    
    NSError *error = nil;
    if (authStatu == NPSYSAuthorizatStatusNotDetermined) {
        ///用户还没有决定是否授权你的程序进行访问
    }else if (authStatu == NPSYSAuthorizatStatusRestricted){
        ///iOS设备上的家长控制或其它一些许可配置阻止程序与通讯录数据库进行交互
        NSString *domain = [NSString stringWithFormat:@"家长控制或其它一些许可配置阻止程序与%@进行交互",_authInfo.name];
        error = [NSError errorWithDomain:domain code:authStatu userInfo:nil];
    }else if (authStatu == NPSYSAuthorizatStatusDenied){
        ///用户明确的拒绝了你的程序对通讯录的访问
        NSString *domain = [NSString stringWithFormat:@"获取%@权限失败",_authInfo.name];
        error = [NSError errorWithDomain:domain code:authStatu userInfo:nil];

    }else if (authStatu == NPSYSAuthorizatStatusAuthorized){
        ///用户已经授权给你的程序对通讯录进行访问
    }
    
    //
    if (error) {
        [self showDeniedStatusAlertWithError:error];
    }
    [self sendResultStatus:authStatu error:error];
    
//    switch (authStatus) {
//        case AVAuthorizationStatusNotDetermined:{
//            ///用户还没有决定是否授权你的程序进行访问
//            [self _requestCameraAuthority];
//            break;
//        }
//        case AVAuthorizationStatusRestricted:{
//            ///iOS设备上的家长控制或其它一些许可配置阻止程序与通讯录数据库进行交互
//            NSString *domain = [NSString stringWithFormat:@"家长控制或其它一些许可配置阻止程序与%@进行交互",@"相机"];
//            error = [NSError errorWithDomain:domain code:authStatus userInfo:nil];
//            break;
//        };
//        case AVAuthorizationStatusDenied:{
//            ///用户明确的拒绝了你的程序对通讯录的访问
//            NSString *domain = [NSString stringWithFormat:@"获取相机权限失败"];
//            error = [NSError errorWithDomain:domain code:authStatus userInfo:nil];
//            break;
//        }
//        case AVAuthorizationStatusAuthorized:{
//            ///用户已经授权给你的程序对通讯录进行访问
//            break;
//        }
//    }

}

#pragma mark - ShowDeniedStatusAlert
///显示提示
- (NSURL *)getAuthorizationSetupOpenUrl{
    NSURL *openUrl = nil;
    if (SYSTEM_VERSION_NPSYSAUTH >= 8.0) {
        openUrl = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    } else{
        NSString *identifier = [[NSBundle mainBundle] bundleIdentifier];
        openUrl = [NSURL URLWithString:[NSString stringWithFormat:@"prefs:root=%@",identifier]];
        //        openUrl = [NSURL URLWithString:[NSString stringWithFormat:@"prefs:root=Privacy"]];
    }
    return openUrl;
}

- (void)showDeniedStatusAlertWithError:(NSError *)error{
    NSString *title = [NSString stringWithFormat:@"获取%@权限失败",_authInfo.name];
    if (error.code == 1) {

        if (SYSTEM_VERSION_NPSYSAUTH >= 8.0) {
            
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:error.domain preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertVC addAction:action];
            UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
            [controller presentViewController:alertVC animated:YES completion:nil];
            
        }else{
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:error.domain  delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
            [alertView show];
        }
        
    }else if (error.code == 2){
        ///禁止了访问权限
        NSURL *openUrl = [self getAuthorizationSetupOpenUrl];
        BOOL canOpenUrl =  [[UIApplication sharedApplication] canOpenURL:openUrl];
        if (canOpenUrl) {
            ///可以调整到设置页面
            if (SYSTEM_VERSION_NPSYSAUTH >= 8.0) {
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:_authInfo.setupPathTip preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
                
                UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [[UIApplication sharedApplication] openURL:openUrl];
                }];
                
                [alertVC addAction:action1];
                [alertVC addAction:action2];
                UIWindow *window = [[UIApplication sharedApplication] keyWindow];
                [window.rootViewController presentViewController:alertVC animated:YES completion:nil];
                
            }else{
                
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:_authInfo.setupPathTip  delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"设置",nil];
                [alertView show];
            }
            
        }else{
            if (SYSTEM_VERSION_NPSYSAUTH >= 8.0) {
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:_authInfo.setupPathTip preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertVC addAction:action];
                UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
                [controller presentViewController:alertVC animated:YES completion:nil];
            }else{
                
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:_authInfo.setupPathTip  delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
                [alertView show];
            }
        }
        
    }else{
    
        if (SYSTEM_VERSION_NPSYSAUTH >= 8.0) {
            
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:error.domain preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertVC addAction:action];
            UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
            [controller presentViewController:alertVC animated:YES completion:nil];
            
        }else{
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:error.domain  delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
            [alertView show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[self getAuthorizationSetupOpenUrl]];
    }
}

@end
