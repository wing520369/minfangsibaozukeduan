//
//  AddContactVC.m
//  ShareBey
//
//  Created by HzB on 16/7/6.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import "AddContactVC.h"
#import "AddContactCell.h"
#import "AddContactSectionHeaderView.h"
#import "NPAddressBookLoadFactory.h"
//#import "SearchContactVC.h"
#import "NPHUD.h"
#import "DZNEmptyDataProvider.h"
#import "NPGeneralApi.h"
#import "NPTip.h"
#import "MailUpdateManager.h"

@interface AddContactVC ()
<AddContactCellDelegate,
UITableViewDataSource,
UITableViewDelegate>

///要调用 initializeTableViewWithStyle 初始化
@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic, strong) NPAddressBookLoad *bookTool;
@property (nonatomic,strong) NSDictionary *dataSource;
@property (nonatomic,strong) NSArray *indexList;

@property (nonatomic,strong) NSMutableArray *allDataSource;

@end

@implementation AddContactVC

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor whiteColor];
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"通讯录"];

    //
    [[MailUpdateManager sharedManager] star];
    //
    _bookTool = [NPAddressBookLoadFactory establishAddressBookLoad];
    //
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight -  64.0) style:UITableViewStylePlain];
    [self.view addSubview:tableView];
    tableView.estimatedRowHeight = 0;
    tableView.estimatedSectionFooterHeight = 0;
    tableView.estimatedSectionHeaderHeight = 0.0f;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor colorWithRGB:248.0 G:248.0 B:248.0];
    tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    tableView.sectionIndexColor = [UIColor colorWithRGB:101.0 G:101.0 B:101.0];
    tableView.rowHeight = [AddContactCell cellHeight];
    tableView.sectionHeaderHeight = [AddContactSectionHeaderView viewHeight];
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 18.0)];
    self.tableView = tableView;

    //获取本地通讯录
     __weak typeof(self) weakSelf = self;
    NPHUD *hud = [NPHUD showHUDAddedTo:self.view];
    [_bookTool readAddressBookResult:^(NSArray<NPPersonModel *> *personList, NSError *error) {
        [hud hide:YES];
        if (error) {
            return;
        }
        [weakSelf loadPhoneStatus];
    }];
}

#pragma mark - 获取手机的注册状态
- (void)loadPhoneStatus{

    //
    NSMutableArray *phoneList = [[NSMutableArray alloc]init];
    for (NPPersonModel *person in _bookTool.personList) {
        [phoneList addObject:person.mobileNumber];
    }

    if (phoneList.count < 1) {
        if (!self.tableView.emptyDataProvider) {
            DZNEmptyDataProvider *dr = [DZNEmptyDataProvider providerWithEmptyType:DZNEmptyDataType_NoData];
            dr.title = [DZNEmptyDataProvider defaultAttTitle:@"没有联系人"];
            self.tableView.emptyDataProvider = dr;
            [self.tableView reloadData];
        }
        return;
    }
    //
    NSString *phoneText = [phoneList componentsJoinedByString:@","];
    
    NSDictionary *dict = @{@"userId" : [WCRUserModelTool getUserId],
                           @"tokenName" : [WCRUserModelTool getToken],
                           @"phones" : phoneText};
    __weak typeof(self) weakSelf = self;
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/userApiController/inviteFriend" ResponseBlock:^(NPGeneralResponse *responseMd) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (responseMd.isOK) {
            NSMutableArray *userList = [WCRUserModel jsonWithArray:responseMd.response];
            [strongSelf matchingData:userList];
        }
    }];
}

#pragma mark - 匹配数据
- (void)matchingData:(NSMutableArray *)userList{
     __weak typeof(self) weakSelf = self;
    [self showHud];
    for (NPPersonModel *model in self.bookTool.personList) {
        AddContactInfoModel *appInfoModel = [[AddContactInfoModel alloc]init];
        model.appModel = appInfoModel;
        
        for (WCRUserModel *user in userList) {
            //如何号码相同就去状态
            if ([model.mobileNumber isEqualToString:user.phone]) {
                appInfoModel.user = user;
                appInfoModel.isReg = YES;
                [userList removeObject:user];
                break;
            }
        }
    }
    //匹配好状态后,按首字母分组
    [NPAddressBookLoad letteGroupedgroupedWithUserList:self.bookTool.personList result:^(NSDictionary<NSString *,NSArray *> *addressBookDict, NSArray *indexList) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
   
        [strongSelf hideHud];
        strongSelf.dataSource = addressBookDict;
        strongSelf.indexList = indexList;
        [strongSelf.tableView reloadData];
    }];
}

#pragma mark - tableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _indexList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *key = [_indexList objectAtIndex:section];
    NSArray *array = [_dataSource objectForKey:key];
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [_indexList objectAtIndex:indexPath.section];
    NSArray *array = [_dataSource objectForKey:key];
    NPPersonModel *model = [array objectAtIndex:indexPath.row];
    
    //
    static NSString *cellId = @"cellId";
    AddContactCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[AddContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.delegate = self;
    }

    cell.model = model;
    cell.lineView.hidden = array.count - 1 == indexPath.row;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView
            viewForHeaderInSection:(NSInteger)section{
    
    static NSString *headId = @"headId";
    AddContactSectionHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headId];
    if (!headerView) {
        headerView = [[AddContactSectionHeaderView alloc]initWithReuseIdentifier:headId];
    }
    headerView.titleLabel.text = [_indexList objectAtIndex:section];
    return headerView;
}

#pragma mark -设置右方表格的索引数组
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return _indexList;
}

- (NSInteger)tableView:(UITableView *)tableView
    sectionForSectionIndexTitle:(NSString *)title
               atIndex:(NSInteger)index{
    return index;
}

#pragma mark - AddContactCellDelegate
- (void)addContactCell:(AddContactCell *)cell
          buttonAction:(UIButton *)button{
    
    __weak typeof(self) weakSelf = self;
    __block NPPersonModel *blockModel = cell.model;
    if (button.tag == AddContactCellButtonTag_Invitation) {
       
        [weakSelf sendInvitationForModel:blockModel];
        
    }else if (button.tag == AddContactCellButtonTag_AddFriend){

//        NSDictionary *dict = @{@"userId" : CURRENT_USERID,
//                               TokenNameKey : TokenName,
//                               @"toId" :   blockModel.appModel.user.userId};
//        NPRequest *api = [NPRequest requestWithRequestParameters:dict tailUrl:@"/api/userApiController/addInvite"];
//        api.tipConfig = [NPRequestTipConfig tipConfigWithAnimatingVC:[UIWindow np_topViewController]];
//        [api startWithReformerCompleteBlock:^(NPBaseRequest * _Nonnull request, NPResponseModel * _Nonnull responseMd) {
//
//            __strong typeof(weakSelf) strongSelf = weakSelf;
//            if (responseMd.isOK) {
//                blockModel.appModel.isSendAddFriend = YES;
//                [NPTip showTip:@"已发送添加好友请求，请耐心等待~~"];
//                [strongSelf.tableView reloadData];
//            }
//        }];
    }
}

#pragma mark - 邀请用户注册
- (void)sendInvitationForModel:(NPPersonModel *)model{

    __weak typeof(self) weakSelf = self;
    __block NPPersonModel *blockModel = model;
    NSDictionary *dict = @{@"userId" : [WCRUserModelTool getUserId],
                           @"tokenName" : [WCRUserModelTool getToken],
                           @"phone" : model.mobileNumber};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/userApiController/sendSMS" ResponseBlock:^(NPGeneralResponse *responseMd) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (responseMd.isOK) {
            [NPTip showTip:@"已发送邀请"];
            blockModel.appModel.isInvitationRegin = YES;
            [strongSelf.tableView reloadData];
        }
    }];
    
 
}

#pragma mark - Action
- (void)rightNavItemAction:(UIBarButtonItem *)item{
//    __weak typeof(self) weakSelf = self;
//    SearchContactVC *vc = [[SearchContactVC alloc]init];
//    vc.dataSource = _bookTool.personList;
//    vc.needUpdateBlock = ^{
//        [weakSelf.tableView reloadData];
//    };
//    [self.navigationController pushViewController:vc animated:NO];
}

@end
