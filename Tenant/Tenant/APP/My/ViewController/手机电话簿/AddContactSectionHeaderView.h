//
//  AddContactSectionHeaderView.h
//  ShareBey
//
//  Created by HzB on 16/7/6.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  添加通讯录好友
 */
@interface AddContactSectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong)  UILabel *titleLabel;

+ (CGFloat)viewHeight;

@end
