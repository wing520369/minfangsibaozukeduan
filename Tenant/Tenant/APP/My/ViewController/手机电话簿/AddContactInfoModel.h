//
//  AddContactInfoModel.h
//  XuanDaXue
//
//  Created by aleven1 on 2017/12/26.
//  Copyright © 2017年 Aleven. All rights reserved.
//

#import "WCRBasicModel.h"
#import "WCRUserModel.h"

@interface AddContactInfoModel : WCRBasicModel

///
@property (nonatomic, copy) NSString *Id;
///
@property (nonatomic, copy) NSString *userId;
///已成为好友：1是，0否
@property (nonatomic, assign) NSInteger isFan;
///已注册：1是，0否
@property (nonatomic, assign) NSInteger isReg;

///是否 发送加好友了 自己写的，用来标记
@property (nonatomic, assign) NSInteger isSendAddFriend;
///是否邀请注册了 自己写的
@property (nonatomic, assign) NSInteger isInvitationRegin;

///姓名
@property (nonatomic, copy) NSString *name;
///手机号码
@property (nonatomic, copy) NSString *phone;
///
@property (nonatomic, copy) NSString *remarks;

@property (nonatomic, copy) NSString *json;
///
@property (nonatomic, strong) WCRUserModel *user;

///创建时间
@property (nonatomic, copy) NSString *createDate;
///更新时间
@property (nonatomic, copy) NSString *updateDate;


@end
