//
//  AddContactSectionHeaderView.m
//  ShareBey
//
//  Created by HzB on 16/7/6.
//  Copyright © 2016年 HzB. All rights reserved.
//

static NSString *AddContactSectionHeaderView_ID = @"AddContactSectionHeaderView.ID";

#import "AddContactSectionHeaderView.h"

@interface AddContactSectionHeaderView()

@end

@implementation AddContactSectionHeaderView

+ (CGFloat)viewHeight{

    return 40.0f;
}

+ (instancetype)headerViewWithTableView:(UITableView *)tableView{
    
    AddContactSectionHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:AddContactSectionHeaderView_ID];
    if (nil == headerView) {
        headerView = [[AddContactSectionHeaderView alloc]initWithReuseIdentifier:AddContactSectionHeaderView_ID];
    }
    return headerView;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
     
        self.contentView.backgroundColor = [UIColor colorWithRGB:236.0 G:236.0 B:236.0];
        CGFloat spaceX = 12.0;
        //标题
        CGFloat x = spaceX + 2;
        CGFloat y = 0.0f;
        CGFloat width = kMainWidth - x;
        CGFloat height = [AddContactSectionHeaderView viewHeight];
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        _titleLabel.font = [UIFont systemFontOfSize:17.0];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = [UIColor blackColor];
        [ self.contentView addSubview:_titleLabel];
        
    }
    return self;
}


@end
