//
//  NPPersonModel.h
//  PPGetAddressBook
//
//  Created by HzB on 2017/1/7.
//  Copyright © 2017年 AndyPang. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "AddContactInfoModel.h"

@interface NPPersonModel : NSObject

///用户名
@property (nonatomic,copy) NSMutableArray *phoneList;
///名字
@property (nonatomic,copy) NSString *name;
///头像
@property (nonatomic,strong) UIImage *avatarImage;
///移动号码，如有多个取第一个
@property (nonatomic,copy,readonly) NSString *mobileNumber;
//
@property (nonatomic,strong)  AddContactInfoModel *appModel;


- (NSString *)searchName;

@end
