//
//  NPAddressBookLoadIOS9Ago.m
//  PPGetAddressBook
//
//  Created by HzB on 2017/1/7.
//  Copyright © 2017年 AndyPang. All rights reserved.
//

#import "NPAddressBookLoadIOS9Ago.h"
#import <AddressBook/AddressBook.h>

@implementation NPAddressBookLoadIOS9Ago

- (void)requestAuthorizationResult:(void (^)(NSError *))result{
    
    // 1.获取授权的状态
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    // 2.判断授权状态,如果是未决定状态,才需要请求
    switch (status) {
        case kABAuthorizationStatusNotDetermined:{
            // 用户还没有决定是否授权你的程序进行访问
            // 创建通讯录进行授权
            ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                NSError *myError = nil;
                if (granted) {
                    //授权成功
                } else {
                    //授权失败
                    myError = [NSError errorWithDomain:@"授权失败" code:kABAuthorizationStatusDenied userInfo:nil];
                    
                    //提示
//                    [self showDeniedStatusAlertWithError:myError];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (result) {
                        result(myError);
                    }
                });
              
            });
            break;
        }
        case kABAuthorizationStatusRestricted:{
            // iOS设备上的家长控制或其它一些许可配置阻止程序与通讯录数据库进行交互
            NSError *myError = [NSError errorWithDomain:@"家长控制或其它一些许可配置阻止程序与通讯录数据库进行交互" code:kABAuthorizationStatusRestricted userInfo:nil];
            
            //提示
//            [self showDeniedStatusAlertWithError:myError];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (result) {
                    result(myError);
                }
            });
            break;
        }

        case kABAuthorizationStatusDenied:{
        // 用户明确的拒绝了你的程序对通讯录的访问
            NSError *myError = [NSError errorWithDomain:@"授权失败" code:kABAuthorizationStatusDenied userInfo:nil];
            
            //提示
//            [self showDeniedStatusAlertWithError:myError];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (result) {
                    result(myError);
                }
            });
            break;
        }
            
        case kABAuthorizationStatusAuthorized:{
            // 用户已经授权给你的程序对通讯录进行访问
            dispatch_async(dispatch_get_main_queue(), ^{
                if (result) {
                    result(nil);
                }
            });
            break;
        }
    }
}

- (void)readAddressBookResult:(NPAddressBookReadResult)result{
    
//    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
//    [self requestAuthorizationResult:^(NSError *error) {
//        dispatch_semaphore_signal(sema);
//        if (error) {
//            if (result) {
//                result(nil,error);
//                [self showDeniedStatusAlertWithError:error];
//            }
//            return;
//        }
//    }];
//    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    //
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    if (status == 1 || status == 2) {
        if (result) {
            NSError *error = [NSError errorWithDomain:@"读取通讯录失败~~" code:-1 userInfo:nil];
            result(nil,error);
        }
        return;
    }
    
    //
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    CFArrayRef results = ABAddressBookCopyArrayOfAllPeople(addressBook);
    self.personList = [[NSMutableArray alloc]init];
    for(int i = 0; i < CFArrayGetCount(results); i++)
    {
        NPPersonModel *bookModel = [[NPPersonModel alloc]init];
        
        ABRecordRef person = CFArrayGetValueAtIndex(results, i);
        
        //名字
        NSString *firstName, *lastName, *middleName,*fullName;
        NSString * sysFirstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
        if(nil != sysFirstName){
            firstName = sysFirstName;
        }else{
            firstName = @"" ;
        }
        
        NSString * sysMiddleName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonMiddleNameProperty);
        if (nil != sysMiddleName) {
            middleName = sysMiddleName;
        }else {
            middleName = @"" ;
        }
        NSString * sysLastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
        if(nil != sysLastName){
            lastName = sysLastName;
        }else{
            lastName = @"";
        }
        fullName = [NSString stringWithFormat:@"%@%@%@",lastName,middleName,firstName];
        if ([fullName isEqualToString:@""]) {
            fullName = @"无名氏";
        }
        bookModel.name = fullName;
        
        //手机号码
        ABMultiValueRef phone =  (ABMultiValueRef)ABRecordCopyValue(person, kABPersonPhoneProperty);
        if ((phone != nil) && ABMultiValueGetCount(phone)>0) {
            
            NSMutableArray *phoneList = [[NSMutableArray alloc]init];
            for (int m = 0; m < ABMultiValueGetCount(phone); m++) {
                
                NSString * aPhone = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, m);
                aPhone = [self filterSpecialOfPhone:aPhone];
                //
                if (aPhone.length == 11) {
                     [phoneList addObject:aPhone];
                }
            }
            bookModel.phoneList = phoneList;
        }
        //获取当前联系人头像图片
//        NSData *userImageData = (__bridge NSData*)(ABPersonCopyImageData(person));
        NSData *userImageData = (__bridge_transfer NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail);
        if (userImageData) {
            bookModel.avatarImage = [UIImage imageWithData:userImageData];
        }
        //
        if (bookModel.phoneList.count > 0) {
            [self.personList addObject:bookModel];
        }
        //
        CFRelease(phone);
        CFRelease(person);
    }
    //
    CFRelease(results);
    //
    if (result) {
        result(self.personList,nil);
    }
    
}

@end
