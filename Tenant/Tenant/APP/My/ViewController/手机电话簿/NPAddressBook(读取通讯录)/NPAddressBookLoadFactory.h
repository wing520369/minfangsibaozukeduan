//
//  NPAddressBookLoadFactory.h
//  PPGetAddressBook
//
//  Created by HzB on 2017/1/9.
//  Copyright © 2017年 AndyPang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NPAddressBookLoad.h"

@interface NPAddressBookLoadFactory : NSObject

///创建获取通讯录工具
+ (NPAddressBookLoad *)establishAddressBookLoad;

@end
