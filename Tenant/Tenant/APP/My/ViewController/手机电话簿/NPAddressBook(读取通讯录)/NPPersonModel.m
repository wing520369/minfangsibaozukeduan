//
//  NPPersonModel.m
//  PPGetAddressBook
//
//  Created by HzB on 2017/1/7.
//  Copyright © 2017年 AndyPang. All rights reserved.
//

#import "NPPersonModel.h"

@implementation NPPersonModel

- (void)setPhoneList:(NSMutableArray *)phoneList{
    _phoneList = phoneList;
    _mobileNumber = nil;
    for (NSString *phone in _phoneList) {
        if (phone.length == 11) {
            _mobileNumber = phone;
            break;
        }
    }
}

- (NSString *)searchName{
    if (_appModel) {
        if (_appModel.isReg) {
            return _appModel.user.showName;
        }
        return _name;
    }
    return _name;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"%@ = %@",_name,_phoneList];
}


@end
