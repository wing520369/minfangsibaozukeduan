//
//  NPAddressBookLoad.h
//  PPGetAddressBook
//
//  Created by HzB on 2017/1/7.
//  Copyright © 2017年 AndyPang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NPPersonModel.h"

///读取通讯通回调
typedef void(^NPAddressBookReadResult)(NSArray<NPPersonModel *> *personList,NSError *error);

///读取通讯通，按首字母分好组，回调
typedef void(^NPAddressBookReadLetteGroupedResult)(NSDictionary<NSString *,NSArray *> *addressBookDict,NSArray *indexList);

@interface NPAddressBookLoad : NSObject

@property (nonatomic, strong) NSMutableArray<NPPersonModel *> *personList;

///请求、检查用户通讯录权限
- (void)requestAuthorizationResult:(void(^)(NSError *error))result;

///读取通讯通
- (void)readAddressBookResult:(NPAddressBookReadResult)result;

///过滤掉号码的特殊字符
- (NSString *)filterSpecialOfPhone: (NSString *)phone;

///把通讯录按按首字母分好组
+ (void)letteGroupedgroupedWithUserList:(NSArray<NPPersonModel *> *)personList
                                 result:(NPAddressBookReadLetteGroupedResult)result;
///显示被拒绝了的提示框
- (void)showDeniedStatusAlertWithError:(NSError *)error;

@end
