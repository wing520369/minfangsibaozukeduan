//
//  NPAddressBookLoadFactory.m
//  PPGetAddressBook
//
//  Created by HzB on 2017/1/9.
//  Copyright © 2017年 AndyPang. All rights reserved.
//

#import "NPAddressBookLoadFactory.h"
#import "NPAddressBookLoadIOS9Ago.h"
#import "NPAddressBookLoadIOS9Later.h"

@implementation NPAddressBookLoadFactory

+ (NPAddressBookLoad *)establishAddressBookLoad{
    NPAddressBookLoad *load = nil;
    float systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (systemVersion >= 9.0) {
        load = [[NPAddressBookLoadIOS9Later alloc]init];
    }else{
        load = [[NPAddressBookLoadIOS9Ago alloc]init];
    }
    return load;
}

@end
