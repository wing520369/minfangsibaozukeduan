//
//  NPAddressBookLoadIOS9Later.m
//  PPGetAddressBook
//
//  Created by HzB on 2017/1/7.
//  Copyright © 2017年 AndyPang. All rights reserved.
//

#import "NPAddressBookLoadIOS9Later.h"
#import <Contacts/Contacts.h>

@interface NPAddressBookLoadIOS9Later()

@property (nonatomic, strong) CNContactStore *contactStore;

@end

@implementation NPAddressBookLoadIOS9Later

- (CNContactStore *)contactStore{
    if(!_contactStore){
        _contactStore = [[CNContactStore alloc] init];
    }
    return _contactStore;
}

- (void)requestAuthorizationResult:(void (^)(NSError *))result{
    
    // 1.获取授权的状态
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    // 2.判断授权状态,如果是未决定状态,才需要请求
    switch (status) {
        case CNAuthorizationStatusNotDetermined:{
            // 用户还没有决定是否授权你的程序进行访问
            // 创建通讯录进行授权
            [self.contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            
                NSError *myError = nil;
                if (granted) {
                    //授权成功
                } else {
                    //授权失败
                    myError = [NSError errorWithDomain:@"授权失败" code:CNAuthorizationStatusDenied userInfo:nil];
//                    [self showDeniedStatusAlertWithError:myError];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (result) {
                        result(myError);
                    }
                });
              
            }];
            break;
        }
        case CNAuthorizationStatusRestricted:{
            // iOS设备上的家长控制或其它一些许可配置阻止程序与通讯录数据库进行交互
            NSError *myError = [NSError errorWithDomain:@"家长控制或其它一些许可配置阻止程序与通讯录数据库进行交互" code:CNAuthorizationStatusRestricted userInfo:nil];
//            [self showDeniedStatusAlertWithError:myError];
            if (result) {
                result(myError);
            }
            break;
        }
        case CNAuthorizationStatusDenied:{
            // 用户明确的拒绝了你的程序对通讯录的访问
            NSError *myError = [NSError errorWithDomain:@"授权失败" code:CNAuthorizationStatusDenied userInfo:nil];
//            [self showDeniedStatusAlertWithError:myError];
            if (result) {
                result(myError);
            }
            break;
        }
            
        case CNAuthorizationStatusAuthorized:{
            // 用户已经授权给你的程序对通讯录进行访问
            
            if (result) {
                result(nil);
            }
            break;
        }
    }
}

- (void)readAddressBookResult:(NPAddressBookReadResult)result{

    //
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if (status != CNAuthorizationStatusAuthorized) {
        if (result) {
            NSError *error = [NSError errorWithDomain:@"获取通讯录权限失败~~" code:0 userInfo:nil];
            result(nil,error);
        }
        return;
    }
    
    // keys决定能获取联系人哪些信息,例:姓名,电话,头像等
    NSArray *fetchKeys = @[[CNContactFormatter descriptorForRequiredKeysForStyle:CNContactFormatterStyleFullName],CNContactPhoneNumbersKey,CNContactThumbnailImageDataKey];
    CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:fetchKeys];
    
    // 3.3.请求联系人
    self.personList = [[NSMutableArray alloc]init];
    [self.contactStore enumerateContactsWithFetchRequest:request error:nil usingBlock:^(CNContact * _Nonnull contact,BOOL * _Nonnull stop) {
        
        // 获取联系人全名
        NSString *name = [CNContactFormatter stringFromContact:contact style:CNContactFormatterStyleFullName];
        
        // 创建联系人模型
        NPPersonModel *model = [NPPersonModel new];
        model.name = name.length > 0 ? name : @"无名氏" ;
        
        // 联系人头像
        model.avatarImage = [UIImage imageWithData:contact.thumbnailImageData];
        
        // 获取一个人的所有电话号码
        NSArray *phones = contact.phoneNumbers;
        NSMutableArray *phoneList = [[NSMutableArray alloc]init];
        for (CNLabeledValue *labelValue in phones)
        {
            CNPhoneNumber *phoneNumber = labelValue.value;
            NSString *mobile = [self filterSpecialOfPhone:phoneNumber.stringValue];
            if (mobile.length == 11) {
                [phoneList addObject:mobile];
            }
        }
        model.phoneList = phoneList;
        if (model.phoneList.count > 0) {
            [self.personList addObject:model];
        }
    }];
    
    //
    if (result) {
        result(self.personList,nil);
    }
}



@end
