//
//  SearchContactVC.m
//  XuanDaXue
//
//  Created by aleven1 on 2018/1/2.
//  Copyright © 2018年 Aleven. All rights reserved.
//

#import "SearchContactVC.h"
//#import "AddContactCell.h"
//#import "UTPinYinHelper.h"
//
//@interface SearchContactVC ()<UITextFieldDelegate,AddContactCellDelegate>
//
//@property (nonatomic,strong) SAMTextField *textField;
/////经过搜索之后的数据源
//@property (nonatomic, strong) NSMutableArray *searchResultList;
//
//@end
//
//@implementation SearchContactVC
//
//#pragma mark - View Lifecycle （View 的生命周期）
//- (void)viewWillDisappear:(BOOL)animated{
//    [super viewWillDisappear:animated];
//    [_textField resignFirstResponder];
//}
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//    [self rightNavItemWithTitle:@"取消"];
//    [self prepareUI];
//}
//
//#pragma mark -  UI
//- (void)prepareUI{
//    //
//    CGFloat x = 40.0;
//    CGFloat height = 32.0;
//    height = 32.0;
//    CGFloat y = Half(NAVBAR_HEIGHT - height);
//    CGFloat width = kMainWidth - 2*x;
//    
//    _textField = [[SAMTextField alloc]initWithFrame:CGRectMake(x, y, width, height)];
//    _textField.textColor = FONT_COLOR_BREAK;
//    _textField.font = [UIFont systemFontOfSize:15.0];
//    _textField.placeholder = @"请输入搜索关键字";
//    [_textField setPlaceholderFont:[UIFont systemFontOfSize:15.0]];
//    _textField.borderStyle = UITextBorderStyleNone;
//    _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
//    _textField.autocorrectionType = UITextAutocorrectionTypeNo;
//    _textField.clearButtonMode = UITextFieldViewModeAlways;
//    _textField.returnKeyType = UIReturnKeySearch;
//    _textField.delegate = self;
//    _textField.backgroundColor = BG_NORMAL_COLOR;
//    _textField.layer.cornerRadius = Half(height);
//    _textField.clipsToBounds = YES;
//    _textField.np_leftImage = [UIImage imageNamed:@"icon_search_bigger"];
//    [_textField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
//    self.navigationItem.titleView = _textField;
//    
//    //
//    [self initializeTableViewWithStyle:UITableViewStylePlain];
//    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
//    //
//    [_textField becomeFirstResponder];
//}
//
//#pragma  mark - TableViewDelegate
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return _searchResultList.count;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView
//         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    NPPersonModel *model = [_searchResultList objectAtIndex:indexPath.row];
//    AddContactCell *cell = [AddContactCell cellWithTableView:tableView];
//    cell.delegate = self;
//    cell.model = model;
//    cell.lineView.hidden = _searchResultList.count - 1 == indexPath.row;
//    return cell;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView
//        heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return [AddContactCell cellHeight];
//}
//
//#pragma mark - Action
//- (void)rightNavItemAction:(UIButton *)button{
//    [self goBackAnimated:NO];
//}
//
//- (void)leftNavItemAction:(UIButton *)button{
//    [self goBackAnimated:NO];
//}
//
//
//#pragma mark - AddContactCellDelegate
//- (void)addContactCell:(AddContactCell *)cell
//          buttonAction:(UIButton *)button{
//    
//    __weak typeof(self) weakSelf = self;
//    __block NPPersonModel *blockModel = cell.model;
//    if (button.tag == AddContactCellButtonTag_Invitation) {
//        
//        [self sendInvitationForModel:cell.model];
//        
//    }else if (button.tag == AddContactCellButtonTag_AddFriend){
//        
//        NSDictionary *dict = @{@"userId" : CURRENT_USERID,
//                               TokenNameKey : TokenName,
//                               @"toId" : blockModel.appModel.userId};
//        NPRequest *api = [NPRequest requestWithRequestParameters:dict tailUrl:@"/api/userApiController/addInvite"];
//        api.tipConfig = [NPRequestTipConfig tipConfigWithAnimatingVC:[UIWindow np_topViewController]];
//        [api startWithReformerCompleteBlock:^(NPBaseRequest * _Nonnull request, NPResponseModel * _Nonnull responseMd) {
//            
//            __strong typeof(weakSelf) strongSelf = weakSelf;
//            if (responseMd.isOK) {
//                blockModel.appModel.isSendAddFriend = YES;
//                [NPTip showTip:@"已发送添加好友请求，请耐心等待~~"];
//                [strongSelf.tableView reloadData];
//                if (strongSelf.needUpdateBlock) {
//                    strongSelf.needUpdateBlock();
//                }
//            }
//        }];
//    }
//}
//
//#pragma mark - 邀请用户注册
//- (void)sendInvitationForModel:(NPPersonModel *)model{
//    
//    __weak typeof(self) weakSelf = self;
//    __block NPPersonModel *blockModel = model;
//    
//    NSDictionary *dict = @{@"userId" : CURRENT_USERID,
//                           TokenNameKey : TokenName,
//                           @"phone" : model.mobileNumber};
//    NPRequest *api = [NPRequest requestWithRequestParameters:dict tailUrl:@"/api/loginApiController/inviteRegSMS"];
//    api.tipConfig = [NPRequestTipConfig tipConfigWithAnimatingVC:self];
//    [api startWithReformerCompleteBlock:^(NPBaseRequest * _Nonnull request, NPResponseModel * _Nonnull responseMd) {
//        __strong typeof(weakSelf) strongSelf = weakSelf;
//        if (responseMd.isOK) {
//            [NPTip showTip:@"已发送邀请"];
//            blockModel.appModel.isInvitationRegin = YES;
//            [strongSelf.tableView reloadData];
//            if (strongSelf.needUpdateBlock) {
//                strongSelf.needUpdateBlock();
//            }
//        }
//    }];
//}
//
//#pragma mark - 清楚数据，在删除了搜索关键字时
//- (void)cleanData{
//    _searchResultList = nil;
//    self.tableView.emptyDataProvider = nil;
//    [self.tableView reloadData];
//}
//
//#pragma mark - textFieldDelegate
//- (void)textFieldChanged:(UITextField *)textField{
//    
//    if (textField.text.length <= 0) {
//        [self cleanData];
//        return;
//    }
//    
//    NSString *strText = textField.text;
//    _searchResultList = nil;
//    
//    __weak typeof(self) weakSelf = self;
//    _searchResultList = [[NSMutableArray alloc]init];
//    [_dataSource enumerateObjectsUsingBlock:^(NPPersonModel *model, NSUInteger index, BOOL *stop) {
//        UTPinYinHelper *pinYinHelper = [UTPinYinHelper sharedPinYinHelper];
//        if ([pinYinHelper isString:model.name MatchsKey:strText IgnorCase:YES]) {
//            [weakSelf.searchResultList addObject:model];
//        }else if ([pinYinHelper isString:model.mobileNumber MatchsKey:strText IgnorCase:YES]) {
//            [weakSelf.searchResultList addObject:model];
//        }else{
//            if (model.appModel.isReg) {
//                if ([pinYinHelper isString:model.appModel.user.showName MatchsKey:strText IgnorCase:YES]) {
//                    [weakSelf.searchResultList addObject:model];
//                }
//            }
//        }
//    }];
//    
//    //
//    if (!self.tableView.emptyDataProvider) {
//        DZNEmptyDataProvider *provider = [DZNEmptyDataProvider providerWithEmptyType:DZNEmptyDataType_NoData];
//        provider.title = [DZNEmptyDataProvider defaultAttDescription:@"搜索结果为空!"];
//        self.tableView.emptyDataProvider = provider;
//    }
//    //
//    [self.tableView reloadData];
//}
//
//- (BOOL)textFieldShouldClear:(UITextField *)textField{
//    [self cleanData];
//    return YES;
//}
//
//
//
//@end

