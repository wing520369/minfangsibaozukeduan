//
//  NPSYSAuthorizatManager.h
//  MyTool
//
//  Created by HzB on 2017/2/15.
//  Copyright © 2017年 Aleven. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NPSYSAuthorizatStatus) {
    ///未申请
    NPSYSAuthorizatStatusNotDetermined = 0,
    ///其他原因
    NPSYSAuthorizatStatusRestricted,
    ///拒绝了
    NPSYSAuthorizatStatusDenied,
    ///同意
    NPSYSAuthorizatStatusAuthorized
};

///权限类型
typedef NS_ENUM(NSInteger,NPSYSAuthorizat){
    ///通讯录
    NPSYSAuthorizat_ABAddress,
    ///相机
    NPSYSAuthorizat_Camera,
    ///相册
    NPSYSAuthorizat_PhotoLibrary,
    ///定位
    NPSYSAuthorizat_Location,
    ///本地音乐
    NPSYSAuthorizat_iTuneLibrary,
};

///验证、注册权限回调
typedef void(^NPSYSAuthResult)(NPSYSAuthorizatStatus status,NSError *error);

///手机权限管理
@interface NPSYSAuthorizatManager : NSObject

+ (instancetype)sharedManager;

///请求、检查用户通讯录权限,如没申请过就注册
- (void)requestAuthorizationWithType:(NPSYSAuthorizat)type
                              result:(NPSYSAuthResult)result;

+ (void)requestAuthorizationWithType:(NPSYSAuthorizat)type
                              result:(NPSYSAuthResult)result;

@end
