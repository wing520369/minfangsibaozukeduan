//
//  AddContactCell.h
//  ShareBey
//
//  Created by HzB on 16/7/6.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NPAddressBookLoadFactory.h"

typedef NS_ENUM(NSInteger,AddContactCellButtonTag){
    ///邀请注册
    AddContactCellButtonTag_Invitation,
    ///加好友
    AddContactCellButtonTag_AddFriend,
};


@class AddContactCell;
@protocol AddContactCellDelegate <NSObject>

- (void)addContactCell:(AddContactCell *)cell
          buttonAction:(UIButton *)button;

@end

/**
 *  添加通讯录好友
 */
@interface AddContactCell : UITableViewCell

/** 状态按钮*/
@property (nonatomic, strong) UIButton *statuButton;
/** 头像*/
@property (nonatomic, strong) UIImageView *avatarImageView;
/** 名字*/
@property (nonatomic, strong) UILabel *nameLabel;
/** 状态*/
@property (nonatomic, strong) UILabel *statuLabel;

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, weak) id<AddContactCellDelegate> delegate;

@property (nonatomic, strong) NPPersonModel *model;

+ (CGFloat)cellHeight;

@end

