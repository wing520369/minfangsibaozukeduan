//
//  AddContactCell.m
//  ShareBey
//
//  Created by HzB on 16/7/6.
//  Copyright © 2016年 HzB. All rights reserved.
//

static NSString *AddContactCell_ID = @"AddContactCellID";

#import "AddContactCell.h"
#import "Masonry.h"
#import <UIImageView+WebCache.h>

@interface AddContactCell()

@property (nonatomic, strong) UIColor *invitationStatuColor;
@property (nonatomic, strong) UIColor *appendStatuColor;
@property (nonatomic, strong) UIColor *enabledStatuColor;
@property (nonatomic, strong) NSDictionary *statuTitleDict;
@property (nonatomic, strong) NSDictionary *statuBgColorDict;

@end

@implementation AddContactCell

+ (CGFloat)cellHeight{

    return 66.0;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    AddContactCell *cell = [tableView dequeueReusableCellWithIdentifier:AddContactCell_ID];
    if (nil == cell) {
        cell = [[AddContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AddContactCell_ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //
        _invitationStatuColor = [UIColor colorWithRed:82.0f/255.0f green:173.0f/255.0f blue:233.0f/255.0f alpha:1];
        _appendStatuColor = [UIColor colorWithRGB:205.0 G:85.0 B:82.0];
        _enabledStatuColor = [UIColor grayColor];
        
        _statuTitleDict  = @{@"1" : @"邀请",
                             @"2" : @"已邀请"};
        
        _statuBgColorDict =  @{@"1" : _invitationStatuColor,
                               @"2" : _enabledStatuColor};
        //
        self.contentView.backgroundColor =  [UIColor whiteColor];
        //
        CGFloat spaceX = 12.0;
        CGFloat x = spaceX;
        CGFloat width = 44.0;
        CGFloat height = width;
        CGFloat y = ([AddContactCell cellHeight] - height) / 2.0;
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(x, y, width,height)];
        _avatarImageView = imageView;
        _avatarImageView.layer.cornerRadius = width / 2.0;
        _avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
        _avatarImageView.clipsToBounds = YES;
        [self.contentView addSubview:_avatarImageView];
        
        //
        width = 66.0;
        height = 28.0;
        x = kMainWidth - width - spaceX;
        y = ([AddContactCell cellHeight] - height) / 2.0;
        _statuButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y , width, height)];
        _statuButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_statuButton setTitleColor:[UIColor whiteColor] forState:0];
        _statuButton.layer.cornerRadius = 6.0;
        _statuButton.clipsToBounds = YES;
        [_statuButton addTarget:self action:@selector(statuButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_statuButton];
        
        //
        UILabel *label = [[UILabel alloc]init];
        label.font = [UIFont systemFontOfSize:14.0];
        label.textColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1];
        label.numberOfLines = 2;
        label.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.contentView addSubview:label];
        _nameLabel = label;
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_avatarImageView.mas_right).offset(8.0);
            make.top.equalTo(_avatarImageView);
            make.right.equalTo(_statuButton.mas_left).offset(-6.0);
        }];
        
//        y = _avatarImageView.bottom_np - 2.0 - height;
//        label = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        label = [[UILabel alloc]init];
        label.font = [UIFont systemFontOfSize:13.0];
        label.textColor = [UIColor colorWithRed:120.0f/255.0f green:120.0f/255.0f blue:100.0f/255.0f alpha:1];
        [self.contentView addSubview:label];
        _statuLabel = label;
        [_statuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameLabel);
            make.bottom.equalTo(_avatarImageView);
        }];
        
        //
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1];
        [self.contentView addSubview:_lineView];
        [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.contentView);
            make.height.equalTo(@1.0);
        }];
    }
    return self;
}

- (void)setModel:(NPPersonModel *)model{
    _model = model;
    //
    NSInteger status = 0;
    NSString *key = nil;
    AddContactInfoModel *appModel = model.appModel;
    if (appModel.isReg) {
          _nameLabel.text = model.name;
        _statuLabel.text = [NSString stringWithFormat:@"租房：%@",appModel.user.showName];
        [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:appModel.user.avatar] placeholderImage:[UIImage imageFileNamed:@"icon_geren"]];
        _statuButton.enabled = NO;
        _statuButton.hidden = YES;
    
    }else{
        _nameLabel.text = [NSString stringWithFormat:@"%@(%@)",model.name,model.mobileNumber];
//        _statuLabel.text = [NSString stringWithFormat:@"(%@)暂未成为租房用户",model.mobileNumber];
        _statuLabel.text = @"暂未成为租房用户";
        _avatarImageView.image = model.avatarImage ? model.avatarImage : [UIImage imageFileNamed:@"icon_geren"];
        if (appModel.isInvitationRegin) {
            key = @"2";
            status = 2;
            _statuButton.enabled = NO;
        }else{
            key = @"1";
            status = 1;
            _statuButton.tag = AddContactCellButtonTag_Invitation;
            _statuButton.enabled = YES;
        }
    }
    ///
    NSString *gradeStr = [NSString stringWithFormat:@"%zd",status];
    [_statuButton setTitle:_statuTitleDict[gradeStr] forState:0];
    UIColor *bgColor = _statuBgColorDict[gradeStr];
    [_statuButton setBackgroundImage:[UIImage imageWithColor:bgColor] forState:0];
    
    //
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    if ([appModel.user.ID isEqualToString:userModel.ID]) {
        _statuButton.hidden = YES;
        _statuButton.enabled = NO;
    }
}

- (void)statuButtonAction:(UIButton *)button{
    if (_delegate && [_delegate respondsToSelector:@selector(addContactCell:buttonAction:)]) {
        [_delegate addContactCell:self buttonAction:button];
    }
}

@end
