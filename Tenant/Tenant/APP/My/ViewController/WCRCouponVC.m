//
//  WCRCouponVC.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRCouponVC.h"

@interface WCRCouponVC ()

@end

@implementation WCRCouponVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"优惠券"];
    [self createUI];
}

- (void)createUI {
    
    UIImage *image = [UIImage imageFileNamed:@"icon_yhq_a"];
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat x = (kMainWidth - width) / 2.0;
    CGFloat y = AUTO_MATE_HEIGHT(60);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [imageView setContentModeToScaleAspectFill];
    imageView.image = image;
    [self.view addSubview:imageView];
    
    UIFont *font = [UIFont CustomFontWithSize:16.0];
    height = 50;
    width = kMainWidth;
    x = 0;
    y = imageView.bottom_wcr;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:@"暂无优惠券"];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
}


@end
