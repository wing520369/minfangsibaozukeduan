//
//  WCRMyInfoDetailVC.m
//  Landlord
//
//  Created by 吴传荣 on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyInfoDetailVC.h"
#import "UINavigationController+WCRCustom.h"
#import "WCRAvatarCell.h"
#import "WCRMyInfoDetailCell.h"
#import "WCRMyInfoDetailModel.h"
#import "WCRModifyMyInfoVC.h"
#import "SelectPhotosTools.h"
#import "WCRPasswordManageVC.h"
#import "NPGeneralApi.h"
#import "RACSubject.h"

static NSString *avatarIdentifier = @"WCRAvatarCell";
static NSString *identifier = @"WCRMyInfoDetailCell";

@interface WCRMyInfoDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSArray *secondSectionDataSource;
@property (nonatomic, strong) WCRUserModel *userModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRMyInfoDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.userModel = [WCRUserModelTool getUserInfo];
    [self addLeftGoBackItemWithTitle:@"个人信息"];
    self.secondSectionDataSource = [WCRMyInfoDetailModel getMyInfoDetailModels];
    __weak typeof(self) weakSelf = self;
    [self.secondSectionDataSource enumerateObjectsUsingBlock:^(WCRMyInfoDetailModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        switch (model.type) {
            case MyInfoDetailTypeNickName:
                model.content = weakSelf.userModel.nickname;
                break;
            case MyInfoDetailTypePhone:
                model.content = weakSelf.userModel.phone;
                break;
            case MyInfoDetailTypeWX:
                model.content = weakSelf.userModel.wxNo;
                break;
            default:
                break;
        }
    }];
    [self createUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserInfo:) name:@"updateUserInfo" object:nil];
}
- (void)updateUserInfo:(NSNotification *)noti {
    self.userModel = [noti.userInfo objectForKey:@"userModel"];
    __weak typeof(self) weakSelf = self;
    [self.secondSectionDataSource enumerateObjectsUsingBlock:^(WCRMyInfoDetailModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        if (model.type == MyInfoDetailTypeWX) {
            model.content = weakSelf.userModel.wxNo;
        }
        else if (model.type == MyInfoDetailTypeNickName) {
            model.content = weakSelf.userModel.nickname;
        }
    }];
    [_tableView reloadData];
}
- (void)createUI {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) style:UITableViewStylePlain];
    _tableView = tableView;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[WCRAvatarCell class] forCellReuseIdentifier:avatarIdentifier];
    [tableView registerClass:[WCRMyInfoDetailCell class] forCellReuseIdentifier:identifier];
}
#pragma mark - Table View DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    else {
        return self.secondSectionDataSource.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        WCRAvatarCell *cell = [tableView dequeueReusableCellWithIdentifier:avatarIdentifier forIndexPath:indexPath];
        cell.userModel = self.userModel;
        return cell;
    }
    else {
        WCRMyInfoDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        cell.model = [self.secondSectionDataSource objectAtIndex:indexPath.row];
        return cell;
    }
}
#pragma mark - Table View delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [WCRAvatarCell cellHeight];
    }
    WCRMyInfoDetailModel *model = [self.secondSectionDataSource objectAtIndex:indexPath.row];
    return model.cellHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [SelectPhotosTools showAtController:self backImage:^(UIImage *image) {
            
            NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
            [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/userApiController/updateUserInfo" ImagesWithNameObj:@"avatar" images:@[image] ResponseBlock:^(NPGeneralResponse *responseMd) {
                if (responseMd.isOK) {
                    WCRUserModel *userModel = [WCRUserModel jsonWithDictionary:responseMd.response];
                    [WCRUserModelTool saveUserInfo:userModel];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateUserInfo" object:nil userInfo:@{@"userModel":userModel}];
                }
            }];
        }];
        
        return;
    }
    WCRMyInfoDetailModel *model = [self.secondSectionDataSource objectAtIndex:indexPath.row];
    switch (model.type) {
        case MyInfoDetailTypeNickName:{
            WCRModifyMyInfoVC *modifyVC = [[WCRModifyMyInfoVC alloc] init];
            modifyVC.modifyType = @"0";
            [self.navigationController pushViewController:modifyVC animated:YES];
        }
            break;
        case MyInfoDetailTypeWX:{
            WCRModifyMyInfoVC *modifyVC = [[WCRModifyMyInfoVC alloc] init];
            modifyVC.modifyType = @"1";
            [self.navigationController pushViewController:modifyVC animated:YES];
        }
            break;
        case MyInfoDetailTypePayPassword:{
            WCRPasswordManageVC *passwordVC = [[WCRPasswordManageVC alloc] init];
            passwordVC.type = _userModel.passwordPay.length ? @"1" : @"0";
            passwordVC.titleString = @"支付密码";
            passwordVC.pwdOperationSignal = [RACSubject subject];
            @weakify(self);
            [passwordVC.pwdOperationSignal subscribeNext:^(WCRUserModel *userModel) {
                @strongify(self);
                self.userModel = userModel;
            }];
            [self.navigationController pushViewController:passwordVC animated:YES];
        }
            break;
        default:
            break;
    }
}

@end
