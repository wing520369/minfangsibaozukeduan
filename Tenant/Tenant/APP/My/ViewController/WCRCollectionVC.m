//
//  WCRCollectionVC.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRCollectionVC.h"
#import "WCRCollectionModel.h"
#import "WCRRoomModel.h"
#import "WCRHousingSourcesCell.h"
#import "BasicListViewModel.h"
#import "NPGeneralApi.h"
#import "NPTip.h"
#import "UIViewController+WCRCustom.h"
#import "WCRHousingResourcesDetailVC.h"
#import "WCRDeleteBottomView.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"
#import "NSObject+RACPropertySubscribing.h"
#import "RACSubject.h"

static NSString *identifier = @"cell";

@interface WCRCollectionVC ()<UITableViewDelegate>
@property (nonatomic, strong) BasicListViewModel *viewModel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) WCRDeleteBottomView *bottomView;
@property (nonatomic, strong) NSMutableArray *selectedModel;
@property (nonatomic, assign) BOOL isDelete;
@end

@implementation WCRCollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self cancelCollectAction];
    [self addLeftGoBackItemWithTitle:@"我的收藏"];
    [self createUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCollectionList) name:@"updateCollectionList" object:nil];
}

- (void)updateCollectionList {
    [self getDataFromNetwork:LoadListType_First];
}

- (void)cancelCollectAction {
    
    _selectedModel = [NSMutableArray array];
    UIButton *rightButton = [self addRightBarButtonItemTitle:@"取消收藏"];
    [rightButton setTitle:@"取消操作" forState:UIControlStateSelected];
    
    @weakify(self);
    [RACObserve(self, isDelete) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        BOOL delete = [x boolValue];
        [self.viewModel.dataSource enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
            WCRCollectionModel *appointmentModel = obj;
            appointmentModel.hidden = !delete;
        }];
        if (delete) {
            [UIView animateWithDuration:0.3 animations:^{
                self.bottomView.y_wcr = kMainHeight - 64 - self.bottomView.height_wcr;
                self.tableView.height_wcr = kMainHeight - 64 - self.bottomView.height_wcr;
            }];
        }
        else {
            [UIView animateWithDuration:0.3 animations:^{
                self.bottomView.y_wcr = kMainHeight;
                self.tableView.height_wcr = kMainHeight - 64;
            }];
            [self.selectedModel enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
                WCRCollectionModel *appointmentModel = obj;
                appointmentModel.isSelected = NO;
            }];
            [self.selectedModel removeAllObjects];
        }
        self.viewModel.isTableViewEdit = delete;
        self.tableView.editing = delete;
        rightButton.selected = delete;
        [self.tableView reloadData];
    }];
}

- (void)rightBarButtonItemAction:(UIButton *)button {
    button.selected = !button.selected;
    self.isDelete = button.selected;
}

- (void)createUI {
    
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"type":@"1"};
    _viewModel = [[BasicListViewModel alloc] initWithParameters:dict tailUrl:@"/api/camCollectController/getCollectList"];
    
    @weakify(self);
    self.tableView = [self.viewModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) CellIdentifier:identifier CellClassName:@"WCRHousingSourcesCell" ModelClassName:@"WCRCollectionModel" ConfigureCellBlock:^(WCRHousingSourcesCell *cell, NSIndexPath *indexPath, WCRCollectionModel *model) {
        @strongify(self);
        cell.model = model.room;
        cell.lineView.hidden = self.viewModel.dataSource.count - 1 == indexPath.row ? YES : NO;
        [cell setButtonSelected:model.isSelected];
        [cell setButtonHidden:model.hidden];
    }];
    self.tableView.rowHeight = [WCRHousingSourcesCell cellHeight];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    
    [self.viewModel tableViewSetRefresh:self.tableView HeaderRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_First];
    } FooterRefresh:^{
        @strongify(self);
        [self getDataFromNetwork:LoadListType_Next];
    }];
    
    [self getDataFromNetwork:LoadListType_First];
    
    _tableView.allowsSelectionDuringEditing = YES;
    [self createDeleteView];
}

#pragma mark - 删除 取消 or 确定
- (void)createDeleteView {
    _bottomView = [[WCRDeleteBottomView alloc] init];
    [self.view addSubview:_bottomView];
    
    self.bottomView.deleteActionSignal = [RACSubject subject];
    @weakify(self);
    [self.bottomView.deleteActionSignal subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if ([x boolValue]) {
            // 确定
            if (self.selectedModel.count == 0) {
                [NPTip showTip:@"请选择要取消收藏的房源"];
                return;
            }
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定取消收藏所选的房源吗？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
                if ([x integerValue] == 1) {
                    NSMutableArray *ids = [NSMutableArray arrayWithCapacity:self.selectedModel.count];
                    [self.selectedModel enumerateObjectsUsingBlock:^(WCRCollectionModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        [ids addObject:obj.room.ID];
                    }];
                    NSString *Ids = [ids componentsJoinedByString:@","];
                    
                    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"objectId":Ids,@"type":@"1"};
                    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/camCollectController/cancelCollect" ResponseBlock:^(NPGeneralResponse *responseMd) {
                        if (responseMd.isOK) {
                            [self.viewModel.dataSource removeObjectsInArray:self.selectedModel];
                            [self.selectedModel removeAllObjects];
                            [self.tableView reloadData];
                            self.isDelete = NO;
                        }
                    }];
                }
            }];
            [alertView show];
        }
        else {
            // 取消
            self.isDelete = NO;
        }
    }];
}
#pragma mark - 获取网络数据
- (void)getDataFromNetwork:(LoadListType)type {
    @weakify(self);
    [self.viewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        @strongify(self);
        [self.viewModel loadListDataCompleteReload:self.tableView IsEnd:isEnd];
    }];
}

#pragma mark - Table View Delegate
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.isDelete ? UITableViewCellEditingStyleDelete & UITableViewCellEditingStyleInsert : UITableViewCellEditingStyleNone;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isDelete) {
        id model = [self.viewModel modelAtIndexPath:indexPath];
        BOOL isSelect = NO;
        WCRCollectionModel *appointmentModel = model;
        appointmentModel.isSelected = !appointmentModel.isSelected;
        isSelect = appointmentModel.isSelected;
        
        WCRHousingSourcesCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell setButtonSelected:isSelect];
        if (isSelect) {
            [_selectedModel addObject:model];
        }
        else {
            [_selectedModel removeObject:model];
        }
    }
    else {
        WCRHousingResourcesDetailVC *detailVC = [[WCRHousingResourcesDetailVC alloc] init];
        WCRCollectionModel *collectModel = [self.viewModel modelAtIndexPath:indexPath];
        detailVC.roomModel = collectModel.room;
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

@end
