//
//  WCRModifyMyInfoVC.m
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRModifyMyInfoVC.h"
#import "UIViewController+WCRCustom.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UITextField+WCRCustom.h"
#import "NPTip.h"
#import "NPGeneralApi.h"

@interface WCRModifyMyInfoVC ()
@property (nonatomic, strong) UITextField *textField;
@end

@implementation WCRModifyMyInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *title = [_modifyType integerValue] == 0 ? @"昵称" : @"微信";
    [self addLeftGoBackItemWithTitle:title];
    [self addRightBarButtonItemTitle:@"保存"];
    [self createUI];
}

#pragma mark - 保存
- (void)rightBarButtonItemAction:(UIButton *)button {
    
    if (!_textField.text.length) {
        [NPTip showTip:_textField.placeholder];
        return;
    }
    __weak typeof(self) weakSelf = self;
    NSString *key = [_modifyType integerValue] == 0 ? @"nickname" : @"wxNo";
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],key:_textField.text};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/userApiController/updateUserInfo" ImagesWithNameObj:nil images:nil ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            WCRUserModel *userModel = [WCRUserModel jsonWithDictionary:responseMd.response];
            [WCRUserModelTool saveUserInfo:userModel];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateUserInfo" object:nil userInfo:@{@"userModel":userModel}];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)createUI {
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 20, kMainWidth, kDefaultHeight)];
    [_textField textFieldAddRightImage:nil Width:kSpaceX RightBorder:0];
    NSString *placeholder = [_modifyType integerValue] == 0 ? @"请输入昵称" : @"请输入微信号";
    [_textField textFieldWithFont:[UIFont CustomFontWithSize:14.0] PlaceHolder:placeholder LeftLabelText:nil Border:0];
    _textField.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:_textField];
    
    WCRUserModel *user = [WCRUserModelTool getUserInfo];
    _textField.text = [_modifyType integerValue] == 0 ? user.nickname : user.wxNo;
}

@end
