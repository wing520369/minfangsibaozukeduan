//
//  WCRSpreadVC.m
//  Landlord
//
//  Created by eliot on 2017/4/28.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRSpreadVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "WCRPhotoBrowser.h"
#import "UITextField+WCRCustom.h"
#import "NSString+WCRCustom.h"
#import "NPGeneralApi.h"
#import "UIViewController+WCRCustom.h"

#import "AddContactVC.h"

@interface WCRSpreadVC ()
@property (nonatomic, strong) UIImageView *QRImageView;
@property (nonatomic, strong) UITextField *moneytextField;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *money;
@end

@implementation WCRSpreadVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"推广"];
    [self addRightBarButtonItemTitle:@"通讯录"];
    
    [self getDataFromNetwork];
}

- (void)getDataFromNetwork {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/publicApiController/getRefAmountInfo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            weakSelf.content = [responseMd.response objectForKey:@"content"];
            weakSelf.money = [NSString stringWithFormat:@"%@",[responseMd.response objectForKey:@"amount"]];
            [weakSelf createUI];
        }
    }];
}

- (void)createUI {
    
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    // 二维码照片
    CGFloat width = AUTO_MATE_WIDTH(150);
    CGFloat height = width;
    CGFloat x = (kMainWidth - width) / 2.0;
    CGFloat y = 40;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [imageView setContentModeToScaleAspectFill];
    imageView.image = [UIImage createQRCodeWithText:[WCRUserModelTool getUserId]];
    _QRImageView = imageView;
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCodeImageView:)];
    [imageView addGestureRecognizer:tap];
    [scrollView addSubview:imageView];
    
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    
    NSString *text = @"推广二维码";
    x = kSpaceX;
    y = imageView.bottom_wcr;
    width = kMainWidth - kSpaceX * 2;
    height = 30;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    label.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:label];
    
    text = self.money;
    y = label.bottom_wcr + 25;
    height = kDefaultHeight;
    _moneytextField = [[UITextField alloc] initWithFrame:CGRectMake(0, y, kMainWidth, height)];
    _moneytextField.backgroundColor = [UIColor whiteColor];
    _moneytextField.text = text;
    _moneytextField.font = font;
    _moneytextField.textAlignment = NSTextAlignmentRight;
    _moneytextField.textColor = [UIColor redColor];
    [_moneytextField textFieldAddRightImage:nil Width:kSpaceX RightBorder:0];
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSpaceX, height)];
    _moneytextField.rightView = rightView;
    _moneytextField.rightViewMode = UITextFieldViewModeAlways;
    //
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, height)];
    UILabel *leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, 0, 80 - kSpaceX, height)];
    [leftLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:@"提成统计"];
    [leftView addSubview:leftLabel];
    _moneytextField.leftView = leftView;
    _moneytextField.leftViewMode = UITextFieldViewModeAlways;
    [scrollView addSubview:_moneytextField];
    
    
    // 推广介绍
    text = self.content;
    x = kSpaceX;
    y = _moneytextField.bottom_wcr + 30;
    width = kMainWidth - x * 2;
    height = [NSString calculateWithContent:text MaxWidth:width Font:font] + 20;
    UILabel *introLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    introLabel.numberOfLines = 0;
    [introLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    [scrollView addSubview:introLabel];
    
    if (introLabel.bottom_wcr > scrollView.height_wcr) {
        scrollView.contentSize = CGSizeMake(kMainWidth, imageView.bottom_wcr + 20);
    }
}

#pragma mark - 点击二维码图片
- (void)tapCodeImageView:(UITapGestureRecognizer *)tap {
    if (!_QRImageView.image) {
        return;
    }
    [WCRPhotoBrowser showWithImageArray:@[_QRImageView.image] currentIndex:0];
}

// 地图
- (void)rightBarButtonItemAction:(UIButton *)button {
    __weak typeof(self) weakSelf = self;
    [NPSYSAuthorizatManager requestAuthorizationWithType:NPSYSAuthorizat_ABAddress result:^(NPSYSAuthorizatStatus status, NSError *error) {
        if (error) {
            return;
        }
        AddContactVC *mapVC = [[AddContactVC alloc] init];
        [weakSelf.navigationController pushViewController:mapVC animated:YES];
    }];

}

@end
