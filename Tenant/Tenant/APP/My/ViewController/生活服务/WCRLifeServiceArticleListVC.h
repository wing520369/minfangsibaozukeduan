//
//  WCRLifeServiceArticleListVC.h
//  Tenant
//
//  Created by HzB on 2017/10/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

/// 生活服务 文章列表
@interface WCRLifeServiceArticleListVC : WCRBasicViewController

///1 招聘 2学历 3外卖 4家政
@property (nonatomic, assign) NSInteger type;

@end
