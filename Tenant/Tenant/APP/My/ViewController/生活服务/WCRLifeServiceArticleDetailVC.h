//
//  WCRLifeServiceArticleDetailVC.h
//  Tenant
//
//  Created by HzB on 2017/10/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
#import "WCRLifeServiceArticleModel.h"

@interface WCRLifeServiceArticleDetailVC : WCRBasicViewController

@property (nonatomic,strong) WCRLifeServiceArticleModel *model;

@end
