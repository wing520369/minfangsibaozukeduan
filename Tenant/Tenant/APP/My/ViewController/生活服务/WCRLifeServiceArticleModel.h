//
//  WCRLifeServiceArticleModel.h
//  Tenant
//
//  Created by HzB on 2017/10/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

@interface WCRLifeServiceArticleModel : WCRBasicModel

@property (nonatomic,assign) NSInteger status;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *title;


@end
