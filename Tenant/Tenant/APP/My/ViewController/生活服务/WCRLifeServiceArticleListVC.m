//
//  WCRLifeServiceArticleListVC.m
//  Tenant
//
//  Created by HzB on 2017/10/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRLifeServiceArticleListVC.h"
#import "NPGeneralApi.h"
#import "WCRLifeServiceArticleModel.h"
#import "WCRLifeServiceArticleDetailVC.h"

@interface WCRLifeServiceArticleListVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation WCRLifeServiceArticleListVC


#pragma mark - View Lifecycle （View 的生命周期）
- (void)viewDidLoad {
    [super viewDidLoad];
    //

    [self getDataFromNetwork];
}

- (void)getDataFromNetwork {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],
                           @"tokenName":[WCRUserModelTool getToken],
                           @"type" : @(_type)};
    [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/publicApiController/getLifeNewsList" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            weakSelf.dataSource = [WCRLifeServiceArticleModel jsonWithArray:responseMd.response];
            [weakSelf prepareUI];
        }
    }];
}

#pragma mark -  UI
- (void)prepareUI{
    
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat height = kMainHeight - 64.0;
    CGFloat width = kMainWidth;
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(x, y, width, height) style:UITableViewStylePlain];
    [self.view addSubview:tableView];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10.0)];
    
}


#pragma  mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellId = @"cellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    }
    WCRLifeServiceArticleModel *model = [_dataSource objectAtIndex:indexPath.row];
    cell.textLabel.text = model.title;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WCRLifeServiceArticleModel *model = [_dataSource objectAtIndex:indexPath.row];
    WCRLifeServiceArticleDetailVC *vc = [[WCRLifeServiceArticleDetailVC alloc]init];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
