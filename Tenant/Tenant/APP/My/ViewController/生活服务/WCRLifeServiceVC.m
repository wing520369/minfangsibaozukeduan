//
//  WCRLifeServiceVC.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRLifeServiceVC.h"
#import "WCRTitleContentCell.h"
#import "WCRDataSourceModel.h"
#import "NPTip.h"
#import "WCRLifeServiceArticleListVC.h"
#import "Tools.h"

static NSString *identifier = @"cell";

@interface WCRLifeServiceVC ()<UIWebViewDelegate>
@property (nonatomic, strong) WCRDataSourceModel *dataSourceModel;
@property (nonatomic, strong) UIWebView *webView;
@end

@implementation WCRLifeServiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"生活服务"];
    
//    NSString *url = @"http://wx.qyzx.com/index.php?g=Wap&m=Store&a=index&token=qjcldl1497506336&news_response=1&code=051s4WSb0RZ8ow16kxRb0RgySb0s4WSd&state=";
//    UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
//    _webView = webView;
//    [webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]]];
//    webView.delegate = self;
//    [self.view addSubview:webView];
    
    [self createUI];
}

//- (void)leftBarButtonItemAction:(UIButton *)button {
//    if (self.webView.loading) {
//        [self.webView stopLoading];
//    }
//    else if ([self.webView canGoBack]) {
//        [self.webView goBack];
//    }
//    else {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//}

- (void)createUI {
    
    NSArray *titles = @[@"招聘",@"学历",@"外卖",@"家政"];
    __block NSMutableArray *dataSource = [NSMutableArray arrayWithCapacity:titles.count];
    [titles enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BOOL haveLine = idx == titles.count - 1 ? NO : YES;
        WCRTitleContentModel *model = [[WCRTitleContentModel alloc] initWithTitle:obj Content:@"" HaveArrow:YES HaveLine:haveLine];
        [dataSource addObject:model];
    }];
    
    _dataSourceModel = [[WCRDataSourceModel alloc] init];
    [_dataSourceModel.dataSource addObjectsFromArray:dataSource];
    
    UITableView *tableView = [self.dataSourceModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) CellIdentifier:identifier CellClassName:@"WCRTitleContentCell" ConfigureCellBlock:^(WCRTitleContentCell *cell, NSIndexPath *indexPath, WCRTitleContentModel *model) {
        cell.model = model;
    }];
    tableView.delegate = self;
    tableView.rowHeight = kDefaultHeight;
    tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    [self.view addSubview:tableView];
    
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [NPTip showTip:@"暂未开放"];
    
    if (![Tools checkIsLogin]) {
        return;
    }
    
    WCRTitleContentModel *model = [_dataSourceModel.dataSource objectAtIndex:indexPath.row];
    WCRLifeServiceArticleListVC *vc = [[WCRLifeServiceArticleListVC alloc]init];
    [self addLeftGoBackItemWithTitle:model.title];
    vc.type = indexPath.row + 1;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
