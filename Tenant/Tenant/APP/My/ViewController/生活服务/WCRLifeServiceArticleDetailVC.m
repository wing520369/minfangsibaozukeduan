//
//  WCRLifeServiceArticleDetailVC.m
//  Tenant
//
//  Created by HzB on 2017/10/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRLifeServiceArticleDetailVC.h"

#import "NPGeneralApi.h"
#import "NSString+WCRCustom.h"
#import "NPHUD.h"
#import <WebKit/WebKit.h>

@interface WCRLifeServiceArticleDetailVC ()<WKNavigationDelegate>

@property (nonatomic, strong) NSString *showString;
@property (nonatomic, strong) WKWebView *webView;
@end

@implementation WCRLifeServiceArticleDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:_model.title];
    _showString = _model.content;
    [self createUI];
}

- (void)leftBarButtonItemAction:(UIButton *)button {
    if (_webView) {
        if (_webView.isLoading) {
            [_webView stopLoading];
        }
    }
    if (_webView.canGoBack) {
        [_webView goForward];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)createUI {
    self.view.backgroundColor = [UIColor whiteColor];
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, kMainHeight - 64)];
    _webView = webView;
    webView.scrollView.showsVerticalScrollIndicator = NO;
    webView.scrollView.showsHorizontalScrollIndicator = NO;
    webView.backgroundColor = [UIColor clearColor];
    webView.navigationDelegate = self;
    [self.view addSubview:webView];
    
      [webView loadHTMLString:_showString baseURL:nil];
    
//    [webView loadHTMLString:[NSString formatHTMLString:_showString MaxWidth:webView.width_wcr] baseURL:nil];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [NPHUD showHUDAddedTo:self.view];
    // 修改字体大小 300%
    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '300%'" completionHandler:nil];
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"screen" ofType:@"js"];
    NSString * jsonString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [_webView evaluateJavaScript:jsonString completionHandler:nil];
    [NPHUD hideHUDForView:self.view animated:YES];
}



@end
