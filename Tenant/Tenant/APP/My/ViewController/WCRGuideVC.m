//
//  WCRGuideVC.m
//  Landlord
//
//  Created by eliot on 2017/4/28.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRGuideVC.h"
#import "UINavigationController+WCRCustom.h"
#import "NPGeneralApi.h"
#import "NSString+WCRCustom.h"
#import "NPHUD.h"
#import <WebKit/WebKit.h>

@interface WCRGuideVC ()<WKNavigationDelegate>
@property (nonatomic, strong) NSString *showString;
@property (nonatomic, strong) WKWebView *webView;
@end

@implementation WCRGuideVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"新手帮助"];
    [self getDataFromNetwork];
}

- (void)leftBarButtonItemAction:(UIButton *)button {
    if (_webView) {
        if (_webView.isLoading) {
            [_webView stopLoading];
        }
    }
    if (_webView.canGoBack) {
        [_webView goForward];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)getDataFromNetwork {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = nil;//@{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/publicApiController/aboutGuide" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            weakSelf.showString = responseMd.response;
            [weakSelf createUI];
        }
    }];
}

- (void)createUI {
    self.view.backgroundColor = [UIColor whiteColor];
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, kMainHeight - 64)];
    _webView = webView;
    webView.scrollView.showsVerticalScrollIndicator = NO;
    webView.scrollView.showsHorizontalScrollIndicator = NO;
    webView.backgroundColor = [UIColor clearColor];
    webView.navigationDelegate = self;
    [self.view addSubview:webView];
    
    [webView loadHTMLString:[NSString formatHTMLString:_showString MaxWidth:webView.width_wcr] baseURL:nil];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [NPHUD showHUDAddedTo:self.view];
    // 修改字体大小 300%
    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '300%'" completionHandler:nil];
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [NPHUD hideHUDForView:self.view animated:YES];
}
@end
