//
//  WCRPasswordManageVC.m
//  Landlord
//
//  Created by eliot on 2017/4/28.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRPasswordManageVC.h"
#import "UIViewController+WCRCustom.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UITextField+WCRCustom.h"
#import "NPTip.h"
#import "NPGeneralApi.h"
#import "NSString+JKHash.h"
#import "MSWeakTimer.h"
#import "RACSubject.h"

@interface WCRPasswordManageVC ()
@property (nonatomic, strong) WCRUserModel *userModel;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *rePasswordTextField;
@property (nonatomic, strong) UITextField *codeTextField;
@property (nonatomic, strong) UIButton *getCodeButton;
@property (nonatomic, strong) MSWeakTimer *timer;
@property (nonatomic, assign) NSInteger time;
@end

@implementation WCRPasswordManageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _time = 60;
    _userModel = [WCRUserModelTool getUserInfo];
    [self addLeftGoBackItemWithTitle:_titleString];
    [self addRightBarButtonItemTitle:@"提交"];
    [self createUI];
}

#pragma mark - 保存
- (void)rightBarButtonItemAction:(UIButton *)button {
    
    if (!_passwordTextField.text.length) {
        [NPTip showTip:_passwordTextField.placeholder];
        return;
    }
    if (![_passwordTextField.text isEqualToString:_rePasswordTextField.text]) {
        [NPTip showTip:@"两次输入的密码不一致"];
        return;
    }
    if ([_type integerValue] != 0) {
        if (!_codeTextField.text.length) {
            [NPTip showTip:_codeTextField.placeholder];
            return;
        }
    }
    
    NSString *requestUrl = nil;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    if ([_type integerValue] == 2) {
        // 修改登录密码
        requestUrl = @"/api/loginApiController/resetPassword";
        [dict setValue:[WCRUserModelTool getCurrentAccount] forKey:@"phone"];
        [dict setValue:[_passwordTextField.text.jk_sha1String uppercaseString] forKey:@"password"];
        [dict setValue:_codeTextField.text forKey:@"code"];
    }
    else {
        // 开启 or 修改
        requestUrl = @"/api/loginApiController/saveTradePassword";
        [dict setValue:[WCRUserModelTool getUserId] forKey:@"userId"];
        [dict setValue:[WCRUserModelTool getToken] forKey:@"tokenName"];
        [dict setValue:[_passwordTextField.text.jk_sha1String uppercaseString] forKey:@"passwordPay"];
        if ([_type integerValue] == 1) {
            // 修改
            requestUrl = @"/api/loginApiController/editTradePassword";
            [dict setValue:_codeTextField.text forKey:@"code"];
        }
    }
    
    __weak typeof(self) weakSelf = self;
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:requestUrl ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            if ([weakSelf.type integerValue] == 0) {
                [NPTip showTip:@"开启支付密码成功"];
            }
            else if ([weakSelf.type integerValue] == 1) {
                [NPTip showTip:@"修改支付密码成功"];
            }
            else if ([weakSelf.type integerValue] == 2) {
                [NPTip showTip:@"修改登录密码成功"];
            }
            WCRUserModel *userModel = [WCRUserModel jsonWithDictionary:responseMd.response];
            if (userModel && userModel.ID.length) {
                [WCRUserModelTool saveUserInfo:userModel];
                [weakSelf.pwdOperationSignal sendNext:userModel];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateUserInfo" object:nil userInfo:@{@"userModel":userModel}];
            }
            
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    
}

- (void)createUI {
    
    TPKeyboardAvoidingScrollView *scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.opaque = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    UITextField *tipTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, kMainWidth, 25)];
    [tipTextField textFieldWithFont:[UIFont CustomFontWithSize:13.0] PlaceHolder:nil LeftLabelText:nil Border:0];
    tipTextField.text = [NSString stringWithFormat:@"你正在为%@修改%@",_userModel.phone,self.title];
    [scrollView addSubview:tipTextField];
    tipTextField.userInteractionEnabled = NO;
    // 是否隐藏
    tipTextField.hidden = [_type integerValue]  == 0 ? YES : NO;
    
    CGFloat border = 10;
    CGFloat y = tipTextField.bottom_wcr;
    NSString *placeholder = [_type integerValue]  == 0 ? @"请输入支付密码" : [NSString stringWithFormat:@"请输入新的%@",self.title];
    _passwordTextField = [self getTextFieldWithPlaceholder:placeholder Ypoint:y SuperView:scrollView];
    
    y = _passwordTextField.bottom_wcr + border;
    placeholder = [_type integerValue]  == 0 ? @"确认支付密码" : [NSString stringWithFormat:@"确认新的%@",self.title];
    _rePasswordTextField = [self getTextFieldWithPlaceholder:placeholder Ypoint:y SuperView:scrollView];
    
    if ([_type integerValue] != 0) {
        placeholder = @"输入验证码";
        // 验证码
        y = _rePasswordTextField.bottom_wcr + border;
        _codeTextField = [self getTextFieldWithPlaceholder:placeholder Ypoint:y SuperView:scrollView];
        _codeTextField.secureTextEntry = NO;
        // 右视图发送验证码
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 75 + kSpaceX, _codeTextField.height_wcr)];
        rightView.backgroundColor = [UIColor clearColor];
        _getCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _getCodeButton.frame = CGRectMake(0, 5, 75, rightView.height_wcr - 10);
        [_getCodeButton buttonWithTitleColor:[UIColor colorWithHexString:kMainColor] Background:nil Font:[UIFont CustomFontWithSize:12.0] Title:@"发送验证码"];
        [_getCodeButton setLayerWithBorderWidth:1.0 Corner:3.0 BorderColor:[UIColor colorWithHexString:kMainColor]];
        [_getCodeButton addTarget:self action:@selector(modifyPwdGetCode:)];
        [rightView addSubview:_getCodeButton];
        _codeTextField.rightView = rightView;
        _codeTextField.rightViewMode = UITextFieldViewModeAlways;
    }
    
}

- (UITextField *)getTextFieldWithPlaceholder:(NSString *)placeholder Ypoint:(CGFloat)yPoint SuperView:(TPKeyboardAvoidingScrollView *)superView {
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, yPoint, kMainWidth, kDefaultHeight)];
    [textField textFieldAddRightImage:nil Width:kSpaceX RightBorder:0];
    [textField textFieldWithFont:[UIFont CustomFontWithSize:14.0] PlaceHolder:placeholder LeftLabelText:nil Border:0];
    textField.backgroundColor = [UIColor whiteColor];
    [superView addSubview:textField];
    
    textField.secureTextEntry = YES;
    
    return textField;
}

#pragma mark - 获取验证码
- (void)modifyPwdGetCode:(UIButton *)button {
    [self.view endEditing:YES];
    button.enabled = NO;
    // 1注册，2忘记密码，3其他
    NSString *type = @"3";
    NSDictionary *dict = @{@"phone":[WCRUserModelTool getCurrentAccount],@"type":type};
    __weak typeof(self) weakSelf = self;
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/loginApiController/smsCode" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            weakSelf.timer = [MSWeakTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(forgetOrRegisterCutTimeDown:) userInfo:nil repeats:YES dispatchQueue:dispatch_get_main_queue()];
        }
        else {
            button.enabled = YES;
        }
    }];
}
- (void)forgetOrRegisterCutTimeDown:(MSWeakTimer *)timer {
    [_getCodeButton setTitle:[NSString stringWithFormat:@"%ld",(long)self.time] forState:UIControlStateDisabled];
    self.time--;
    if (self.time < 0) {
        [timer invalidate];
        timer = nil;
        self.time = 60;
        [_getCodeButton setTitle:@"重新获取" forState:UIControlStateDisabled];
        [_getCodeButton setTitle:@"重新获取" forState:0];
        _getCodeButton.enabled = YES;
    }
}
@end
