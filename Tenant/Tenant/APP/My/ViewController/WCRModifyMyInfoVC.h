//
//  WCRModifyMyInfoVC.h
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

/// 修改昵称，微信号
@interface WCRModifyMyInfoVC : WCRBasicViewController
/// 0:昵称 1:微信号
@property (nonatomic, copy) NSString *modifyType;
@end
