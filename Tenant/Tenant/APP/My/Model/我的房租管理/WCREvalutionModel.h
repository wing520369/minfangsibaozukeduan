//
//  WCREvalutionModel.h
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

@interface WCREvalutionModel : WCRBasicModel
@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSString * createDate;
@property (nonatomic, strong) NSString * hostId;
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * level;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * orderId;
@property (nonatomic, strong) NSString * remarks;
@property (nonatomic, strong) NSString * updateDate;
@property (nonatomic, strong) NSString * userId;
@end
