//
//  WCRMessageKindModel.h
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

typedef NS_ENUM(NSInteger, MyRentType) {
    /// 我的租房
    MyRentTypeTenement = 1221,
    /// 缴费单
    MyRentTypeDeposit,
    /// 评分星级
    MyRentTypeEvalution,
    /// 报修服务
    MyRentTypeModifyService,
};

/// 消息类型模型
@interface WCRMyRentModel : WCRBasicModel
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *className;
@property (nonatomic, assign) MyRentType type;
+ (NSArray *)getMyRentModels;
@end
