//
//  WCRModifyServiceModel.m
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRModifyServiceListModel.h"

@implementation WCRModifyServiceListModel
+ (NSDictionary *)objectClassInArray{
    return @{@"repairImgs" : [WCRRepairImgsModel class]};
}
- (instancetype)init {
    if (self = [super init]) {
        self.hidden = YES;
        self.isSelected = NO;
    }
    return self;
}
@end

@implementation WCRRepairImgsModel

@end
