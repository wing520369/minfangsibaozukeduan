//
//  WCRDepositCostModel.h
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

@interface WCRDepositCostModel : WCRBasicModel
/// 费用名
@property (nonatomic, copy) NSString *name;
/// 费用明细单价
@property (nonatomic, assign) CGFloat price;
/// 费用明细类型：0- 抄表计算 1-手动填写 2-固定收取 3-减免 4-租客押金 5-租客租金
@property (nonatomic, copy) NSString *type;
/// 费用明细单位：0-元/度 1-元/吨 2-元/期 3-元/方 4-元
@property (nonatomic, copy) NSString *unit;
@property (nonatomic, copy) NSString *unitString;
/// 费用明细数值(每月的抄表读数 : 本期读数 - 上期读数)
@property (nonatomic, assign) CGFloat value;
/// 费用明细价格
@property (nonatomic, assign) CGFloat money;
/// 上期读数
@property (nonatomic, assign) CGFloat lastValue;
/// 本期读数
@property (nonatomic, assign) CGFloat thisValue;
/// 排序：租金0   押金1    抄表计算2   固定收取3   手动填写4   减免5
@property (nonatomic, assign) NSInteger order;
@end
