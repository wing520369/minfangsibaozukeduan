//
//  WCRRentDepositModel.m
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRentDepositModel.h"
#import "WCRRoomModel.h"

@implementation WCRRentDepositModel
+ (NSDictionary *)objectClassInArray{
    return @{@"roomImgs" : [WCRRoomImagesModel class]};
}
@end
