//
//  WCRDepositCostModel.m
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRDepositCostModel.h"

@implementation WCRDepositCostModel
- (NSString *)unitString {
    // 费用明细单位：0-元/度 1-元/吨 2-元/期 3-元/方 4-元
    NSString *string = @"元";
    switch ([self.unit integerValue]) {
        case 0:
            string = @"元/度";
            break;
        case 1:
            string = @"元/吨";
            break;
        case 2:
            string = @"元/期";
            break;
        case 3:
            string = @"元/方";
            break;
        case 4:
            string = @"元";
            break;
        default:
            break;
    }
    return string;
}

- (NSInteger)order {
    // 排序：租金0   押金1    抄表计算2   固定收取3   手动填写4   减免5
    NSInteger index = 0;
    // 费用明细类型：0- 抄表计算 1-手动填写 2-固定收取 3-减免 4-租客押金 5-租客租金
    switch ([self.type integerValue]) {
        case 0:
            index = 2;
            break;
        case 1:
            index = 4;
            break;
        case 2:
            index = 3;
            break;
        case 3:
            index = 5;
            break;
        case 4:
            index = 1;
            break;
        case 5:
            index = 0;
            break;
        default:
            break;
    }
    return index;
}

@end
