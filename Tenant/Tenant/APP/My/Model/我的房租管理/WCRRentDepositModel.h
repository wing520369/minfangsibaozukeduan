//
//  WCRRentDepositModel.h
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRRoomImagesModel;
/// 缴费单
@interface WCRRentDepositModel : WCRBasicModel
@property (nonatomic, strong) NSString * amount;
@property (nonatomic, strong) NSString * rent;
@property (nonatomic, strong) NSString * billId;
/// 账单状态 0-未支付 1-待支付 2-到账 3-未清算 4-清算 5-租客确认 6-取消
@property (nonatomic, strong) NSString * billStatus;
/// 缴费单状态 0-取消 1-待支付 2-待收款 3-支付完成
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSString * createDate;
@property (nonatomic, strong) NSString * delFlag;
@property (nonatomic, strong) NSString * delHost;
@property (nonatomic, strong) NSString * delTenant;
@property (nonatomic, strong) NSString * describe;
@property (nonatomic, strong) NSString * hall;
@property (nonatomic, strong) NSString * hostId;
@property (nonatomic, strong) NSString * houseId;
@property (nonatomic, strong) NSString * houseName;
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * isRead;
@property (nonatomic, strong) NSString * rentEndDate;
@property (nonatomic, strong) NSString * rentStartDate;
@property (nonatomic, strong) NSString * room;
@property (nonatomic, strong) NSString * roomArea;
@property (nonatomic, strong) NSString * roomId;
@property (nonatomic, strong) NSString * roomNo;
@property (nonatomic, strong) NSString * tenantId;
@property (nonatomic, strong) NSString * tenantName;
@property (nonatomic, strong) NSString * hostName;
@property (nonatomic, strong) NSString * tier;
@property (nonatomic, strong) NSString * toilet;
@property (nonatomic, strong) NSString * totalTier;
@property (nonatomic, strong) NSString * updateDate;
@property (nonatomic, strong) NSString * remarks;
@property (nonatomic, strong) NSArray<WCRRoomImagesModel *> * roomImgs;
@end
