//
//  WCRMessageKindModel.m
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyRentModel.h"

@implementation WCRMyRentModel
+ (NSArray *)getMyRentModels {
    
    NSArray *titles = @[@"我的租房",@"缴费单",@"评分星级",@"报修信息"];
    NSArray *imageNames = @[@"icon_fangyuanguanli",@"icon_jiaofei",@"icon_xingxing",@"icon_baoxiu_a"];
    NSArray *tags = @[@(MyRentTypeTenement),@(MyRentTypeDeposit),@(MyRentTypeEvalution),@(MyRentTypeModifyService)];
    NSArray *classNames = @[@"WCRTenementMainVC",@"WCRRentDepositMainVC",@"WCREvalutionMainVC",@"WCRModifyServiceListVC"];
    
    NSMutableArray *models = [NSMutableArray arrayWithCapacity:titles.count];
    for (NSInteger i = 0; i < titles.count; i++) {
        WCRMyRentModel *model = [[self alloc] init];
        model.title = [titles objectAtIndex:i];
        model.imageName = [imageNames objectAtIndex:i];
        model.type = [[tags objectAtIndex:i] integerValue];
        model.className = [classNames objectAtIndex:i];
        [models addObject:model];
    }
    return models;
}

@end
