//
//  WCRTenementModel.m
//  __projectName__
//
//  Created by aleven on 17/06/05.
//  Copyright © 2017年 eliot. All rights reserved.
//

#import "WCRTenementModel.h"
#import "WCRRoomModel.h"

@implementation WCRTenementModel

+ (NSDictionary *)mj_objectClassInArray{
	return @{@"room":[WCRRoomModel class]};
}

@end
