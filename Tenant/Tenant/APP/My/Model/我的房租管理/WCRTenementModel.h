//
//  WCRTenementModel.h
//  __projectName__
//
//  Created by aleven on 17/06/05.
//  Copyright © 2017年 eliot. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRRoomModel;

@interface WCRTenementModel : WCRBasicModel
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *orderId;
/// 1：在租    0：退租申请中
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * beginRental;
@property (nonatomic, strong) NSString * endRental;
@property (nonatomic, strong) NSString * hostName;
@property (nonatomic, strong) NSString * rent;
@property (nonatomic, strong) WCRRoomModel * room;
/// 租客是否评价（0未评价，1评价）
@property (nonatomic, copy) NSString *tenantComment;
/// 房东是否评价（0未评价，1评价）
@property (nonatomic, copy) NSString *hostComment;
@end
