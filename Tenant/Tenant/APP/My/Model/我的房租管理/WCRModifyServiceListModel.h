//
//  WCRModifyServiceModel.h
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRRepairImgsModel;
/// 报修 model
@interface WCRModifyServiceListModel : WCRBasicModel
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL hidden;
@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSString * createDate;
@property (nonatomic, strong) NSString * delFlag;
@property (nonatomic, strong) NSString * delFrom;
@property (nonatomic, strong) NSString * delTo;
@property (nonatomic, strong) NSString * fromUserId;
@property (nonatomic, strong) NSString * houseId;
@property (nonatomic, strong) NSString * houseName;
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * isRead;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * phone;
@property (nonatomic, strong) NSArray<WCRRepairImgsModel *> * repairImgs;
@property (nonatomic, strong) NSString * roomId;
@property (nonatomic, strong) NSString * roomNo;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * toUserId;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * updateDate;
@end

@interface WCRRepairImgsModel : WCRBasicModel
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * img;
@end
