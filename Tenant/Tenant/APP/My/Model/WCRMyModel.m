//
//  WCRMyModel.m
//  Landlord
//
//  Created by 吴传荣 on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyModel.h"

@implementation WCRMyModel
+ (NSArray *)getMyInfoModels {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"WCRMyModel.plist" ofType:nil];
    NSArray *models = [WCRMyModel jsonWithArray:[NSArray arrayWithContentsOfFile:path]];
    return [models copy];
}
@end
