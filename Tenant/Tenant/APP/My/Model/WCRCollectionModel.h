//
//  WCRCollectionModel.h
//  Tenant
//
//  Created by eliot on 2017/6/2.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRRoomModel;
@interface WCRCollectionModel : WCRBasicModel
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL hidden;
@property (nonatomic, strong) NSString * createDate;
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * objectId;
@property (nonatomic, strong) WCRRoomModel * room;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * updateDate;
@property (nonatomic, strong) NSString * userId;
@end
