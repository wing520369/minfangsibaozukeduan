//
//  WCRRentTipInfoModel.h
//  Landlord
//
//  Created by eliot on 2017/5/18.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

/// 交租金提醒 model
@interface WCRRentTipInfoModel : WCRBasicModel
/// 缴费单id
@property (nonatomic, copy) NSString * billId;
/// 房号
@property (nonatomic, copy) NSString * roomNo;
/// 房产名称
@property (nonatomic, copy) NSString * houseName;
/// 创建时间
@property (nonatomic, copy) NSString * rentDate;
@end
