//
//  WCRAppointLookRoomModel.m
//  __projectName__
//
//  Created by aleven on 17/06/02.
//  Copyright © 2017年 eliot. All rights reserved.
//

#import "WCRAppointLookRoomModel.h"

@implementation WCRAppointLookRoomModel

- (instancetype)init {
    if (self = [super init]) {
        self.hidden = YES;
        self.isSelected = NO;
    }
    return self;
}

@end
