//
//  WCRMyInfoDetailModel.m
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyInfoDetailModel.h"

@implementation WCRMyInfoDetailModel
+ (NSArray *)getMyInfoDetailModels {
    //
    NSString *path = [[NSBundle mainBundle] pathForResource:@"WCRMyInfoDetailModel.plist" ofType:nil];
    NSArray *models = [WCRMyInfoDetailModel jsonWithArray:[NSArray arrayWithContentsOfFile:path]];
    return [models copy];
}
@end
