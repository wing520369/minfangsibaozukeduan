//
//  WCRMyInfoDetailModel.h
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

typedef NS_ENUM(NSInteger, MyInfoDetailType) {
    /// 昵称
    MyInfoDetailTypeNickName = 1100,
    /// 电话
    MyInfoDetailTypePhone,
    /// 微信
    MyInfoDetailTypeWX,
    /// 支付密码
    MyInfoDetailTypePayPassword,
};

@interface WCRMyInfoDetailModel : WCRBasicModel
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) CGFloat startY;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, assign) BOOL haveLine;
@property (nonatomic, assign) BOOL haveArrow;
@property (nonatomic, assign) MyInfoDetailType type;
+ (NSArray *)getMyInfoDetailModels;
@end
