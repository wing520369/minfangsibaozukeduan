//
//  WCRMyModel.h
//  Landlord
//
//  Created by 吴传荣 on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

typedef NS_ENUM(NSInteger, MyInfoType) {
    /// 我的房租管理
    MyInfoTypeRentManage = 2140,
    /// 积分商城
    MyInfoTypeMerchant,
    /// 优惠券
    MyInfoTypeCoupon,
    /// 我的账户
    MyInfoTypeMyAccount,
    /// 预约看房
    MyInfoTypeAppointLookRoom,
    /// 我的收藏
    MyInfoTypeCollect,
    /// 我的客服
    MyInfoTypeCustomer,
    /// 推广
    MyInfoTypeSpread,
    /// 生活服务
    MyInfoTypeLifeService,
    /// 新手指南
    MyInfoTypeGuide,
};

@interface WCRMyModel : WCRBasicModel
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) CGFloat startY;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, assign) BOOL haveLine;
@property (nonatomic, assign) MyInfoType type;

@property (nonatomic, copy) NSString *className;

+ (NSArray *)getMyInfoModels;

@end
