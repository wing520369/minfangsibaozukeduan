//
//  WCRPaymentListModel.h
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

/// 收支明细 model
@interface WCRPaymentListModel : WCRBasicModel
/// 动作类型：1充值入账，2消费出账，3提现出账，4售卖入账
@property (nonatomic, copy) NSString * actionType;
/// 流动金额
@property (nonatomic, assign) CGFloat amount;
@property (nonatomic, copy) NSString * createDate;
@property (nonatomic, copy) NSString * describe;
@property (nonatomic, copy) NSString * ID;
/// 金额类型：1支付宝，2微信支付，3平台余额
@property (nonatomic, copy) NSString * moneyType;
@property (nonatomic, copy) NSString * objectId;
@property (nonatomic, copy) NSString * remarks;
/// 类型：-1出账，1入账
@property (nonatomic, copy) NSString * type;
@property (nonatomic, copy) NSString * updateDate;
@property (nonatomic, copy) NSString * userId;
@end
