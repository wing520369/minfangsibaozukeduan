//
//  WCRCollectionModel.m
//  Tenant
//
//  Created by eliot on 2017/6/2.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRCollectionModel.h"
#import "WCRRoomModel.h"

@implementation WCRCollectionModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{@"room":[WCRRoomModel class]};
}
- (instancetype)init {
    if (self = [super init]) {
        self.hidden = YES;
        self.isSelected = NO;
    }
    return self;
}
@end
