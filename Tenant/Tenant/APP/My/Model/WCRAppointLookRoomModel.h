//
//  WCRAppointLookRoomModel.h
//  __projectName__
//
//  Created by aleven on 17/06/02.
//  Copyright © 2017年 eliot. All rights reserved.
//

#import "WCRBasicModel.h"

/// 预约看房 model 
@interface WCRAppointLookRoomModel : WCRBasicModel
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL hidden;
@property (nonatomic, copy) NSString * createDate;
@property (nonatomic, copy) NSString * delFlag;
@property (nonatomic, copy) NSString * delFrom;
@property (nonatomic, copy) NSString * delTo;
/// 租客用户id
@property (nonatomic, copy) NSString * fromUserId;
/// 房产id
@property (nonatomic, copy) NSString * houseId;
/// 房产名称
@property (nonatomic, copy) NSString * houseName;
@property (nonatomic, copy) NSString * ID;
/// 状态 0 未读 1已读
@property (nonatomic, copy) NSString * isRead;
/// 看房人
@property (nonatomic, copy) NSString * name;
/// 看房人联系方式
@property (nonatomic, copy) NSString * phone;
/// 房间id
@property (nonatomic, copy) NSString * roomId;
/// 房号
@property (nonatomic, copy) NSString * roomNo;
/// 看房时间
@property (nonatomic, copy) NSString * time;
/// 房东用户id
@property (nonatomic, copy) NSString * toUserId;
@property (nonatomic, copy) NSString * updateDate;
@end
