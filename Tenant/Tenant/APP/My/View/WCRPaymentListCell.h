//
//  WCRPaymentListCell.h
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRPaymentListModel;
/// 收支明细 cell
@interface WCRPaymentListCell : UITableViewCell
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) WCRPaymentListModel *model;
+ (CGFloat)cellHeight;
@end
