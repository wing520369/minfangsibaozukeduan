//
//  WCREvalutionCell.h
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCREvalutionModel;
@interface WCREvalutionCell : UITableViewCell
@property (nonatomic, strong) WCREvalutionModel *model;
@end
