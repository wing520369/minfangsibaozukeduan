//
//  WCRDepositCell.h
//  Tenant
//
//  Created by eliot on 2017/6/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRTenementModel,WCRRentDepositModel;
typedef NS_ENUM(NSInteger, ActionType) {
    /// 退租
    ActionTypeExitRent = 330,
    /// 退租中
    ActionTypeOnExitRent,
    /// 待评价
    ActionTypeGoToEvalution,
    /// 支付
    ActionTypePay,
    /// 确定（退租单）
    ActionTypeConfirmExit,
    /// 待收款（待房东付款）
    ActionTypeOnReceiveMoney,
    /// 删除
    ActionTypeDelete,
};

@protocol WCRTenenmentCellDelegate <NSObject>
@optional
- (void)tenenmentCellClickAction:(ActionType)type Model:(WCRTenementModel *)model;
- (void)tenenmentCellDepositClickAction:(ActionType)type DepositModel:(WCRRentDepositModel *)model;
@end

@interface WCRDepositCell : UITableViewCell
@property (nonatomic, strong) WCRTenementModel *model;
@property (nonatomic, strong) WCRRentDepositModel *rentDepositModel;
@property (nonatomic, weak) id<WCRTenenmentCellDelegate> delegate;
+ (CGFloat)cellHeight;
@end
