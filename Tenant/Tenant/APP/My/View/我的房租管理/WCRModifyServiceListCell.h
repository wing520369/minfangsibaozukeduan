//
//  WCRModifyServiceListCell.h
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRModifyServiceListModel;
/// 报修列表 cell
@interface WCRModifyServiceListCell : UITableViewCell
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) WCRModifyServiceListModel *model;
- (void)setButtonSelected:(BOOL)isSelected;
- (void)setButtonHidden:(BOOL)isHidden;
+ (CGFloat)cellHeight;
@end
