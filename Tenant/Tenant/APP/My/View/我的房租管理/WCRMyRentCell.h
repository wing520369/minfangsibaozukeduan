//
//  WCRMessageKindCell.h
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WCRMyRentModel;
@interface WCRMyRentCell : UITableViewCell
@property (nonatomic, strong) WCRMyRentModel *model;
+ (CGFloat)cellHeight;
@end
