//
//  WCRTenenmentCell.m
//  Tenant
//
//  Created by eliot on 2017/6/5.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRTenenmentCell.h"
#import "myUILabel.h"
#import "WCRTenementModel.h"
#import "WCRRentDepositModel.h"
#import "WCRRoomModel.h"
#import "UIImageView+WebCache.h"
#import "NSString+WCRCustom.h"

@interface WCRTenenmentCell ()
@property (nonatomic, strong) UIImageView *statusImageView;
@property (nonatomic, strong) myUILabel *houseRoomLabel;
@property (nonatomic, strong) UIImageView *arrowImageView;
@property (nonatomic, strong) UIImageView *houseImageView;
@property (nonatomic, strong) myUILabel *houseLabel;
@property (nonatomic, strong) myUILabel *roomLabel;
@property (nonatomic, strong) myUILabel *infoLabel;
@property (nonatomic, strong) myUILabel *ownerLabel;
//@property (nonatomic, strong) myUILabel *rentTitleLabel;
//@property (nonatomic, strong) myUILabel *rentContenLabel;
@property (nonatomic, strong) myUILabel *dateLabel;
@property (nonatomic, strong) myUILabel *dateContentLabel;
@property (nonatomic, strong) myUILabel *leftTimeLabel;
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;
@end

@implementation WCRTenenmentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat x = 0;
        CGFloat y = 0;
        CGFloat width = kMainWidth;
        CGFloat height = 20;
        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
        [self.contentView addSubview:grayView];
        
        // 状态
        UIImage *image = [UIImage imageFileNamed:@"icon_in-rent"];
        width = image.size.width;
        height = image.size.height;
        y = grayView.bottom_wcr - height * (10 / 22.0);
        x = kMainWidth - width - kSpaceX;
        _statusImageView = [self getImageViewWithFrame:CGRectMake(x, y, width, height)];
        
        UIFont *font = [UIFont CustomFontWithSize:11.0];
        // 房产房号
        x = kSpaceX;
        y = grayView.bottom_wcr;
        width = 100;
        height = 25;
        _houseRoomLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        
        width = 10;
        y += (height - width) / 2.0;
        _arrowImageView = [self getImageViewWithFrame:CGRectMake(x, y, width, width)];
        _arrowImageView.image = [UIImage imageFileNamed:@"icon-xiangyoujiantou"];
        
        y = _houseRoomLabel.bottom_wcr;
        y = [self getLineViewWithYpoint:y];
        
        // 房产图片
        height = 80;
        width = height * (260 / 200.0);
        y += 10;
        _houseImageView = [self getImageViewWithFrame:CGRectMake(x, y, width, height)];
        
        font = [UIFont CustomFontWithSize:13.0];
        // 房产
        x = _houseImageView.right_wcr + 10;
        width = kMainWidth - x - kSpaceX;
        height = font.lineHeight + 5;
        _houseLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _houseLabel.verticalAlignment = VerticalAlignmentTop;
        
        // 房号
        y = _houseLabel.bottom_wcr;
        _roomLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _roomLabel.verticalAlignment = VerticalAlignmentTop;
        
        font = [UIFont CustomFontWithSize:11.0];
        // 其他信息
        y = _roomLabel.bottom_wcr;
        _infoLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _infoLabel.verticalAlignment = VerticalAlignmentTop;
        _infoLabel.textColor = [UIColor colorWithHexString:kCustomGrayColor];
        
        // 房东
        y = _infoLabel.bottom_wcr;
        _ownerLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _ownerLabel.verticalAlignment = VerticalAlignmentBottom;
        
        y = _houseImageView.bottom_wcr + 10;
        y = [self getLineViewWithYpoint:y];
        
        // 租金
        font = [UIFont CustomFontWithSize:13.0];
        x = kSpaceX;
        width = 40;
        height = 40;
//        _rentTitleLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
//        _rentTitleLabel.text = @"租金";
//        
//        x = _rentTitleLabel.right_wcr;
//        width = kMainWidth - x - kSpaceX;
//        _rentContenLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
//        _rentContenLabel.textAlignment = NSTextAlignmentRight;
//        
//        y = _rentTitleLabel.bottom_wcr;
//        y = [self getLineViewWithYpoint:y];
        
        // 租期
        _dateLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _dateLabel.text = @"租期";
        
        x = _dateLabel.right_wcr;
        width = kMainWidth - x - kSpaceX;
        _dateContentLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _dateContentLabel.textAlignment = NSTextAlignmentRight;
        
        y = _dateLabel.bottom_wcr;
        y = [self getLineViewWithYpoint:y];
        
        // 倒计时
        x = kSpaceX;
        width = 200;
        _leftTimeLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _leftTimeLabel.textColor = [UIColor colorWithHexString:kMainColor];
        
        // 按钮二
        width = 70;
        height = font.lineHeight + 5;
        x = kMainWidth - width - kSpaceX;
        y += (_leftTimeLabel.height_wcr - height) / 2.0;
        _rightButton = [self getButtonWithFrame:CGRectMake(x, y, width, height) Font:font];
        
        // 按钮一
        x = _rightButton.x_wcr - width - 10;
        _leftButton = [self getButtonWithFrame:CGRectMake(x, y, width, height) Font:font];
        
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 20 + 25 + 10 + 80 + 10 + 40 + 40;
}

- (CGFloat)getLineViewWithYpoint:(CGFloat)yPoint {
    UIView *lineView = [UIView lineView];
    lineView.frame = CGRectMake(kSpaceX, yPoint, kMainWidth - kSpaceX * 2, 1);
    [self.contentView addSubview:lineView];
    return lineView.bottom_wcr;
}

- (UIButton *)getButtonWithFrame:(CGRect)frame Font:(UIFont *)font {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    [button buttonWithTitleColor:[UIColor colorWithHexString:kMainColor] Background:nil Font:font Title:nil];
    [self.contentView addSubview:button];
    [button setLayerWithBorderWidth:1.0 Corner:3.0 BorderColor:[UIColor lineColor]];
    [button addTarget:self action:@selector(buttonClickAction:)];
    [button setEnlargeEdgeWithTop:15 Right:5 Bottom:10 Left:5];
    return button;
}

- (UIImageView *)getImageViewWithFrame:(CGRect)frame {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    [imageView setContentModeToScaleAspectFill];
    [self.contentView addSubview:imageView];
    return imageView;
}

- (myUILabel *)getLabelWithFrame:(CGRect)frame Font:(UIFont *)font {
    myUILabel *label = [[myUILabel alloc] initWithFrame:frame];
    [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
    [self.contentView addSubview:label];
    return label;
}

- (void)buttonClickAction:(UIButton *)button {
    if (_model) {
        if (_delegate && [_delegate respondsToSelector:@selector(tenenmentCellClickAction:Model:)]) {
            [_delegate tenenmentCellClickAction:button.tag Model:_model];
        }
    }
    else if (_rentDepositModel) {
        if (_delegate && [_delegate respondsToSelector:@selector(tenenmentCellDepositClickAction:DepositModel:)]) {
            [_delegate tenenmentCellDepositClickAction:button.tag DepositModel:_rentDepositModel];
        }
    }
}

- (void)setModel:(WCRTenementModel *)model {
    _model = model;
    
    NSString *imageName = @"icon_rent-back";
    _leftTimeLabel.hidden = YES;
    _leftButton.hidden = YES;
    _rightButton.hidden = YES;
    switch ([model.room.type integerValue]) {
        case 0:{
            // 在租 icon_in-rent   退租中 icon_rent-back
            
            imageName = [model.status integerValue] == 1 ? @"icon_in-rent" : @"icon_rent-back";
            _rightButton.hidden = NO;
            NSString *text = [model.status integerValue] == 1 ? @"退租" : @"退租中";
            [_rightButton setTitle:text forState:0];
            [_rightButton setTitleColor:[UIColor colorWithHexString:kCustomGrayColor] forState:0];
            _rightButton.tag =  [model.status integerValue] == 1 ? ActionTypeExitRent : ActionTypeOnExitRent;
            _rightButton.userInteractionEnabled = [model.status integerValue] == 1 ? YES : NO;
        }
            break;
        case 1:{
            imageName = @"icon_pending-evaluation";
            
            _leftButton.hidden = NO;
            [_leftButton setTitle:@"删除" forState:0];
            [_leftButton setTitleColor:[UIColor colorWithHexString:kCustomGrayColor] forState:0];
            _leftButton.tag = ActionTypeDelete;
            
            _rightButton.hidden = NO;
            [_rightButton setTitle:@"评价" forState:0];
            [_rightButton setTitleColor:[UIColor colorWithHexString:kMainColor] forState:0];
            _rightButton.tag = ActionTypeGoToEvalution;
        }
            break;
        case 2:{
            imageName = @"icon_retired-rent";
            if ([model.tenantComment integerValue] == 0) {
                // 待评价
                _leftButton.hidden = NO;
                [_leftButton setTitle:@"删除" forState:0];
                [_leftButton setTitleColor:[UIColor colorWithHexString:kCustomGrayColor] forState:0];
                _leftButton.tag = ActionTypeDelete;
                
                _rightButton.hidden = NO;
                [_rightButton setTitle:@"评价" forState:0];
                [_rightButton setTitleColor:[UIColor colorWithHexString:kMainColor] forState:0];
                _rightButton.tag = ActionTypeGoToEvalution;
            }
            else {
                _rightButton.hidden = NO;
                [_rightButton setTitle:@"删除" forState:0];
                [_rightButton setTitleColor:[UIColor colorWithHexString:kCustomGrayColor] forState:0];
                _rightButton.tag = ActionTypeDelete;
            }
        }
            break;
        default:
            break;
    }
    _statusImageView.image = [UIImage imageFileNamed:imageName];
    
    _houseRoomLabel.text = [NSString stringWithFormat:@"房产：%@ - 房号：%@",model.room.houseName,model.room.roomNo];
    _houseRoomLabel.width_wcr = [NSString calculateWithFont:_houseRoomLabel.font Content:_houseRoomLabel.text];
    if (_houseRoomLabel.right_wcr > _statusImageView.x_wcr - 20) {
        _houseRoomLabel.width_wcr = _statusImageView.x_wcr - 20 - kSpaceX;
    }
    _arrowImageView.x_wcr = _houseRoomLabel.right_wcr + 5;
    
    NSString *imageUrl = [model.room.roomImgs firstObject].img;
    [_houseImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageFileNamed:@"1206icon_tupian"]];
    
    _houseLabel.text = [NSString stringWithFormat:@"房产：%@",model.room.houseName];
    
    _roomLabel.text = [NSString stringWithFormat:@"房号：%@",model.room.roomNo];
    
    _infoLabel.text = [NSString stringWithFormat:@"%@室%@厅%@卫 %@㎡ %@/%@层",model.room.room,model.room.hall,model.room.toilet,model.room.roomArea,model.room.tier,model.room.totalTier];
    
    _ownerLabel.text = [NSString stringWithFormat:@"房东：%@",model.hostName];
    
//    _rentContenLabel.text = [NSString stringWithFormat:@"%@元/月",model.rent];
    
    _dateContentLabel.text = [NSString stringWithFormat:@"%@ - %@",[model.beginRental formatAllDateString],[model.endRental formatAllDateString]];
    
}

@end
