//
//  WCRMessageKindCell.m
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyRentCell.h"
#import "WCRMyRentModel.h"

@interface WCRMyRentCell ()
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@end

@implementation WCRMyRentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat cellHeight = [WCRMyRentCell cellHeight];
        CGFloat x = kSpaceX;
        CGFloat width = 22;
        CGFloat height = 22;
        CGFloat y = (cellHeight- height) / 2.0;
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_iconImageView setContentModeToScaleAspectFill];
        [self.contentView addSubview:_iconImageView];
        
        x = _iconImageView.right_wcr + _iconImageView.x_wcr;
        y = 0;
        height = cellHeight;
        width = kMainWidth - x - 40;
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_titleLabel labelWithTextColor:nil Font:[UIFont CustomFontWithSize:15.0] BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_titleLabel];
        
        width = 14;
        height = 14;
        x = kMainWidth - width - kSpaceX;
        y = (cellHeight - height) / 2.0;
        UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
        [self.contentView addSubview:arrowImageView];
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 55.0;
}

- (void)setModel:(WCRMyRentModel *)model {
    _iconImageView.image = [UIImage imageFileNamed:model.imageName];
    _titleLabel.text = model.title;
}

@end
