//
//  WCRModifyServiceListCell.m
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRModifyServiceListCell.h"
#import "WCRModifyServiceListModel.h"
#import "myUILabel.h"

@interface WCRModifyServiceListCell ()
@property (nonatomic, strong) myUILabel *titleLabel;
@property (nonatomic, strong) myUILabel *contenLabel;
@property (nonatomic, strong) UIButton *multiSelectedButton;
@end

@implementation WCRModifyServiceListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIFont *font = [UIFont CustomFontWithSize:15.0];
        
        CGFloat cellHeight = [WCRModifyServiceListCell cellHeight];
        CGFloat x = kSpaceX;
        CGFloat y = 0;
        CGFloat width = kMainWidth - x - 40;
        CGFloat height = 38;
        _titleLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_titleLabel labelWithTextColor:[UIColor colorWithHexString:kMainColor] Font:font BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_titleLabel];
        
        x = kSpaceX;
        width = kMainWidth - x * 2;
        y = _titleLabel.bottom_wcr;
        height = cellHeight - height;
        font = [UIFont CustomFontWithSize:13.0];
        _contenLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_contenLabel labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:nil];
        _contenLabel.verticalAlignment = VerticalAlignmentTop;
        [self.contentView addSubview:_contenLabel];
        _lineView = [_contenLabel addLineViewPositionType:kBottom_Type];
        
        width = 14;
        height = 14;
        x = kMainWidth - width - kSpaceX;
        y = (cellHeight - height) / 2.0;
        UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
        [self.contentView addSubview:arrowImageView];
        
        UIView *selectedBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, cellHeight)];
        selectedBgView.backgroundColor = [UIColor clearColor];
        self.backgroundView = selectedBgView;
        
        _multiSelectedButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, selectedBgView.height_wcr)];
        [_multiSelectedButton buttonWithImageName:@"icon_a" SelectedImageName:@"icon_b"];
        _multiSelectedButton.backgroundColor = [UIColor clearColor];
        [selectedBgView addSubview:_multiSelectedButton];
        _multiSelectedButton.userInteractionEnabled = NO;
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 60;
}

- (void)setModel:(WCRModifyServiceListModel *)model {
    
    _titleLabel.text = model.title;
    _contenLabel.text =[NSString stringWithFormat:@"房产：%@ - 房号：%@",model.houseName,model.roomNo];

    [self setButtonHidden:model.hidden];
    [self setButtonSelected:model.isSelected];
}

- (void)setButtonHidden:(BOOL)isHidden {
    _multiSelectedButton.hidden = isHidden;
}
- (void)setButtonSelected:(BOOL)isSelected {
    _multiSelectedButton.selected = isSelected;
}

@end
