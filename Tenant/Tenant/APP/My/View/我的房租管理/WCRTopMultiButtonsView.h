//
//  WCRTopMultiButtonsView.h
//  Landlord
//
//  Created by eliot on 2017/5/11.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TopMultipleButtonActionBlock)(NSInteger index);

@interface WCRTopMultiButtonsView : UIView
/** 按钮点击回调 */
@property (nonatomic,copy) TopMultipleButtonActionBlock actionBlock;
/** 线 */
@property (nonatomic, strong) UIView  *movingLineView;

@property (nonatomic, strong) UIColor *norTitleColor;
@property (nonatomic, strong) UIColor *disabledTitleColor;

@property (nonatomic,strong) NSMutableArray *buttonArray;

/**
 *  根据按钮数量初始化一个topbuttonview
 *
 *  @param frame       fareme
 *  @param titleArray  按钮标题数组
 *  @param actionBlock 按钮点击回调
 *
 *  @return view
 */
- (instancetype)initWithFrame:(CGRect)frame
             buttonTitleArray:(NSArray *)titleArray
                  actionBlock:(TopMultipleButtonActionBlock)actionBlock;
/**
 *  根据下标给某个按钮设置标题
 *
 *  @param title 标题
 *  @param index 下标
 */
- (void)setTitle:(NSString *)title atIndex:(NSInteger)index;

/**
 *  根据下标把某个按钮设置为不可点击
 *
 *  @param index 下标
 */
- (void)selectAtIndex:(NSInteger)index;

@end
