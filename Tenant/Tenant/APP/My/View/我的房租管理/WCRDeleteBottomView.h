//
//  WCRDeleteBottomView.h
//  Landlord
//
//  Created by eliot on 2017/5/4.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RACSubject;
/// 底部弹出的删除动作View
@interface WCRDeleteBottomView : UIView
@property (nonatomic, strong) RACSubject *deleteActionSignal;
@end
