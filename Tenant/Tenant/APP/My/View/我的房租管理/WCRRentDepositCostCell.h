//
//  WCRRentDepositDetailCell.h
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRDepositCostModel;
@interface WCRRentDepositCostCell : UITableViewCell
@property (nonatomic, strong) WCRDepositCostModel *model;
+ (CGFloat)cellHeight;
@end
