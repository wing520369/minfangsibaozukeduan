//
//  WCRRentDepositDetailCell.m
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRentDepositCostCell.h"
#import "WCRDepositCostModel.h"

@interface WCRRentDepositCostCell()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@end

@implementation WCRRentDepositCostCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIFont *font = [UIFont CustomFontWithSize:11.0];
        CGFloat x = kSpaceX;
        CGFloat y = 0;
        CGFloat width = kMainWidth - kSpaceX - 100;
        CGFloat height = [WCRRentDepositCostCell cellHeight];
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_titleLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_titleLabel];
        
        width = 100 - kSpaceX;
        x = kMainWidth - width - kSpaceX;
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_contentLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        _contentLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_contentLabel];
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 25;
}

- (void)setModel:(WCRDepositCostModel *)model {
    _titleLabel.text = model.name;
    if ([model.type integerValue] == 0) {
        // 抄表计算
        _titleLabel.text = [NSString stringWithFormat:@"%@      %.2f%@      %.2f - %.2f",model.name,model.price,model.unitString,model.lastValue,model.thisValue];
    }
    
    _contentLabel.text = [NSString stringWithFormat:@"%.2f",model.money];
}

@end
