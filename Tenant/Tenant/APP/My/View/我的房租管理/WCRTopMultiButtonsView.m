//
//  WCRTopMultiButtonsView.m
//  Landlord
//
//  Created by eliot on 2017/5/11.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRTopMultiButtonsView.h"

@implementation WCRTopMultiButtonsView

- (instancetype)initWithFrame:(CGRect)frame
             buttonTitleArray:(NSArray *)titleArray actionBlock:(TopMultipleButtonActionBlock)actionBlock {
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        self.actionBlock = actionBlock;
        
        int count = (int)titleArray.count;
        CGFloat lineViewWidth = 1.0;
        CGFloat width = (frame.size.width - lineViewWidth * (count - 1)) / count;
        CGFloat height = frame.size.height;
        CGFloat x = 0;
        CGFloat y = 0;
        _buttonArray = [NSMutableArray array];
        for (int i = 0; i < count; i++) {
            //添加按钮
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.tag = i;
            button.frame = CGRectMake(x, y, width, height);
            NSString *title = [titleArray objectAtIndex:i];
            [button setTitle:title forState:0];
            [button setTitleColor:[UIColor blackColor] forState:0];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
            [button setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:0];
            [button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:kMainColor]] forState:UIControlStateDisabled];
            [button addTarget:self action:@selector(buttonAction:)forControlEvents:UIControlEventTouchUpInside];
            
            [self addSubview:button];
            [_buttonArray addObject:button];
            
            if (i > 0) {
                //添加分隔线
                UIView *lineView = [UIView lineView];
                lineView.frame = CGRectMake(x - lineViewWidth, y,lineViewWidth, height);
                [self addSubview:lineView];
            }
            x += lineViewWidth + width;
            
        }
        
        x = 0;
        height = 1.0;
        y = self.height_wcr - height;
        width = kMainWidth;
        _movingLineView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, height)];
        _movingLineView.backgroundColor = [UIColor lineColor];
        [self addSubview:_movingLineView];
        
        //
        UIButton *button = [_buttonArray objectAtIndex:0];
        [self changeStatueButton:button];
        //
    }
    return self;
}

- (void)setDisabledTitleColor:(UIColor *)disabledTitleColor{
    
    for (UIButton *button in _buttonArray) {
        [button setTitleColor:disabledTitleColor forState:UIControlStateDisabled];
    }
}

- (void)setNorTitleColor:(UIColor *)norTitleColor{
    for (UIButton *button in _buttonArray) {
        [button setTitleColor:norTitleColor forState:UIControlStateNormal];
    }
}

#pragma mark - 按钮点击响应
- (void)buttonAction:(UIButton *)button {
    [self changeStatueButton:button];
    if (_actionBlock) {
        _actionBlock(button.tag);
    }
}

- (void)setTitle:(NSString *)title atIndex:(NSInteger)index {
    if (_buttonArray.count > index) {
        UIButton *button = [_buttonArray objectAtIndex:index];
        [button setTitle:title forState:0];
    }
}

#pragma mark - 更改button的选择状态
- (void)changeStatueButton:(UIButton *)button {
    
    for (UIButton *but in _buttonArray) {
        if (button != but) {
            but.enabled = YES;
            but.titleLabel.font = [UIFont CustomFontWithSize:14.0];
        }
    }
    button.enabled = NO;
    button.titleLabel.font = [UIFont CustomFontWithSize:15.0];
//    [UIView animateWithDuration:0.4 animations:^{
//        _movingLineView.centerX_wcr = button.centerX_wcr;
//    } completion:nil];
}

- (void)selectAtIndex:(NSInteger)index {
    UIButton *button = [_buttonArray objectAtIndex:index];
    [self changeStatueButton:button];
}

@end
