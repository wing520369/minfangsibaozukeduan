//
//  WCREvalutionCell.m
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCREvalutionCell.h"
#import "WCREvalutionModel.h"
#import "UITextField+WCRCustom.h"
#import "NSString+WCRCustom.h"

@interface WCREvalutionCell ()
@property (nonatomic, strong) NSMutableArray *buttons;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UITextField *timeTextField;
@end

@implementation WCREvalutionCell

- (NSMutableArray *)buttons {
    if (!_buttons) {
        _buttons = [NSMutableArray arrayWithCapacity:5];
    }
    return _buttons;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat x = kSpaceX;
        CGFloat y = 0;
        CGFloat height = 25;
        CGFloat width = 15;
        NSString *imageName = @"icon_stars_default";
        NSString *selectImageName = @"icon_stars_selected";
        for (NSInteger i = 0; i < 5; i++) {
            UIButton *starButton = [UIButton buttonWithType:UIButtonTypeCustom];
            starButton.frame = CGRectMake(x, y, width, height);
            [starButton buttonWithImageName:imageName SelectedImageName:selectImageName];
            starButton.userInteractionEnabled = NO;
            [self.contentView addSubview:starButton];
            [self.buttons addObject:starButton];
            x = starButton.right_wcr;
        }
        
        y += height;
        UIView *lineView = [UIView lineView];
        lineView.frame = CGRectMake(0, y, kMainWidth, 1);
        [self.contentView addSubview:lineView];
        
        x = kSpaceX;
        width = kMainWidth - x * 2;
        y = lineView.bottom_wcr + 10;
        height = 18;
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        _contentLabel.numberOfLines = 0;
        [_contentLabel labelWithTextColor:nil Font:[UIFont CustomFontWithSize:13.0] BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_contentLabel];
        
        y = _contentLabel.bottom_wcr;
        _timeTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, y, kMainWidth, 30)];
        [_timeTextField textFieldAddRightImage:nil Width:kSpaceX RightBorder:0];
        _timeTextField.userInteractionEnabled = NO;
        _timeTextField.font =  _contentLabel.font;
        _timeTextField.textColor = [UIColor colorWithHexString:kCustomGrayColor];
        _timeTextField.backgroundColor = [UIColor colorWithRGB:223 G:223 B:223];
        _timeTextField.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_timeTextField];
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size {
    return CGSizeMake(size.width, _timeTextField.bottom_wcr);
}

- (void)setModel:(WCREvalutionModel *)model {
    
    for (NSInteger i = 0; i < self.buttons.count; i++) {
        UIButton *button = [self.buttons objectAtIndex:i];
        if (i < [model.level integerValue]) {
            button.selected = YES;
        }
        else {
            button.selected = NO;
        }
    }
    
    CGFloat height = [NSString calculateWithContent:model.content MaxWidth:_contentLabel.width_wcr Font:_contentLabel.font];
    _contentLabel.height_wcr = height;
    _contentLabel.text = model.content;
    
    _timeTextField.y_wcr = _contentLabel.bottom_wcr + 10;
    _timeTextField.text = [model.createDate formatAllDateString];
}

@end
