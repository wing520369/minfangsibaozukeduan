//
//  WCRDeleteBottomView.m
//  Landlord
//
//  Created by eliot on 2017/5/4.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRDeleteBottomView.h"
#import "RACSubject.h"

typedef NS_ENUM(NSInteger, DeleteAcitonType) {
    DeleteAcitonTypeConfirm = 409,
    DeleteAcitonTypeCancel
};

@implementation WCRDeleteBottomView

- (instancetype)init {
    if (self = [super init]) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    
    self.frame = CGRectMake(0, kMainHeight, kMainWidth, kDefaultHeight);
    self.backgroundColor = [UIColor whiteColor];
    [self addLineViewPositionType:kTop_Type];
    
    NSArray *titles = @[@"取消",@"确定"];
    NSArray *tags = @[@(DeleteAcitonTypeCancel),@(DeleteAcitonTypeConfirm)];
    CGFloat x = 0;
    for (NSInteger i = 0; i < titles.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(x, 0, kMainWidth / 2.0, kDefaultHeight);
        [button buttonWithTitleColor:[UIColor colorWithHexString:kCustomGrayColor] Background:nil Font:[UIFont CustomFontWithSize:14.0] Title:[titles objectAtIndex:i]];
        button.tag = [[tags objectAtIndex:i] integerValue];
        [button addTarget:self action:@selector(deleteAction:)];
        [self addSubview:button];
        
        UIView *lineView = [UIView lineView];
        lineView.frame = CGRectMake(button.right_wcr, 0, 1, kDefaultHeight);
        [self addSubview:lineView];
        
        x = lineView.right_wcr;
    }
}

- (void)deleteAction:(UIButton *)button {
    BOOL delete = button.tag == DeleteAcitonTypeCancel ? NO : YES;
    [_deleteActionSignal sendNext:@(delete)];
}

@end
