//
//  WCRRentDepositHouseInfoCell.h
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRRentDepositModel;
@interface WCRRentDepositHouseInfoCell : UITableViewCell
@property (nonatomic, strong) WCRRentDepositModel *rentDepositModel;
+ (CGFloat)cellHeight;
@end
