//
//  WCRRentDepositHouseInfoCell.m
//  Tenant
//
//  Created by eliot on 2017/6/6.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRentDepositHouseInfoCell.h"
#import "myUILabel.h"
#import "WCRRentDepositModel.h"
#import "UIImageView+WebCache.h"
#import "NSString+WCRCustom.h"
#import "WCRRoomModel.h"

@interface WCRRentDepositHouseInfoCell ()
@property (nonatomic, strong) UIImageView *statusImageView;
@property (nonatomic, strong) myUILabel *houseRoomLabel;
@property (nonatomic, strong) UIImageView *arrowImageView;
@property (nonatomic, strong) UIImageView *houseImageView;
@property (nonatomic, strong) myUILabel *houseLabel;
@property (nonatomic, strong) myUILabel *roomLabel;
@property (nonatomic, strong) myUILabel *infoLabel;
@property (nonatomic, strong) myUILabel *ownerLabel;
@end

@implementation WCRRentDepositHouseInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat x = 0;
        CGFloat y = 0;
        CGFloat width = kMainWidth;
        CGFloat height = 20;
        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
        [self.contentView addSubview:grayView];
        
        // 状态
        UIImage *image = [UIImage imageFileNamed:@"icon_in-rent"];
        width = image.size.width;
        height = image.size.height;
        y = grayView.bottom_wcr - height * (10 / 22.0);
        x = kMainWidth - width - kSpaceX;
        _statusImageView = [self getImageViewWithFrame:CGRectMake(x, y, width, height)];
        
        UIFont *font = [UIFont CustomFontWithSize:11.0];
        // 房产房号
        x = kSpaceX;
        y = grayView.bottom_wcr;
        width = 100;
        height = 25;
        _houseRoomLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        
        width = 10;
        y += (height - width) / 2.0;
        _arrowImageView = [self getImageViewWithFrame:CGRectMake(x, y, width, width)];
        _arrowImageView.image = [UIImage imageFileNamed:@"icon-xiangyoujiantou"];
        
        y = _houseRoomLabel.bottom_wcr;
        y = [self getLineViewWithYpoint:y];
        
        // 房产图片
        height = 80;
        width = height * (260 / 200.0);
        y += 15;
        _houseImageView = [self getImageViewWithFrame:CGRectMake(x, y, width, height)];
        
        font = [UIFont CustomFontWithSize:13.0];
        // 房产
        x = _houseImageView.right_wcr + 10;
        width = kMainWidth - x - kSpaceX;
        height = font.lineHeight + 5;
        _houseLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _houseLabel.verticalAlignment = VerticalAlignmentTop;
        
        // 房号
        y = _houseLabel.bottom_wcr;
        _roomLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _roomLabel.verticalAlignment = VerticalAlignmentTop;
        
        font = [UIFont CustomFontWithSize:11.0];
        // 其他信息
        y = _roomLabel.bottom_wcr;
        _infoLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _infoLabel.verticalAlignment = VerticalAlignmentTop;
        _infoLabel.textColor = [UIColor colorWithHexString:kCustomGrayColor];
        
        // 房东
        y = _infoLabel.bottom_wcr;
        _ownerLabel = [self getLabelWithFrame:CGRectMake(x, y, width, height) Font:font];
        _ownerLabel.verticalAlignment = VerticalAlignmentBottom;
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 20 + 25 + 15 + 80 + 15;
}

- (CGFloat)getLineViewWithYpoint:(CGFloat)yPoint {
    UIView *lineView = [UIView lineView];
    lineView.frame = CGRectMake(kSpaceX, yPoint, kMainWidth - kSpaceX * 2, 1);
    [self.contentView addSubview:lineView];
    return lineView.bottom_wcr;
}

- (UIImageView *)getImageViewWithFrame:(CGRect)frame {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    [imageView setContentModeToScaleAspectFill];
    [self.contentView addSubview:imageView];
    return imageView;
}

- (myUILabel *)getLabelWithFrame:(CGRect)frame Font:(UIFont *)font {
    myUILabel *label = [[myUILabel alloc] initWithFrame:frame];
    [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
    [self.contentView addSubview:label];
    return label;
}

- (void)setRentDepositModel:(WCRRentDepositModel *)rentDepositModel {
    _rentDepositModel = rentDepositModel;
    
    NSString *imageName = @"icon_tzf";
    // 账单状态 0-取消 1-待支付 2-支付完成
    switch ([rentDepositModel.status integerValue]) {
        case 0:{
            
        }
            break;
        case 1:{
            
        }
            break;
        case 2:{
            imageName = @"icon_ywc";
        }
            break;
        default:
            break;
    }
    _statusImageView.image = [UIImage imageFileNamed:imageName];
    
    _houseRoomLabel.text = [NSString stringWithFormat:@"房产：%@ - 房号：%@",rentDepositModel.houseName,rentDepositModel.roomNo];
    _houseRoomLabel.width_wcr = [NSString calculateWithFont:_houseRoomLabel.font Content:_houseRoomLabel.text];
    if (_houseRoomLabel.right_wcr > _statusImageView.x_wcr - 20) {
        _houseRoomLabel.width_wcr = _statusImageView.x_wcr - 20 - kSpaceX;
    }
    _arrowImageView.x_wcr = _houseRoomLabel.right_wcr + 5;
    
    NSString *imageUrl = [rentDepositModel.roomImgs firstObject].img;
    [_houseImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageFileNamed:@"1206icon_tupian"]];
    
    _houseLabel.text = [NSString stringWithFormat:@"房产：%@",rentDepositModel.houseName];
    
    _roomLabel.text = [NSString stringWithFormat:@"房号：%@",rentDepositModel.roomNo];
    
    _infoLabel.text = [NSString stringWithFormat:@"%@室%@厅%@卫 %@㎡ %@/%@层",rentDepositModel.room,rentDepositModel.hall,rentDepositModel.toilet,rentDepositModel.roomArea,rentDepositModel.tier,rentDepositModel.totalTier];
    
    _ownerLabel.text = [NSString stringWithFormat:@"房东：%@",rentDepositModel.hostName];
}

@end
