//
//  WCRMyInfoDetailCell.h
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WCRMyInfoDetailModel;
@interface WCRMyInfoDetailCell : UITableViewCell
@property (nonatomic, strong) WCRMyInfoDetailModel *model;
@end
