//
//  WCRModifyInfoCell.m
//  Landlord
//
//  Created by eliot on 2017/5/11.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRInfoListCell.h"
#import "myUILabel.h"
#import "WCRRentDepositModel.h"

#define kRedPointWH 6

@interface WCRInfoListCell ()
@property (nonatomic, strong) myUILabel *titleLabel;
@property (nonatomic, strong) myUILabel *contenLabel;
@property (nonatomic, strong) UIView *redPointView;
@property (nonatomic, strong) UIButton *multiSelectedButton;
@end

@implementation WCRInfoListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIFont *font = [UIFont CustomFontWithSize:15.0];
        
        CGFloat cellHeight = [WCRInfoListCell cellHeight];
        CGFloat x = kSpaceX;
        CGFloat y = 0;
        CGFloat width = kMainWidth - x - 40;
        CGFloat height = 38;
        _titleLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_titleLabel labelWithTextColor:[UIColor colorWithHexString:kMainColor] Font:font BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_titleLabel];
        
        x = kSpaceX;
        width = kMainWidth - x * 2;
        y = _titleLabel.bottom_wcr;
        height = cellHeight - height;
        font = [UIFont CustomFontWithSize:13.0];
        _contenLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_contenLabel labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:nil];
        _contenLabel.verticalAlignment = VerticalAlignmentTop;
        [self.contentView addSubview:_contenLabel];
        _lineView = [_contenLabel addLineViewPositionType:kBottom_Type];
        
        width = 14;
        height = 14;
        x = kMainWidth - width - kSpaceX;
        y = (cellHeight - height) / 2.0;
        UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
        [self.contentView addSubview:arrowImageView];
        
        // 红点
        _redPointView = [[UIView alloc] initWithFrame:CGRectZero];
        _redPointView.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:_redPointView];
        
        UIView *selectedBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, cellHeight)];
        selectedBgView.backgroundColor = [UIColor clearColor];
        self.backgroundView = selectedBgView;
        
        _multiSelectedButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, selectedBgView.height_wcr)];
        [_multiSelectedButton buttonWithImageName:@"0503icon_a" SelectedImageName:@"0503icon_b"];
        _multiSelectedButton.backgroundColor = [UIColor clearColor];
        [selectedBgView addSubview:_multiSelectedButton];
        _multiSelectedButton.userInteractionEnabled = NO;
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 60;
}

- (void)setModel:(WCRRentDepositModel *)model {
    _model = model;
    _titleLabel.text = @"交租提醒";
    _contenLabel.text = [NSString stringWithFormat:@"房产：%@ - 房号：%@",model.houseName,model.roomNo];
    BOOL hidden = [model.isRead integerValue] == 0 ? NO : YES;
    [self setRedPointFrameWithTitle:_titleLabel.text Hidden:hidden];
}

- (void)setRedPointFrameWithTitle:(NSString *)title Hidden:(BOOL)hidden {
    
    CGFloat x = _titleLabel.x_wcr + _titleLabel.font.pointSize * title.length + 3;
    if (x > _titleLabel.right_wcr) {
        x = _titleLabel.right_wcr + 3;
    }
    CGFloat y = (_titleLabel.height_wcr - _titleLabel.font.lineHeight) / 2.0;
    _redPointView.frame = CGRectMake(x, y, kRedPointWH, kRedPointWH);
    [_redPointView setViewCorner:kRedPointWH / 2.0];
    
    _redPointView.hidden = hidden;
}

@end
