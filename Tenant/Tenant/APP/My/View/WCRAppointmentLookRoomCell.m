//
//  WCRAppointmentLookRoomCell.m
//  Tenant
//
//  Created by eliot on 2017/6/2.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRAppointmentLookRoomCell.h"
#import "WCRAppointLookRoomModel.h"

@interface WCRAppointmentLookRoomCell ()
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UIButton *multiSelectedButton;
@end

@implementation WCRAppointmentLookRoomCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat cellHeight = kDefaultHeight;
        CGFloat x = kSpaceX;
        CGFloat width = kMainWidth - x - 35;
        CGFloat y = 0;
        CGFloat height = cellHeight;
        UIFont *font = [UIFont CustomFontWithSize:14.0];
        _leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_leftLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_leftLabel];
        
        width = 13;
        x = kMainWidth - width - kSpaceX;
        y = (cellHeight - width) / 2.0;
        UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, width)];
        [arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
        [self.contentView addSubview:arrowImageView];
        
        _lineView = [UIView lineView];
        _lineView.frame = CGRectMake(kSpaceX, cellHeight - 1, kMainWidth - kSpaceX * 2, 1);
        [self.contentView addSubview:_lineView];
        
        UIView *selectedBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, cellHeight)];
        selectedBgView.backgroundColor = [UIColor clearColor];
        self.backgroundView = selectedBgView;
        
        _multiSelectedButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, selectedBgView.height_wcr)];
        [_multiSelectedButton buttonWithImageName:@"icon_a" SelectedImageName:@"icon_b"];
        _multiSelectedButton.backgroundColor = [UIColor clearColor];
        [selectedBgView addSubview:_multiSelectedButton];
        _multiSelectedButton.userInteractionEnabled = NO;
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setModel:(WCRAppointLookRoomModel *)model {
    _leftLabel.text = [NSString stringWithFormat:@"房产：%@ - 房号：%@",model.houseName,model.roomNo];
    [self setButtonHidden:model.hidden];
    [self setButtonSelected:model.isSelected];
}

- (void)setButtonHidden:(BOOL)isHidden {
    _multiSelectedButton.hidden = isHidden;
}
- (void)setButtonSelected:(BOOL)isSelected {
    _multiSelectedButton.selected = isSelected;
}


@end
