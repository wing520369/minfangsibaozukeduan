//
//  WCRAvatarCell.h
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 头像
@class WCRUserModel;
@interface WCRAvatarCell : UITableViewCell
@property (nonatomic, strong) WCRUserModel *userModel;
+ (CGFloat)cellHeight;
@end
