//
//  WCRAppointmentLookRoomCell.h
//  Tenant
//
//  Created by eliot on 2017/6/2.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRAppointLookRoomModel;
/// 预约看房 cell
@interface WCRAppointmentLookRoomCell : UITableViewCell
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) WCRAppointLookRoomModel *model;
- (void)setButtonSelected:(BOOL)isSelected;
- (void)setButtonHidden:(BOOL)isHidden;
@end
