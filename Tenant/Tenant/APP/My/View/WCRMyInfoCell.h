//
//  WCRMyInfoCell.h
//  Landlord
//
//  Created by 吴传荣 on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WCRMyModel;
@interface WCRMyInfoCell : UITableViewCell
@property (nonatomic, strong) WCRMyModel *model;
@end
