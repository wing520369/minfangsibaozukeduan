//
//  WCRAvatarCell.m
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRAvatarCell.h"
#import "UIImageView+WebCache.h"
#import "WCRUserModel.h"

@interface WCRAvatarCell ()
@property (nonatomic, strong) UIImageView *avatarImageView;
@end

@implementation WCRAvatarCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
        grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
        [self.contentView addSubview:grayView];
        
        CGFloat x = kSpaceX;
        CGFloat y = grayView.bottom_wcr;
        CGFloat height = [WCRAvatarCell cellHeight] - grayView.bottom_wcr;
        CGFloat width = 100;
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [titleLabel labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:@"头像"];
        [self.contentView addSubview:titleLabel];
        
        y = grayView.bottom_wcr + 15 / 2.0;
        height = [WCRAvatarCell cellHeight] - grayView.bottom_wcr - 15;
        width = height;
        x = kMainWidth - width - kSpaceX;
        _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_avatarImageView setContentModeToScaleAspectFill];
        [self.contentView addSubview:_avatarImageView];
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 10 + 15 + 45;
}

- (void)setUserModel:(WCRUserModel *)userModel {
    [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:userModel.avatar] placeholderImage:[UIImage imageFileNamed:kPlaceholderImageName]];
}

@end
