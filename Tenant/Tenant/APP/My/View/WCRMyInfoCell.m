//
//  WCRMyInfoCell.m
//  Landlord
//
//  Created by 吴传荣 on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMyInfoCell.h"
#import "WCRMyModel.h"
#import "NSMutableAttributedString+WCRCustom.h"

@interface WCRMyInfoCell ()
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIView *grayView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIImageView *arrowImageView;
@end

@implementation WCRMyInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _grayView = [[UIView alloc] initWithFrame:CGRectZero];
        _grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
        [self.contentView addSubview:_grayView];
        
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_iconImageView setContentModeToScaleAspectFill];
        [self.contentView addSubview:_iconImageView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_titleLabel labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_titleLabel];
        
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_contentLabel labelWithTextColor:[UIColor redColor] Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
        _contentLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_contentLabel];
        
        _lineView = [UIView lineView];
        _lineView.frame = CGRectZero;
        [self.contentView addSubview:_lineView];
        
        _arrowImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
        [self.contentView addSubview:_arrowImageView];
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = kMainWidth;
    CGFloat height = _model.startY;
    _grayView.frame = CGRectMake(x, y, width, height);
    
    width = 20;
    height = 20;
    x = kSpaceX;
    y = _grayView.bottom_wcr + (_model.cellHeight - _model.startY - height) / 2.0;
    _iconImageView.frame = CGRectMake(x, y, width, height);
    
    x = _iconImageView.right_wcr + _iconImageView.x_wcr;
    y = _grayView.bottom_wcr;
    height = _model.cellHeight - _model.startY;
    width = 100;
    _titleLabel.frame = CGRectMake(x, y, width, height);
    
    x = _titleLabel.right_wcr;
    width = kMainWidth - x - kSpaceX - 20;
    _contentLabel.frame = CGRectMake(x, y, width, height);
    
    width = 13;
    height = 13;
    y = _model.startY + (_model.cellHeight - _model.startY - height) / 2.0;
    x = kMainWidth - width - kSpaceX;
    _arrowImageView.frame = CGRectMake(x, y, width, height);
    
    x = kSpaceX;
    width = kMainWidth - x * 2;
    y = _titleLabel.bottom_wcr - 1;
    _lineView.frame = CGRectMake(x, y, width, 1);
}

- (void)setModel:(WCRMyModel *)model {
    _model = model;
    _iconImageView.image = [UIImage imageFileNamed:model.imageName];
    _titleLabel.text = model.title;
    _lineView.hidden = !model.haveLine;
    if (_model.content.length) {
        if (model.type == MyInfoTypeMerchant) {
            NSString *content = [NSString stringWithFormat:@"%@积分",model.content];
            NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:content];
            [mutableString setColor:[UIColor blackColor] range:[content rangeOfString:@"积分"]];
            [_contentLabel setAttributedText:mutableString];
        }
        else {
            _contentLabel.text = model.content;
        }
    }
}

@end
