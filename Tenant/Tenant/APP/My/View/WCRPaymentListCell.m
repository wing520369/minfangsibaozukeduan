//
//  WCRPaymentListCell.m
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRPaymentListCell.h"
#import "myUILabel.h"
#import "WCRPaymentListModel.h"
#import "NSString+WCRCustom.h"

@interface WCRPaymentListCell ()
@property (nonatomic, strong) myUILabel *nameLabel;
@property (nonatomic, strong) myUILabel *timeLabel;
@property (nonatomic, strong) myUILabel *moneyLabel;
@end

@implementation WCRPaymentListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIFont *font = [UIFont CustomFontWithSize:13.0];
        CGFloat cellHeight = [WCRPaymentListCell cellHeight];
        
        CGFloat x = kSpaceX;
        CGFloat y = 0;
        CGFloat width = 100;
        CGFloat height = 28;
        _nameLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_nameLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_nameLabel];
        
        y = _nameLabel.bottom_wcr;
        height = cellHeight - height;
        _timeLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_timeLabel labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_timeLabel];
        _timeLabel.verticalAlignment = VerticalAlignmentTop;
        
        x = _nameLabel.right_wcr + 5;
        width = kMainWidth - x - kSpaceX;
        height = cellHeight;
        y = 0;
        _moneyLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_moneyLabel labelWithTextColor:[UIColor colorWithHexString:kMainColor] Font:font BackgroundColor:nil Text:nil];
        _moneyLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_moneyLabel];
        
        _lineView = [UIView lineView];
        _lineView.frame = CGRectMake(kSpaceX, cellHeight - 1, kMainWidth - kSpaceX * 2, 1);
        [self.contentView addSubview:_lineView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setModel:(WCRPaymentListModel *)model {
    _nameLabel.text = model.describe;
    _timeLabel.text = [model.createDate formatAllDateString];
    NSString *flag = @"-";
    if ([model.type integerValue] == 1) {
        // 入账
        flag = @"+";
    }
    _moneyLabel.text = [NSString stringWithFormat:@"%@%.2f",flag,model.amount];
}

+ (CGFloat)cellHeight {
    return 50;
}

- (myUILabel *)getLabel {
    myUILabel *label = [[myUILabel alloc] init];
    [label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:13.0] BackgroundColor:nil Text:nil];
    return label;
}

@end
