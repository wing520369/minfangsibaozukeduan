//
//  WCRModifyInfoCell.h
//  Landlord
//
//  Created by eliot on 2017/5/11.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRRentDepositModel;
/// 报修信息 model
@interface WCRInfoListCell : UITableViewCell
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic) WCRRentDepositModel *model;
+ (CGFloat)cellHeight;
@end
