//
//  WCRTabBarController.m
//  Landlord
//
//  Created by eliot on 2017/4/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRTabBarController.h"
#import "UINavigationController+WCRCustom.h"
#import "WCRIntegralMallVC.h"

@interface WCRTabBarController ()

@end

@implementation WCRTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self addChildVC];
}

- (void)addChildVC {
    
    NSArray *classNames = @[@"WCRHousingResourcesVC",@"WCRRentServiceVC",@"WCRIntegralMallVC",@"WCRMyVC"];
    NSArray *titles = @[@"房源",@"租房服务",@"积分商城",@"我的"];
    
    NSArray *norArray = @[@"Landlordhome_light",@"home_light",@"shop",@"icon_wode"];
    NSArray *selArray = @[@"Landlordhome_fill_light",@"home_fill_light",@"shop_fill",@"icon_wode_default"];
    
    UIColor *disFontColor = [UIColor colorWithRGB:192 G:192 B:192];
    UIColor *selFontColor = [UIColor colorWithHexString:kMainColor];
    UIFont *font = [UIFont CustomFontWithSize:12.0];
    
    NSMutableArray *navis = [NSMutableArray arrayWithCapacity:classNames.count];
    for (int i = 0; i < classNames.count; i++) {
        UIViewController *vc = [[NSClassFromString(classNames[i]) alloc]init];
        if ([classNames[i] isEqualToString:@"WCRIntegralMallVC"]) {
            ((WCRIntegralMallVC *)vc).type = @"0";
        }
        
        vc.tabBarItem.image = [[UIImage imageNamed:norArray[i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:selArray[i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        vc.tabBarItem.title = titles[i];
        [vc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:disFontColor,NSForegroundColorAttributeName,font,NSFontAttributeName,nil] forState:UIControlStateNormal];
        [vc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:selFontColor,NSForegroundColorAttributeName,nil] forState:UIControlStateSelected];
        vc.title = [titles objectAtIndex:i];
        
        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:vc];
        [navi setDefaultStyle];
        
        [navis addObject:navi];
    }
    self.viewControllers = navis;
}


@end
