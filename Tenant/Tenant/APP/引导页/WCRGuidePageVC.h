//
//  WCRGuidePageVC.h
//  GuidePageDemo
//
//  Created by 吴前途 on 16/4/10.
//  Copyright © 2016年 吴前途. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WCRGuidePageVC;
typedef void (^enterAppBlock) (__weak WCRGuidePageVC *weakGuideVC);

/** 引导页 */
@interface WCRGuidePageVC : UIViewController

/**
 *  初始化引导页
 *
 *  @param images           图片数组
 *  @param buttonTitle      最后一页进入APP的button的标题
 *  @param enterBlock       从引导页跳转
 *
 *  @return 引导页控制器
 */
- (instancetype)initWithImages:(NSArray *)images ButtonTitle:(NSString *)buttonTitle EnterAppBlock:(enterAppBlock)enterBlock;

@end
