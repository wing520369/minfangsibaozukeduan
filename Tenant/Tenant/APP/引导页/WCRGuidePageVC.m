//
//  WCRGuidePageVC.m
//  GuidePageDemo
//
//  Created by 吴前途 on 16/4/10.
//  Copyright © 2016年 吴前途. All rights reserved.
//

#import "WCRGuidePageVC.h"
#import "WCRPageControl.h"

@interface WCRGuidePageVC ()<UIScrollViewDelegate>
@property (nonatomic, strong) WCRPageControl *pageControl;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSString *buttonTitle;
@property (nonatomic, strong) enterAppBlock enterBlock;
@end

@implementation WCRGuidePageVC

- (instancetype)initWithImages:(NSArray *)images ButtonTitle:(NSString *)buttonTitle EnterAppBlock:(enterAppBlock)enterBlock{
    if (self = [super init]) {
        self.images = images;
        self.buttonTitle = buttonTitle;
        self.enterBlock = enterBlock;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    // 设置导航页
    [self createScrollView];
    
}

- (void)createScrollView {
    
    CGFloat mainViewWidth = self.view.frame.size.width;
    CGFloat mainViewHeight = self.view.frame.size.height;
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, mainViewWidth, mainViewHeight)];
    for (NSInteger i = 0; i < self.images.count; i++) {
        
        // 引导页的图片为png，如果为jpg需要修改
        NSString *imageFilePath = [[NSBundle mainBundle] pathForResource:self.images[i] ofType:@"png"];
        UIImage *image = [[UIImage alloc]initWithContentsOfFile:imageFilePath];
        UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
        
        // 设置frame属性
        CGRect frame = CGRectZero;
        frame.origin = CGPointMake(mainViewWidth * i, 0);
        frame.size = self.view.frame.size;
        imageView.frame = frame;
        
        // 将图片视图添加到scrollView中
        [scrollView addSubview:imageView];
    }
    
    // 设置scrollView的属性
    scrollView.contentSize = CGSizeMake(mainViewWidth * self.images.count, mainViewHeight);
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    scrollView.delegate = self;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    // 设置按钮点击最后一页进入应用
    UIButton *enterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    CGFloat width = kMainWidth;
    CGFloat height = kMainHeight;
    CGFloat x = mainViewWidth * (self.images.count - 1);// + (mainViewWidth - width) / 2.0;
    CGFloat y = 0;
    enterBtn.frame = CGRectMake(x, y, width, height);
    [enterBtn.layer setBorderWidth:1.0];
    [enterBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [enterBtn addTarget:self action:@selector(enterApp:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:enterBtn];
    
    // 分页控件
//    WCRPageControl *pageControl = [[WCRPageControl alloc]initWithFrame:CGRectMake(0, mainViewHeight - 50, mainViewWidth, 20)];
//    self.pageControl = pageControl;
//    pageControl.numberOfPages = self.images.count;
//    pageControl.currentPage = 0;
//    pageControl.userInteractionEnabled = NO;
//    [self.view addSubview:pageControl];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.pageControl) {
        CGFloat offsetX = scrollView.contentOffset.x;
        int page = offsetX / scrollView.frame.size.width;
        self.pageControl.currentPage = page;
    }
}


#pragma mark - 进入APP
-(void)enterApp:(UIButton*)button{
    
    __weak typeof(self) weakSelf = self;
    if (self.enterBlock) {
        self.enterBlock(weakSelf);
    }
    
}


@end
