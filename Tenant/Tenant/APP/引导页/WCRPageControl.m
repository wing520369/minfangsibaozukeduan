//
//  WCRPageControl.m
//  GuidePageDemo
//
//  Created by 吴前途 on 16/4/10.
//  Copyright © 2016年 吴前途. All rights reserved.
//

#import "WCRPageControl.h"

@implementation WCRPageControl

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}
// 重载setCurrentPage方法进行自定义pageControl
- (void)setCurrentPage:(NSInteger)currentPage {
    [super setCurrentPage:currentPage];
    [self updateDots];
}
- (void)updateDots {
    for (int i = 0; i < [self.subviews count]; i++) {
        UIView *dot = [self.subviews objectAtIndex:i];
        
        // 设置大小
        CGFloat width = 8;
        CGRect bounds = dot.bounds;
        bounds.size.width = width;
        bounds.size.height = width;
        dot.bounds = bounds;
        
        // 设置颜色和形状
        [dot.layer setBorderColor:[UIColor whiteColor].CGColor];
        [dot.layer setBorderWidth:1.0];
        [dot.layer setCornerRadius:width / 2.0];
        
        // 设置当前点的颜色
        if (i == self.currentPage) {
            dot.backgroundColor = [UIColor whiteColor];
        }
        else {
            dot.backgroundColor = [UIColor clearColor];
        }
       
    }
}

@end
