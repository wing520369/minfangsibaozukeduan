//
//  WCRCityCell.h
//  Tenant
//
//  Created by eliot on 2017/5/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRLetterSortModel;
@interface WCRCityCell : UITableViewCell
@property (nonatomic, strong) WCRLetterSortModel *model;
@property (nonatomic, strong) UIView *lineView;
@end
