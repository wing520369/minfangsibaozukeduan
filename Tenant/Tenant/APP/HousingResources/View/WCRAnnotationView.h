//
//  MyAnimatedAnnotationView.h
//  IphoneMapSdkDemo
//
//  Created by wzy on 14-11-27.
//  Copyright (c) 2014年 Baidu. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKMapComponent.h>
@class BMKAnnotationView;
@protocol WCRAnnotationViewDelegate <NSObject>

- (void)annotationViewClickAction:(BMKAnnotationView *)annotation;

@end

@interface WCRAnnotationView : BMKAnnotationView
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, weak) id<WCRAnnotationViewDelegate> delegate;
@end
