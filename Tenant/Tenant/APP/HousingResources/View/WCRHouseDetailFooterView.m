//
//  WCRHouseDetailFooterView.m
//  Tenant
//
//  Created by 吴传荣 on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRHouseDetailFooterView.h"
#import "WCRRoomDetailModel.h"
#import "NSString+WCRCustom.h"
#import "UIImageView+WebCache.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import "WCRRoomModel.h"
#import "WCRHouseModel.h"

@interface WCRHouseDetailFooterView ()<BMKMapViewDelegate>
@property (nonatomic, strong) BMKMapView *mapView;
@end

@implementation WCRHouseDetailFooterView

- (void)dealloc {
    _mapView.delegate = nil;
}

- (instancetype)initWithRoomDetailModel:(WCRRoomDetailModel *)roomDetailModel {
    if (self = [super init]) {
        [self createUIWithRoomDetailModel:roomDetailModel];
    }
    return self;
}

- (void)createUIWithRoomDetailModel:(WCRRoomDetailModel *)roomDetailModel {
    self.backgroundColor = [UIColor whiteColor];
    self.frame = CGRectMake(0, 0, kMainWidth, 0.1);
    
    // 地址
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    NSString *text = @"地址：";
    CGFloat x = kSpaceX;
    CGFloat y = 10;
    CGFloat height = font.lineHeight;
    CGFloat width = text.length * font.pointSize;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    [self addSubview:label];
    
    text = [NSString stringWithFormat:@"%@%@%@",roomDetailModel.house.cityName,roomDetailModel.house.districeName,roomDetailModel.house.streetAddress];
    x = label.right_wcr;
    width = kMainWidth - x - kSpaceX;
    height = [NSString calculateWithContent:text MaxWidth:width Font:font];
    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [addressLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    addressLabel.numberOfLines = 0;
    [self addSubview:addressLabel];
    
    // 地图
    y = addressLabel.bottom_wcr + 10;
    y = [self mapWithYpoint:y roomDetailModel:roomDetailModel];
    
    // 房产描述
    if (roomDetailModel.house.describe.length) {
        y = [self houseDetailWithYpoint:y roomDetailModel:roomDetailModel];
    }
    
    // 房东信息
    text = @"房东信息";
    x = kSpaceX;
    width = 100;
    height = 45;
    UILabel *landlordLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [landlordLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    [self addSubview:landlordLabel];
    
    y = landlordLabel.bottom_wcr;
    UIView *lineView = [UIView lineView];
    lineView.frame = CGRectMake(0, y, kMainWidth, 1);
    [self addSubview:lineView];
    
    CGFloat border = 15;
    y = lineView.bottom_wcr + border;
    // 头像
    width = 40;
    height = width;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [imageView setContentModeToScaleAspectFill];
    [self addSubview:imageView];
    [imageView sd_setImageWithURL:[NSURL URLWithString:roomDetailModel.hostAvatar] placeholderImage:[UIImage imageFileNamed:@"icon_geren"]];
    
    font = [UIFont CustomFontWithSize:12.0];
    // 昵称
    text = [NSString stringWithFormat:@"昵称：%@",roomDetailModel.hostName];
    x = imageView.right_wcr + border;
    height = imageView.height_wcr / 2.0;
    width = text.length * font.pointSize;
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [nameLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    [self addSubview:nameLabel];
    
    // 电话
    text = [NSString stringWithFormat:@"电话：%@",roomDetailModel.hostPhone];
    width = kMainWidth - x - kSpaceX;
    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [phoneLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    [self addSubview:phoneLabel];
    phoneLabel.bottom_wcr = imageView.bottom_wcr;
    
    // 评价星
    // 等级
    width = font.lineHeight - 4;
    height = width;
    x = nameLabel.right_wcr + 10;
    if ((width * 5 + kSpaceX) > (kMainWidth - x)) {
        x = kMainWidth - (width * 5 + kSpaceX);
        nameLabel.width_wcr = x - nameLabel.x_wcr;
    }
    NSInteger level = [roomDetailModel.hostComment integerValue];
    for (NSInteger i = 0; i < 5; i++) {
        UIButton *startButton = [[UIButton alloc] initWithFrame:CGRectMake(x, y, width, height)];
        startButton.centerY_wcr = nameLabel.centerY_wcr;
        startButton.selected = i < level ? YES : NO;
        [startButton buttonWithImageName:@"icon_stars_default" SelectedImageName:@"icon_stars_selected"];
        startButton.userInteractionEnabled = NO;
        [self addSubview:startButton];
        x = startButton.right_wcr;
    }
    
    y = imageView.bottom_wcr + border;
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, 20)];
    grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
    [self addSubview:grayView];
    
    self.height_wcr = grayView.bottom_wcr;
}

// 房间描述
- (CGFloat)houseDetailWithYpoint:(CGFloat)yPoint roomDetailModel:(WCRRoomDetailModel *)roomDetailModel {
    
    CGFloat y = yPoint;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, y, 100, 45)];
    [label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:@"房产描述"];
    [self addSubview:label];
    
    NSString *text = roomDetailModel.house.describe;
    UIFont *font = [UIFont CustomFontWithSize:13.0];
    CGFloat x = kSpaceX;
    CGFloat width = kMainWidth - x * 2;
    CGFloat height = [NSString calculateWithContent:text MaxWidth:width Font:font];
    y = label.bottom_wcr;
    UILabel *desribeLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [desribeLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    desribeLabel.numberOfLines = 0;
    [self addSubview:desribeLabel];
    
    y = desribeLabel.bottom_wcr + 20;
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, 10)];
    grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
    [self addSubview:grayView];
    
    return grayView.bottom_wcr;
}

// 地图
- (CGFloat)mapWithYpoint:(CGFloat)yPoint roomDetailModel:(WCRRoomDetailModel *)roomDetailModel {
    
    CGFloat height = (400 / 750.0) * kMainWidth;
    BMKMapView *mapView = [[BMKMapView alloc]initWithFrame:CGRectMake(0, yPoint, kMainWidth, height)];
    _mapView = mapView;
    mapView.userTrackingMode = 0;  // 设置定位状态
    mapView.showsUserLocation = YES;
    mapView.delegate = self;
    [self addSubview:mapView];
    
    BMKCoordinateRegion region;
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([roomDetailModel.house.lat doubleValue], [roomDetailModel.house.lng doubleValue]);
    region.center = coordinate;
    region.span.latitudeDelta = 0.01;
    region.span.longitudeDelta = 0.01;
    BMKCoordinateRegion adjustedRegion = [mapView regionThatFits:region];
    [mapView setRegion:adjustedRegion animated:YES];
    
    BMKPointAnnotation *annotation = [[BMKPointAnnotation alloc]init];
    annotation.coordinate = coordinate;
    [mapView addAnnotation:annotation];
    
    mapView.gesturesEnabled = YES;
    
    return mapView.bottom_wcr;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    UIView *view = [super hitTest:point withEvent:event];
    UITableView *tableView = (UITableView *)self.superview;
    tableView.scrollEnabled = YES;
    if (!view) {
        if (CGRectContainsPoint(_mapView.frame, point)) {
            tableView.scrollEnabled = NO;
        }
    }
    
    return view;
}

@end
