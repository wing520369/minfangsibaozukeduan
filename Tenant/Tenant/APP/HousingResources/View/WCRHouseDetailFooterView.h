//
//  WCRHouseDetailFooterView.h
//  Tenant
//
//  Created by 吴传荣 on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRRoomDetailModel;
/// 房源详情 脚视图 : 地址 地图 房产描述 房东信息
@interface WCRHouseDetailFooterView : UIView
- (instancetype)initWithRoomDetailModel:(WCRRoomDetailModel *)roomDetailModel;
@end
