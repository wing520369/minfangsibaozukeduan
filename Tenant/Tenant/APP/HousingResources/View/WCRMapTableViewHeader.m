//
//  WCRMapTableViewHeader.m
//  Tenant
//
//  Created by eliot on 2017/6/9.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMapTableViewHeader.h"
#import "WCRMapHousingSourcesModel.h"
#import "WCRHouseModel.h"
#import "NSMutableAttributedString+WCRCustom.h"

@interface WCRMapTableViewHeader ()
@property (nonatomic, strong) UILabel *label;
@end

@implementation WCRMapTableViewHeader

- (instancetype)init {
    if (self = [super init]) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    
    _label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, kDefaultHeight)];
    [_label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:15.0] BackgroundColor:nil Text:nil];
    [self addSubview:_label];
    
    self.frame = CGRectMake(0, 0, kMainWidth, _label.height_wcr);
    [self addLineViewPositionType:kBottom_Type];
}

- (void)setModel:(WCRMapHousingSourcesModel *)model {
    NSString *count = [NSString stringWithFormat:@"%@套",model.count];
    WCRHouseModel *houseModel = model.house;
    NSString *houseName = houseModel.houseName;
    NSString *text = [NSString stringWithFormat:@"%@  %@",houseName,count];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text];
    NSRange range = NSMakeRange(text.length - count.length, count.length);
    [string setColor:[UIColor colorWithHexString:kCustomGrayColor] range:range];
    [string setFont:[UIFont CustomFontWithSize:11.0] range:range];
    [_label setAttributedText:string];
}

@end
