//
//  MyAnimatedAnnotationView.m
//  IphoneMapSdkDemo
//
//  Created by wzy on 14-11-27.
//  Copyright (c) 2014年 Baidu. All rights reserved.
//

#import "WCRAnnotationView.h"

@implementation WCRAnnotationView

- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIFont *font = [UIFont CustomFontWithSize:15.0];
        CGFloat height = font.lineHeight + 20;
        
        [self setBounds:CGRectMake(0, 0, 50, height)];
        [self setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
        
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.frame = CGRectMake(5, 0, self.width_wcr - 10, self.height_wcr);
        [_button buttonWithTitleColor:nil Background:nil Font:font Title:nil];
        [self addSubview:_button];
        
        [_button addTarget:self action:@selector(clickAnnotationAction)];
    }
    return self;
}

- (void)clickAnnotationAction {
    if (_delegate && [_delegate respondsToSelector:@selector(annotationViewClickAction:)]) {
        [_delegate annotationViewClickAction:self];
    }
}

@end
