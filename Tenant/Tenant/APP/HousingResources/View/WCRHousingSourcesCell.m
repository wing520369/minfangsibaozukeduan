//
//  WCRHousingSourcesCell.m
//  Tenant
//
//  Created by eliot on 2017/5/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRHousingSourcesCell.h"
#import "WCRRoomModel.h"
#import "UIImageView+WebCache.h"
#import "WCRLeftImageButton.h"
#import "NSMutableAttributedString+WCRCustom.h"
#import "NSDate+WCRCustom.h"
#import "myUILabel.h"

@interface WCRHousingSourcesCell ()
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) myUILabel *titleLabel;
@property (nonatomic, strong) UILabel *houseInfoLabel;
@property (nonatomic, strong) WCRLeftImageButton *addressButton;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIButton *multiSelectedButton;
@end

@implementation WCRHousingSourcesCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat height = 80;
        CGFloat width = height * (260 / 200.0);
        CGFloat y = 15;
        CGFloat x = kSpaceX;
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_iconImageView setContentModeToScaleAspectFill];
        [self.contentView addSubview:_iconImageView];
        
        UIFont *font = [UIFont CustomFontWithSize:14.0];
        x = _iconImageView.right_wcr + 10;
        width = kMainWidth - x - kSpaceX;
        height = font.lineHeight * 2 + 5;
        _titleLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_titleLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        _titleLabel.verticalAlignment = VerticalAlignmentTop;
        _titleLabel.numberOfLines = 2;
        [self.contentView addSubview:_titleLabel];
        
        font = [UIFont CustomFontWithSize:12.0];
        y = _titleLabel.bottom_wcr;
        height = font.lineHeight + 4;
        _houseInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_houseInfoLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_houseInfoLabel];
        
        font = [UIFont CustomFontWithSize:10.0];
        height = font.lineHeight + 3;
        y = _iconImageView.bottom_wcr - height;
        _addressButton = [[WCRLeftImageButton alloc] initWithFrame:CGRectMake(x, y, width, height) ImageTitleBorder:0.01 LeftBorder:0.01 ImageSize:CGSizeMake(13, 13)];
        [_addressButton buttonWithTitleColor:[UIColor colorWithHexString:kCustomGrayColor] Background:nil Font:font Title:nil];
        [_addressButton setImage:[UIImage imageFileNamed:@"icon_address"] forState:0];
        _addressButton.userInteractionEnabled = NO;
        [self.contentView addSubview:_addressButton];
        
        y = _iconImageView.bottom_wcr;
        height = 25;
        x = kSpaceX;
        width = kMainWidth - x * 2;
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_timeLabel labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_timeLabel];
        _lineView = [_timeLabel addLineViewPositionType:kBottom_Type];
        
        CGFloat cellHeight = [WCRHousingSourcesCell cellHeight];
        UIView *selectedBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, cellHeight)];
        selectedBgView.backgroundColor = [UIColor clearColor];
        self.backgroundView = selectedBgView;
        
        _multiSelectedButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, selectedBgView.height_wcr)];
        [_multiSelectedButton buttonWithImageName:@"icon_a" SelectedImageName:@"icon_b"];
        _multiSelectedButton.backgroundColor = [UIColor clearColor];
        [selectedBgView addSubview:_multiSelectedButton];
        _multiSelectedButton.userInteractionEnabled = NO;
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 15 + 80 + 25;
}

- (void)setModel:(WCRRoomModel *)model {
    
    _model = model;
    
    WCRRoomImagesModel *imageModel = [model.roomImgs firstObject];
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:imageModel.img] placeholderImage:[UIImage imageFileNamed:@"1206icon_tupian"]];
    
    _titleLabel.text = model.roomName;
    
    NSString *rent = [NSString stringWithFormat:@"%@元/月",model.rent];
    NSString *houseInfo = [NSString stringWithFormat:@"%@室%@厅%@卫  %@㎡    %@",model.room,model.hall,model.toilet,model.roomArea,rent];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:houseInfo];
    [string setColor:[UIColor redColor] range:NSMakeRange(houseInfo.length - rent.length, rent.length)];
    _houseInfoLabel.attributedText = string;
    
    NSString *address = [NSString stringWithFormat:@"%@%@%@%@",model.provinceName,model.cityName,model.districeName,model.streetAddress];
    [_addressButton setTitle:address forState:0];
    
    NSTimeInterval timeSp = [NSDate dateGetTimeSpWithDateString:model.createDate Format:nil];
    NSString *timeString = [NSDate dateChangeTimeFormatWithTimeStamp:timeSp showDetail:NO];
    _timeLabel.text = [NSString stringWithFormat:@"%@发布",timeString];
}

- (void)setButtonHidden:(BOOL)isHidden {
    _multiSelectedButton.hidden = isHidden;
}

- (void)setButtonSelected:(BOOL)isSelected {
    _multiSelectedButton.selected = isSelected;
}

@end
