//
//  WCRHousingSourcesCell.h
//  Tenant
//
//  Created by eliot on 2017/5/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRRoomModel;
/// 房源 cell
@interface WCRHousingSourcesCell : UITableViewCell
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) WCRRoomModel *model;
- (void)setButtonSelected:(BOOL)isSelected;
- (void)setButtonHidden:(BOOL)isHidden;
+ (CGFloat)cellHeight;
@end
