//
//  WCRHouseDetailHeaderView.m
//  Tenant
//
//  Created by 吴传荣 on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRHouseDetailHeaderView.h"
#import "WCRRoomModel.h"
#import "SDCycleScrollView.h"
#import "NSString+WCRCustom.h"
#import "NSDate+WCRCustom.h"
#import "NSMutableAttributedString+WCRCustom.h"
#import "WCRLeftImageButton.h"
#import "WCRPhotoBrowser.h"

@interface WCRHouseDetailHeaderView ()<SDCycleScrollViewDelegate>
@property (nonatomic, strong) NSMutableArray *imageUrls;
@end

@implementation WCRHouseDetailHeaderView
- (instancetype)initWithRoomModel:(WCRRoomModel *)roomModel {
    if (self = [super init]) {
        self.imageUrls = [NSMutableArray arrayWithCapacity:roomModel.roomImgs.count];
        [self createUIWithRoomModel:roomModel];
    }
    return self;
}

- (void)createUIWithRoomModel:(WCRRoomModel *)roomModel {
    self.backgroundColor = [UIColor whiteColor];
    self.frame = CGRectMake(0, 0, kMainWidth, 0.1);
    
    // 轮播图
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = kMainWidth;
    CGFloat height = width * (380 / 750.0);
    SDCycleScrollView *sdScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(x, y, width, height) delegate:self placeholderImage:nil];
    sdScrollView.backgroundColor = [UIColor clearColor];
    sdScrollView.currentPageDotColor = [UIColor colorWithHexString:kMainColor];
    sdScrollView.pageDotColor = [UIColor whiteColor];
    sdScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:sdScrollView];
    
    __block NSMutableArray *imageUrls = [NSMutableArray arrayWithCapacity:roomModel.roomImgs.count];
    [roomModel.roomImgs enumerateObjectsUsingBlock:^(WCRRoomImagesModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [imageUrls addObject:obj.img];
    }];
    [self.imageUrls addObjectsFromArray:imageUrls];
    sdScrollView.imageURLStringsGroup = [imageUrls copy];
    
    // 房间名
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    NSString *text = roomModel.roomName;
    x = kSpaceX;
    y = sdScrollView.bottom_wcr + 10;
    width = kMainWidth - x * 2;
    height = [NSString calculateWithContent:text MaxWidth:width Font:font];
    UILabel *roomNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [roomNameLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    roomNameLabel.numberOfLines = 0;
    [self addSubview:roomNameLabel];
    
    // 发布时间
    font = [UIFont CustomFontWithSize:10.0];
    NSTimeInterval timeSp = [NSDate dateGetTimeSpWithDateString:roomModel.createDate Format:nil];
    NSString *timeString = [NSDate dateChangeTimeFormatWithTimeStamp:timeSp showDetail:NO];
    text = [NSString stringWithFormat:@"%@发布",timeString];
    y = roomNameLabel.bottom_wcr;
    height = 32;
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [timeLabel labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:text];
    [self addSubview:timeLabel];
    
    y = [self addLineViewWithYpoint:timeLabel.bottom_wcr];
    
    font = [UIFont CustomFontWithSize:14.0];
    NSArray *titles = @[
                        [NSString stringWithFormat:@"租金\n%@元/月",roomModel.rent],
                        [NSString stringWithFormat:@"户型\n%@室%@厅",roomModel.room,roomModel.hall],
                        [NSString stringWithFormat:@"面积\n%@平米",roomModel.roomArea]
                        ];
    x = 0;
    width = kMainWidth / titles.count;
    height = 60;
    for (NSInteger i = 0; i < titles.count; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        label.numberOfLines = 2;
        label.textAlignment = NSTextAlignmentCenter;
        text = [titles objectAtIndex:i];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text];
        [string setColor:[UIColor colorWithHexString:kMainColor] range:NSMakeRange(2, text.length - 2)];
        [label setAttributedText:string];
        [self addSubview:label];
        x = label.right_wcr;
        
        UIView *lineView = [UIView lineView];
        lineView.frame = CGRectMake(label.width_wcr - 1, 0, 1, label.height_wcr);
        [label addSubview:lineView];
        lineView.hidden = titles.count - 1 == i ? YES : NO;
    }
    
    y += height;
    y = [self addLineViewWithYpoint:y];
    
    // 房号
    text = [NSString stringWithFormat:@"房号：%@",roomModel.roomNo];
    x = kSpaceX;
    width = kMainWidth - x - 85;
    height = 40;
    UILabel *roomNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [roomNoLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    [self addSubview:roomNoLabel];
    
    // 更多房号
    text = @"更多房号》";
    width = 70;
    x = kMainWidth - width - kSpaceX;
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(x, y, width, height);
    [moreButton buttonWithTitleColor:[UIColor colorWithHexString:kMainColor] Background:nil Font:font Title:text];
    moreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [moreButton setImagePosition:1 spacing:5];
    [moreButton addTarget:self action:@selector(moreRoomAction)];
    [self addSubview:moreButton];
    
    y = [self addLineViewWithYpoint:roomNoLabel.bottom_wcr];
    
    // 房屋信息
    y = [self houseInformationWithYpoint:y RoomModel:roomModel];
    
    // 配套设施
    y = [self supportingFacilitiesWithYpoint:y RoomModel:roomModel];
    
    // 房间描述
    if (roomModel.describe.length) {
        y = [self roomDetailWithYpoint:y RoomModel:roomModel];
    }
    
    self.height_wcr = y;
}

// 房屋信息
- (CGFloat)houseInformationWithYpoint:(CGFloat)yPoint RoomModel:(WCRRoomModel *)roomModel {
    
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, yPoint, kMainWidth, 10)];
    grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
    [self addSubview:grayView];
    
    CGFloat x = kSpaceX;
    CGFloat y = grayView.bottom_wcr;
    CGFloat width = 100;
    CGFloat height = 45;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:16.0] BackgroundColor:nil Text:@"房屋信息"];
    [self addSubview:label];
    
    UIView *lineView = [UIView lineView];
    lineView.frame = CGRectMake(0, label.bottom_wcr, kMainWidth, 1);
    [self addSubview:lineView];
    
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    UIFont *font12 = [UIFont CustomFontWithSize:12.0];
    UIColor *grayCol = [UIColor colorWithHexString:kCustomGrayColor];
    NSArray *titles = @[@"租金押付",@"房型",@"楼层",@"面积",@"朝向"];
    // @"付1押2" @"1室2厅" @"2/20楼" @"42.23m²"
    NSArray *contents = @[
                          [NSString stringWithFormat:@"付%@押%@",roomModel.pay,roomModel.pledge],
                          [NSString stringWithFormat:@"%@室%@厅%@卫",roomModel.room,roomModel.hall,roomModel.toilet],
                          [NSString stringWithFormat:@"%@/%@楼",roomModel.tier,roomModel.totalTier],
                          [NSString stringWithFormat:@"%@m²",roomModel.roomArea],
                          roomModel.orientationName];
    x = kSpaceX;
    width = (kMainWidth - kSpaceX * 2) / 2.0 - 5;
    CGFloat titleW = font12.pointSize * 5;
    CGFloat contentW = width - titleW;
    height = font.lineHeight + 4;
    CGFloat border = 15;
    y = lineView.bottom_wcr + border;
    for (NSInteger i = 0; i < titles.count; i++) {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, titleW, height)];
        NSString *title = [titles objectAtIndex:i];
        [titleLabel labelWithTextColor:grayCol Font:font12 BackgroundColor:nil Text:title];
        [self addSubview:titleLabel];
        
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.right_wcr, y, contentW, height)];
        NSString *content = [contents objectAtIndex:i];
        [contentLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:content];
        [self addSubview:contentLabel];
        
        x = contentLabel.right_wcr + 5;
        if ((i + 1) % 2 == 0) {
            x = kSpaceX;
            y = contentLabel.bottom_wcr + border;
        }
        if (i == titles.count - 1) {
            y = contentLabel.bottom_wcr + border;
        }
    }
    
    UIView *lineView1 = [UIView lineView];
    lineView1.frame = CGRectMake(0, y, kMainWidth, 1);
    [self addSubview:lineView1];
    
    return lineView1.bottom_wcr;
}

// 配套设施
- (CGFloat)supportingFacilitiesWithYpoint:(CGFloat)yPoint RoomModel:(WCRRoomModel *)roomModel {

    CGFloat x = kSpaceX;
    CGFloat y = yPoint;
    CGFloat width = 100;
    CGFloat height = 45;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:16.0] BackgroundColor:nil Text:@"配套设施"];
    [self addSubview:label];
    
    UIFont *font = [UIFont CustomFontWithSize:12.0];
    NSString *json = roomModel.facility;
    NSArray *jsonArray = [json jsonObject];
    NSDictionary *dict = [jsonArray firstObject];
    NSArray *titles = [dict allKeys];
    width = (kMainWidth - kSpaceX * 2) / 4.0;
    x = kSpaceX;
    y = label.bottom_wcr;
    height = 35;
    CGFloat imageWH = font.lineHeight + 2;
    for (NSInteger i = 0; i < titles.count; i++) {
        WCRLeftImageButton *button = [[WCRLeftImageButton alloc] initWithFrame:CGRectMake(x, y, width, height) ImageTitleBorder:3 LeftBorder:5 ImageSize:CGSizeMake(imageWH, imageWH)];
        NSString *title = [titles objectAtIndex:i];
        [button buttonWithTitleColor:nil Background:nil Font:font Title:title];
        [button buttonWithImageName:@"disabled" SelectedImageName:@"default"];
        [self addSubview:button];
        
        x = button.right_wcr;
        if ((i + 1) % 4 == 0) {
            x = kSpaceX;
            y = button.bottom_wcr;
        }
        if (i == titles.count - 1) {
            y = button.bottom_wcr;
        }
        
        button.userInteractionEnabled = NO;
        if ([[dict objectForKey:title] isEqualToString:@"0"]) {
            button.selected = NO;
        }
        else {
            button.selected = YES;
        }
    }
    
    y += 10;
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, 10)];
    grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
    [self addSubview:grayView];
    
    return grayView.bottom_wcr;
}

// 房间描述
- (CGFloat)roomDetailWithYpoint:(CGFloat)yPoint RoomModel:(WCRRoomModel *)roomModel {

    CGFloat y = yPoint;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, y, 100, 45)];
    [label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:@"房屋描述"];
    [self addSubview:label];
    
    NSString *text = roomModel.describe;
    UIFont *font = [UIFont CustomFontWithSize:13.0];
    CGFloat x = kSpaceX;
    CGFloat width = kMainWidth - x * 2;
    CGFloat height = [NSString calculateWithContent:text MaxWidth:width Font:font];
    y = label.bottom_wcr;
    UILabel *desribeLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [desribeLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    desribeLabel.numberOfLines = 0;
    [self addSubview:desribeLabel];
    
    y = desribeLabel.bottom_wcr + 20;
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, 10)];
    grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
    [self addSubview:grayView];
    
    return grayView.bottom_wcr;
}

- (CGFloat)addLineViewWithYpoint:(CGFloat)yPoint {
    UIView *lineView = [UIView lineView];
    lineView.frame = CGRectMake(0, yPoint, kMainWidth, 1);
    [self addSubview:lineView];
    return lineView.bottom_wcr;
}

#pragma mark - 点击轮播图
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
//    if (index >= self.imageUrls.count) {
//        return;
//    }
//    [WCRPhotoBrowser showWithPhotoUrlArray:self.imageUrls currentIndex:index];
}

#pragma mark - 更多房号
- (void)moreRoomAction {
    if (_delegate && [_delegate respondsToSelector:@selector(houseDetailHeaderViewSkipMoreRoomVC)]) {
        [_delegate houseDetailHeaderViewSkipMoreRoomVC];
    }
}

@end
