//
//  WCRHousingSourcesSectionView.m
//  Tenant
//
//  Created by eliot on 2017/5/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRHousingSourcesSectionView.h"
#import "SDCycleScrollView.h"

@interface WCRHousingSourcesSectionView ()<SDCycleScrollViewDelegate>

@end

@implementation WCRHousingSourcesSectionView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
        grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
        [self.contentView addSubview:grayView];
        
        CGFloat height = kMainWidth * (140 / 750.0);
        _sdScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, grayView.bottom_wcr, kMainWidth, height) delegate:self placeholderImage:nil];
        _sdScrollView.backgroundColor = [UIColor clearColor];
        _sdScrollView.currentPageDotColor = [UIColor colorWithHexString:kMainColor];
        _sdScrollView.pageDotColor = [UIColor whiteColor];
        _sdScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_sdScrollView];
        
        height = 50;
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, _sdScrollView.bottom_wcr, kMainWidth, height)];
        [self.contentView addSubview:_bgView];
        
        UIView *lineView = [UIView lineView];
        CGFloat x = 50;
        lineView.frame = CGRectMake(x, 0, kMainWidth - x * 2, 1);
        [_bgView addSubview:lineView];
        
        UIFont *font = [UIFont CustomFontWithSize:14.0];
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((kMainWidth - 80) / 2.0, 0, 80, _bgView.height_wcr)];
        [_titleLabel labelWithTextColor:nil Font:font BackgroundColor:[UIColor whiteColor] Text:nil];
        [_bgView addSubview:_titleLabel];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        lineView.centerY_wcr = _titleLabel.centerY_wcr;
        
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _moreButton.frame = CGRectMake(kMainWidth - 50 - kSpaceX, 0, 50, 20);
        _moreButton.centerY_wcr = _titleLabel.centerY_wcr;
        [_moreButton setEnlargeEdgeWithTop:10 Right:kSpaceX Bottom:30 Left:30];
        [_moreButton buttonWithTitleColor:[UIColor colorWithHexString:kCustomGrayColor] Background:nil Font:[UIFont CustomFontWithSize:11.0] Title:@"更多"];
        _moreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_bgView addSubview:_moreButton];
        
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

+ (CGFloat)sectionHeightWithSection:(NSInteger)section {
    if (section == 0) {
        return 10 + 50;
    }
    return 10 + 50 + kMainWidth * (140 / 750.0);
}

#pragma mark - 点击轮播图
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
    
}

@end
