//
//  WCRHousingSourcesSectionView.h
//  Tenant
//
//  Created by eliot on 2017/5/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SDCycleScrollView;
/// 头视图
@interface WCRHousingSourcesSectionView : UITableViewHeaderFooterView
@property (nonatomic, strong) SDCycleScrollView *sdScrollView;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *moreButton;
+ (CGFloat)sectionHeightWithSection:(NSInteger)section;
@end
