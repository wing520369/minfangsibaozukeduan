//
//  WCRHouseDetailHeaderView.h
//  Tenant
//
//  Created by 吴传荣 on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRRoomModel;

@protocol WCRHouseDetailHeaderViewDelegate <NSObject>

- (void)houseDetailHeaderViewSkipMoreRoomVC;

@end

/// 房源详情 头视图 :房间图片 房间名 租金户型面积 房屋信息 描述等
@interface WCRHouseDetailHeaderView : UIView
@property (nonatomic, weak) id<WCRHouseDetailHeaderViewDelegate> delegate;
- (instancetype)initWithRoomModel:(WCRRoomModel *)roomModel;
@end
