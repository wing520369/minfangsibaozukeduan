//
//  WCRMoreRoomCell.h
//  Tenant
//
//  Created by eliot on 2017/5/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRMoreRoomModel;
@interface WCRMoreRoomCell : UICollectionViewCell
@property (nonatomic, strong) WCRMoreRoomModel *model;
+ (CGFloat)cellHeight;
@end
