//
//  WCRMoreRoomCell.m
//  Tenant
//
//  Created by eliot on 2017/5/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMoreRoomCell.h"
#import "WCRMoreRoomModel.h"

@interface WCRMoreRoomCell ()
@property (nonatomic, strong) UILabel *nameLabel;
@end

@implementation WCRMoreRoomCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIFont *font = [UIFont CustomFontWithSize:12.0];
        CGFloat width = 55;
        CGFloat x = (frame.size.width - width) / 2.0;
        CGFloat height = font.lineHeight + 8;
        CGFloat y = (frame.size.height - height) / 2.0;
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_nameLabel labelWithTextColor:[UIColor colorWithHexString:kCustomGrayColor] Font:font BackgroundColor:nil Text:nil];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [_nameLabel.layer setBorderWidth:1.0];
        [_nameLabel.layer setCornerRadius:3.0];
        [self.contentView addSubview:_nameLabel];
        
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

+ (CGFloat)cellHeight {
    UIFont *font = [UIFont CustomFontWithSize:13.0];
    return font.lineHeight + 30;
}

- (void)setModel:(WCRMoreRoomModel *)model {
    _model = model;
    _nameLabel.text = model.name;
    
    NSString *colorString = kCustomGrayColor;
    if (model.isSelected) {
        colorString = kMainColor;
    }
    _nameLabel.textColor = [UIColor colorWithHexString:colorString];
    [_nameLabel.layer setBorderColor:_nameLabel.textColor.CGColor];
}

@end
