//
//  WCRCityCell.m
//  Tenant
//
//  Created by eliot on 2017/5/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRCityCell.h"
#import "WCRLetterSortModel.h"

@interface WCRCityCell ()
@property (nonatomic, strong) UILabel *label;
@end

@implementation WCRCityCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, kDefaultHeight)];
        [_label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_label];
        
        _lineView = [UIView lineView];
        _lineView.frame = CGRectMake(0, _label.bottom_wcr - 1, kMainWidth, 1);
        [self.contentView addSubview:_lineView];
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setModel:(WCRLetterSortModel *)model {
    _label.text = model.name;
}

@end
