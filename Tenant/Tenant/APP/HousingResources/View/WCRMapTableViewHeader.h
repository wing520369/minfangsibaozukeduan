//
//  WCRMapTableViewHeader.h
//  Tenant
//
//  Created by eliot on 2017/6/9.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRMapHousingSourcesModel;
@interface WCRMapTableViewHeader : UIView
@property (nonatomic, strong) WCRMapHousingSourcesModel *model;
@end
