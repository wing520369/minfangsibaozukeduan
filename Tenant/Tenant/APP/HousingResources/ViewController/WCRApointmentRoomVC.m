//
//  WCRApointmentRoomVC.m
//  Tenant
//
//  Created by eliot on 2017/5/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRApointmentRoomVC.h"
#import "WCRRoomModel.h"
#import "TPKeyboardAvoidingTableView.h"
#import "myUILabel.h"
#import "UIImageView+WebCache.h"
#import "UITextField+WCRCustom.h"
#import "WCRDatePickerView.h"
#import "NPTip.h"
#import "NPGeneralApi.h"
#import "RegExpValidate.h"

@interface WCRApointmentRoomVC ()<UITextFieldDelegate>
@property (nonatomic, strong) UITextField *contactTF;
@property (nonatomic, strong) UITextField *phoneTF;
@property (nonatomic, strong) UITextField *timeTF;
@end

@implementation WCRApointmentRoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"预约看房"];
    [self createUI];
}

- (void)createUI {
    TPKeyboardAvoidingTableView *tableView = [[TPKeyboardAvoidingTableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64 - kDefaultHeight) style:UITableViewStylePlain];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    
    [self addTableViewHeader:tableView];
    [self addTableViewFooter:tableView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, kMainHeight - 64 - kDefaultHeight, kMainWidth, kDefaultHeight);
    [button buttonWithTitleColor:[UIColor whiteColor] Background:[UIColor colorWithHexString:kMainColor] Font:[UIFont CustomFontWithSize:14.0] Title:@"提交"];
    [button addTarget:self action:@selector(buttonAction)];
    [self.view addSubview:button];
}
#pragma mark - 提交
- (void)buttonAction {
    if (!_contactTF.text.length) {
        [NPTip showTip:_contactTF.placeholder];
        return;
    }
    if (!_phoneTF.text.length) {
        [NPTip showTip:_phoneTF.placeholder];
        return;
    }
    if (![RegExpValidate validateMobile:_phoneTF.text]) {
        [NPTip showTip:@"手机号码输入有误"];
        return;
    }
    if (!_timeTF.text.length) {
        [NPTip showTip:_timeTF.placeholder];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"houseId":_roomModel.houseId,@"roomId":_roomModel.ID,@"name":_contactTF.text,@"phone":_phoneTF.text,@"time":_timeTF.text};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/roomApiController/addOrderTable" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            [NPTip showTip:@"预约看房提交成功"];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)addTableViewHeader:(UITableView *)tableView {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 0.1)];
    headerView.backgroundColor = [UIColor whiteColor];
    // 图片
    CGFloat x = kSpaceX;
    CGFloat y = 15;
    CGFloat height = 80;
    CGFloat width = height * (260 / 200.0);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [imageView setContentModeToScaleAspectFill];
    [headerView addSubview:imageView];
    NSString *url = [self.roomModel.roomImgs firstObject].img;
    [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageFileNamed:@"1206icon_tupian"]];
    
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    NSString *text = [NSString stringWithFormat:@"房产：%@",self.roomModel.houseName];
    x = imageView.right_wcr + 10;
    width = kMainWidth - x - kSpaceX;
    height = 30;
    // 房产
    myUILabel *houseLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [houseLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    houseLabel.verticalAlignment = VerticalAlignmentTop;
    [headerView addSubview:houseLabel];
    
    // 房号
    text = [NSString stringWithFormat:@"房号：%@",self.roomModel.roomNo];
    y = houseLabel.bottom_wcr;
    myUILabel *roomLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [roomLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:text];
    roomLabel.verticalAlignment = VerticalAlignmentTop;
    [headerView addSubview:roomLabel];
    
    // 租金
    text = [NSString stringWithFormat:@"租金：%@",self.roomModel.rent];
    y = roomLabel.bottom_wcr;
    myUILabel *rentLabel = [[myUILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [rentLabel labelWithTextColor:[UIColor redColor] Font:font BackgroundColor:nil Text:text];
    rentLabel.verticalAlignment = VerticalAlignmentBottom;
    [headerView addSubview:rentLabel];
    rentLabel.bottom_wcr = imageView.bottom_wcr;
    
    headerView.height_wcr = imageView.bottom_wcr + imageView.y_wcr;
    tableView.tableHeaderView = headerView;
}

- (void)addTableViewFooter:(UITableView *)tableView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 0.1)];
    footerView.backgroundColor = [UIColor whiteColor];
    
    CGFloat height = 45;
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, height)];
    grayView.backgroundColor = self.view.backgroundColor;
    [footerView addSubview:grayView];
    
    CGFloat y = 10;
    height -= y;
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, y, kMainWidth - kSpaceX * 2, height)];
    [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:@"完善看房信息"];
    [footerView addSubview:label];
    
    y = label.bottom_wcr;
    _contactTF = [self getTextFieldWithYpoint:y Font:font Placeholder:@"请输入联系人" LeftTitle:@"联系人" SuperView:footerView];
    [_contactTF addLineViewPositionType:kBottom_Type];
    
    y = _contactTF.bottom_wcr;
    _phoneTF = [self getTextFieldWithYpoint:y Font:font Placeholder:@"请输入联系电话" LeftTitle:@"联系电话" SuperView:footerView];
    _phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    
    y = _phoneTF.bottom_wcr;
    UIView *grayView1 = [[UIView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, 10)];
    grayView1.backgroundColor = self.view.backgroundColor;
    [footerView addSubview:grayView1];
    
    y = grayView1.bottom_wcr;
    _timeTF = [self getTextFieldWithYpoint:y Font:font Placeholder:@"添加看房时间" LeftTitle:@"请选择时间" SuperView:footerView];
    // 右视图
    [_timeTF textFieldAddRightImage:@"icon-xiangyoujiantou" Width:14 RightBorder:0];
    
    footerView.height_wcr = _timeTF.bottom_wcr;
    tableView.tableFooterView = footerView;
}

- (UITextField *)getTextFieldWithYpoint:(CGFloat)yPoint Font:(UIFont *)font Placeholder:(NSString *)placeholder LeftTitle:(NSString *)title SuperView:(UIView *)superView {
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(kSpaceX, yPoint, kMainWidth - kSpaceX * 2, 45)];
    textField.delegate = self;
    [textField textFieldWithPlaceholder:placeholder Font:font];
    // 左视图
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 85, textField.height_wcr)];
    [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:title];
    textField.leftView = label;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    textField.textAlignment = NSTextAlignmentRight;
    [superView addSubview:textField];
    
    return textField;
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == _timeTF) {
        WCRDatePickerView *datePickerView = [WCRDatePickerView popDatePickerViewWithDatePickerMode:UIDatePickerModeDateAndTime ReturnDateFormat:kYYYYMMddHHmm_Type ReturnBlock:^(NSString *dateString, NSDate *date) {
            textField.text = dateString;
        }];
        [datePickerView.datePicker setMinimumDate:[NSDate date]];
        return NO;
    }
    return YES;
}

@end
