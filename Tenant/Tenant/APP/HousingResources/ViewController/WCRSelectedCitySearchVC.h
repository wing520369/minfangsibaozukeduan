//
//  WCRSelectedCitySearchVC.h
//  Tenant
//
//  Created by eliot on 2017/5/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class RACSubject;
/// 搜索城市
@interface WCRSelectedCitySearchVC : WCRBasicViewController
@property (nonatomic, strong) NSArray *allCityModels;
@property (nonatomic, strong) RACSubject *selectSignal;
@end
