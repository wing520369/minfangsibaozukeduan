//
//  WCRMapHousingResourcesVC.m
//  Tenant
//
//  Created by eliot on 2017/6/8.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMapHousingResourcesVC.h"
#import "UIViewController+WCRCustom.h"
#import "UITextField+WCRCustom.h"
#import "WCRHousingSourcesSearchVC.h"
#import "NSString+WCRCustom.h"
#import "WCRMapHousingSourcesModel.h"
#import "WCRHouseModel.h"
#import "WCRRoomModel.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "WCRAnnotationView.h"
#import "NPGeneralApi.h"
#import "WCRMapTableViewHeader.h"
#import "WCRHousingSourcesCell.h"
#import "WCRHousingResourcesDetailVC.h"
#import "NSObject+RACPropertySubscribing.h"
#import "RACSignal.h"
#import "MSWeakTimer.h"
#import "NSDate+WCRCustom.h"

static NSString *identifier = @"cell";
static NSString *annotationIdentifier = @"annotation";

@interface WCRMapHousingResourcesVC ()<BMKMapViewDelegate,BMKLocationServiceDelegate,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, WCRAnnotationViewDelegate>
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) BMKMapView *mapView;
@property (nonatomic,assign) BMKCoordinateRegion region;
@property (nonatomic,strong) BMKLocationService *locationService;
@property (nonatomic,assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSMutableArray *allModels;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) BOOL isPopTableView;
@end

@implementation WCRMapHousingResourcesVC

- (void)dealloc {
    self.mapView.delegate = nil;
    self.locationService.delegate = nil;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:nil];
    [self addRightBarButtonItemTitle:@"列表"];
    [self createUI];
    self.fd_interactivePopDisabled = YES;
    _allModels = [NSMutableArray array];
    _dataSource = [NSMutableArray array];
    [self observeValue];
}
- (void)observeValue {
    
    self.isPopTableView = NO;
    @weakify(self);
    [RACObserve(self, isPopTableView) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        BOOL isPop = [x boolValue];
        if (isPop) {
            [UIView animateWithDuration:0.1 animations:^{
                self.tableView.y_wcr = kMainHeight - self.tableView.height_wcr - 64;
                [self.tableView reloadData];
            }];
        }
        else {
            self.tableView.y_wcr = kMainHeight;
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_textField];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_textField removeFromSuperview];
}
- (void)rightBarButtonItemAction:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}
- (UITableView *)tableView {
    if (!_tableView) {
        CGFloat height = (800 / 1334.0) * kMainHeight;
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kMainHeight, kMainWidth, height) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_tableView];
        
        [_tableView registerClass:[WCRHousingSourcesCell class] forCellReuseIdentifier:identifier];
        _tableView.rowHeight = [WCRHousingSourcesCell cellHeight];
        
        _tableView.tableHeaderView = [[WCRMapTableViewHeader alloc] init];
    }
    return _tableView;
}
- (void)createUI {
    // 搜索
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    CGFloat x = 60;
    CGFloat width = kMainWidth - x * 2;
    CGFloat y = 5;
    CGFloat height = 44 - y * 2;
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [textField textFieldWithFont:font PlaceHolder:@"请输入你想住的地方" LeftLabelText:nil Border:0];
    textField.delegate = self;
    _textField = textField;
    [textField setLayerWithBorderWidth:1.0 Corner:3.0 BorderColor:[UIColor lineColor]];
    [self.navigationController.navigationBar addSubview:textField];
    
    // 地图
    _locationService = [[BMKLocationService alloc]init];
    // 打开定位服务
    [_locationService startUserLocationService];
    _locationService.delegate = self;
    
    _mapView = [[BMKMapView alloc]initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    _mapView.userTrackingMode = 0;  // 设置定位状态
    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    [self.view addSubview:_mapView];
}
- (void)getRoomListDataFromNetwork {
    double lng = self.coordinate.longitude;
    double lat = self.coordinate.latitude;
    if (lng == 0) {
        return;
    }
    // @"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],
    NSDictionary *dict = @{@"lng":@(lng),@"lat":@(lat)};
    @weakify(self);
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/roomApiController/getMapInfo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            NSArray *array = [WCRMapHousingSourcesModel jsonWithArray:responseMd.response];
            [array enumerateObjectsUsingBlock:^(WCRMapHousingSourcesModel *newObj, NSUInteger idx, BOOL * _Nonnull stop) {
                __block BOOL have = NO;
                [self.allModels enumerateObjectsUsingBlock:^(WCRMapHousingSourcesModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj.house.ID isEqualToString:newObj.house.ID]) {
                        obj = newObj;
                        have = YES;
                    }
                }];
                if (!have) {
                    [self.allModels addObject:newObj];
                }
            }];
            [self addHousingResoucesAnnotations];
        }
    }];

}
- (void)addHousingResoucesAnnotations {
    
    NSArray *annotations = [_mapView annotations];
    [_mapView removeAnnotations:annotations];
    for (NSInteger i = 0; i < self.allModels.count; i++) {
        BMKPointAnnotation *annotation = [[BMKPointAnnotation alloc]init];
        CLLocationCoordinate2D location;
        WCRMapHousingSourcesModel *model = [self.allModels objectAtIndex:i];
        WCRHouseModel *houseModel = model.house;
        location.latitude = [houseModel.lat doubleValue];
        location.longitude = [houseModel.lng doubleValue];
        annotation.coordinate = location;
        annotation.title = [NSString stringWithFormat:@"%@(%@套)",houseModel.houseName,model.count];
        annotation.subtitle = houseModel.ID;
        [_mapView addAnnotation:annotation];
    }
}
#pragma mark - 搜索
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    WCRHousingSourcesSearchVC *searchVC = [[WCRHousingSourcesSearchVC alloc] init];
    searchVC.cityId = self.cityId;
    [self.navigationController pushViewController:searchVC animated:YES];
    return NO;
}
#pragma mark - Map Delegate
// 生成的标注View：大头针
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation{

    // 检查是否有重用的缓存
    WCRAnnotationView *annotationView = (WCRAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    
    // 一般首次添加annotation代码会运行到此处
    if (annotationView == nil) {
        annotationView = [[WCRAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
        annotationView.canShowCallout = NO;
    }
    
    NSString *title = annotation.title;
    [annotationView.button setTitle:title forState:0];
    CGFloat width = [NSString calculateWithFont:annotationView.button.titleLabel.font Content:title];
    if (width > 150) {
        width = 150;
    }
    annotationView.width_wcr = width + 10;
    annotationView.button.width_wcr = width;
    [annotationView setLayerWithBorderWidth:1.0 Corner:4.0 BorderColor:[UIColor lineColor]];

    annotationView.delegate = self;
    
    return annotationView;
}
// 点空白处会回调此接口   coordinate 空白处坐标点的经纬度
- (void)mapView:(BMKMapView *)mapView onClickedMapBlank:(CLLocationCoordinate2D)coordinate {
    self.isPopTableView = NO;
    self.coordinate = coordinate;
    self.mapView.centerCoordinate = self.coordinate;
}
- (void)mapView:(BMKMapView *)mapView onDrawMapFrame:(BMKMapStatus *)status {
    if (self.isPopTableView) {
        self.isPopTableView = NO;
    }
}
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    self.coordinate = mapView.centerCoordinate;
    [self getRoomListDataFromNetwork];
}
#pragma mark - 定位 location Delegate
// 用户位置更新后，会调用此函数 userLocation 新的用户位置
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation {
    self.coordinate = userLocation.location.coordinate;
    [self updateRegionWithCoordinate];
    // 关闭定位服务
    [_locationService stopUserLocationService];
}
// 定位失败后，会调用此函数
- (void)didFailToLocateUserWithError:(NSError *)error {
    CLLocationCoordinate2D coordinate;
    coordinate.longitude = 113.341319;
    coordinate.latitude = 23.138784;
    self.coordinate = coordinate;
    [self updateRegionWithCoordinate];
}
- (void)updateRegionWithCoordinate {
    if (_coordinate.longitude == 0) {
        return;
    }
    // 设置中心点及显示范围
    _region.span.latitudeDelta = 0.1;
    _region.span.longitudeDelta = 0.1;
    _region.center = _coordinate;
    BMKCoordinateRegion adjustedRegion = [_mapView regionThatFits:_region];
    [_mapView setRegion:adjustedRegion animated:YES];
    
    [self getRoomListDataFromNetwork];
}
#pragma mark - 点击大头针，弹出房源列表
- (void)annotationViewClickAction:(BMKAnnotationView *)annotation {
    
    NSString *houseId = annotation.annotation.subtitle;
    [self.dataSource removeAllObjects];
    @weakify(self);
    [self.allModels enumerateObjectsUsingBlock:^(WCRMapHousingSourcesModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @strongify(self);
        WCRHouseModel *houseModel = obj.house;
        if ([houseModel.ID isEqualToString:houseId]) {
            [self.dataSource addObjectsFromArray:obj.rooms];
            WCRMapTableViewHeader *headerView = (WCRMapTableViewHeader *)self.tableView.tableHeaderView;
            headerView.model = obj;
            *stop = YES;
        }
    }];
    
    self.isPopTableView = YES;
}

#pragma mark - Table View DataSorce
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRHousingSourcesCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.model = [_dataSource objectAtIndex:indexPath.row];
    return cell;
}
#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRHousingResourcesDetailVC *detailVC = [[WCRHousingResourcesDetailVC alloc] init];
    detailVC.roomModel = [_dataSource objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
}

@end
