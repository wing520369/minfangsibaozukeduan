//
//  WCRHousingResourcesDetailVC.m
//  Tenant
//
//  Created by 吴传荣 on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRHousingResourcesDetailVC.h"
#import "WCRHouseDetailHeaderView.h"
#import "WCRHouseDetailFooterView.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "Tools.h"
#import "NPGeneralApi.h"
#import "NPTip.h"
#import "WCRApointmentRoomVC.h"
#import "WCRRoomDetailModel.h"
#import "WCRMoreRoomVC.h"
#import "RACSubject.h"
#import "WCRRoomModel.h"
#import "WCRLoginVC.h"

#define kBottomHeight 45

typedef NS_ENUM(NSInteger, BottomViewType) {
    BottomViewTypeContactLoadlord = 1035,
    BottomViewTypeCollectRoom,
    BottomViewTypeAppointmentRoom,
};

@interface WCRHousingResourcesDetailVC ()<WCRHouseDetailHeaderViewDelegate>
@property (nonatomic, strong) WCRRoomDetailModel *detailModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRHousingResourcesDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_interactivePopDisabled = YES;
    [self addLeftGoBackItemWithTitle:@"房源详情"];
    [self createUI];
    [self getRoomDetalInfoFromNetwork];
}

#pragma mark - 获取房源详情
- (void)getRoomDetalInfoFromNetwork {
    @weakify(self);
    // @"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],
    NSDictionary *dict = @{@"roomId":self.roomModel.ID};
    [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/roomApiController/getPublishRoomInfo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            self.detailModel = [WCRRoomDetailModel jsonWithDictionary:responseMd.response];
            self.roomModel = self.detailModel.room;
            
            WCRHouseDetailHeaderView *headerView = [[WCRHouseDetailHeaderView alloc] initWithRoomModel:self.roomModel];
            headerView.delegate = self;
            self.tableView.tableHeaderView = headerView;
            
            WCRHouseDetailFooterView *footerView = [[WCRHouseDetailFooterView alloc] initWithRoomDetailModel:self.detailModel];
            self.tableView.tableFooterView = footerView;
            
            UIButton *button = (UIButton *)[self.view viewWithTag:BottomViewTypeCollectRoom];
            button.selected = [self.detailModel.collect integerValue] == 1 ? YES : NO;
        }
    }];
}

- (void)createUI {
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64 - kBottomHeight) style:UITableViewStylePlain];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    _tableView = tableView;
    [self.view addSubview:tableView];
    
    [self addBottomView];
}

- (void)addBottomView {
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    NSArray *titles = @[@"联系房东",@"收藏房源",@"预约看房"];
    NSArray *tags = @[@(BottomViewTypeContactLoadlord),@(BottomViewTypeCollectRoom),@(BottomViewTypeAppointmentRoom)];
    NSArray *imageNames = @[@"icon_dianhua",@"icon_shoucang_default",@"0525icon_yuyuekanfang"];
    NSArray *bgColors = @[
                          [UIColor whiteColor],
                          [UIColor colorWithHexString:kCustomBlueColor],
                          [UIColor colorWithHexString:kMainColor],
                          ];
    NSArray *colors = @[
                        [UIColor colorWithHexString:kMainColor],
                        [UIColor whiteColor],
                        [UIColor whiteColor]
                        ];
    CGFloat x = 0;
    CGFloat width = kMainWidth / 3.0;
    CGFloat y = kMainHeight - 64 - kBottomHeight;
    for (NSInteger i = 0; i < titles.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(x, y, width, kBottomHeight);
        [button buttonWithTitleColor:[colors objectAtIndex:i] Background:[bgColors objectAtIndex:i] Font:font Title:[titles objectAtIndex:i]];
        [button setImage:[UIImage imageFileNamed:[imageNames objectAtIndex:i]] forState:0];
        [button setImagePosition:0 spacing:3];
        button.tag = [[tags objectAtIndex:i] integerValue];
        [button addTarget:self action:@selector(bottomButtonAction:)];
        [self.view addSubview:button];
        x = button.right_wcr;
        
        if (button.tag == BottomViewTypeCollectRoom) {
            [button setImage:[UIImage imageFileNamed:@"icon_shoucang_selected"] forState:UIControlStateSelected];
        }
    }
}

#pragma mark - 更多房号
- (void)houseDetailHeaderViewSkipMoreRoomVC {
    WCRMoreRoomVC *moreRoomVC = [[WCRMoreRoomVC alloc] init];
    moreRoomVC.roomDetailModel = self.detailModel;
    moreRoomVC.selectRoomSignal = [RACSubject subject];
    @weakify(self);
    [moreRoomVC.selectRoomSignal subscribeNext:^(NSString *roomId) {
        @strongify(self);
        self.roomModel.ID = roomId;
        [self getRoomDetalInfoFromNetwork];
    }];
    [self.navigationController pushViewController:moreRoomVC animated:YES];
}

#pragma mark - 联系房东 收藏房源 预约看房
- (void)bottomButtonAction:(UIButton *)button {
    switch (button.tag) {
        case BottomViewTypeContactLoadlord:{
            if (self.detailModel.hostPhone.length) {
                [Tools dialPhoneNumber:self.detailModel.hostPhone];
            }
        }
            break;
        case BottomViewTypeCollectRoom:{
            // 收藏
            
            if (![Tools checkIsLogin]) {
                return;
            }
            
            NSString *requestUrl = @"/api/camCollectController/addCollect";
            NSString *tip = @"收藏成功";
            NSString *collect = @"1";
            if ([self.detailModel.collect integerValue] == 1) {
                // 取消收藏
                requestUrl = @"/api/camCollectController/cancelCollect";
                tip = @"取消收藏成功";
                collect = @"0";
            }
            __weak typeof(self) weakSelf = self;
            NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"type":@"1",@"objectId":_roomModel.ID};
            [NPGeneralApi WCRApiWithParameters:dict tailUrl:requestUrl ResponseBlock:^(NPGeneralResponse *responseMd) {
                if (responseMd.isOK) {
                    weakSelf.detailModel.collect = collect;
                    button.selected = [collect integerValue] == 1 ? YES : NO;
                    [NPTip showTip:tip];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateCollectionList" object:nil];
                }
            }];
        }
            break;
        case BottomViewTypeAppointmentRoom:{
            
            if (![Tools checkIsLogin]) {
                return;
            }
            
            if (!self.detailModel) {
                return;
            }
            WCRApointmentRoomVC *appointmentVC = [[WCRApointmentRoomVC alloc] init];
            appointmentVC.roomModel = _roomModel;
            [self.navigationController pushViewController:appointmentVC animated:YES];
        }
            break;
        default:
            break;
    }
    
}

@end
