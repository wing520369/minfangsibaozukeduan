//
//  WCRSelectedCitySearchVC.m
//  Tenant
//
//  Created by eliot on 2017/5/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRSelectedCitySearchVC.h"
#import "UITextField+WCRCustom.h"
#import "WCRCityCell.h"
#import "WCRDataSourceModel.h"
#import "WCRLetterSortModel.h"
#import "UIViewController+WCRCustom.h"
#import "RACSubject.h"
#import "UTPinYinHelper.h"
#import "DZNEmptyDataProvider.h"

static NSString *identifier = @"cell";

@interface WCRSelectedCitySearchVC ()<UITextFieldDelegate, UITableViewDelegate>
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) WCRDataSourceModel *dataSourceModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRSelectedCitySearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    [self createUI];
}

//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    [self.navigationController.navigationBar addSubview:_cancelButton];
//    [self.navigationController.navigationBar addSubview:_textField];
//}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_cancelButton removeFromSuperview];
    [_textField removeFromSuperview];
}

- (void)createUI {
    
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    CGFloat width = 40;
    CGFloat height = _cancelButton.height_wcr;
    CGFloat x = kMainWidth - width - kSpaceX;
    CGFloat y = (44.0 - height) / 2.0;
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _cancelButton.frame = CGRectMake(x, y, width, height);
    [_cancelButton buttonWithTitleColor:nil Background:nil Font:font Title:@"取消"];
    _cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [_cancelButton addTarget:self action:@selector(cancelAction:)];
//    [self.navigationController.navigationBar addSubview:_cancelButton];
    
    [self addRightBarButtonItemTitle:@"取消"];
    
    x = 12.0;
    y = 7.0;
    width = _cancelButton.x_wcr - 12.0 - x;
    height = 30.0;
    _textField = [[UITextField alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [_textField textFieldWithFont:font Placeholder:@"搜索城市" LeftImageName:@"icon_sousuo_a" ImageWidth:14 Border:kSpaceX];
    _textField.delegate = self;
    _textField.returnKeyType = UIReturnKeyDone;
    _textField.backgroundColor = [UIColor whiteColor];
    [_textField setLayerWithBorderWidth:1.0 Corner:4.0 BorderColor:[UIColor lineColor]];
    [_textField addTarget:self action:@selector(textFieldValueChange:) forControlEvents:UIControlEventEditingChanged];
    [self.navigationController.navigationBar addSubview:_textField];
    
    _dataSourceModel = [[WCRDataSourceModel alloc] init];
    @weakify(self);
    self.tableView = [self.dataSourceModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) CellIdentifier:identifier CellClassName:@"WCRCityCell" ConfigureCellBlock:^(WCRCityCell *cell, NSIndexPath *indexPath, WCRLetterSortModel *model) {
        @strongify(self);
        cell.model = model;
        cell.lineView.hidden = indexPath.row == self.dataSourceModel.dataSource.count - 1 ? YES : NO;
    }];
    self.tableView.rowHeight = kDefaultHeight;
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
}

- (void)rightBarButtonItemAction:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)cancelAction:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRLetterSortModel *model = [_dataSourceModel.dataSource objectAtIndex:indexPath.row];
    UIViewController *vc = [UIViewController FindSpecificViewController:self.navigationController.viewControllers outViewController:NSClassFromString(@"WCRHousingResourcesVC")];
    [self.selectSignal sendNext:model];
    [self.navigationController popToViewController:vc animated:YES];
}


- (void)textFieldValueChange:(UITextField *)textField {
    NSString *strText = textField.text;
    [self.dataSourceModel.dataSource removeAllObjects];
    if (strText.length == 0) {
        [self.tableView reloadData];
        return;
    }
    
    @weakify(self);
    // 城市名
    [self.allCityModels enumerateObjectsUsingBlock:^(WCRLetterSortModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @strongify(self);
        UTPinYinHelper *pinYinHelper = [UTPinYinHelper sharedPinYinHelper];
        if ([pinYinHelper isString:obj.name MatchsKey:strText IgnorCase:YES]) {
            [self.dataSourceModel.dataSource addObject:obj];
        }
    }];
    
    //
    if (!self.tableView.emptyDataProvider) {
        DZNEmptyDataProvider *provider = [DZNEmptyDataProvider providerWithEmptyType:DZNEmptyDataType_NoData];
        provider.title = [DZNEmptyDataProvider defaultAttDescription:@"搜索结果为空!"];
        self.tableView.emptyDataProvider = provider;
    }
    
    [self.tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
