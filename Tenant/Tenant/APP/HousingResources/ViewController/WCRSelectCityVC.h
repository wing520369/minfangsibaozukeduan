//
//  WCRSelectCityVC.h
//  Tenant
//
//  Created by eliot on 2017/5/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class RACSubject;
/// 选择城市
@interface WCRSelectCityVC : WCRBasicViewController
@property (nonatomic, strong) RACSubject *selectSignal;
@end
