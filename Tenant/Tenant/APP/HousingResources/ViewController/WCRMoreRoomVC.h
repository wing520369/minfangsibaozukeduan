//
//  WCRMoreRoomDetailVC.h
//  Tenant
//
//  Created by eliot on 2017/5/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class WCRRoomDetailModel, RACSubject;
/// 更多房号
@interface WCRMoreRoomVC : WCRBasicViewController
@property (nonatomic, strong) WCRRoomDetailModel *roomDetailModel;
@property (nonatomic, strong) RACSubject *selectRoomSignal;
@end
