//
//  WCRHousingResourcesListVC.h
//  Tenant
//
//  Created by eliot on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class WCRSelectConditionModel;
/// 房源列表
@interface WCRHousingResourcesListVC : WCRBasicViewController
@property (nonatomic, strong) WCRSelectConditionModel *conditionModel;
@end
