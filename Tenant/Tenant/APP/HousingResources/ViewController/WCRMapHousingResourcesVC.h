//
//  WCRMapHousingResourcesVC.h
//  Tenant
//
//  Created by eliot on 2017/6/8.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
/// 地图上 显示房源
@interface WCRMapHousingResourcesVC : WCRBasicViewController
@property (nonatomic, strong) NSString *cityId;
@end
