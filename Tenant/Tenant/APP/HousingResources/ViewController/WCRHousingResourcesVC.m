//
//  WCRHousingResourcesVC.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRHousingResourcesVC.h"
#import "WCRHousingSourcesCell.h"
#import "WCRHousingSourcesModel.h"
#import "NPGeneralApi.h"
#import "WCRLeftImageButton.h"
#import "SDCycleScrollView.h"
#import "WCRTopImageButton.h"
#import "WCRHousingSourcesSectionView.h"
#import "WCRBMKTool.h"
#import "RegionPickerView.h"
#import "UIScrollView+NPRefresh.h"
#import "NPTip.h"
#import "WCRCarouselPictureModel.h"
#import "WCRHousingResourcesListVC.h"
#import "WCRHousingSourcesSearchVC.h"
#import "WCRHousingResourcesDetailVC.h"
#import "UINavigationController+WCRCustom.h"
#import "WCRDoubleModelPickerView.h"
#import "WCRSecondGradeFirstModel.h"
#import "WCRSelectCityVC.h"
#import "RACSubject.h"
#import "WCRLetterSortModel.h"
#import "WCRSelectConditionModel.h"

typedef NS_ENUM(NSInteger, ViewType) {
    ViewTypeMember = 325,
    ViewTypeMoney,
};

static NSString *identifier = @"cell";
static NSString *sectionIdentifier = @"sectionView";

@interface WCRHousingResourcesVC ()<UITableViewDelegate,UITableViewDataSource,SDCycleScrollViewDelegate,DRDAPIBatchAPIRequestsProtocol>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) WCRHousingSourcesModel *housingModel;
@property (nonatomic, strong) SDCycleScrollView *sdScrollView;
@property (nonatomic, strong) NSMutableArray *homeCarouselModels; // 首页轮播图
@property (nonatomic, strong) NSMutableArray *hotCarouselModels; // 热门房源轮播图
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) UIButton *cityButton;
@end

@implementation WCRHousingResourcesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _homeCarouselModels = [NSMutableArray array];
    _hotCarouselModels = [NSMutableArray array];
    self.city = @"广州市";
    self.cityId = @"198";
    [self createUI];
    [self startLocation];
    [self getDataFromNetwork];
}
- (void)startLocation {
    if ([WCRBMKTool sharedInstance].city.length) {
        // 已经提前定位到城市
        [self updateCityName:[WCRBMKTool sharedInstance].city];
        return;
    }
    // 开启定位服务
    [[WCRBMKTool sharedInstance] startLocation];
    @weakify(self);
    [WCRBMKTool sharedInstance].addressBlock = ^(NSString *province, NSString *city, NSString *district, NSString *street, NSString *streetNumber) {
        @strongify(self);
        if (![self.city isEqualToString:city] && city.length) {
            [self updateCityName:city];
            [self getDataFromNetwork];
        }
        // 关闭定位服务
        [[WCRBMKTool sharedInstance] stopLocation];
    };
}
- (void)updateCityName:(NSString *)city {
    self.city = city;
    self.cityId = [RegionPickerView getCityID:self.city];
    [self.cityButton setTitle:self.city forState:0];
    [self.cityButton setImagePosition:1 spacing:2];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)getDataFromNetwork {
    
    @weakify(self);
    DRDAPIBatchAPIRequests *apiBatchApis = [[DRDAPIBatchAPIRequests alloc]init];
    apiBatchApis.delegate = self;
    
    // 首页轮播图
    NPGeneralApi *homeCycleImageApi = [NPGeneralApi WCRmultiApiWithParameters:nil tailUrl:@"/api/publicApiController/upShowList" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            NSArray *models = [WCRCarouselPictureModel jsonWithArray:responseMd.response];
            [self.homeCarouselModels removeAllObjects];
            [self.homeCarouselModels addObjectsFromArray:models];
        }
    }];
    [apiBatchApis addAPIRequest:homeCycleImageApi];
    
    // 房源
    if (!self.cityId.length) {
        self.cityId = @"198";
    }
    // @"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],
    NSDictionary *dict = @{@"cityId":self.cityId};
    NPGeneralApi *roomApi = [NPGeneralApi WCRmultiApiWithParameters:dict tailUrl:@"/api/indexApiController/getRecHotRoom" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            self.housingModel = [WCRHousingSourcesModel jsonWithDictionary:responseMd.response];
            UILabel *memberLabel = [self.view viewWithTag:ViewTypeMember];
            memberLabel.text = [NSString stringWithFormat:@"会员数：%@",self.housingModel.uCount];
            UILabel *moneyLabel = [self.view viewWithTag:ViewTypeMoney];
            moneyLabel.text = [NSString stringWithFormat:@"交易额：%.2f",[self.housingModel.turnover floatValue]];
        }
    }];
    [apiBatchApis addAPIRequest:roomApi];
    
    // 热门房源轮播图
    NPGeneralApi *hotRoomCycleImageApi = [NPGeneralApi WCRmultiApiWithParameters:nil tailUrl:@"/api/publicApiController/centreShowList" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            NSArray *models = [WCRCarouselPictureModel jsonWithArray:responseMd.response];
            [self.hotCarouselModels removeAllObjects];
            [self.hotCarouselModels addObjectsFromArray:models];
        }
    }];
    [apiBatchApis addAPIRequest:hotRoomCycleImageApi];

    [apiBatchApis start];
}

// 多个网络请求完成
- (void)batchAPIRequestsDidFinished:(DRDAPIBatchAPIRequests *)batchApis {
    
    [_tableView.mj_header endRefreshing];
    __block BOOL succeess = YES;
    [batchApis.apiRequestsSet enumerateObjectsUsingBlock:^(NPGeneralApi * obj, BOOL * _Nonnull stop) {
        if (obj.fail) {
            // 请求失败
            succeess = NO;
            [NPTip showTip:@"网络异常"];
            *stop = YES;
        }
    }];
    if (!succeess) {
        return;
    }
    
    __block NSMutableArray *urls = [NSMutableArray arrayWithCapacity:self.homeCarouselModels.count];
    [self.homeCarouselModels enumerateObjectsUsingBlock:^(WCRCarouselPictureModel * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [urls addObject:obj.img];
    }];
    self.sdScrollView.imageURLStringsGroup = [urls copy];
    
    [self.tableView reloadData];
    [self addTableViewFooter];
}

- (void)createUI {
    
    CGFloat y = [self addTopView];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, kMainHeight - 49 - y) style:UITableViewStyleGrouped];
    _tableView = tableView;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    
    tableView.rowHeight = [WCRHousingSourcesCell cellHeight];
    tableView.sectionFooterHeight = 0.001;
    [tableView registerClass:[WCRHousingSourcesCell class] forCellReuseIdentifier:identifier];
    
    [self addTableViewHeader];
    
    
    @weakify(self);
    [tableView MJHeaderWithRefreshingBlock:^{
        @strongify(self);
        [self getDataFromNetwork];
    }];
}

- (void)addTableViewFooter{

    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat height = 40.0;
    CGFloat width = kMainWidth;
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [button setTitle:@"更多" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:16.0];
    [button setTitleColor:[UIColor colorWithRGB:58.0 G:58.0 B:58.0] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(getMoreRoomList:) forControlEvents:UIControlEventTouchUpInside];
    _tableView.tableFooterView = button;
}

- (void)addTableViewHeader {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 0.1)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // 轮播图
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = kMainWidth;
    CGFloat height = (385 / 750.0) * width;
    _sdScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(x, y, width, height) delegate:self placeholderImage:nil];
    _sdScrollView.backgroundColor = [UIColor clearColor];
    _sdScrollView.currentPageDotColor = [UIColor colorWithHexString:kMainColor];
    _sdScrollView.pageDotColor = [UIColor whiteColor];
    _sdScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    [headerView addSubview:_sdScrollView];
    [_sdScrollView addLineViewPositionType:kBottom_Type];
    
    UIFont *font = [UIFont CustomFontWithSize:13.0];
    x = kSpaceX;
    y = _sdScrollView.bottom_wcr;
    height = 40;
    width = kMainWidth / 2.0 - x - 5;
    // 会员数 交易额
    NSArray *titles = @[@"会员数：0",@"交易额：0"];
    NSArray *tags = @[@(ViewTypeMember),@(ViewTypeMoney)];
    for (NSInteger i = 0; i < titles.count; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:[titles objectAtIndex:i]];
        label.tag = [[tags objectAtIndex:i] integerValue];
        [headerView addSubview:label];
        x = kMainWidth / 2.0 + kSpaceX;
    }
    
    UIView *verticalLineView = [UIView lineView];
    verticalLineView.frame = CGRectMake(kMainWidth / 2.0 - 0.5, y, 1, height);
    [headerView addSubview:verticalLineView];
    
    UIView *lineView = [UIView lineView];
    lineView.frame = CGRectMake(0, y + height, kMainWidth, 1);
    [headerView addSubview:lineView];
    
    // 房产类型 房产类型 0-住宅/小区/公寓 1-商铺/门市房 2-厂房/车间 3-仓库 4-车库/停车位 5-写字楼/办公室
    titles = @[@"住宅/小区",@"商铺/门市房",@"厂房/车间",@"仓库",@"车库/停车位",@"写字楼/办公室"];
    NSArray *imageNames = @[@"icon_residential",@"icon_shops",@"icon_workshop",@"icon_warehouse",@"icon_garage",@"icon_office-building"];
    CGFloat border = 15;
    x = 0;
    width = kMainWidth / 3.0;
    CGFloat imageWH = AUTO_MATE_WIDTH(60);
    height = 90.0;
    y = lineView.bottom_wcr + border;
    for (NSInteger i = 0; i < titles.count; i++) {
        WCRTopImageButton *button = [[WCRTopImageButton alloc] initWithFrame:CGRectMake(x, y, width, height) ImageSize:CGSizeMake(imageWH, imageWH) TitleColor:[UIColor blackColor] Font:font Title:[titles objectAtIndex:i]];
        [button setImage:[UIImage imageFileNamed:[imageNames objectAtIndex:i]] forState:0];
        button.tag = i;
        [button addTarget:self action:@selector(selectRoomType:)];
        [headerView addSubview:button];
        x = button.right_wcr;
        if (i == 2) {
            x = 0;
            y = button.bottom_wcr + border;
        }
    }
    
    y = y + height + border;

    headerView.height_wcr = y;
    _tableView.tableHeaderView = headerView;
}

- (CGFloat)addTopView {
    CGFloat height = 70;
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, height)];
    topView.backgroundColor = [UIColor colorWithHexString:kMainColor];
    [self.view addSubview:topView];
    
    // 城市
    CGFloat y = 20;
    height = topView.height_wcr - y;
    CGFloat x = 8;
    CGFloat width = 65;
    UIFont *font = [UIFont CustomFontWithSize:12.0];
    UIButton *cityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _cityButton = cityButton;
    cityButton.frame = CGRectMake(x, y, width, height);
    [cityButton buttonWithTitleColor:[UIColor whiteColor] Background:nil Font:font Title:@"广州市"];
    [cityButton setImage:[UIImage imageFileNamed:@"icon_button"] forState:0];
    [cityButton addTarget:self action:@selector(selectCityAction)];
    [cityButton setImagePosition:1 spacing:2];
    [topView addSubview:cityButton];
    
    x = cityButton.right_wcr;
    width = kMainWidth - x - kSpaceX;
    y += 5;
    height -= 10;
    WCRLeftImageButton *searchButton = [[WCRLeftImageButton alloc] initWithFrame:CGRectMake(x, y, width, height) ImageTitleBorder:10 LeftBorder:20 ImageSize:CGSizeMake(18, 18)];
    [searchButton buttonWithTitleColor:[UIColor whiteColor] Background:[UIColor colorWithRGB:250 G:112 B:46] Font:font Title:@"想住哪儿呢"];
    [searchButton setImage:[UIImage imageFileNamed:@"icon_sousuo"] forState:0];
    [searchButton setViewCorner:4.0];
    [searchButton addTarget:self action:@selector(searchRoomAction)];
    [topView addSubview:searchButton];
    
    return topView.bottom_wcr;
}

#pragma mark - Table View DataSorce
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.housingModel.recRoom.count;
    }
    return self.housingModel.hotRoom.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRHousingSourcesCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    if (indexPath.section == 0) {
        cell.model = [self.housingModel.recRoom objectAtIndex:indexPath.row];
        cell.lineView.hidden = self.housingModel.recRoom.count - 1 == indexPath.row ? YES : NO;
    }
    else {
        cell.model = [self.housingModel.hotRoom objectAtIndex:indexPath.row];
        cell.lineView.hidden = self.housingModel.hotRoom.count - 1 == indexPath.row ? YES : NO;
    }
    return cell;
}
#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [WCRHousingSourcesSectionView sectionHeightWithSection:section];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    WCRHousingSourcesSectionView *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionIdentifier];
    if (!sectionView) {
        sectionView = [[WCRHousingSourcesSectionView alloc] initWithReuseIdentifier:sectionIdentifier];
    }
    if (section == 0) {
        sectionView.sdScrollView.hidden = YES;
        sectionView.bgView.y_wcr = 10;
        sectionView.titleLabel.text = @"推荐房源";
    }
    else {
        sectionView.sdScrollView.hidden = NO;
        sectionView.bgView.y_wcr = sectionView.sdScrollView.bottom_wcr;
        sectionView.titleLabel.text = @"热门房源";
        
        __block NSMutableArray *urls = [NSMutableArray arrayWithCapacity:self.homeCarouselModels.count];
        [self.hotCarouselModels enumerateObjectsUsingBlock:^(WCRCarouselPictureModel * obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [urls addObject:obj.img];
        }];
        sectionView.sdScrollView.imageURLStringsGroup = [urls copy];
        
    }
    sectionView.moreButton.tag = section;
    [sectionView.moreButton addTarget:self action:@selector(getMoreRoomList:)];
    return sectionView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRHousingResourcesDetailVC *detailVC = [[WCRHousingResourcesDetailVC alloc] init];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    if (indexPath.section == 0) {
        detailVC.roomModel = [self.housingModel.recRoom objectAtIndex:indexPath.row];
    }
    else {
        detailVC.roomModel = [self.housingModel.hotRoom objectAtIndex:indexPath.row];
    }
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - 更多
- (void)getMoreRoomList:(UIButton *)button {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    WCRHousingResourcesListVC *listVC = [[WCRHousingResourcesListVC alloc] init];
    WCRSelectConditionModel *conditionModel = [[WCRSelectConditionModel alloc] init];
    conditionModel.cityId = self.cityId;
    conditionModel.city = self.city;
    listVC.conditionModel = conditionModel;
    listVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:listVC animated:YES];
}

#pragma mark - 选择房源类型
- (void)selectRoomType:(UIButton *)button {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    WCRHousingResourcesListVC *listVC = [[WCRHousingResourcesListVC alloc] init];
    WCRSelectConditionModel *conditionModel = [[WCRSelectConditionModel alloc] init];
    conditionModel.cityId = self.cityId;
    conditionModel.city = self.city;
    conditionModel.houseType = [NSString stringWithFormat:@"%ld",(long)button.tag];
    conditionModel.houseTypeString = button.titleLabel.text;
    listVC.hidesBottomBarWhenPushed = YES;
    listVC.conditionModel = conditionModel;
    [self.navigationController pushViewController:listVC animated:YES];
}

#pragma mark - 点击轮播图
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
    
}

#pragma mark - 选择城市
- (void)selectCityAction {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    WCRSelectCityVC *selectCity = [[WCRSelectCityVC alloc] init];
    selectCity.selectSignal = [RACSubject subject];
    @weakify(self);
    [selectCity.selectSignal subscribeNext:^(WCRLetterSortModel *cityModel) {
        @strongify(self);
        self.cityId = cityModel.ID;
        self.city = cityModel.name;
        [self.cityButton setTitle:self.city forState:0];
        [self.cityButton setImagePosition:1 spacing:2];
        [self getDataFromNetwork];
    }];
    selectCity.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:selectCity animated:YES];
}

#pragma mark - 搜索房源
- (void)searchRoomAction {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    WCRHousingSourcesSearchVC *searchVC = [[WCRHousingSourcesSearchVC alloc] init];
    searchVC.cityId = self.cityId;
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}


@end
