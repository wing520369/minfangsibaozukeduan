//
//  WCRHousingResourcesListVC.m
//  Tenant
//
//  Created by eliot on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRHousingResourcesListVC.h"
#import "UIViewController+WCRCustom.h"
#import "UITextField+WCRCustom.h"
#import "BasicListViewModel.h"
#import "WCRHousingSourcesCell.h"
#import "WCRPullDownMenuView.h"
#import "RegionPickerView.h"
#import "WCRHousingSourcesSearchVC.h"
#import "WCRHousingResourcesDetailVC.h"
#import "WCRMapHousingResourcesVC.h"
#import "WCRSelectConditionModel.h"

typedef NS_ENUM(NSInteger, RoomSelectType) {
    /// 地区
    RoomSelectTypeRegion = 0,
    /// 租金
    RoomSelectTypeRent,
    /// 户型(室)
    RoomSelectTypeHouseType,
    /// 物业性质（房产类型）
    RoomSelectTypeTenement,
};

static NSString *identifier = @"cell";

@interface WCRHousingResourcesListVC ()<UITextFieldDelegate, UITableViewDelegate>
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) BasicListViewModel *viewModel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *allDatas;
@property (nonatomic, strong) NSMutableArray *buttons;
@end

@implementation WCRHousingResourcesListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:nil];
    [self addRightBarButtonItemTitle:@"地图"];
    [self initialData];
    [self createUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_textField];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_textField removeFromSuperview];
}
// 地图
- (void)rightBarButtonItemAction:(UIButton *)button {
    WCRMapHousingResourcesVC *mapVC = [[WCRMapHousingResourcesVC alloc] init];
    mapVC.cityId = self.conditionModel.cityId;
    [self.navigationController pushViewController:mapVC animated:YES];
}
- (void)initialData {
    // 所有的数据
    NSMutableArray *allDatas = [NSMutableArray arrayWithCapacity:4];
    // 每一项的内容
    NSMutableArray *subMenuModels = [NSMutableArray array];
    
    // 获取所有的区
    // 不限
    WCRMenuModel *menuModel = [[WCRMenuModel alloc] initWithID:@"" Name:@"不限"];
    [subMenuModels addObject:menuModel];
    
    NSArray *regions = [RegionPickerView getAllDistrictWithCityId:self.conditionModel.cityId];
    [regions enumerateObjectsUsingBlock:^(DistrictModel *districtModel, NSUInteger idx, BOOL * _Nonnull stop) {
        WCRMenuModel *otherMenuModel = [[WCRMenuModel alloc] initWithID:districtModel.districtId Name:districtModel.district];
        [subMenuModels addObject:otherMenuModel];
    }];
    [allDatas addObject:[subMenuModels copy]];
    
    [subMenuModels removeAllObjects];
    // 租金类型
    NSArray *rentTypes = @[@"不限",@"500以下",@"500~1000",@"1000~2000",@"2000~3000",@"3000~5000",@"5000~8000",@"8000以上"];
    NSArray *rentIds = @[@"",@"1",@"2",@"3",@"4",@"5",@"6",@"7"];
    [self dealWithContents:rentTypes Ids:rentIds SubMenuModels:subMenuModels AllDatas:allDatas];
    
    // 户型
    NSArray *rooms = @[@"不限",@"1室",@"2室",@"3室",@"4室",@"5室",@"5室以上"];
    NSArray *roomIds = @[@"",@"1",@"2",@"3",@"4",@"5",@"6"];
    [self dealWithContents:rooms Ids:roomIds SubMenuModels:subMenuModels AllDatas:allDatas];
    
    // 物业性质 房产类型 0-住宅/小区 1-商铺/门市房 2-厂房/车间 3-仓库 4-车库/停车位 5-写字楼/办公室
    NSArray *houseTypes = @[@"不限",@"住宅/小区",@"商铺/门市房",@"厂房/车间",@"仓库",@"车库/停车位",@"写字楼/办公室"];
    NSArray *houseTypeIds = @[@"",@"0",@"1",@"2",@"3",@"4",@"5"];
    [self dealWithContents:houseTypes Ids:houseTypeIds SubMenuModels:subMenuModels AllDatas:allDatas];
    
    self.allDatas = [NSMutableArray arrayWithCapacity:allDatas.count];
    [self.allDatas addObjectsFromArray:allDatas];
}
- (void)dealWithContents:(NSArray *)contents Ids:(NSArray *)ids SubMenuModels:(NSMutableArray *)subMenuModels AllDatas:(NSMutableArray *)allDatas {
    [contents enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *Id = [ids objectAtIndex:idx];
        WCRMenuModel *otherMenuModel = [[WCRMenuModel alloc] initWithID:Id Name:obj];
        [subMenuModels addObject:otherMenuModel];
    }];
    
    [allDatas addObject:[subMenuModels copy]];
    [subMenuModels removeAllObjects];
}

- (void)createUI {
    // 搜索
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    CGFloat x = 60;
    CGFloat width = kMainWidth - x * 2;
    CGFloat y = 5;
    CGFloat height = 44 - y * 2;
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [textField textFieldWithFont:font PlaceHolder:@"请输入你想住的地方" LeftLabelText:nil Border:0];
    textField.delegate = self;
    _textField = textField;
    [textField setLayerWithBorderWidth:1.0 Corner:3.0 BorderColor:[UIColor lineColor]];
    [self.navigationController.navigationBar addSubview:textField];
    
    // 选择类型
    NSString *houseType = self.conditionModel.houseTypeString.length ? self.conditionModel.houseTypeString : @"物业性质";
    NSArray *titles = @[@"地区",@"租金",@"户型",houseType];
    _buttons = [NSMutableArray arrayWithCapacity:titles.count];
    NSArray *tags = @[@(RoomSelectTypeRegion),@(RoomSelectTypeRent),@(RoomSelectTypeHouseType),@(RoomSelectTypeTenement)];
    x = 0;
    width = kMainWidth / titles.count;
    y = 0;
    height = kDefaultHeight;
    UIImage *image = [UIImage imageFileNamed:@"icon_button_a"];
    for (NSInteger i = 0; i < titles.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(x, y, width, height);
        [button buttonWithTitleColor:nil Background:[UIColor whiteColor] Font:font Title:[titles objectAtIndex:i]];
        button.tag = [[tags objectAtIndex:i] integerValue];
        [button setImage:image forState:0];
        [button setImagePosition:1 spacing:2];
        [button addTarget:self action:@selector(roomSelectAction:)];
        [self.view addSubview:button];
        UIView *lineView = [UIView lineView];
        lineView.frame = CGRectMake(button.width_wcr - 1, 10, 1, button.height_wcr - 20);
        [button addSubview:lineView];
        lineView.hidden = titles.count - 1 == i ? YES : NO;
        x = button.right_wcr;
        
        [self.buttons addObject:button];
    }
    UIView *bottomLineView = [UIView lineView];
    bottomLineView.frame = CGRectMake(0, y + height, kMainWidth, 1);
    [self.view addSubview:bottomLineView];
    
    _viewModel = [[BasicListViewModel alloc] initWithParameters:nil tailUrl:@"/api/roomApiController/getQueryTypeList"];
    
    y = bottomLineView.bottom_wcr;
    height = kMainHeight - 64 - y;
    @weakify(self);
    self.tableView = [self.viewModel createTableViewWithFrame:CGRectMake(0, y, kMainWidth, height) CellIdentifier:identifier CellClassName:@"WCRHousingSourcesCell" ModelClassName:@"WCRRoomModel" ConfigureCellBlock:^(WCRHousingSourcesCell *cell, NSIndexPath *indexPath, WCRRoomModel *model) {
        @strongify(self);
        cell.model = model;
        cell.lineView.hidden = self.viewModel.dataSource.count - 1 == indexPath.row ? YES : NO;
    }];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    self.tableView.delegate = self;
    self.tableView.rowHeight = [WCRHousingSourcesCell cellHeight];
    [self.view addSubview:self.tableView];
    
    [self.viewModel tableViewSetRefresh:self.tableView HeaderRefresh:^{
        @strongify(self);
        [self getRoomListDataFromNetwork:LoadListType_First];
    } FooterRefresh:^{
        @strongify(self);
        [self getRoomListDataFromNetwork:LoadListType_Next];
    }];
    
    [self getRoomListDataFromNetwork:LoadListType_First];
}

- (void)getRoomListDataFromNetwork:(LoadListType)type {
    @weakify(self);
    // @"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],
    self.viewModel.parameters = @{
                                  @"cityId":self.conditionModel.cityId,
                                  @"districeId":self.conditionModel.districeId,
                                  @"rentType":self.conditionModel.rentType,
                                  @"room":self.conditionModel.room,
                                  @"houseType":self.conditionModel.houseType
                                  };
    [self.viewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        @strongify(self);
        [self.viewModel loadListDataCompleteReload:self.tableView IsEnd:isEnd];
    }];
}

#pragma mark - 搜索
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    WCRHousingSourcesSearchVC *searchVC = [[WCRHousingSourcesSearchVC alloc] init];
    searchVC.cityId = self.conditionModel.cityId;
    [self.navigationController pushViewController:searchVC animated:YES];
    return NO;
}

#pragma mark - 选择房源类型 ：地区 租金 户型 物业性质
- (void)roomSelectAction:(UIButton *)button {
    
    if (button.tag >= self.allDatas.count) {
        return;
    }
    
    NSArray *subModels = [self.allDatas objectAtIndex:button.tag];
    
    @weakify(self);
    WCRPullDownMenuView *muneView = [WCRPullDownMenuView popMenuViewWithClickViewBottomY:button.bottom_wcr MenuModels:subModels ClickActionBlock:^(WCRMenuModel *model) {
        @strongify(self);
        NSString *title = model.name;
        switch (button.tag) {
            case RoomSelectTypeRegion:
                self.conditionModel.districeId = model.ID;
                title = [title isEqualToString:@"不限"] ? @"地区" : title;
                self.conditionModel.districe = title;
                break;
            case RoomSelectTypeRent:
                self.conditionModel.rentType = model.ID;
                title = [title isEqualToString:@"不限"] ? @"租金" : title;
                self.conditionModel.rentTypeString = title;
                break;
            case RoomSelectTypeHouseType:
                self.conditionModel.room = model.ID;
                title = [title isEqualToString:@"不限"] ? @"户型" : title;
                self.conditionModel.roomString = title;
                break;
            case RoomSelectTypeTenement:
                self.conditionModel.houseType = model.ID;
                title = [title isEqualToString:@"不限"] ? @"物业性质" : title;
                self.conditionModel.houseTypeString = title;
                break;
        }
        
        [button setTitle:title forState:0];
        [button setImagePosition:1 spacing:2];
        
        [self getRoomListDataFromNetwork:LoadListType_First];
    }];
    
    switch (button.tag) {
        case RoomSelectTypeRegion:
            [muneView setSelectWithMenuModelId:self.conditionModel.districeId];
            break;
        case RoomSelectTypeRent:
            [muneView setSelectWithMenuModelId:self.conditionModel.rentType];
            break;
        case RoomSelectTypeHouseType:
            [muneView setSelectWithMenuModelId:self.conditionModel.room];
            break;
        case RoomSelectTypeTenement:
            [muneView setSelectWithMenuModelId:self.conditionModel.houseType];
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRHousingResourcesDetailVC *detailVC = [[WCRHousingResourcesDetailVC alloc] init];
    detailVC.roomModel = [_viewModel modelAtIndexPath:indexPath];
    [self.navigationController pushViewController:detailVC animated:YES];
}

@end
