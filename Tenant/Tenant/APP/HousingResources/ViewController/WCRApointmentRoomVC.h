//
//  WCRApointmentRoomVC.h
//  Tenant
//
//  Created by eliot on 2017/5/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class WCRRoomModel;
/// 预约看房
@interface WCRApointmentRoomVC : WCRBasicViewController
@property (nonatomic, strong) WCRRoomModel *roomModel;
@end
