//
//  WCRSelectCityVC.m
//  Tenant
//
//  Created by eliot on 2017/5/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRSelectCityVC.h"
#import "UITextField+WCRCustom.h"
#import "NPGeneralApi.h"
#import "WCRBMKTool.h"
#import "NSString+WCRCustom.h"
#import "WCRLetterSortTool.h"
#import "WCRLetterSortModel.h"
#import "RACSubject.h"
#import "WCRCityCell.h"
#import "WCRSelectedCitySearchVC.h"

#define RECENTCITY @"recentCity"
#define RECENTBUTTONSTARTINDEX 302
#define SECTIONHEIGHT 30

@interface WCRCitySectionView : UITableViewHeaderFooterView
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIView *lineView;
@end
@implementation WCRCitySectionView
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        _label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, SECTIONHEIGHT)];
        [_label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_label];
        _lineView = [UIView lineView];
        _lineView.frame = CGRectMake(0, _label.bottom_wcr - 1, kMainWidth, 1);
        [self.contentView addSubview:_lineView];
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}
@end

static NSString *identifier = @"cell";

@interface WCRSelectCityVC ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (nonatomic, strong) UITextField *searchTextField;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *rencentCities;
@property (nonatomic, strong) NSMutableArray *allDataSource;
@property (nonatomic, strong) UIView *rightIndexView; // 右边的索引栏
@property (nonatomic, strong) NSMutableArray *indexs; // 索引数组
@property (nonatomic, strong) NSMutableArray *dataSource; // 分好组的数组：[@{index,models},]
@end

@implementation WCRSelectCityVC

- (void)viewDidLoad {
    [super viewDidLoad];

    _allDataSource = [NSMutableArray array];
    _indexs = [NSMutableArray array];
    _dataSource = [NSMutableArray array];
    _rencentCities = [NSMutableArray arrayWithCapacity:4];
    [_rencentCities addObjectsFromArray:[WCRLetterSortModel coderModelArrayWithKey:RECENTCITY]];
    [self addLeftGoBackItemWithTitle:@"选择城市"];
    [self createUI];
    [self getCityDataFromNetwork];
}

- (void)getCityDataFromNetwork {
    @weakify(self);
    NSDictionary *dict = nil;//@{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/indexApiController/getCityList" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            NSArray *array = [WCRLetterSortModel jsonWithArray:responseMd.response];
            [self.allDataSource removeAllObjects];
            [array enumerateObjectsUsingBlock:^(WCRLetterSortModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.name.length && obj.ID.length) {
                    [self.allDataSource addObject:obj];
                }
            }];
            [WCRLetterSortTool sortWithModelArray:array Block:^(NSArray<WCRLetterSortModel *> *resultArray, NSArray *letters) {
                [self.indexs addObjectsFromArray:letters];
                [self.dataSource addObjectsFromArray:resultArray];
                // 自定义右边的索引栏
                [self customIndexColumnWithIndexs:self.indexs];
                [self.tableView reloadData];
            }];
        }
    }];
}

- (void)createUI {
    [self addTopView];
    
    CGFloat y = 60;
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, y, kMainWidth, kMainHeight - 64 - y) style:UITableViewStylePlain];
    _tableView = tableView;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    
    [self addTableViewHeader];
    
    [tableView registerClass:[WCRCityCell class] forCellReuseIdentifier:identifier];
    tableView.rowHeight = kDefaultHeight;
    tableView.sectionFooterHeight = 0.001;
}
#pragma mark - 自定义右边的索引栏
- (void)customIndexColumnWithIndexs:(NSMutableArray *)allIndexs {
    CGFloat width = 20;
    CGFloat x = kMainWidth - width;
    CGFloat y = 60;
    CGFloat height = kMainHeight  - 64 - y - 30;
    
    if (!_rightIndexView) {
        _rightIndexView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, height)];
        _rightIndexView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_rightIndexView];
    }
    [_rightIndexView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    height = height / allIndexs.count;
    if (height > 20) {
        height = 20;
    }

    // 居中显示
    y = (_rightIndexView.height_wcr - allIndexs.count * height) / 2.0;
    for (int i = 0; i < allIndexs.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, y, width, height);
        [button buttonWithTitleColor:[UIColor colorWithHexString:kCustomGrayColor] Background:nil Font:[UIFont CustomFontWithSize:11.0] Title:[allIndexs objectAtIndex:i]];
        button.tag = i;
        [button addTarget:self action:@selector(selectedIndex:)];
        [_rightIndexView addSubview:button];
        y = button.maxY_wcr;
    }
}
- (void)selectedIndex:(UIButton *)button {
    NSInteger index = button.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)addTableViewHeader {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 0.1)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UIFont *font = [UIFont CustomFontWithSize:12.0];
    // 当前定位的城市
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = kMainWidth;
    CGFloat height = 35;
    UITextField *locationTF = [[UITextField alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [locationTF textFieldWithFont:font PlaceHolder:nil LeftLabelText:nil Border:0];
    locationTF.backgroundColor = self.view.backgroundColor;
    locationTF.text = @"当前定位城市";
    locationTF.textColor = [UIColor colorWithHexString:kCustomGrayColor];
    locationTF.userInteractionEnabled = NO;
    [headerView addSubview:locationTF];
    
    NSString *city = [WCRBMKTool sharedInstance].city;
    if (!city.length) {
        city = @"定位失败";
    }
    font = [UIFont CustomFontWithSize:14.0];
    x = kSpaceX;
    y = locationTF.bottom_wcr;
    height = kDefaultHeight;
    width = kMainWidth - kSpaceX * 2;
    UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [locationLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:city];
    [headerView addSubview:locationLabel];
    
    UITextField *recentAccessTF = [[UITextField alloc] initWithFrame:locationTF.frame];
    recentAccessTF.y_wcr = locationLabel.bottom_wcr;
    [recentAccessTF textFieldWithFont:locationTF.font PlaceHolder:nil LeftLabelText:nil Border:0];
    recentAccessTF.backgroundColor = self.view.backgroundColor;
    recentAccessTF.text = @"最近访问城市";
    recentAccessTF.textColor = [UIColor colorWithHexString:kCustomGrayColor];
    recentAccessTF.userInteractionEnabled = NO;
    [headerView addSubview:recentAccessTF];
    
    // 最近访问的城市，最多四个
    font = locationLabel.font;
    x = kSpaceX;
    width = font.pointSize * 4;
    height = font.lineHeight + 4;
    y = recentAccessTF.bottom_wcr + (locationLabel.height_wcr - height) / 2.0;
    for (NSInteger i = 0; i < _rencentCities.count; i++) {
        UIButton *buttton = [UIButton buttonWithType:UIButtonTypeCustom];
        buttton.frame = CGRectMake(x, y, width, height);
        WCRLetterSortModel *cityModel = [_rencentCities objectAtIndex:i];
        [buttton buttonWithTitleColor:nil Background:nil Font:font Title:cityModel.name];
        [buttton setLayerWithBorderWidth:1.0 Corner:2.0 BorderColor:[UIColor lineColor]];
        buttton.tag = i + RECENTBUTTONSTARTINDEX;
        [buttton addTarget:self action:@selector(clickRecentAccessCity:)];
        [headerView addSubview:buttton];
        x = buttton.right_wcr + 10;
    }
    
    headerView.height_wcr = recentAccessTF.bottom_wcr + kDefaultHeight;
    self.tableView.tableHeaderView = headerView;
}
- (void)addTopView {
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    // 搜索
    CGFloat x = kSpaceX;
    CGFloat y = 10;
    CGFloat width = kMainWidth - x * 2;
    CGFloat height = 40;
    _searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [_searchTextField textFieldWithFont:font Placeholder:@"搜索城市" LeftImageName:@"icon_sousuo_a" ImageWidth:14 Border:kSpaceX];
    _searchTextField.delegate = self;
    _searchTextField.returnKeyType = UIReturnKeySearch;
    _searchTextField.backgroundColor = [UIColor whiteColor];
    [_searchTextField setViewCorner:3.0];
    [self.view addSubview:_searchTextField];
}
#pragma mark - Table View DataSorce
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.indexs.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *subModels = [self.dataSource objectAtIndex:section];
    return subModels.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRCityCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    NSArray *subModels = [self.dataSource objectAtIndex:indexPath.section];
    WCRLetterSortModel *model = [subModels objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}
#pragma mark - Table View Delegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *sectionIdentifier = @"section";
    WCRCitySectionView *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionIdentifier];
    if (!sectionView) {
        sectionView = [[WCRCitySectionView alloc] initWithReuseIdentifier:identifier];
    }
    sectionView.label.text = [self.indexs objectAtIndex:section];
    return sectionView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return SECTIONHEIGHT;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSArray *subModels = [self.dataSource objectAtIndex:indexPath.section];
    WCRLetterSortModel *model = [subModels objectAtIndex:indexPath.row];
    [self cacheModel:model Pop:YES];
}

#pragma mark - 点击最近访问的城市
- (void)clickRecentAccessCity:(UIButton *)button {
    NSInteger index = button.tag - RECENTBUTTONSTARTINDEX;
    if (index >= _rencentCities.count) {
        return;
    }
    WCRLetterSortModel *model = [_rencentCities objectAtIndex:index];
    [self cacheModel:model Pop:YES];
}
- (void)cacheModel:(WCRLetterSortModel *)model Pop:(BOOL)isPop {
    
    __block WCRLetterSortModel *tempModel = nil;
    [self.rencentCities enumerateObjectsUsingBlock:^(WCRLetterSortModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.ID isEqualToString:model.ID]) {
            tempModel = obj;
            *stop = YES;
        }
    }];
    if (tempModel) {
        [self.rencentCities removeObject:tempModel];
    }
    
    [self.rencentCities insertObject:model atIndex:0];
    if (self.rencentCities.count > 4) {
        [self.rencentCities removeObjectsInRange:NSMakeRange(4, self.rencentCities.count - 4)];
    }
    
    [WCRLetterSortModel encodeModelArray:self.rencentCities key:RECENTCITY];
    [self.selectSignal sendNext:model];
    
    if (isPop) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark - 搜索城市
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    WCRSelectedCitySearchVC *searchVC = [[WCRSelectedCitySearchVC alloc] init];
    searchVC.allCityModels = self.allDataSource;
    searchVC.selectSignal = [RACSubject subject];
    @weakify(self);
    [searchVC.selectSignal subscribeNext:^(WCRLetterSortModel *model) {
        @strongify(self);
        [self cacheModel:model Pop:NO];
    }];
    [self.navigationController pushViewController:searchVC animated:YES];
    return NO;
}

@end
