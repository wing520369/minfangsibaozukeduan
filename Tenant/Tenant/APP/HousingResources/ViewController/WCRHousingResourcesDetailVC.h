//
//  WCRHousingResourcesDetailVC.h
//  Tenant
//
//  Created by 吴传荣 on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"
@class WCRRoomModel;
/// 房源详情
@interface WCRHousingResourcesDetailVC : WCRBasicViewController
@property (nonatomic, strong) WCRRoomModel *roomModel;
@end
