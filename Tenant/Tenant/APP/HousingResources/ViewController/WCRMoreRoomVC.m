//
//  WCRMoreRoomDetailVC.m
//  Tenant
//
//  Created by eliot on 2017/5/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMoreRoomVC.h"
#import "UIViewController+WCRCustom.h"
#import "WCRRoomDetailModel.h"
#import "WCRMoreRoomCell.h"
#import "WCRMoreRoomModel.h"
#import "NPGeneralApi.h"
#import "NPTip.h"
#import "RACSubject.h"
#import "WCRHouseModel.h"

@interface WCRMoreRoomSectionHeaderView : UICollectionReusableView
@property (nonatomic, strong) UILabel *label;
+ (CGFloat)sectionHeaderHeight;
@end
@implementation WCRMoreRoomSectionHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
        grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
        [self addSubview:grayView];
        _label = [[UILabel alloc] initWithFrame:CGRectMake(kSpaceX, grayView.bottom_wcr, kMainWidth - kSpaceX * 2, 45)];
        [_label labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
        [self addSubview:_label];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
+ (CGFloat)sectionHeaderHeight {
    return 55;
}
@end

static NSString *identifier = @"cell";
static NSString *sectionIdentifier = @"sectionView";

@interface WCRMoreRoomVC ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *firstDataSources;
@property (nonatomic, strong) NSMutableArray *secondDataSource;
@property (nonatomic, strong) WCRMoreRoomModel *selectRoomTypeModel;
@property (nonatomic, strong) WCRMoreRoomModel *selectRoomModel;
@end

@implementation WCRMoreRoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"更多房号"];
    [self addRightBarButtonItemTitle:@"确定"];
    [self initailData];
    [self createUI];
    [self getRoomDataFromNetwork];
}

- (void)rightBarButtonItemAction:(UIButton *)button {
    if (!self.selectRoomModel) {
        [NPTip showTip:@"请选择房号"];
        return;
    }
    [self.selectRoomSignal sendNext:self.selectRoomModel.ID];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initailData {
    NSArray *rooms = @[@"1室",@"2室",@"3室",@"4室",@"5室",@"5室以上"];
    NSArray *roomIds = @[@"1",@"2",@"3",@"4",@"5",@"6"];
    __block NSMutableArray *dataSource = [NSMutableArray arrayWithCapacity:rooms.count];
    [rooms enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *Id = [roomIds objectAtIndex:idx];
        WCRMoreRoomModel *model = [[WCRMoreRoomModel alloc] initWithId:Id Name:obj];
        [dataSource addObject:model];
    }];
    self.firstDataSources = [dataSource copy];
    self.selectRoomTypeModel = [self.firstDataSources firstObject];
    self.selectRoomTypeModel.isSelected = YES;
    
    self.secondDataSource = [NSMutableArray array];
}

- (void)createUI {
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(kMainWidth / 4.0, [WCRMoreRoomCell cellHeight]);
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.headerReferenceSize = CGSizeMake(kMainWidth, [WCRMoreRoomSectionHeaderView sectionHeaderHeight]);
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.showsHorizontalScrollIndicator = NO;
    [_collectionView registerClass:[WCRMoreRoomCell class] forCellWithReuseIdentifier:identifier];
    
    [_collectionView registerClass:[WCRMoreRoomSectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:sectionIdentifier];
    
    [self.view addSubview:_collectionView];
}

- (void)getRoomDataFromNetwork {
    @weakify(self);
    // @"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],
    NSDictionary *dict = @{@"houseId":self.roomDetailModel.house.ID,@"room":self.selectRoomTypeModel.ID};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/roomApiController/getRoomNo" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            NSArray *array = [WCRMoreRoomModel jsonWithArray:responseMd.response];
            [self.secondDataSource removeAllObjects];
            [self.secondDataSource addObjectsFromArray:array];
            [self.collectionView reloadData];
        }
    }];
    
}

#pragma mark - Collection View dataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return _firstDataSources.count;
    }
    return _secondDataSource.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WCRMoreRoomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (indexPath.section == 0) {
        cell.model = [_firstDataSources objectAtIndex:indexPath.item];
    }
    else {
        cell.model = [_secondDataSource objectAtIndex:indexPath.item];
    }
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        WCRMoreRoomSectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:sectionIdentifier forIndexPath:indexPath];
        NSString *string = indexPath.section == 0 ? @"房型" : @"房号";
        headerView.label.text = string;
        return headerView;
    }
    return nil;
}

#pragma mark - Collection View Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        // 选择房型
        WCRMoreRoomModel *firstModel = [_firstDataSources objectAtIndex:indexPath.item];
        if ([firstModel.ID isEqualToString:_selectRoomTypeModel.ID]) {
            return;
        }
        _selectRoomTypeModel.isSelected = NO;
        firstModel.isSelected = YES;
        _selectRoomTypeModel = firstModel;
        
        _selectRoomModel = nil;
        [_secondDataSource removeAllObjects];
        
        [collectionView reloadData];
        [self getRoomDataFromNetwork];
    }
    else {
       
        // 选择房号
        WCRMoreRoomModel *secondModel = [_secondDataSource objectAtIndex:indexPath.item];
        if ([self.selectRoomModel.ID isEqualToString:secondModel.ID]) {
            return;
        }
        secondModel.isSelected = YES;
        _selectRoomModel.isSelected = NO;
        _selectRoomModel = secondModel;
        [collectionView reloadData];
    }
}

@end
