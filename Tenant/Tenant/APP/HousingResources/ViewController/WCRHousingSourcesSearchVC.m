//
//  WCRHousingSourcesSearchVC.m
//  Tenant
//
//  Created by eliot on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRHousingSourcesSearchVC.h"
#import "UITextField+WCRCustom.h"
#import "BasicListViewModel.h"
#import "WCRHousingSourcesCell.h"
#import "WCRRoomModel.h"
#import "WCRHousingResourcesDetailVC.h"

static NSString *identifier = @"cell";

@interface WCRHousingSourcesSearchVC ()<UITextFieldDelegate,UITableViewDelegate>
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) NSString *searchText;
@property (nonatomic, strong) BasicListViewModel *viewModel;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRHousingSourcesSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:nil];
    [self createUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:_textField];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_textField removeFromSuperview];
}
- (void)createUI {
    
    UIFont *font = [UIFont CustomFontWithSize:13.0];
    
    CGFloat y = 5;
    CGFloat x = 50;
    CGFloat width = kMainWidth - x - kSpaceX;
    CGFloat height = 44 - y * 2;
    UITextField *searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, width, height)];
    _textField = searchTextField;
    [searchTextField textFieldWithFont:font Placeholder:@"想住哪儿呢" LeftImageName:@"icon_sousuo_a" ImageWidth:font.lineHeight Border:10];
    searchTextField.delegate = self;
    searchTextField.returnKeyType = UIReturnKeyDone;
    [searchTextField setLayerWithBorderWidth:1.0 Corner:3.0 BorderColor:[UIColor lineColor]];
    [searchTextField addTarget:self action:@selector(textFieldValueChange:) forControlEvents:UIControlEventEditingChanged];
    [self.navigationController.navigationBar addSubview:searchTextField];
    
    _viewModel = [[BasicListViewModel alloc] initWithParameters:nil tailUrl:@"/api/roomApiController/getQueryList"];
    
    @weakify(self);
    self.tableView = [self.viewModel createTableViewWithFrame:CGRectMake(0, y, kMainWidth, kMainHeight - 64) CellIdentifier:identifier CellClassName:@"WCRHousingSourcesCell" ModelClassName:@"WCRRoomModel" ConfigureCellBlock:^(WCRHousingSourcesCell *cell, NSIndexPath *indexPath, WCRRoomModel *model) {
        @strongify(self);
        cell.model = model;
        cell.lineView.hidden = self.viewModel.dataSource.count - 1 == indexPath.row ? YES : NO;
    }];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    self.tableView.delegate = self;
    self.tableView.rowHeight = [WCRHousingSourcesCell cellHeight];
    [self.view addSubview:self.tableView];
    
    [self.viewModel tableViewSetRefresh:self.tableView HeaderRefresh:^{
        @strongify(self);
        [self getRoomDataFromNetwork:LoadListType_First];
    } FooterRefresh:^{
        @strongify(self);
        [self getRoomDataFromNetwork:LoadListType_Next];
    }];
}

- (void)getRoomDataFromNetwork:(LoadListType)type {
    if (!self.searchText.length) {
        return;
    }
    @weakify(self);
    // @"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],
    self.viewModel.parameters = @{@"cityId":self.cityId,@"keyword":self.searchText};
    [self.viewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        @strongify(self);
        [self.viewModel loadListDataCompleteReload:self.tableView IsEnd:isEnd];
    }];
}


- (void)textFieldValueChange:(UITextField *)textField {
    
    NSString *lang = [self.textInputMode primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"] || [lang isEqualToString:@"zh-Hant"]) {
        // 简体中文输入，包括简体拼音，简体五笔，简体手写(zh-Hans)
        // 繁体中文输入，包括繁体拼音，繁体五笔，繁体手写(zh-Hant)
        UITextRange *selectedRange = [textField markedTextRange];
        // 获取高亮部分（联想部分）
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有联想，则对已输入的文字进行字数统计和限制
        if (!position) {
            
        }
        // 有联想，则暂不对联想的文字进行统计
        else {
            return;
        }
    }
    
    self.searchText = textField.text;
    [self getRoomDataFromNetwork:LoadListType_First];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRHousingResourcesDetailVC *detailVC = [[WCRHousingResourcesDetailVC alloc] init];
    detailVC.roomModel = [_viewModel modelAtIndexPath:indexPath];
    [self.navigationController pushViewController:detailVC animated:YES];
}

@end
