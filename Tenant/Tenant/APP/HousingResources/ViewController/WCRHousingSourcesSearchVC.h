//
//  WCRHousingSourcesSearchVC.h
//  Tenant
//
//  Created by eliot on 2017/5/24.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

/// 房源 搜索
@interface WCRHousingSourcesSearchVC : WCRBasicViewController
@property (nonatomic, copy) NSString *cityId;
@end
