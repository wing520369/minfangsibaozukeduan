//
//  WCRSelectConditionModel.m
//  Tenant
//
//  Created by eliot on 2017/6/8.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRSelectConditionModel.h"

@implementation WCRSelectConditionModel

- (instancetype)init {
    if (self = [super init]) {
        
        self.city = @"广州";
        self.cityId = @"198";
        
        self.districe = @"地区";
        self.districeId = @"";
        
        self.rentType = @"";
        self.rentTypeString = @"租金";
        
        self.room = @"";
        self.roomString = @"户型";
        
        self.houseType = @"";
        self.houseTypeString = @"物业性质";
    }
    return self;
}

@end
