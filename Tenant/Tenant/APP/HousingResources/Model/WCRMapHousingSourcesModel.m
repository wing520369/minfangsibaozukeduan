//
//  WCRMapHousingSourcesModel.m
//  Tenant
//
//  Created by eliot on 2017/6/8.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMapHousingSourcesModel.h"
#import "WCRHouseModel.h"
#import "WCRRoomModel.h"

@implementation WCRMapHousingSourcesModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{@"house":[WCRHouseModel class],@"rooms":[WCRRoomModel class]};
}
@end
