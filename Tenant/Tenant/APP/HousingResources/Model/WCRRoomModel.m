//
//  WCRRoomModel.m
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRoomModel.h"

@implementation WCRRoomModel
+ (NSDictionary *)objectClassInArray{
    return @{@"roomImgs" : [WCRRoomImagesModel class]};
}

- (NSString *)orientationName {
    // 房屋朝向 1-东 2-南 3-西 4-北 5-南北 6-东西 7-东南 8-西南 9-东北 10-西北
    NSString *name = @"东";
    switch ([self.orientation integerValue]) {
        case 1:
            name = @"东";
            break;
        case 2:
            name = @"南";
            break;
        case 3:
            name = @"西";
            break;
        case 4:
            name = @"北";
            break;
        case 5:
            name = @"南北";
            break;
        case 6:
            name = @"东西";
            break;
        case 7:
            name = @"东南";
            break;
        case 8:
            name = @"西南";
            break;
        case 9:
            name = @"东北";
            break;
        case 10:
            name = @"西北";
            break;
        default:
            break;
    }
    return name;
}

@end

@implementation WCRRoomImagesModel

@end
