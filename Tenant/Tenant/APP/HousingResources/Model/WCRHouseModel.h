//
//  WCRHouseModel.h
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

@interface WCRHouseModel : WCRBasicModel
@property (nonatomic, strong) NSString * batchSwitch;
@property (nonatomic, strong) NSString * cityId;
@property (nonatomic, strong) NSString * cityName;
@property (nonatomic, strong) NSString * createDate;
/// 房产描述
@property (nonatomic, strong) NSString * describe;
@property (nonatomic, strong) NSString * districeId;
@property (nonatomic, strong) NSString * districeName;
@property (nonatomic, strong) NSString * houseImg;
@property (nonatomic, strong) NSString * houseName;
@property (nonatomic, strong) NSString * houseType;
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * lat;
@property (nonatomic, strong) NSString * lng;
@property (nonatomic, strong) NSString * provinceId;
@property (nonatomic, strong) NSString * provinceName;
@property (nonatomic, strong) NSString * rank;
@property (nonatomic, strong) NSString * roomPrefix;
@property (nonatomic, strong) NSString * rooms;
@property (nonatomic, strong) NSString * streetAddress;
@property (nonatomic, strong) NSString * tierNumber;
@property (nonatomic, strong) NSString * totalRoom;
@property (nonatomic, strong) NSString * totalTier;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * updateDate;
@property (nonatomic, strong) NSString * usedRoom;
@property (nonatomic, strong) NSString * userId;
@end
