//
//  WCRHousingSourcesModel.h
//  Tenant
//
//  Created by eliot on 2017/5/23.
//  strongright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRRoomModel;
/// 房源 model
@interface WCRHousingSourcesModel : WCRBasicModel
/// 热门房源
@property (nonatomic, strong) NSArray<WCRRoomModel *> * hotRoom;
/// 推荐房源
@property (nonatomic, strong) NSArray<WCRRoomModel *> * recRoom;
/// 交易额
@property (nonatomic, strong) NSString * turnover;
/// 会员数
@property (nonatomic, strong) NSString * uCount;
@end
