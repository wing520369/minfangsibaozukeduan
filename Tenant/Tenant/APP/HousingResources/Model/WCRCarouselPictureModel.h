//
//  WCRCarouselPictureModel.h
//  Tenant
//
//  Created by eliot on 2017/5/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

/// 轮播图
@interface WCRCarouselPictureModel : WCRBasicModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *name;
@end
