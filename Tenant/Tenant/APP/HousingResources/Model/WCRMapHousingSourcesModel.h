//
//  WCRMapHousingSourcesModel.h
//  Tenant
//
//  Created by eliot on 2017/6/8.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRHouseModel, WCRRoomModel;
@interface WCRMapHousingSourcesModel : WCRBasicModel
@property (nonatomic, strong) NSString * count;
@property (nonatomic, strong) WCRHouseModel * house;
@property (nonatomic, strong) NSArray<WCRRoomModel *> * rooms;
@end
