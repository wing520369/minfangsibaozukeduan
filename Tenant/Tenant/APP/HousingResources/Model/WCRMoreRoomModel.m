//
//  WCRMoreRoomModel.m
//  Tenant
//
//  Created by eliot on 2017/5/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRMoreRoomModel.h"

@implementation WCRMoreRoomModel
- (instancetype)initWithId:(NSString *)Id Name:(NSString *)name {
    if (self = [super init]) {
        self.ID = Id;
        self.name = name;
        self.isSelected = NO;
    }
    return self;
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID":@"roomId",@"name":@"roomNo"};
}
@end
