//
//  WCRRoomDetailModel.h
//  Tenant
//
//  Created by eliot on 2017/5/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRHouseModel,WCRRoomModel;

/// 房源详情 : 房产 房号 房东
@interface WCRRoomDetailModel : WCRBasicModel
/// 房东评价星
@property (nonatomic, strong) NSString * hostComment;
/// 房东名字
@property (nonatomic, strong) NSString * hostName;
/// 房东电话
@property (nonatomic, strong) NSString * hostPhone;
/// 房东头像
@property (nonatomic, strong) NSString * hostAvatar;
/// 房产
@property (nonatomic, strong) WCRHouseModel * house;
/// 房号
@property (nonatomic, strong) WCRRoomModel * room;
/// 房东微信
@property (nonatomic, strong) NSString * wxNo;
/// 收藏 0否 1是,
@property (nonatomic, strong) NSString * collect;
@end
