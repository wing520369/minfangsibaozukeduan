//
//  WCRMoreRoomModel.h
//  Tenant
//
//  Created by eliot on 2017/5/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
/// 更多房号 model
@interface WCRMoreRoomModel : WCRBasicModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL isSelected;
- (instancetype)initWithId:(NSString *)Id Name:(NSString *)name;
@end
