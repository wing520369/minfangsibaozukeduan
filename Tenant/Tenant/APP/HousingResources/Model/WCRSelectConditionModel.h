//
//  WCRSelectConditionModel.h
//  Tenant
//
//  Created by eliot on 2017/6/8.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

/// 房源列表 筛选条件model
@interface WCRSelectConditionModel : WCRBasicModel
/// 市
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *cityId;
/// 区
@property (nonatomic, copy) NSString *districe;
@property (nonatomic, copy) NSString *districeId;
/// 租金 类型
@property (nonatomic, copy) NSString *rentType;
@property (nonatomic, copy) NSString *rentTypeString;
/// 户型(室)
@property (nonatomic, copy) NSString *room;
@property (nonatomic, copy) NSString *roomString;
/// 物业性质（房产类型）
@property (nonatomic, copy) NSString *houseType;
@property (nonatomic, copy) NSString *houseTypeString;
@end
