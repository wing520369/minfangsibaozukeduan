//
//  WCRRoomModel.h
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRRoomImagesModel;
@interface WCRRoomModel : WCRBasicModel
/// 0：在租    1：待评价   2：已退租
@property (nonatomic, strong) NSString *type;
/// 房屋朝向
@property (nonatomic, strong) NSString * orientation;
/// 房间照片
@property (nonatomic, strong) NSArray<WCRRoomImagesModel *> * roomImgs;
/// 租金
@property (nonatomic, strong) NSString * rent;
/// 房间描述
@property (nonatomic, strong) NSString * describe;
/// 房源设施
@property (nonatomic, strong) NSString * facility;
/// 房产id
@property (nonatomic, strong) NSString * houseId;
/// 房产名
@property (nonatomic, strong) NSString * houseName;
/// 厅
@property (nonatomic, strong) NSString * hall;
/// 房屋朝向 1-东 2-南 3-西 4-北 5-南北 6-东西 7-东南 8-西南 9-东北 10-西北
@property (nonatomic, strong) NSString * orientationName;
/// 付
@property (nonatomic, strong) NSString * pay;
/// 押
@property (nonatomic, strong) NSString * pledge;
/// 室
@property (nonatomic, strong) NSString * room;
/// 房屋面积
@property (nonatomic, strong) NSString * roomArea;
/// 第几层
@property (nonatomic, strong) NSString * tier;
/// 卫
@property (nonatomic, strong) NSString * toilet;
/// 总层数
@property (nonatomic, strong) NSString * totalTier;
/// 房源名称
@property (nonatomic, strong) NSString * roomName;
/// 房号
@property (nonatomic, strong) NSString * roomNo;
/// 状态 0-闲置 1-已发布 2-居住
@property (nonatomic, strong) NSString * status;
/// 城市ID
@property (nonatomic, strong) NSString * cityId;
/// 城市名
@property (nonatomic, strong) NSString * cityName;
@property (nonatomic, strong) NSString * createDate;
/// 区ID
@property (nonatomic, strong) NSString * districeId;
/// 区名
@property (nonatomic, strong) NSString * districeName;
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * lat;
@property (nonatomic, strong) NSString * lng;
@property (nonatomic, strong) NSString * provinceName;
@property (nonatomic, strong) NSString * rank;
@property (nonatomic, strong) NSString * rentType;
@property (nonatomic, strong) NSString * streetAddress;
@property (nonatomic, strong) NSString * updateDate;
@end

@interface WCRRoomImagesModel : WCRBasicModel

@property (nonatomic, strong) NSString * createDate;
@property (nonatomic, strong) NSString * ID;
@property (nonatomic, strong) NSString * img;
@property (nonatomic, strong) NSString * objectId;
@property (nonatomic, strong) NSString * rank;
@property (nonatomic, strong) NSString * remarks;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * updateDate;

@end
