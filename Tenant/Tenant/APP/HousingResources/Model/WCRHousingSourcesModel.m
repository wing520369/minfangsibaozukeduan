//
//  WCRHousingSourcesModel.m
//  Tenant
//
//  Created by eliot on 2017/5/23.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRHousingSourcesModel.h"
#import "WCRRoomModel.h"

@implementation WCRHousingSourcesModel
+ (NSDictionary *)objectClassInArray{
    return @{@"hotRoom" : [WCRRoomModel class], @"recRoom" : [WCRRoomModel class]};
}
@end
