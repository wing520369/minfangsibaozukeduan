//
//  WCRIntegralMallVC.h
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

/// 积分商城
@interface WCRIntegralMallVC : WCRBasicViewController
/// 0 首页积分商城 1 从其他界面跳转过来
@property (nonatomic, strong) NSString *type;
@end
