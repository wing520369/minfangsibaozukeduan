//
//  WCRExchangeRecordVC.m
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRExchangeRecordVC.h"
#import "WCRIntegraMallCell.h"
#import "WCRIntegralMallModel.h"
#import "BasicListViewModel.h"
#import "UIScrollView+NPRefresh.h"
#import "DZNEmptyDataProvider.h"
#import "NPGeneralApi.h"
#import "UIViewController+WCRCustom.h"
#import "Tools.h"
#import "WCRIntegralMallModel.h"

static NSString *identifier = @"cell";

@interface WCRExchangeRecordVC ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) BasicListViewModel *listViewModel;
@property (nonatomic, strong) NSString *phone;
@end

@implementation WCRExchangeRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"兑换记录"];
    [self getCustomPhoneFromNetwork];
    [self createUI];
}

- (void)rightBarButtonItemAction:(UIButton *)button {
    [Tools dialPhoneNumber:self.phone];
}

- (void)createUI {
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(kMainWidth / 2.0, [WCRIntegraMallCell cellHeight]);
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.showsHorizontalScrollIndicator = NO;
    [_collectionView registerClass:[WCRIntegraMallCell class] forCellWithReuseIdentifier:identifier];
    [self.view addSubview:_collectionView];
    
    __weak typeof(self) weakSelf = self;
    [_collectionView MJHeaderWithRefreshingBlock:^{
        [weakSelf getDataFromNetworkWithType:LoadListType_First];
    }];
    [_collectionView MJFooterWithRefreshingBlock:^{
        [weakSelf getDataFromNetworkWithType:LoadListType_Next];
    }];
    
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    _listViewModel = [[BasicListViewModel alloc] initWithParameters:dict tailUrl:@"/api/publicApiController/getexchangeGoodsInfo"];
    _listViewModel.modelClass = [WCRIntegralMallModel class];
    [self getDataFromNetworkWithType:LoadListType_First];
}

- (void)getCustomPhoneFromNetwork {
    __weak typeof(self) weakSelf = self;
    NSDictionary *dict = nil;//@{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiShowFailureTipsBgViewWithParameters:dict tailUrl:@"/api/publicApiController/getCustomerService" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            if ([responseMd.response isKindOfClass:[NSDictionary class]]) {
                weakSelf.phone = [responseMd.response objectForKey:@"phone"];
                [weakSelf addRightBarButtonItemTitle:@"联系客服"];
            }
        }
    }];
}

- (void)getDataFromNetworkWithType:(LoadListType)type {
    
    __weak typeof(self) weakSelf = self;
    [_listViewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
        weakSelf.collectionView.mj_footer.hidden = isEnd;
        
        if (!weakSelf.collectionView.emptyDataProvider) {
            weakSelf.collectionView.emptyDataProvider = [DZNEmptyDataProvider providerWithEmptyType:DZNEmptyDataType_NoData];
        }
        
        [weakSelf.collectionView reloadData];
    }];
}

#pragma mark - Collection View dataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _listViewModel.dataSource.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WCRIntegraMallCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    WCRIntegralMallModel *recordModel = [_listViewModel.dataSource objectAtIndex:indexPath.item];
    cell.recordModel = recordModel;
    cell.changeButton.enabled = NO;
    if ((indexPath.item + 1) % 2 == 0) {
        cell.middleLineView.hidden = YES;
    }
    else {
        cell.middleLineView.hidden = NO;
    }
    return cell;
}

@end
