//
//  WCRIntegralMallVC.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRIntegralMallVC.h"
#import "UINavigationController+WCRCustom.h"
#import "WCRIntegraMallCell.h"
#import "WCRIntegralMallModel.h"
#import "WCRIntegraMallHeaderView.h"
#import "BasicListViewModel.h"
#import "UIScrollView+NPRefresh.h"
#import "DZNEmptyDataProvider.h"
#import "NPGeneralApi.h"
#import "NPTip.h"
#import "UIAlertView+RACSignalSupport.h"
#import "RACSignal.h"
#import "Tools.h"

static NSString *identifier = @"cell";
static NSString *headerViewIdentifier = @"headerView";

@interface WCRIntegralMallVC ()<UICollectionViewDataSource,UICollectionViewDelegate, WCRIntegraMallCellDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) BasicListViewModel *listViewModel;
@property (nonatomic, strong) NSMutableArray *imageUrls;
@end

@implementation WCRIntegralMallVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setOtherStyle];
    if ([self.type integerValue] == 1) {
        [self addLeftGoBackItemWithTitle:@"积分商城"];
    }
    _imageUrls = [NSMutableArray array];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)createUI {
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(kMainWidth / 2.0, [WCRIntegraMallCell cellHeight]);
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.headerReferenceSize = CGSizeMake(kMainWidth, [WCRIntegraMallHeaderView headerHeight]);
    
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64 - 49) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.showsHorizontalScrollIndicator = NO;
    [_collectionView registerClass:[WCRIntegraMallCell class] forCellWithReuseIdentifier:identifier];
    [_collectionView registerClass:[WCRIntegraMallHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerViewIdentifier];
    [self.view addSubview:_collectionView];
    
    __weak typeof(self) weakSelf = self;
    [_collectionView MJHeaderWithRefreshingBlock:^{
        [weakSelf getCycleImageFromNetwork];
        [weakSelf getDataFromNetworkWithType:LoadListType_First];
    }];
    [_collectionView MJFooterWithRefreshingBlock:^{
        [weakSelf getDataFromNetworkWithType:LoadListType_Next];
    }];
    
    NSDictionary *dict = nil;//@{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    _listViewModel = [[BasicListViewModel alloc] initWithParameters:dict tailUrl:@"/api/publicApiController/getStoreList"];
    _listViewModel.modelClass = [WCRIntegralMallModel class];
    [self getDataFromNetworkWithType:LoadListType_First];
    [self getCycleImageFromNetwork];
}

- (void)getDataFromNetworkWithType:(LoadListType)type {
    
    __weak typeof(self) weakSelf = self;
    [_listViewModel loadListDataWithType:type completion:^(NPGeneralResponse *responseMd, NSInteger page, BOOL isEnd) {
        
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
        weakSelf.collectionView.mj_footer.hidden = isEnd;
        
        if (!weakSelf.collectionView.emptyDataProvider) {
            weakSelf.collectionView.emptyDataProvider = [DZNEmptyDataProvider providerWithEmptyType:DZNEmptyDataType_NoData];
        }
        
        [weakSelf.collectionView reloadData];
    }];
}

- (void)getCycleImageFromNetwork {
    __weak typeof(self) weakSelf = self;
    [NPGeneralApi WCRApiWithParameters:nil tailUrl:@"/api/publicApiController/goodsShowList" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            NSArray *array = responseMd.response;
            [weakSelf.imageUrls removeAllObjects];
            [array enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString *imageUrl = [dict objectForKey:@"img"];
                [weakSelf.imageUrls addObject:imageUrl];
            }];
            [weakSelf.collectionView reloadData];
        }
    }];
}

#pragma mark - Collection View dataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _listViewModel.dataSource.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WCRIntegraMallCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.model = [_listViewModel.dataSource objectAtIndex:indexPath.item];
    if ((indexPath.item + 1) % 2 == 0) {
        cell.middleLineView.hidden = YES;
    }
    else {
        cell.middleLineView.hidden = NO;
    }
    cell.delegate = self;
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        WCRIntegraMallHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:headerViewIdentifier forIndexPath:indexPath];
        [headerView setCycleImageViewWithUrls:self.imageUrls];
        [headerView updateUserInfo];
        return headerView;
    }
    return nil;
}

- (void)integraMallCellExchangeGoods:(WCRIntegralMallModel *)goodsModel {
    
    if (![Tools checkIsLogin]) {
        return;
    }
    
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    __block long score = [userModel.score longLongValue];
    if (goodsModel.score > score) {
        [NPTip showTip:@"积分不足，不能兑换"];
        return;
    }
    
    @weakify(self);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定使用积分兑换此商品" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView.rac_buttonClickedSignal subscribeNext:^(NSNumber * _Nullable x) {
        if ([x integerValue] == 1) {
            NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken],@"goodsId":goodsModel.ID};
            [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/publicApiController/exchangeGoods" ResponseBlock:^(NPGeneralResponse *responseMd) {
                if (responseMd.isOK) {
                    @strongify(self);
                    score -= goodsModel.score;
                    userModel.score = [NSString stringWithFormat:@"%ld",(long)score];
                    [WCRUserModelTool saveUserInfo:userModel];
                    [NPTip showTip:@"兑换成功"];
                    [self.collectionView reloadData];
                }
            }];
        }
    }];
    [alertView show];
}

@end
