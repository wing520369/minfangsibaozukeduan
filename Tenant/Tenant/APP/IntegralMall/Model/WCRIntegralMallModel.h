//
//  WCRIntegralMallModel.h
//  __projectName__
//
//  Created by aleven on 17/05/17.
//  Copyright © 2017年 eliot. All rights reserved.
//

#import "WCRBasicModel.h"

/// 积分商城Model
@interface WCRIntegralMallModel : WCRBasicModel
@property (nonatomic, copy) NSString * ID;
/// 商品描述
@property (nonatomic, copy) NSString * describe;
/// 商品图片
@property (nonatomic, copy) NSString * img;
@property (nonatomic, copy) NSString * goodsImg;
/// 商品名称
@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * goodsName;
/// 商品数量
@property (nonatomic, assign) long number;
/// 商品积分
@property (nonatomic, assign) long score;
@property (nonatomic, assign) long goodsScore;
/// 商品状态 0 下架 1 上架
@property (nonatomic, copy) NSString * status;
@end
