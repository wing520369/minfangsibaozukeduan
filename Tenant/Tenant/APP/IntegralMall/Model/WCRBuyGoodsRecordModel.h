//
//  WCRBuyGoodsRecordModel.h
//  __projectName__
//
//  Created by aleven on 17/05/17.
//  Copyright © 2017年 eliot. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRIntegralMallModel;
/// 兑换记录
@interface WCRBuyGoodsRecordModel : WCRBasicModel

@property (nonatomic, copy) NSString * createDate;
@property (nonatomic, copy) NSString * exchangeDate;
@property (nonatomic, strong) NSArray<WCRIntegralMallModel *> * goods;
@property (nonatomic, copy) NSString * goodsId;
@property (nonatomic, copy) NSString * goodsName;
@property (nonatomic, copy) NSString * ID;
@property (nonatomic, copy) NSString * remarks;
@property (nonatomic, copy) NSString * status;
@property (nonatomic, copy) NSString * updateDate;
@property (nonatomic, copy) NSString * userId;

@end
