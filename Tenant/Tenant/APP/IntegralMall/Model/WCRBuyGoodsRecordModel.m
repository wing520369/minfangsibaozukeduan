//
//  WCRBuyGoodsRecordModel.m
//  __projectName__
//
//  Created by aleven on 17/05/17.
//  Copyright © 2017年 eliot. All rights reserved.
//

#import "WCRBuyGoodsRecordModel.h"
#import "WCRIntegralMallModel.h"

@implementation WCRBuyGoodsRecordModel
+ (NSDictionary *)objectClassInArray{
    return @{@"goods" : [WCRIntegralMallModel class]};
}
@end
