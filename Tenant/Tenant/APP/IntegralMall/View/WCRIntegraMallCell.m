//
//  WCRIntegraMallCell.m
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRIntegraMallCell.h"
#import "WCRIntegralMallModel.h"
#import "UIImageView+WebCache.h"
#import "NSMutableAttributedString+WCRCustom.h"
#import "NPGeneralApi.h"
#import "NPTip.h"

@interface WCRIntegraMallCell ()
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *integraLabel;
@end

@implementation WCRIntegraMallCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // 30 + 60 + 30 + 40 + 10 + 20 + 10
        
        CGFloat y = 30;
        CGFloat height = 60;
        CGFloat width = height * (200 / 120.0);
        CGFloat x = (frame.size.width - width) / 2.0;
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_iconImageView setContentModeToScaleAspectFill];
        [self.contentView addSubview:_iconImageView];
        
        UIFont *font = [UIFont CustomFontWithSize:13.0];
        
        y = _iconImageView.bottom_wcr + y;
        x = kSpaceX;
        width = frame.size.width - x * 2;
        height = 40;
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_nameLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        _nameLabel.numberOfLines = 2;
        [self.contentView addSubview:_nameLabel];
        
        font = [UIFont CustomFontWithSize:12.0];
        NSString *title = @"立即兑换";
        y = _nameLabel.bottom_wcr + 10;
        height = 20;
        width = font.pointSize * title.length + 10;
        _changeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _changeButton.frame = CGRectMake(x, y, width, height);
        [_changeButton buttonWithTitleColor:[UIColor whiteColor] Background:nil Font:font Title:title];
        [_changeButton addTarget:self action:@selector(changeGood:)];
        [_changeButton setEnlargeEdgeWithTop:20 Right:20 Bottom:10 Left:10];
        [_changeButton setViewCorner:3.0];
        [_changeButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:kMainColor]] forState:0];
        [_changeButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRGB:194 G:194 B:194]] forState:UIControlStateDisabled];
        [_changeButton setTitle:@"已兑换" forState:UIControlStateDisabled];
        [self.contentView addSubview:_changeButton];
        
        x = _changeButton.right_wcr + 2;
        width = frame.size.width - x - kSpaceX;
        _integraLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_integraLabel labelWithTextColor:[UIColor colorWithHexString:kCustomBlueColor] Font:font BackgroundColor:nil Text:nil];
        _integraLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_integraLabel];
        
        _bottomLineView = [self getLineWithFrame:CGRectMake(0, frame.size.height - 1, frame.size.width, 1)];
        
        _middleLineView = [self getLineWithFrame:CGRectMake(frame.size.width - 1, 0, 1, frame.size.height)];
        
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (UIView *)getLineWithFrame:(CGRect)frame {
    UIView *lineView = [UIView lineView];
    lineView.frame = frame;
    [self.contentView addSubview:lineView];
    return lineView;
}

+ (CGFloat)cellHeight {
    return 30 + 60 + 30 + 40 + 10 + 20 + 10;
}

- (void)setModel:(WCRIntegralMallModel *)model {
    _model = model;
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:[UIImage imageFileNamed:@"icon_tupian"]];
    _nameLabel.text = model.name;
    
    NSString *text = [NSString stringWithFormat:@"%ld积分",(long)model.score];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:text];
    [attr setColor:[UIColor blackColor] range:NSMakeRange(text.length - 2, 2)];
    _integraLabel.attributedText = attr;
}

- (void)setRecordModel:(WCRIntegralMallModel *)recordModel {
    _recordModel = recordModel;
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:recordModel.goodsImg] placeholderImage:[UIImage imageFileNamed:@"icon_tupian"]];
    _nameLabel.text = recordModel.goodsName;
    
    NSString *text = [NSString stringWithFormat:@"%ld积分",(long)recordModel.goodsScore];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:text];
    [attr setColor:[UIColor blackColor] range:NSMakeRange(text.length - 2, 2)];
    _integraLabel.attributedText = attr;
}

#pragma mark - 立即兑换
- (void)changeGood:(UIButton *)button {
    
    if (_delegate && [_delegate respondsToSelector:@selector(integraMallCellExchangeGoods:)]) {
        [_delegate integraMallCellExchangeGoods:_model];
    }
}

@end
