//
//  WCRIntegraMallHeaderView.h
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WCRIntegraMallHeaderView : UICollectionReusableView
- (void)updateUserInfo;
- (void)setCycleImageViewWithUrls:(NSMutableArray *)urls;
+ (CGFloat)headerHeight;
@end
