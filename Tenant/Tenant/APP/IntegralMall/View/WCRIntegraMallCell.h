//
//  WCRIntegraMallCell.h
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRIntegralMallModel;

@protocol WCRIntegraMallCellDelegate <NSObject>

- (void)integraMallCellExchangeGoods:(WCRIntegralMallModel *)goodsModel;

@end

/// 积分商城cell
@interface WCRIntegraMallCell : UICollectionViewCell
@property (nonatomic, weak) id<WCRIntegraMallCellDelegate> delegate;
/// 商品列表的model
@property (nonatomic, strong) WCRIntegralMallModel *model;
/// 兑换列表的model
@property (nonatomic, strong) WCRIntegralMallModel *recordModel;
@property (nonatomic, strong) UIButton *changeButton;
@property (nonatomic, strong) UIView *middleLineView;
@property (nonatomic, strong) UIView *bottomLineView;
+ (CGFloat)cellHeight;
@end
