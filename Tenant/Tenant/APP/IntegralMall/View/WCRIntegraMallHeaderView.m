//
//  WCRIntegraMallHeaderView.m
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRIntegraMallHeaderView.h"
#import "SDCycleScrollView.h"
#import "UIImageView+WebCache.h"
#import "Tools.h"

typedef NS_ENUM(NSInteger, ButtonActionType) {
    ButtonActionTypeRecord = 1207,
    ButtonActionTypeRule,
};

@interface WCRIntegraMallHeaderView ()<SDCycleScrollViewDelegate>
@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *integraLabel;
@property (nonatomic, strong) SDCycleScrollView *sdScrollView;
@end

@implementation WCRIntegraMallHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        CGFloat x = kSpaceX;
        CGFloat y = 15;
        CGFloat width = 35;
        CGFloat height = 35;
        _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_avatarImageView setContentModeToScaleAspectFill];
        [_avatarImageView setViewCorner:height / 2.0];
        [self addSubview:_avatarImageView];
        
        UIFont *font = [UIFont CustomFontWithSize:15.0];
        
        x = _avatarImageView.right_wcr + kSpaceX;
        width = kMainWidth - x - kSpaceX;
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_nameLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        [self addSubview:_nameLabel];
        
        font = [UIFont CustomFontWithSize:13.0];
        height = font.lineHeight + 5;
        y = _avatarImageView.bottom_wcr - height;
        _integraLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_integraLabel labelWithTextColor:[UIColor colorWithHexString:kMainColor] Font:font BackgroundColor:nil Text:nil];
        _integraLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:_integraLabel];
        
        UIView *lineView = [UIView lineView];
        lineView.frame = CGRectMake(0, _avatarImageView.bottom_wcr + _avatarImageView.y_wcr, kMainWidth, 1);
        [self addSubview:lineView];
        
        y = lineView.bottom_wcr;
        // 兑换记录 积分规则
        x = 0;
        width = kMainWidth / 2.0;
        height = 40;
        NSArray *titles = @[@"兑换记录",@"积分规则"];
        NSArray *imageNames = @[@"0517icon_duihuanjilu",@"0517icon_integral-rule"];
        NSArray *tags = @[@(ButtonActionTypeRecord),@(ButtonActionTypeRule)];
        for (NSInteger i = 0; i < titles.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(x, y, width, height);
            [button buttonWithTitleColor:nil Background:nil Font:font Title:[titles objectAtIndex:i]];
            [button setImage:[UIImage imageFileNamed:[imageNames objectAtIndex:i]] forState:0];
            button.tag = [[tags objectAtIndex:i] integerValue];
            [button setImagePosition:0 spacing:8];
            [button addTarget:self action:@selector(buttonAction:)];
            [self addSubview:button];
            
            UIView *lineView = [UIView lineView];
            lineView.frame = CGRectMake(button.right_wcr, y, 1, height);
            [self addSubview:lineView];
            
            x = lineView.right_wcr;
        }
        
        y += height;
        UIView *lineView1 = [UIView lineView];
        lineView1.frame = CGRectMake(0, y, kMainWidth, 1);
        [self addSubview:lineView1];
        
        x = 0;
        y = lineView1.bottom_wcr;
        width = kMainWidth;
        height = width * (160 / 375.0);
        // 轮播图
        CGRect frame = CGRectMake(x, y, width, height);
        _sdScrollView = [SDCycleScrollView cycleScrollViewWithFrame:frame delegate:self placeholderImage:nil];
        _sdScrollView.backgroundColor = [UIColor clearColor];
        _sdScrollView.currentPageDotColor = [UIColor colorWithHexString:kMainColor];
        _sdScrollView.pageDotColor = [UIColor whiteColor];
        _sdScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_sdScrollView];
        
        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, _sdScrollView.bottom_wcr, kMainWidth, 10)];
        grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
        [self addSubview:grayView];
        [grayView addLineViewPositionType:kBottom_Type];
    }
    return self;
}

+ (CGFloat)headerHeight {
    return 15 + 35 + 15 + 40 + kMainWidth * (160 / 375.0) + 10;
}

#pragma mark - 兑换记录 积分规则
- (void)buttonAction:(UIButton *)button {
    NSString *className = nil;
    if (button.tag == ButtonActionTypeRecord) {
        className = @"WCRExchangeRecordVC";
        if (![Tools checkIsLogin]) {
            return;
        }
    }
    else {
        className = @"WCRIntegraMallRuleVC";
    }
    UIViewController *vc = [[NSClassFromString(className) alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [[self getCurrentViewController].navigationController pushViewController:vc animated:YES];
}

- (void)setCycleImageViewWithUrls:(NSMutableArray *)urls {
    _sdScrollView.imageURLStringsGroup = urls;
}

- (void)updateUserInfo {
    WCRUserModel *userModel = [WCRUserModelTool getUserInfo];
    [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:userModel.avatar] placeholderImage:[UIImage imageFileNamed:@"icon_geren"]];
    _nameLabel.text = userModel.nickname.length ? userModel.nickname : userModel.phone;
    
    NSString *score = userModel.score.length ? userModel.score : @"";
    _integraLabel.text = [NSString stringWithFormat:@"积分：%@",score];
}

#pragma mark - 点击轮播图
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
}

@end
