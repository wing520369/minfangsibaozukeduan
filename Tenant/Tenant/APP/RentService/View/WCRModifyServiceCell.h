//
//  WCRAddHouseFirstCell.h
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 添加房产：名称，类型，照片，地址
@class WCRModifyServiceModel;
@interface WCRModifyServiceCell : UITableViewCell
@property (nonatomic, strong) WCRModifyServiceModel *model;
@end
