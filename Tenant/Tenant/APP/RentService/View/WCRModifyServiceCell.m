//
//  WCRAddHouseFirstCell.m
//  Landlord
//
//  Created by eliot on 2017/4/25.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRModifyServiceCell.h"
#import "WCRModifyServiceModel.h"
#import "WCRSelectHouseAndRoomVC.h"
#import "UITextField+WCRCustom.h"

@interface WCRModifyServiceCell ()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *grayView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIImageView *arrowImageView;
@end

@implementation WCRModifyServiceCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _grayView = [[UIView alloc] initWithFrame:CGRectZero];
        _grayView.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
        [self.contentView addSubview:_grayView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_titleLabel labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14.0] BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_titleLabel];
        
        _textField = [[UITextField alloc] initWithFrame:CGRectZero];
        _textField.textAlignment = NSTextAlignmentRight;
        _textField.font = [UIFont CustomFontWithSize:14.0];
        _textField.delegate = self;
        [self.contentView addSubview:_textField];
        
        [_textField addDoneButtonCloseKeyBoard];
        
        _lineView = [UIView lineView];
        _lineView.frame = CGRectZero;
        [self.contentView addSubview:_lineView];
        
        _arrowImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
        [self.contentView addSubview:_arrowImageView];
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setModel:(WCRModifyServiceModel *)model {
    _model = model;
    _titleLabel.text = model.title;
    _textField.placeholder = model.placeholder;
    _textField.text = model.content;
    _arrowImageView.hidden = !model.isPop;
    _lineView.hidden = !model.haveLine;
    if (model.type == ModifyServiceTypePhone) {
        _textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    else {
        _textField.keyboardType = UIKeyboardTypeDefault;
    }
    
    [self updateFrame];
}

- (void)updateFrame {

    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = kMainWidth;
    CGFloat height = _model.startY;
    _grayView.frame = CGRectMake(x, y, width, height);
    
    x = kSpaceX;
    y = _grayView.bottom_wcr;
    height = _model.cellHeight - _model.startY;
    width = _titleLabel.text.length * _titleLabel.font.pointSize;
    _titleLabel.frame = CGRectMake(x, y, width, height);
    
    x = _titleLabel.right_wcr;
    width = kMainWidth - x - kSpaceX;
    if (!_arrowImageView.hidden) {
        width -= 20;
    }
    _textField.frame = CGRectMake(x, y, width, height);
    
    width = 13;
    height = 13;
    y = _model.startY + (_model.cellHeight - _model.startY - height) / 2.0;
    x = kMainWidth - width - kSpaceX;
    _arrowImageView.frame = CGRectMake(x, y, width, height);
    
    x = kSpaceX;
    width = kMainWidth - x * 2;
    y = _model.cellHeight - 1;
    _lineView.frame = CGRectMake(x, y, width, 1);
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    BOOL flag = YES;
    if (_model.type == ModifyServiceTypeRoomNo) {
        WCRSelectHouseAndRoomVC *selectVC = [[WCRSelectHouseAndRoomVC alloc] init];
        selectVC.type = @"0";
        [[self getCurrentViewController].navigationController pushViewController:selectVC animated:YES];
        flag = NO;
    }
    return flag;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    _model.content = textField.text;
}

@end
