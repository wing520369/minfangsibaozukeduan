//
//  WCRModifyServiceModel.h
//  Tenant
//
//  Created by eliot on 2017/5/27.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

typedef NS_ENUM(NSInteger, ModifyServiceType) {
    /// 选择房号
    ModifyServiceTypeRoomNo = 420,
    /// 姓名
    ModifyServiceTypeName,
    /// 联系电话
    ModifyServiceTypePhone,
    /// 标题
    ModifyServiceTypeTitle,
    /// 内容
    ModifyServiceTypeContent,
};

@interface WCRModifyServiceModel : WCRBasicModel
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *placeholder;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) CGFloat startY;
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, assign) BOOL haveLine;
@property (nonatomic, assign) BOOL isPop;
@property (nonatomic, assign) ModifyServiceType type;

@property (nonatomic, copy) NSString *key;

@property (nonatomic, copy) NSString *houseId;
@property (nonatomic, copy) NSString *roomId;

+ (NSArray *)getModifyServiceModels;
@end
