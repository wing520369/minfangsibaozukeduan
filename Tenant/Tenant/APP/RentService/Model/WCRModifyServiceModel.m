//
//  WCRModifyServiceModel.m
//  Tenant
//
//  Created by eliot on 2017/5/27.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRModifyServiceModel.h"

@implementation WCRModifyServiceModel

+ (NSArray *)getModifyServiceModels {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"WCRModifyServiceModel.plist" ofType:nil];
    NSArray *models = [WCRModifyServiceModel jsonWithArray:[NSArray arrayWithContentsOfFile:path]];
    return [models copy];
}
@end
