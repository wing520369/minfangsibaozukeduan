//
//  WCRRentServiceModel.m
//  Tenant
//
//  Created by eliot on 2017/5/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRentServiceModel.h"

@implementation WCRRentServiceModel

+ (NSArray *)getModels {
    
    WCRRentServiceModel *modifyModel = [[WCRRentServiceModel alloc] initWithImageName:@"icon_baoxiu_a" ClassName:@"WCRModifyServiceVC" Type:RentServiceTypeModify Title:@"报修信息"];
    
    WCRRentServiceModel *intergeModel = [[WCRRentServiceModel alloc] initWithImageName:@"icon_jifen" ClassName:@"WCRIntegralMallVC" Type:RentServiceTypeIntegralMall Title:@"积分商城"];
    
    WCRRentServiceModel *conpouModel = [[WCRRentServiceModel alloc] initWithImageName:@"icon_fangyuanguanli" ClassName:@"WCRMyRentVC" Type:RentServiceTypeMyRent Title:@"房租管理"];
    
    WCRRentServiceModel *serviceModel = [[WCRRentServiceModel alloc] initWithImageName:@"icon_wodekefu" ClassName:@"WCRMyCustomerVC" Type:RentServiceTypeMyCustomer Title:@"我的客服"];
    
    WCRRentServiceModel *lifeModel = [[WCRRentServiceModel alloc] initWithImageName:@"icon_shenghuofuwu" ClassName:@"WCRLifeServiceVC" Type:RentServiceTypeLifeService Title:@"生活服务"];
    
    WCRRentServiceModel *exitRentModel = [[WCRRentServiceModel alloc] initWithImageName:@"0527icon_check" ClassName:@"WCRTenementMainVC" Type:RentServiceTypeTenement Title:@"退租"];
    
    return @[modifyModel,intergeModel,conpouModel,serviceModel,lifeModel,exitRentModel];
}

- (instancetype)initWithImageName:(NSString *)imageName ClassName:(NSString *)className Type:(RentServiceType)type Title:(NSString *)title {
    if (self = [super init]) {
        self.imageName = imageName;
        self.className = className;
        self.type = type;
        self.title = title;
    }
    return self;
}

@end
