//
//  WCRRentServiceModel.h
//  Tenant
//
//  Created by eliot on 2017/5/26.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"

typedef NS_ENUM(NSInteger,RentServiceType) {
    /// 报修信息
    RentServiceTypeModify = 0,
    /// 积分商城
    RentServiceTypeIntegralMall,
    /// 房租管理
    RentServiceTypeMyRent,
    /// 我的客服
    RentServiceTypeMyCustomer,
    /// 生活服务
    RentServiceTypeLifeService,
    /// 退租
    RentServiceTypeTenement,
};

/// 租房服务 model
@interface WCRRentServiceModel : WCRBasicModel
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *className;
@property (nonatomic, assign) RentServiceType type;
+ (NSArray *)getModels;
@end
