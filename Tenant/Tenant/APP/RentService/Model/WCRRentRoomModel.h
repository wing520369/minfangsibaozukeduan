//
//  WCRRentRoomModel.h
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicModel.h"
@class WCRRoomModel;
/// 租客 在租 房号
@interface WCRRentRoomModel : WCRBasicModel
@property (nonatomic, strong) NSString * beginRental;
@property (nonatomic, strong) NSString * endRental;
@property (nonatomic, strong) NSString * hostName;
@property (nonatomic, strong) NSNumber * rent;
@property (nonatomic, strong) WCRRoomModel * room;
@end
