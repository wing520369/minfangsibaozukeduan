//
//  WCRRentServiceVC.m
//  Tenant
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRRentServiceVC.h"
#import "UINavigationController+WCRCustom.h"
#import "SDCycleScrollView.h"
#import "NPGeneralApi.h"
#import "WCRCarouselPictureModel.h"
#import "WCRRentServiceModel.h"
#import "WCRTopImageButton.h"
#import "UIScrollView+NPRefresh.h"
#import "WCRIntegralMallVC.h"
#import "Tools.h"

@interface WCRRentServiceVC ()<SDCycleScrollViewDelegate>
@property (nonatomic, strong) SDCycleScrollView *sdScrollView;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRRentServiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self getDataFromNetwork];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController setDefaultStyle];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)getDataFromNetwork {
    // 首页轮播图
    @weakify(self);
    [NPGeneralApi WCRApiWithParameters:nil tailUrl:@"/api/publicApiController/upShowList" ResponseBlock:^(NPGeneralResponse *responseMd) {
        @strongify(self);
        if (responseMd.isOK) {
            NSArray *models = [WCRCarouselPictureModel jsonWithArray:responseMd.response];
            __block NSMutableArray *urls = [NSMutableArray arrayWithCapacity:models.count];
            [models enumerateObjectsUsingBlock:^(WCRCarouselPictureModel * obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [urls addObject:obj.img];
            }];
            self.sdScrollView.imageURLStringsGroup = [urls copy];
        }
        [self.tableView.mj_header endRefreshing];
        [self.tableView reloadData];
    }];
}
- (void)createUI {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 64)];
    bgView.backgroundColor = [UIColor colorWithHexString:kMainColor];
    [self.view addSubview:bgView];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, kMainWidth, 44)];
    [label labelWithTextColor:[UIColor whiteColor] Font:[UIFont CustomBoldFontWithSize:17.0] BackgroundColor:nil Text:@"租房服务"];
    label.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:label];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, bgView.bottom_wcr, kMainWidth, kMainHeight - 49) style:UITableViewStylePlain];
    _tableView = tableView;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 0.1)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // 轮播图
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = kMainWidth;
    CGFloat height = (385 / 750.0) * width;
    _sdScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(x, y, width, height) delegate:self placeholderImage:nil];
    _sdScrollView.backgroundColor = [UIColor clearColor];
    _sdScrollView.currentPageDotColor = [UIColor colorWithHexString:kMainColor];
    _sdScrollView.pageDotColor = [UIColor whiteColor];
    _sdScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    [headerView addSubview:_sdScrollView];
    [_sdScrollView addLineViewPositionType:kBottom_Type];
    
    UIFont *font = [UIFont CustomFontWithSize:15.0];
    NSArray *models = [WCRRentServiceModel getModels];
    y = _sdScrollView.bottom_wcr;
    width = kMainWidth / 3.0;
    height = width;
    for (NSInteger i = 0; i < models.count; i++) {
        WCRRentServiceModel *model = [models objectAtIndex:i];
        WCRTopImageButton *button = [[WCRTopImageButton alloc] initWithFrame:CGRectMake(x, y, width, height) ImageSize:CGSizeMake(25, 25) TitleColor:[UIColor blackColor] Font:font Title:model.title];
        [button setImage:[UIImage imageFileNamed:model.imageName] forState:0];
        [button addTarget:self action:@selector(rentServiceButtonAction:)];
        [headerView addSubview:button];
        [button addLineViewPositionType:kBottom_Type];
        button.tag = i;
        x = button.right_wcr;
        
        UIView *lineView = [UIView lineView];
        lineView.frame = CGRectMake(button.width_wcr - 1, 0, 1, button.height_wcr);
        [button addSubview:lineView];
        
        if ((i + 1) % 3 == 0) {
            x = 0;
            y = button.bottom_wcr;
            lineView.hidden = YES;
        }
        if (i == models.count - 1) {
            y = button.bottom_wcr;
        }
    }
    
    headerView.height_wcr = y;
    tableView.tableHeaderView = headerView;
    
    @weakify(self);
    [tableView MJHeaderWithRefreshingBlock:^{
        @strongify(self);
        [self getDataFromNetwork];
    }];
}

- (void)rentServiceButtonAction:(UIButton *)button {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    NSArray *models = [WCRRentServiceModel getModels];
    WCRRentServiceModel *model = [models objectAtIndex:button.tag];
    UIViewController *vc = [[NSClassFromString(model.className) alloc] init];
    if ([model.className isEqualToString:@"WCRIntegralMallVC"]) {
        ((WCRIntegralMallVC *)vc).type = @"1";
    }
    
    if (model.type == RentServiceTypeModify || model.type == RentServiceTypeMyRent || model.type == RentServiceTypeTenement) {
        if (![Tools checkIsLogin]) {
            return;
        }
    }
    
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 点击轮播图
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
    
}

@end
