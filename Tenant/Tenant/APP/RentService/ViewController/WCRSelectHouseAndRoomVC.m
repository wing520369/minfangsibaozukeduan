//
//  WCRSelectHouseAndRoomVC.m
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRSelectHouseAndRoomVC.h"
#import "WCROneRowCell.h"
#import "UIViewController+WCRCustom.h"
#import "NPGeneralApi.h"
#import "WCRRoomModel.h"
#import "NPTip.h"
#import "WCRHouseModel.h"
#import "WCRRentRoomModel.h"
#import "WCRDataSourceModel.h"

static NSString *identifier = @"cell";

@interface WCRSelectHouseAndRoomVC ()<UITableViewDelegate>
@property (nonatomic, strong) NSMutableDictionary *allRoomModelsDict;
@property (nonatomic, strong) WCRDataSourceModel *dataSourceModel;
@property (nonatomic, strong) NSMutableArray *roomModels;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation WCRSelectHouseAndRoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLeftGoBackItemWithTitle:@"选择房产房号"];
    [self createUI];
    if ([_type integerValue] == 0) {
        [self getDataFromNetwork];
    }
    else {
       [self.dataSourceModel.dataSource addObjectsFromArray:self.roomModels];
       [self.tableView reloadData];
    }
}

- (void)getDataFromNetwork {
    
    _allRoomModelsDict = [NSMutableDictionary dictionary];
    // 获取租客所在租的房号
    @weakify(self);
    NSDictionary *dict = @{@"userId":[WCRUserModelTool getUserId],@"tokenName":[WCRUserModelTool getToken]};
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/leaveApiController/getRentRoom" ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            NSArray *array = [WCRRentRoomModel jsonWithArray:responseMd.response];
            __block NSMutableArray *houseModels = [NSMutableArray arrayWithCapacity:array.count];
            __block NSMutableDictionary *mutabeDict = [NSMutableDictionary dictionary];
            [array enumerateObjectsUsingBlock:^(WCRRentRoomModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                // 房产
                WCRHouseModel *houseModel = [[WCRHouseModel alloc] init];
                houseModel.ID = obj.room.houseId;
                houseModel.houseName = obj.room.houseName;
                
                // 房号
                if ([mutabeDict objectForKey:houseModel.ID]) {
                    [[mutabeDict objectForKey:houseModel.ID] addObject:obj.room];
                }
                else {
                    
                    [houseModels addObject:houseModel];
                    
                    NSMutableArray *subRoomModels = [NSMutableArray array];
                    [subRoomModels addObject:obj.room];
                    [mutabeDict setObject:subRoomModels forKey:houseModel.ID];
                }
            }];
            self.allRoomModelsDict = [mutabeDict mutableCopy];
            [self.dataSourceModel.dataSource addObjectsFromArray:houseModels];
            [self.tableView reloadData];
        }
    }];
}

- (void)createUI {

    WCRDataSourceModel *dataSourceModel = [[WCRDataSourceModel alloc] init];
    _dataSourceModel = dataSourceModel;
    // 创建tableView
    @weakify(self);
    _tableView = [dataSourceModel createTableViewWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) CellIdentifier:identifier CellClassName:@"WCROneRowCell" ConfigureCellBlock:^(WCROneRowCell *cell, NSIndexPath *indexPath, id model) {
        @strongify(self);
        if ([model isKindOfClass:[WCRHouseModel class]]) {
            cell.title = ((WCRHouseModel *)model).houseName;
        }
        else if ([model isKindOfClass:[WCRRoomModel class]]) {
            cell.title = ((WCRRoomModel *)model).roomNo;
        }
        cell.arrowImageView.hidden = [self.type integerValue] == 0 ? NO : YES;
    }];
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
    
    // 头视图
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 10)];
    headerView.backgroundColor = [UIColor clearColor];
    _tableView.tableHeaderView = headerView;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_type integerValue] == 0) {
        // 选择房号
        WCRSelectHouseAndRoomVC *exchangeVC = [[WCRSelectHouseAndRoomVC alloc] init];
        exchangeVC.type = @"1";
        WCRHouseModel *houseModel = [self.dataSourceModel.dataSource objectAtIndex:indexPath.row];
        exchangeVC.roomModels = [self.allRoomModelsDict objectForKey:houseModel.ID];
        [self.navigationController pushViewController:exchangeVC animated:YES];
    }
    else {
        
        WCRRoomModel *roomModel = [self.roomModels objectAtIndex:indexPath.row];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"selectRoomComplete" object:nil userInfo:@{@"roomModel":roomModel}];
        
        // 选择完成
        UIViewController *vc = [UINavigationController FindSpecificViewController:self.navigationController.viewControllers outViewController:NSClassFromString(@"WCRModifyServiceVC")];
        if (vc) {
            [self.navigationController popToViewController:vc animated:YES];
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

@end
