//
//  WCRSelectHouseAndRoomVC.h
//  Tenant
//
//  Created by eliot on 2017/5/31.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRBasicViewController.h"

/// 选择房产 房号
@interface WCRSelectHouseAndRoomVC : WCRBasicViewController
/// type:0：房产  1：房号
@property (nonatomic, copy) NSString *type;
@end
