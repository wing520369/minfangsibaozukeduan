//
//  WCRModifyServiceVC.m
//  Tenant
//
//  Created by eliot on 2017/5/27.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRModifyServiceVC.h"
#import "WCRModifyServiceCell.h"
#import "WCRModifyServiceModel.h"
#import "SAMTextView.h"
#import "WCRMutilImageViews.h"
#import "WCRRoomModel.h"
#import "NPTip.h"
#import "NPGeneralApi.h"
#import "RegExpValidate.h"
#import "TPKeyboardAvoidingTableView.h"
#import "UITextView+WCRCustom.h"

static NSString *identifier = @"cell";

@interface WCRModifyServiceVC ()<UITableViewDataSource,UITableViewDelegate,WCRMutilImageViewsDelegate>
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) TPKeyboardAvoidingTableView *tableView;
@property (nonatomic, strong) WCRMutilImageViews *mutilImageViews;
@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) SAMTextView *textView;
@end

@implementation WCRModifyServiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _images = [NSMutableArray arrayWithCapacity:8];
    [self addLeftGoBackItemWithTitle:@"报修服务"];
    [self createUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectRoomComplete:) name:@"selectRoomComplete" object:nil];
}
- (void)selectRoomComplete:(NSNotification *)noti {
    WCRRoomModel *roomModel = [noti.userInfo objectForKey:@"roomModel"];
    [self.dataSource enumerateObjectsUsingBlock:^(WCRModifyServiceModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.type == ModifyServiceTypeRoomNo) {
            obj.roomId = roomModel.ID;
            obj.houseId = roomModel.houseId;
            obj.content = [NSString stringWithFormat:@"房产：%@ - 房号：%@",roomModel.houseName,roomModel.roomNo];
            *stop = YES;
        }
    }];
    [self.tableView reloadData];
}
- (void)createUI {
    
    NSArray *models = [WCRModifyServiceModel getModifyServiceModels];
    _dataSource = [NSMutableArray arrayWithCapacity:models.count];
    [_dataSource addObjectsFromArray:models];
    
    TPKeyboardAvoidingTableView *tableView = [[TPKeyboardAvoidingTableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64 - 60) style:UITableViewStylePlain];
    _tableView = tableView;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[WCRModifyServiceCell class] forCellReuseIdentifier:identifier];
    
    [self addTableViewFooter];
    
    // 提交
    UIButton *button = [UIButton getButtonWithYpoint:kMainHeight - 64 - 50 Title:@"提交"];
    [button addTarget:self action:@selector(completeAction)];
    [self.view addSubview:button];
}

// 内容 图片
- (void)addTableViewFooter {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 0.01)];
    _footerView = footerView;
    footerView.backgroundColor = [UIColor whiteColor];
    
    UIView *whiteBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 0.001)];
    
    UIFont *font = [UIFont CustomFontWithSize:14.0];
    // 内容：
    CGFloat x = kSpaceX;
    CGFloat y = 0;
    CGFloat width = kMainWidth - x * 2;
    CGFloat height = 45;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label labelWithTextColor:nil Font:font BackgroundColor:nil Text:@"内容："];
    [whiteBgView addSubview:label];
    
    y = label.bottom_wcr;
    height = 130;
    UIView *borderView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [borderView setLayerWithBorderWidth:1.0 Corner:0.0 BorderColor:[UIColor lineColor]];
    [whiteBgView addSubview:borderView];
    
    SAMTextView *textView = [[SAMTextView alloc] initWithFrame:CGRectMake(x + 5, y + 5, width - 10, height - 10)];
    _textView = textView;
    textView.font = [UIFont CustomFontWithSize:12.0];
    textView.placeholder = @"请输入内容......";
    [whiteBgView addSubview:textView];
    
    [textView addDoneButtonCloseKeyBoard];
    
    whiteBgView.height_wcr = borderView.bottom_wcr + 15;
    [footerView addSubview:whiteBgView];
    
    // 上传图片
    y = whiteBgView.bottom_wcr;
    _mutilImageViews = [[WCRMutilImageViews alloc] initWithFrame:CGRectMake(x, y, width, 0.1)];
    _mutilImageViews.delegate = self;
    [footerView addSubview:_mutilImageViews];
    
    footerView.height_wcr = _mutilImageViews.bottom_wcr + 15;
    self.tableView.tableFooterView = footerView;
}

- (void)mutilImageViewsUpdateImages:(NSMutableArray *)images IsNeedRefreshHeight:(BOOL)isRefresh {
    [_images removeAllObjects];
    [_images addObjectsFromArray:images];
    if (isRefresh) {
        _footerView.height_wcr = _mutilImageViews.bottom_wcr + 15;
        _tableView.tableFooterView = nil;
        _tableView.tableFooterView = _footerView;
    }
}
#pragma mark - Table View DataSorce
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRModifyServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    WCRModifyServiceModel *model = [self.dataSource objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}
#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    WCRModifyServiceModel *model = [self.dataSource objectAtIndex:indexPath.row];
    return model.cellHeight;
}
#pragma mark - 提交
- (void)completeAction {
    __block BOOL flag = YES;
    __block NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:10];
    [self.dataSource enumerateObjectsUsingBlock:^(WCRModifyServiceModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        if (!model.content.length) {
            flag = NO;
            [NPTip showTip:model.placeholder];
            *stop = YES;
        }
        if (model.type == ModifyServiceTypePhone) {
            if (![RegExpValidate validateMobile:model.content]) {
                flag = NO;
                [NPTip showTip:@"手机号输入有误"];
                *stop = YES;
            }
        }
        if (model.type == ModifyServiceTypeRoomNo) {
            [dict setValue:model.houseId forKey:@"houseId"];
            [dict setValue:model.roomId forKey:@"roomId"];
        }
        else {
            [dict setValue:model.content forKey:model.key];
        }
    }];
    if (!flag) {
        return;
    }
    if (!_textView.text.length) {
        [NPTip showTip:@"请输入内容"];
        return;
    }
    if (_images.count == 0) {
        [NPTip showTip:@"选择上传的照片"];
        return;
    }
    
    [dict setValue:[WCRUserModelTool getUserId] forKey:@"userId"];
    [dict setValue:[WCRUserModelTool getToken] forKey:@"tokenName"];
    [dict setValue:_textView.text forKey:@"content"];
    
    @weakify(self);
    [NPGeneralApi WCRApiWithParameters:dict tailUrl:@"/api/roomApiController/addRepair" ImagesWithNameObj:@"repairImgs" images:self.images ResponseBlock:^(NPGeneralResponse *responseMd) {
        if (responseMd.isOK) {
            @strongify(self);
            [NPTip showTip:@"报修申请成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

@end
