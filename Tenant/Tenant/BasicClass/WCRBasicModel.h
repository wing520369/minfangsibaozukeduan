//
//  WCRBasicModel.h
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/4.
//  Copyright © 2016年 吴传荣. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WCRBasicModel : NSObject

/**
 *  将字典转成模型
 *
 *  @param dict 字典
 *
 *  @return 模型
 */
+ (id)jsonWithDictionary:(id)dict;

/**
 *  将数组转为模型数组
 *
 *  @param array 数组
 *
 *  @return 模型数组
 */
+ (NSMutableArray *)jsonWithArray:(NSArray *)array;


/**
 *  将一个模型归档
 *
 *  @param model 模型 如model== nil 即将原来保存的删除
 *  @param key   key
 *
 *  @return 是否归档成功
 */
+ (BOOL)encodeModel:(id)model key:(NSString *)key;

/**
 *  根据关键字解归档某个模型
 *
 *  @param key 关键字
 *
 *  @return 模型
 */
+ (id)coderModelWithKey:(NSString *)key;


/**
 *  把一个数组的模型归档
 *
 *  @param array 如 array == nil 即将原来保存的删除
 *  @param key   key
 *
 *  @return YES:  NO:
 */
+ (BOOL)encodeModelArray:(NSArray *)array key:(NSString *)key;

/**
 *  解归档一个数组的模型
 *
 *  @param key 关键字
 *
 *  @return 模型数组
 */
+ (NSMutableArray *)coderModelArrayWithKey:(NSString *)key;


/**
 *  打印模型所有的属性
 *
 */
- (void)LogAllProperty;

@end
