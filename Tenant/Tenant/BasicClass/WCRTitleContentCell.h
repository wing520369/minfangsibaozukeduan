//
//  WCRTitleContentCell.h
//  Landlord
//
//  Created by 吴传荣 on 2017/5/9.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRTitleContentModel;
/// 左标题+右边内容（是否有下划线，箭头）
@interface WCRTitleContentCell : UITableViewCell
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UITextField *contentTextField;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) WCRTitleContentModel *model;
@end

@interface WCRTitleContentModel : NSObject
@property (nonatomic, copy) NSString *className;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) BOOL haveArrowImageView;
@property (nonatomic, assign) BOOL haveLine;
- (instancetype)initWithTitle:(NSString *)title Content:(NSString *)content HaveArrow:(BOOL)haveArrow HaveLine:(BOOL)haveLine;
@end
