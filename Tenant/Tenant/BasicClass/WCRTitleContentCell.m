//
//  WCRTitleContentCell.m
//  Landlord
//
//  Created by 吴传荣 on 2017/5/9.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRTitleContentCell.h"

@interface WCRTitleContentCell ()

@end

@implementation WCRTitleContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIFont *font = [UIFont CustomFontWithSize:14.0];
        _contentTextField = [[UITextField alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, kDefaultHeight)];
        _contentTextField.font = font;
        _contentTextField.userInteractionEnabled = NO;
        _contentTextField.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_contentTextField];
        
        // 添加下划线
        _lineView = [_contentTextField addLineViewPositionType:kBottom_Type];
        
        // 左视图
        _leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, _contentTextField.height_wcr)];
        [_leftLabel labelWithTextColor:nil Font:font BackgroundColor:nil Text:nil];
        _contentTextField.leftView = _leftLabel;
        _contentTextField.leftViewMode = UITextFieldViewModeAlways;
        
        // 右视图
        UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, (_contentTextField.height_wcr - 13) / 2.0, 13, 13)];
        [arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
        _contentTextField.rightView = arrowImageView;
        _contentTextField.rightViewMode = UITextFieldViewModeAlways;
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}

- (void)setModel:(WCRTitleContentModel *)model {
    _model = model;
    _leftLabel.text = model.title;
    _contentTextField.text = model.content;
    
    _lineView.hidden = !model.haveLine;
    
    if (model.haveArrowImageView) {
        _contentTextField.rightViewMode = UITextFieldViewModeAlways;
    }
    else {
        _contentTextField.rightViewMode = UITextFieldViewModeNever;
    }
}

@end



@implementation WCRTitleContentModel
- (instancetype)initWithTitle:(NSString *)title Content:(NSString *)content HaveArrow:(BOOL)haveArrow HaveLine:(BOOL)haveLine {
    if (self = [super init]) {
        self.title = title;
        self.content = content;
        self.haveArrowImageView = haveArrow;
        self.haveLine = haveLine;
    }
    return self;
}
@end
