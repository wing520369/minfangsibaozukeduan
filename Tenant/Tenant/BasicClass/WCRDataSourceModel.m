//
//  WCRDataSourceModel.m
//  SchoolFriendMeet
//
//  Created by eliot on 17/2/9.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRDataSourceModel.h"
#import "UIScrollView+NPRefresh.h"
#import "DZNEmptyDataProvider.h"

@interface WCRDataSourceModel ()
@property (nonatomic, copy) WCRDataSourceModelConfigureBlock configureBlock;
@end

@implementation WCRDataSourceModel

- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

// 创建tableView 及相关设置
- (UITableView *)createTableViewWithFrame:(CGRect)frame CellIdentifier:(NSString *)identifier CellClassName:(NSString *)cellClass ConfigureCellBlock:(WCRDataSourceModelConfigureBlock)configureBlock {
    
    self.identifier = identifier;
    self.cellClass = NSClassFromString(cellClass);
    self.configureBlock = configureBlock;
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    
    [tableView registerClass:self.cellClass forCellReuseIdentifier:self.identifier];
    
    return tableView;
}
// 上下拉刷新
- (void)tableViewSetRefresh:(UITableView *)tableView HeaderRefresh:(void (^)())headerBlock FooterRefresh:(void (^)())footerBlock {
    [tableView MJHeaderWithRefreshingBlock:^{
        if (headerBlock) {
            headerBlock();
        }
    }];
    [tableView MJFooterWithRefreshingBlock:^{
        if (footerBlock) {
            footerBlock();
        }
    }];
}

#pragma mark - TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.identifier forIndexPath:indexPath];
    
    id model = [self modelAtIndexPath:indexPath];
    if (self.configureBlock) {
        self.configureBlock(cell, indexPath, model);
    }
    return cell;
}
// 根据indexpath 获取模型
- (id)modelAtIndexPath:(NSIndexPath *)indexPath {
    return [self.dataSource objectAtIndex:indexPath.row];
}

- (void)loadListDataCompleteReload:(UITableView *)tableView IsEnd:(BOOL)isEnd {
    
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
    tableView.mj_footer.hidden = isEnd;
    
    if (!tableView.emptyDataProvider) {
        tableView.emptyDataProvider = [DZNEmptyDataProvider providerWithEmptyType:DZNEmptyDataType_NoData];
    }
    [tableView reloadData];
}

@end
