//
//  WCRBasicTableView.m
//  CarAccessories
//
//  Created by eliot on 16/9/10.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCRBasicTableView.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface WCRBasicTableView ()<DZNEmptyDataSetDelegate,DZNEmptyDataSetSource>

@end

@implementation WCRBasicTableView

- (void)setNoDataString:(NSString *)noDataString {
    
    _noDataText = noDataString;
    
    if (_noDataText) {
        
        if (self.emptyDataSetDelegate == nil) {
            
            self.emptyDataSetSource = self;
            self.emptyDataSetDelegate = self;
        }
        //        [self reloadEmptyDataSet];
    }
    else
    {
        self.emptyDataSetSource = nil;
        self.emptyDataSetDelegate = nil;
    }
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSAttributedString *string = [[NSAttributedString alloc]
                                  initWithString:_noDataText attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17]}];
    
    return string;
}


- (CGPoint)offsetForEmptyDataSet:(UIScrollView *)scrollView {
    return CGPointMake(0, -64.0);
}


#pragma mark - DZNEmptyDataSetDelegate Methods

- (BOOL)emptyDataSetShouldShow:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView {
    NSLog(@"%s",__FUNCTION__);
}

- (void)emptyDataSetDidTapButton:(UIScrollView *)scrollView {
    
}

@end
