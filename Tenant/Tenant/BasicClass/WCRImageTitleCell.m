//
//  WCRImageTitleCell.m
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRImageTitleCell.h"

@interface WCRImageTitleCell ()
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UITextField *contentTextField;
@end

@implementation WCRImageTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIFont *font = [UIFont CustomFontWithSize:14.0];
        _contentTextField = [[UITextField alloc] initWithFrame:CGRectMake(kSpaceX, 0, kMainWidth - kSpaceX * 2, kDefaultHeight)];
        _contentTextField.font = font;
        _contentTextField.userInteractionEnabled = NO;
        [self.contentView addSubview:_contentTextField];
        
        // 添加下划线
        _lineView = [_contentTextField addLineViewPositionType:kBottom_Type];
        
        // 左视图
        CGFloat imageWH = 25;
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imageWH + kSpaceX, _contentTextField.height_wcr)];
        
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, (leftView.height_wcr - imageWH) / 2.0, imageWH, imageWH)];
        [_iconImageView setContentModeToScaleAspectFill];
        [leftView addSubview:_iconImageView];
        _contentTextField.leftView = leftView;
        _contentTextField.leftViewMode = UITextFieldViewModeAlways;
        
        // 右视图
        UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, (_contentTextField.height_wcr - 13) / 2.0, 13, 13)];
        [arrowImageView imageViewWithImageName:@"icon-xiangyoujiantou"];
        _contentTextField.rightView = arrowImageView;
        _contentTextField.rightViewMode = UITextFieldViewModeAlways;
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}

- (void)setModel:(WCRImageTitleModel *)model {
    _model = model;
    _iconImageView.image = [UIImage imageFileNamed:model.imageName];
    _contentTextField.text = model.title;
    
    _lineView.hidden = !model.haveLine;
    
    if (model.haveArrowImageView) {
        _contentTextField.rightViewMode = UITextFieldViewModeAlways;
    }
    else {
        _contentTextField.rightViewMode = UITextFieldViewModeNever;
    }
}

@end

@implementation WCRImageTitleModel

- (instancetype)initWithTitle:(NSString *)title ImageName:(NSString *)imageName HaveArrow:(BOOL)haveArrow HaveLine:(BOOL)haveLine {
    if (self = [super init]) {
        self.title = title;
        self.imageName = imageName;
        self.haveArrowImageView = haveArrow;
        self.haveLine = haveLine;
    }
    return self;
}

@end
