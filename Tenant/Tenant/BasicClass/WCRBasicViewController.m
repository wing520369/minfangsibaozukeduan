//
//  WCRBasicViewController.m
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/2.
//  Copyright © 2016年 吴传荣. All rights reserved.
//

#import "WCRBasicViewController.h"
#import "SDImageCache.h"
#import "UIViewController+WCRCustom.h"
#import "UINavigationController+WCRCustom.h"

@interface WCRBasicViewController ()

@end

@implementation WCRBasicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // 如果要设置导航栏透明的话，这句话需要去掉
    self.edgesForExtendedLayout = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor colorWithHexString:kViewControllerBgColor];
}
- (UIButton *)addLeftGoBackItemWithTitle:(NSString *)title {
    self.title = title;
    NSString *imageName = @"icon_xiangzuo-";
    //
    if (self.navigationController) {
        UIColor *color = [UIColor colorWithHexString:kMainColor];
        UIColor *barColor = self.navigationController.navigationBar.barTintColor;
        if (CGColorEqualToColor(color.CGColor, barColor.CGColor)) {
            imageName = @"0522icon_xiangzuo";
        }
    }
    
    UIButton *button = [self addLeftBarButtonItemImageName:imageName width:0];
    return button;
}

- (void)leftBarButtonItemAction:(UIButton *)button {
    [self.view endEditing:YES];
    if (self.navigationController) {
        if (self.navigationController.viewControllers.count > 1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
#pragma mark - init ScrollView
/**
- (void)initializationScrollView {
    _scrollView = [[WCRBasicScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64)];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.delegate = self;
    [self.view addSubview:_scrollView];
}
*/

#pragma mark - init Table View
/**
- (void)initializationTableViewStyle:(UITableViewStyle)style{
    _tableView = [[WCRBasicTableView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) style:style];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];
}

#pragma mark- Table View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}
 */
#pragma mark - init CollectionView
/**
- (void)initializationCollectionView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0.0;

    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainHeight - 64) collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;

    [self.view addSubview:_collectionView];
}
#pragma mark- Collection View Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}
*/
#pragma mark - 接收到内存警告通知
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // 清理内存
    [[SDImageCache sharedImageCache] clearMemory];
    // 清理磁盘
    [[SDImageCache sharedImageCache] clearDisk];
}

@end
