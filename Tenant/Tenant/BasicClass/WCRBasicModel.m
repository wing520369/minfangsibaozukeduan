//
//  WCRBasicModel.m
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/4.
//  Copyright © 2016年 吴传荣. All rights reserved.
//

#import "WCRBasicModel.h"
#import "MJExtension.h"
//#import <MJExtension/MJExtension.h>

@implementation WCRBasicModel

MJExtensionCodingImplementation // NSCoding实现
MJExtensionLogAllProperties //打印所有属性

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID":@"id"};
}

+ (id)jsonWithDictionary:(id)dict{
    return [[self class] mj_objectWithKeyValues:dict];
}
+ (NSMutableArray *)jsonWithArray:(NSArray *)array{
    return [[self class] mj_objectArrayWithKeyValuesArray:array];
}

+ (BOOL)encodeModel:(id)model key:(NSString *)key{
    NSString *path = [[self class] getPathWithKey:key];
    return [NSKeyedArchiver archiveRootObject:model toFile:path];
}
+ (id)coderModelWithKey:(NSString *)key{
    NSString *path = [[self class] getPathWithKey:key];
    id model = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    return model;
}


+ (BOOL)encodeModelArray:(NSArray *)array key:(NSString *)key{
    NSString *path = [[self class] getPathWithKey:[NSString stringWithFormat:@"array%@",key]];
    return [NSKeyedArchiver archiveRootObject:array toFile:path];
}
+ (NSMutableArray *)coderModelArrayWithKey:(NSString *)key{
    NSString *path = [[self class] getPathWithKey:[NSString stringWithFormat:@"array%@",key]];
    id models = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObjectsFromArray:models];
    return array;
}

+ (NSString *)getPathWithKey:(NSString *)key{
    NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *path = [NSString stringWithFormat:@"%@/%@%@.data",documentPath,NSStringFromClass([self class]),key];
    return path;
}

- (void)LogAllProperty {
    MJExtensionLog(@"%@->%@",NSStringFromClass([self class]),self);
}

@end
