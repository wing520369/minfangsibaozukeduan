//
//  WCROneRowCell.m
//  CarAccessories
//
//  Created by eliot on 16/9/22.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "WCROneRowCell.h"

@interface WCROneRowCell ()
@property (nonatomic, strong) UILabel *titleStrLable;
@end

@implementation WCROneRowCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat x = kSpaceX;
        CGFloat y = 0;
        CGFloat width = kMainWidth - x * 2;
        CGFloat height = [WCROneRowCell cellHeight];
        _titleStrLable = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [_titleStrLable labelWithTextColor:nil Font:[UIFont CustomFontWithSize:14] BackgroundColor:nil Text:nil];
        [self.contentView addSubview:_titleStrLable];
        _lineView = [_titleStrLable addLineViewPositionType:kBottom_Type];
        
        UIImage *arrowImage = [UIImage imageFileNamed:@"icon-xiangyoujiantou"];
        width = kArrowImageWidth;
        height = kArrowImageHeight;
        x = kMainWidth - width - _titleStrLable.x_wcr;
        _arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, width, height)];
        _arrowImageView.image = arrowImage;
        [self.contentView addSubview:_arrowImageView];
        _arrowImageView.centerY_wcr = _titleStrLable.centerY_wcr;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
    
}

- (void)setTitle:(NSString *)title {
    _titleStrLable.text = title;
}

+ (CGFloat)cellHeight {
    return 46.0;
}

@end
