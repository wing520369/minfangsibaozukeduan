//
//  WCRBasicTableView.h
//  CarAccessories
//
//  Created by eliot on 16/9/10.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import "TPKeyboardAvoidingTableView.h"

@interface WCRBasicTableView : TPKeyboardAvoidingTableView

/** 没数据时显示的文字*/
@property (nonatomic,copy) NSString *noDataText;

@end
