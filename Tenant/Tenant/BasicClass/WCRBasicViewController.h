//
//  WCRBasicViewController.h
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/2.
//  Copyright © 2016年 吴传荣. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 **  根视图控制器类，一切普通视图控制器都继承此类。
 */
@interface WCRBasicViewController : UIViewController

- (UIButton *)addLeftGoBackItemWithTitle:(NSString *)title;
- (void)leftBarButtonItemAction:(UIButton *)button;

@end
