//
//  WCRNavigationController.m
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/3.
//  Copyright © 2016年 吴传荣. All rights reserved.
//

#import "WCRNavigationController.h"

#define NavigationBarColor [UIColor grayColor]

@interface WCRNavigationController ()

@end

@implementation WCRNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 默认设置
    // 设置标题字体，颜色
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont CustomBoldFontWithSize:18.0],NSFontAttributeName, nil];
    [self.navigationBar setTitleTextAttributes:attributes];
    // 设置导航栏颜色
    [self.navigationBar setBarTintColor:NavigationBarColor];
    // 设置导航栏按钮文字颜色
    [self.navigationBar setTintColor:[UIColor whiteColor]];
}

- (void)setBarTittleColor:(UIColor *)titleColor TintColor:(UIColor *)tintColor BarTintColor:(UIColor *)barColor {
    //设置标题字体，颜色
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:titleColor,NSForegroundColorAttributeName,[UIFont CustomBoldFontWithSize:18.0],NSFontAttributeName, nil];
    [self.navigationBar setTitleTextAttributes:attributes];
    // 设置导航栏颜色
    [self.navigationBar setBarTintColor:barColor];
    // 设置导航栏按钮文字颜色
    [self.navigationBar setTintColor:tintColor];
}

@end
