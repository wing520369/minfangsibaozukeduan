//
//  WCRImageTitleCell.h
//  Landlord
//
//  Created by eliot on 2017/5/17.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WCRImageTitleModel;

/// 左图片+右边标题（是否有下划线，箭头）
@interface WCRImageTitleCell : UITableViewCell
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) WCRImageTitleModel *model;
@end

@interface WCRImageTitleModel : WCRBasicModel
@property (nonatomic, copy) NSString *className;
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) BOOL haveArrowImageView;
@property (nonatomic, assign) BOOL haveLine;
- (instancetype)initWithTitle:(NSString *)title ImageName:(NSString *)imageName HaveArrow:(BOOL)haveArrow HaveLine:(BOOL)haveLine;
@end
