//
//  WCRDataSourceModel.h
//  SchoolFriendMeet
//
//  Created by eliot on 17/2/9.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WCRDataSourceModel : NSObject<UITableViewDataSource>

// Cell 赋值
typedef void (^WCRDataSourceModelConfigureBlock)(id cell, NSIndexPath *indexPath, id model);

/** cell模型 */
@property (nonatomic, strong) Class cellClass;
/** cell复用标识 */
@property (nonatomic, strong) NSString *identifier;

@property (nonatomic, strong) NSMutableArray *dataSource;

/**
 *  创建tableView 及相关设置（数据源代理，注册Cell）
 *
 *  @param frame          坐标
 *  @param identifier     cell复用标识
 *  @param cellClass      cell类名
 *  @param configureBlock 对Cell赋值等操作
 *
 *  @return 返回tableView
 */
- (UITableView *)createTableViewWithFrame:(CGRect)frame
                           CellIdentifier:(NSString *)identifier
                            CellClassName:(NSString *)cellClass
                       ConfigureCellBlock:(WCRDataSourceModelConfigureBlock)configureBlock;

/// 上下拉刷新
- (void)tableViewSetRefresh:(UITableView *)tableView HeaderRefresh:(void (^)())headerBlock FooterRefresh:(void (^)())footerBlock;

/// 返回指定indexPath的item 默认dataSource第indexPath.row个
- (id)modelAtIndexPath:(NSIndexPath *)indexPath;

- (void)loadListDataCompleteReload:(UITableView *)tableView IsEnd:(BOOL)isEnd;

@end
