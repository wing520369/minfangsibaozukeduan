//
//  WCRNavigationController.h
//  WCRToolsDemo
//
//  Created by 吴传荣 on 16/9/3.
//  Copyright © 2016年 吴传荣. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WCRNavigationController : UINavigationController

/**
 *  修改设置导航栏的属性
 *
 *  @param titleColor 标题颜色
 *  @param tintColor  设置导航栏按钮颜色
 *  @param barColor   设置导航栏颜色
 */
- (void)setBarTittleColor:(UIColor *)titleColor TintColor:(UIColor *)tintColor BarTintColor:(UIColor *)barColor;

@end
