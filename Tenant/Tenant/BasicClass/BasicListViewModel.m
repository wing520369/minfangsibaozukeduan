//
//  BasicListViewModel.m
//  Demo0801
//
//  Created by HzB on 16/8/2.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import "BasicListViewModel.h"
#import <MJExtension/MJExtension.h>
#import "NPGeneralApi.h"
#import "UIScrollView+NPRefresh.h"
#import "DZNEmptyDataProvider.h"

@interface BasicListViewModel()
@property (nonatomic, copy) LoadListDataCompletion listCompletion;
@property (nonatomic, copy) BasicListViewModelConfigureBlock configureBlock;
@end

@implementation BasicListViewModel

- (void)initData {
    _starPage = 0;
    _currentpage = _starPage;
    _pageSize = 20;
    _end = YES;
    _delegate = self;
    _hiddenEmptyDataTip = NO;
    _api = [[NPGeneralApi alloc]init];
}

+ (instancetype)listViewModelWithParameters:(NSDictionary *)parameters tailUrl:(NSString *)tailUrl {
    return [[self alloc]initWithParameters:parameters tailUrl:tailUrl];
}

- (instancetype)initWithParameters:(NSDictionary *)parameters tailUrl:(NSString *)tailUrl{
    if (self = [super init]) {
        [self initData];
        _parameters = parameters;
        _api.tailUrl = tailUrl;
    }
    return self;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
    }
    return _dataSource;
}

- (void)setStarPage:(NSInteger)starPage {
    _starPage = starPage;
    _currentpage = _starPage;
}

- (void)loadListDataWithType:(LoadListType)type
                  completion:(LoadListDataCompletion)completion {
    self.listCompletion = nil;
    self.listCompletion = completion;
    if (type == LoadListType_First) {
        // 如果列表数据小于1 说明是第一次加载 请求失败时,显示重新加载的tipsView
        if (_dataSource.count < 1) {
            _api.showFailureTipsBgView = YES;
        }
        else {
            //下拉刷新不显示
            _api.showFailureTipsBgView = NO;
        }
        [self loadListDataWithPage:self.starPage];
    }
    else if(type == LoadListType_Next) {
        _api.showFailureTipsBgView = NO;
        [self loadListDataWithPage:_currentpage + 1];
    }
};

#pragma mark - 获取列表数据
- (void)loadListDataWithPage:(NSInteger)page {
    
    __weak typeof(self) weakSelf = self;
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:_parameters];
    [dict setValue:@(page) forKey:@"page"];
    [dict setValue:@(self.pageSize) forKey:@"pageSize"];
    _api.requestParameters = dict;
    [_api startWithResponseCompletion:^(NPGeneralResponse *responseMd) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        NSMutableArray *listData = nil;
        switch (responseMd.code) {
            case NPGeneralResponseCode_Success: {
                if (page == strongSelf.starPage) {
                    // 如何是首页，将旧的数据删除
                    [strongSelf.dataSource removeAllObjects];
                }
                // 请求成功才设置当前的页面
                _currentpage = page;
                // 获取解析好的模型数组
                if (strongSelf.listApiResponseListReformerBlock) {
                    listData = strongSelf.listApiResponseListReformerBlock(responseMd);
                }
                else{
                    if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(analyzeListDataWithResponse:)]) {
                        listData = [strongSelf.delegate analyzeListDataWithResponse:responseMd];
                    }
                }
                [strongSelf handleFormattedListData:listData];
                break;
            }
            case NPGeneralResponseCode_Error: {
                if (strongSelf.refeshFailRemoveData && strongSelf.starPage == page) {
                    [strongSelf.dataSource removeAllObjects];
                }
                break;
            }
            case NPGeneralResponseCode_Failure: {
                if (strongSelf.refeshFailRemoveData && strongSelf.starPage == page) {
                    [strongSelf.dataSource removeAllObjects];
                }
                break;
            }
        }
        if (strongSelf.listCompletion) {
            strongSelf.listCompletion(responseMd,page,strongSelf.isEnd);
        }
    }];
}

- (void)handleFormattedListData:(id)listData{
    
    if ([listData isKindOfClass:[NSArray class]]) {
        // 如返回数据个数小于，页大小说明没有更多数据了
        NSArray *listModel = (NSArray *)listData;
        _end = listModel.count < self.pageSize ? YES : NO;
        [self.dataSource addObjectsFromArray:listModel];
    }
    else{
        _end = YES;
    }
}

#pragma mark - ListViewModelProtocol
- (NSMutableArray *)analyzeListDataWithResponse:(NPGeneralResponse *)responseMd{
    if (_modelClass) {
        if ([responseMd.response isKindOfClass:[NSDictionary class]]) {
            NSDictionary *responseDict = responseMd.response;
            if ([[responseDict allKeys] containsObject:@"list"]) {
                NSArray *responseListArray = [responseDict objectForKey:@"list"];
                NSMutableArray *listModel = [_modelClass mj_objectArrayWithKeyValuesArray:responseListArray];
                return listModel;
            }
            else {
                return nil;
            }
        }
        NSMutableArray *listModel = [_modelClass mj_objectArrayWithKeyValuesArray:responseMd.response];
        return listModel;
    }
    return nil;
}

#pragma mark ***************************
#pragma mark - TableView
// 创建tableView 及相关设置
- (UITableView *)createTableViewWithFrame:(CGRect)frame CellIdentifier:(NSString *)identifier CellClassName:(NSString *)cellClass ModelClassName:(NSString *)modelClass ConfigureCellBlock:(BasicListViewModelConfigureBlock)configureBlock {
    
    self.identifier = identifier;
    self.cellClass = NSClassFromString(cellClass);
    self.modelClass = NSClassFromString(modelClass);
    self.configureBlock = configureBlock;
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    
    [tableView registerClass:self.cellClass forCellReuseIdentifier:self.identifier];
    
    return tableView;
}

// 上下拉刷新
- (void)tableViewSetRefresh:(UITableView *)tableView HeaderRefresh:(void (^)())headerBlock FooterRefresh:(void (^)())footerBlock {
    [tableView MJHeaderWithRefreshingBlock:^{
        if (headerBlock) {
            headerBlock();
        }
    }];
    [tableView MJFooterWithRefreshingBlock:^{
        if (footerBlock) {
            footerBlock();
        }
    }];
}

#pragma mark - TableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.identifier forIndexPath:indexPath];
    
    id model = [self modelAtIndexPath:indexPath];
    if (self.configureBlock) {
        self.configureBlock(cell, indexPath, model);
    }
    return cell;
}
// 根据indexpath 获取模型
- (id)modelAtIndexPath:(NSIndexPath *)indexPath {
    return [self.dataSource objectAtIndex:indexPath.row];
}

- (void)loadListDataCompleteReload:(UITableView *)tableView IsEnd:(BOOL)isEnd {
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
    tableView.mj_footer.hidden = isEnd;
    
    if (!self.hiddenEmptyDataTip) {
        if (!tableView.emptyDataProvider) {
            tableView.emptyDataProvider = [DZNEmptyDataProvider providerWithEmptyType:DZNEmptyDataType_NoData];
        }
    }
    
    [tableView reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.isTableViewEdit;
}

@end
