//
//  BasicListViewModel.h
//  Demo0801
//
//  Created by HzB on 16/8/2.
//  Copyright © 2016年 HzB. All rights reserved.
//

#import <Foundation/Foundation.h>

//加载列表数据类型
typedef NS_ENUM(NSInteger,LoadListType){
    /** 首页，下拉刷新*/
    LoadListType_First,
    /** 下一页，上拉加载更多*/
    LoadListType_Next,
};
@class NPGeneralApi;
@class NPGeneralResponse;
// 加载列表数据完成回调
typedef void(^LoadListDataCompletion)(NPGeneralResponse * responseMd,NSInteger page,BOOL isEnd);

@protocol ListViewModelProtocol <NSObject,UITableViewDataSource>

//返回的数据提取出列表数据，并转为对应的模型数组
- (NSMutableArray *)analyzeListDataWithResponse:(NPGeneralResponse *)responseMd;

@end


@interface BasicListViewModel : NSObject <ListViewModelProtocol>

/** 参数 */
@property (nonatomic, strong) NSDictionary *parameters;
/** 首页的页码 默认0 */
@property (nonatomic, assign) NSInteger starPage;
/** 当前页码 */
@property (nonatomic, assign,readonly) NSInteger currentpage;
/** 每页个数 默认20 */
@property (nonatomic, assign) NSInteger pageSize;
/** 保存数据的数组 */
@property (nonatomic, strong) NSMutableArray *dataSource;
/** 是否更多数据了 */
@property (nonatomic, assign,readonly,getter=isEnd) BOOL end;
/** 要解析模型 */
@property (nonatomic, strong) Class modelClass;
/** 刷新失败时是否删除数据 默认不 */
@property (nonatomic, assign) BOOL refeshFailRemoveData;
/// 是否隐藏 没有数据的提示
@property (nonatomic, assign) BOOL hiddenEmptyDataTip;

@property (nonatomic, strong,readonly) NPGeneralApi *api;

@property (nonatomic, weak) id <ListViewModelProtocol> delegate;

/**
 *  返回的数据提取出列表数据，并转为对应的模型数组 比delegate 优先
 */
@property (nonatomic, copy) NSMutableArray *(^listApiResponseListReformerBlock)(NPGeneralResponse *responseMd);


+ (instancetype)listViewModelWithParameters:(NSDictionary *)parameters
                                    tailUrl:(NSString *)tailUrl;

- (instancetype)initWithParameters:(NSDictionary *)parameters
                           tailUrl:(NSString *)tailUrl;

/**
 *  处理提取出的列表数据
 */
- (void)handleFormattedListData:(id)listData;

/**
 *  加载列表数据
 *
 *  @param type       类型
 *  @param completion 回调
 *
 */
- (void)loadListDataWithType:(LoadListType)type
                  completion:(LoadListDataCompletion)completion;


#pragma mark - *****************************
#pragma mark - TableView的相关设置

// Cell 赋值
typedef void (^BasicListViewModelConfigureBlock)(id cell, NSIndexPath *indexPath, id model);
@property (nonatomic, assign) BOOL isTableViewEdit;
/** cell模型 */
@property (nonatomic, strong) Class cellClass;
/** cell复用标识 */
@property (nonatomic, strong) NSString *identifier;

/**
 *  创建tableView 及相关设置（数据源代理，注册Cell）
 *
 *  @param frame          坐标
 *  @param identifier     cell复用标识
 *  @param cellClass      cell类名
 *  @param modelClass     model类名
 *  @param configureBlock 对Cell赋值等操作
 *
 *  @return 返回tableView
 */
- (UITableView *)createTableViewWithFrame:(CGRect)frame
                           CellIdentifier:(NSString *)identifier
                            CellClassName:(NSString *)cellClass
                           ModelClassName:(NSString *)modelClass
                       ConfigureCellBlock:(BasicListViewModelConfigureBlock)configureBlock;

/// 上下拉刷新
- (void)tableViewSetRefresh:(UITableView *)tableView HeaderRefresh:(void (^)())headerBlock FooterRefresh:(void (^)())footerBlock;

/// 返回指定indexPath的item 默认dataSource第indexPath.row个
- (id)modelAtIndexPath:(NSIndexPath *)indexPath;

- (void)loadListDataCompleteReload:(UITableView *)tableView IsEnd:(BOOL)isEnd;

@end
