//
//  WCROneRowCell.h
//  CarAccessories
//
//  Created by eliot on 16/9/22.
//  Copyright © 2016年 aleven. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 ** 单行Cell：标题+箭头
 */
@interface WCROneRowCell : UITableViewCell
/// 标题
@property (nonatomic, strong) NSString *title;
/// 右边的箭头
@property (nonatomic, strong) UIImageView *arrowImageView;
/// 下划线
@property (nonatomic, strong) UIView *lineView;
+ (CGFloat)cellHeight;
@end
