//
//  WCRCustomNaviTitleView.m
//  Landlord
//
//  Created by eliot on 2017/5/22.
//  Copyright © 2017年 aleven. All rights reserved.
//

#import "WCRCustomNaviTitleView.h"

#define kDvSwitchWidth 150

@implementation WCRCustomNaviTitleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}
- (void)setFrame:(CGRect)frame {
    // self.superview.frame.size.width
    [super setFrame:CGRectMake((self.superview.frame.size.width - kDvSwitchWidth) / 2.0, 0, kDvSwitchWidth, self.superview.bounds.size.height)];
}

@end
